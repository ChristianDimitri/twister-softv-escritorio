using System;

namespace Softv.Entities
{
  /// <summary>
  /// Class                   : Softv.Entities.DetLimiteDeCreditoEntity.cs
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : DetLimiteDeCredito entity
  /// File                    : DetLimiteDeCreditoEntity.cs
  /// Creation date           : 11/08/2010
  /// Creation time           : 04:47:35 p.m.
  /// </summary>
  public class DetLimiteDeCreditoEntity
  {
  
      #region attributes
  
      private int? _IdDetalle;
      /// <summary>
      /// IdDetalle
      /// </summary>
      public int? IdDetalle
      {
          get { return _IdDetalle; }
          set { _IdDetalle = value; }
      }
  
      private int? _IdLimiteDeCredito;
      /// <summary>
      /// IdLimiteDeCredito
      /// </summary>
      public int? IdLimiteDeCredito
      {
          get { return _IdLimiteDeCredito; }
          set { _IdLimiteDeCredito = value; }
      }
  
      private int? _IdRango;
      /// <summary>
      /// IdRango
      /// </summary>
      public int? IdRango
      {
          get { return _IdRango; }
          set { _IdRango = value; }
      }
  
      private Decimal? _LimiteDeCredito;
      /// <summary>
      /// LimiteDeCredito
      /// </summary>
      public Decimal? LimiteDeCredito
      {
          get { return _LimiteDeCredito; }
          set { _LimiteDeCredito = value; }
      }

      #endregion

      #region Constructors
      /// <summary>
      /// Default constructor
      /// </summary>

      public DetLimiteDeCreditoEntity()
      {
      }

      /// <summary>
      ///  Parameterized constructor
      /// </summary>
      /// <param name="IdDetalle"></param>
      /// <param name="IdLimiteDeCredito"></param>
      /// <param name="IdRango"></param>
      /// <param name="LimiteDeCredito"></param>
      public DetLimiteDeCreditoEntity(int?  IdDetalle,int?  IdLimiteDeCredito,int?  IdRango,Decimal?  LimiteDeCredito)
      {
          this._IdDetalle = IdDetalle;
          this._IdLimiteDeCredito = IdLimiteDeCredito;
          this._IdRango = IdRango;
          this._LimiteDeCredito = LimiteDeCredito;
      }
      #endregion

  }
}
