using System;
namespace Softv.Entities
{
  /// <summary>
  /// Class                   : Softv.Entities.PromocionesEntity.cs
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : Promociones entity
  /// File                    : PromocionesEntity.cs
  /// Creation date           : 29/08/2011
  /// Creation time           : 04:05:27 p.m.
  /// </summary>
  public class PromocionesEntity
  {
  
      #region attributes
  
      private int? _idPromocion;
      /// <summary>
      /// idPromocion
      /// </summary>
      public int? idPromocion
      {
          get { return _idPromocion; }
          set { _idPromocion = value; }
      }
  
      private String _nombrePromocion;
      /// <summary>
      /// nombrePromocion
      /// </summary>
      public String nombrePromocion
      {
          get { return _nombrePromocion; }
          set { _nombrePromocion = value; }
      }
  
      private bool? _contratacion;
      /// <summary>
      /// contratacion
      /// </summary>
      public bool? contratacion
      {
          get { return _contratacion; }
          set { _contratacion = value; }
      }
  
      private bool? _mensualidad;
      /// <summary>
      /// mensualidad
      /// </summary>
      public bool? mensualidad
      {
          get { return _mensualidad; }
          set { _mensualidad = value; }
      }
  
      private bool? _recontratacion;
      /// <summary>
      /// recontratacion
      /// </summary>
      public bool? recontratacion
      {
          get { return _recontratacion; }
          set { _recontratacion = value; }
      }

      private DateTime _fechaInicial;

      public DateTime FechaInicial
      {
          get { return _fechaInicial; }
          set { _fechaInicial = value; }
      }
      private DateTime _fechaFinal;

      public DateTime FechaFinal
      {
          get { return _fechaFinal; }
          set { _fechaFinal = value; }
      }

      private String _activa;
      /// <summary>
      /// activa
      /// </summary>
      public String activa
      {
          get { return _activa; }
          set { _activa = value; }
      }

      #endregion

      
      #region Constructors
      /// <summary>
      /// Default constructor
      /// </summary>

      public PromocionesEntity()
      {
      }

      /// <summary>
      ///  Parameterized constructor
      /// </summary>
      /// <param name="idPromocion"></param>
      /// <param name="nombrePromocion"></param>
      /// <param name="contratacion"></param>
      /// <param name="mensualidad"></param>
      /// <param name="recontratacion"></param>
      /// <param name="activa"></param>
      public PromocionesEntity(int?  idPromocion,String nombrePromocion,bool?  contratacion,bool?  mensualidad,bool?  recontratacion,String activa)
      {
          this._idPromocion = idPromocion;
          this._nombrePromocion = nombrePromocion;
          this._contratacion = contratacion;
          this._mensualidad = mensualidad;
          this._recontratacion = recontratacion;
          this._activa = activa;
      }
      #endregion

  }
}
