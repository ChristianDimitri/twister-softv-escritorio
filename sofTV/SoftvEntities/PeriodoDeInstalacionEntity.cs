using System;

namespace Softv.Entities
{
  /// <summary>
  /// Class                   : Softv.Entities.PeriodoDeInstalacionEntity.cs
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : PeriodoDeInstalacion entity
  /// File                    : PeriodoDeInstalacionEntity.cs
  /// Creation date           : 01/10/2011
  /// Creation time           : 11:51:51 a.m.
  /// </summary>
  public class PeriodoDeInstalacionEntity
  {
  
      #region attributes
  
      private int? _id;
      /// <summary>
      /// id
      /// </summary>
      public int? id
      {
          get { return _id; }
          set { _id = value; }
      }
  
      private int? _idPeriodoCorte;
      /// <summary>
      /// idPeriodoCorte
      /// </summary>
      public int? idPeriodoCorte
      {
          get { return _idPeriodoCorte; }
          set { _idPeriodoCorte = value; }
      }
  
      private byte? _diaInicial;
      /// <summary>
      /// diaInicial
      /// </summary>
      public byte? diaInicial
      {
          get { return _diaInicial; }
          set { _diaInicial = value; }
      }
  
      private byte? _diaFinal;
      /// <summary>
      /// diaFinal
      /// </summary>
      public byte? diaFinal
      {
          get { return _diaFinal; }
          set { _diaFinal = value; }
      }
  
      private byte? _diaGeneracionEC;
      /// <summary>
      /// diaGeneracionEC
      /// </summary>
      public byte? diaGeneracionEC
      {
          get { return _diaGeneracionEC; }
          set { _diaGeneracionEC = value; }
      }
  
      private byte? _diaEnvioECDecos;
      /// <summary>
      /// diaEnvioECDecos
      /// </summary>
      public byte? diaEnvioECDecos
      {
          get { return _diaEnvioECDecos; }
          set { _diaEnvioECDecos = value; }
      }
  
      private byte? _diaLimiteDePago;
      /// <summary>
      /// diaLimiteDePago
      /// </summary>
      public byte? diaLimiteDePago
      {
          get { return _diaLimiteDePago; }
          set { _diaLimiteDePago = value; }
      }
  
      private byte? _diaCierreDeSeñal;
      /// <summary>
      /// diaCierreDeSeñal
      /// </summary>
      public byte? diaCierreDeSeñal
      {
          get { return _diaCierreDeSeñal; }
          set { _diaCierreDeSeñal = value; }
      }
  
      private String _usrCapturo;
      /// <summary>
      /// usrCapturo
      /// </summary>
      public String usrCapturo
      {
          get { return _usrCapturo; }
          set { _usrCapturo = value; }
      }
  
      private DateTime? _fechaGeneracion;
      /// <summary>
      /// fechaGeneracion
      /// </summary>
      public DateTime? fechaGeneracion
      {
          get { return _fechaGeneracion; }
          set { _fechaGeneracion = value; }
      }
  
      private DateTime? _fechaUltimaModificacion;
      /// <summary>
      /// fechaUltimaModificacion
      /// </summary>
      public DateTime? fechaUltimaModificacion
      {
          get { return _fechaUltimaModificacion; }
          set { _fechaUltimaModificacion = value; }
      }

      #endregion

      #region Constructors
      /// <summary>
      /// Default constructor
      /// </summary>

      public PeriodoDeInstalacionEntity()
      {
      }

      /// <summary>
      ///  Parameterized constructor
      /// </summary>
      /// <param name="id"></param>
      /// <param name="idPeriodoCorte"></param>
      /// <param name="diaInicial"></param>
      /// <param name="diaFinal"></param>
      /// <param name="diaGeneracionEC"></param>
      /// <param name="diaEnvioECDecos"></param>
      /// <param name="diaLimiteDePago"></param>
      /// <param name="diaCierreDeSeñal"></param>
      /// <param name="usrCapturo"></param>
      /// <param name="fechaGeneracion"></param>
      /// <param name="fechaUltimaModificacion"></param>
      public PeriodoDeInstalacionEntity(int?  id,int?  idPeriodoCorte,byte?  diaInicial,byte?  diaFinal,byte?  diaGeneracionEC,byte?  diaEnvioECDecos,byte?  diaLimiteDePago,byte?  diaCierreDeSeñal,String usrCapturo,DateTime?  fechaGeneracion,DateTime?  fechaUltimaModificacion)
      {
          this._id = id;
          this._idPeriodoCorte = idPeriodoCorte;
          this._diaInicial = diaInicial;
          this._diaFinal = diaFinal;
          this._diaGeneracionEC = diaGeneracionEC;
          this._diaEnvioECDecos = diaEnvioECDecos;
          this._diaLimiteDePago = diaLimiteDePago;
          this._diaCierreDeSeñal = diaCierreDeSeñal;
          this._usrCapturo = usrCapturo;
          this._fechaGeneracion = fechaGeneracion;
          this._fechaUltimaModificacion = fechaUltimaModificacion;
      }
      #endregion

  }
}
