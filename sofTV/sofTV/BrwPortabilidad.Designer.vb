﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwPortabilidad
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.bnImprimir = New System.Windows.Forms.Button()
        Me.gbStatus = New System.Windows.Forms.GroupBox()
        Me.rbPendientes = New System.Windows.Forms.RadioButton()
        Me.rbAmbos = New System.Windows.Forms.RadioButton()
        Me.rbActivos = New System.Windows.Forms.RadioButton()
        Me.bnModificar = New System.Windows.Forms.Button()
        Me.bnNuevo = New System.Windows.Forms.Button()
        Me.CMBBuscarPor = New System.Windows.Forms.Label()
        Me.bnConsultar = New System.Windows.Forms.Button()
        Me.dgvPortabilidad = New System.Windows.Forms.DataGridView()
        Me.Folio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CMBlabel200 = New System.Windows.Forms.Label()
        Me.bnBuscarNombre = New System.Windows.Forms.Button()
        Me.tbBuscarNombre = New System.Windows.Forms.TextBox()
        Me.CMBlabel100 = New System.Windows.Forms.Label()
        Me.bnBuscarFolio = New System.Windows.Forms.Button()
        Me.tbBuscarFolio = New System.Windows.Forms.TextBox()
        Me.CMBlabel2 = New System.Windows.Forms.Label()
        Me.bnBuscarContrato = New System.Windows.Forms.Button()
        Me.tbBuscarContrato = New System.Windows.Forms.TextBox()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.gbStatus.SuspendLayout()
        CType(Me.dgvPortabilidad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnImprimir
        '
        Me.bnImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnImprimir.Location = New System.Drawing.Point(844, 140)
        Me.bnImprimir.Name = "bnImprimir"
        Me.bnImprimir.Size = New System.Drawing.Size(136, 36)
        Me.bnImprimir.TabIndex = 26
        Me.bnImprimir.Text = "&IMPRIMIR"
        Me.bnImprimir.UseVisualStyleBackColor = True
        '
        'gbStatus
        '
        Me.gbStatus.Controls.Add(Me.rbPendientes)
        Me.gbStatus.Controls.Add(Me.rbAmbos)
        Me.gbStatus.Controls.Add(Me.rbActivos)
        Me.gbStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStatus.Location = New System.Drawing.Point(10, 11)
        Me.gbStatus.Name = "gbStatus"
        Me.gbStatus.Size = New System.Drawing.Size(234, 101)
        Me.gbStatus.TabIndex = 17
        Me.gbStatus.TabStop = False
        Me.gbStatus.Text = "Filtrar por Status"
        '
        'rbPendientes
        '
        Me.rbPendientes.AutoSize = True
        Me.rbPendientes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbPendientes.Location = New System.Drawing.Point(27, 70)
        Me.rbPendientes.Name = "rbPendientes"
        Me.rbPendientes.Size = New System.Drawing.Size(97, 19)
        Me.rbPendientes.TabIndex = 2
        Me.rbPendientes.Text = "Pendientes"
        Me.rbPendientes.UseVisualStyleBackColor = True
        '
        'rbAmbos
        '
        Me.rbAmbos.AutoSize = True
        Me.rbAmbos.Checked = True
        Me.rbAmbos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbAmbos.Location = New System.Drawing.Point(27, 20)
        Me.rbAmbos.Name = "rbAmbos"
        Me.rbAmbos.Size = New System.Drawing.Size(68, 19)
        Me.rbAmbos.TabIndex = 0
        Me.rbAmbos.TabStop = True
        Me.rbAmbos.Text = "Ambos"
        Me.rbAmbos.UseVisualStyleBackColor = True
        '
        'rbActivos
        '
        Me.rbActivos.AutoSize = True
        Me.rbActivos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbActivos.Location = New System.Drawing.Point(27, 45)
        Me.rbActivos.Name = "rbActivos"
        Me.rbActivos.Size = New System.Drawing.Size(69, 19)
        Me.rbActivos.TabIndex = 1
        Me.rbActivos.Text = "Activos"
        Me.rbActivos.UseVisualStyleBackColor = True
        '
        'bnModificar
        '
        Me.bnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnModificar.Location = New System.Drawing.Point(844, 98)
        Me.bnModificar.Name = "bnModificar"
        Me.bnModificar.Size = New System.Drawing.Size(136, 36)
        Me.bnModificar.TabIndex = 25
        Me.bnModificar.Text = "&MODIFICAR"
        Me.bnModificar.UseVisualStyleBackColor = True
        '
        'bnNuevo
        '
        Me.bnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnNuevo.Location = New System.Drawing.Point(844, 14)
        Me.bnNuevo.Name = "bnNuevo"
        Me.bnNuevo.Size = New System.Drawing.Size(136, 36)
        Me.bnNuevo.TabIndex = 23
        Me.bnNuevo.Text = "&NUEVO"
        Me.bnNuevo.UseVisualStyleBackColor = True
        '
        'CMBBuscarPor
        '
        Me.CMBBuscarPor.AutoSize = True
        Me.CMBBuscarPor.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBuscarPor.Location = New System.Drawing.Point(10, 151)
        Me.CMBBuscarPor.Name = "CMBBuscarPor"
        Me.CMBBuscarPor.Size = New System.Drawing.Size(96, 20)
        Me.CMBBuscarPor.TabIndex = 20
        Me.CMBBuscarPor.Text = "Buscar por"
        '
        'bnConsultar
        '
        Me.bnConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnConsultar.Location = New System.Drawing.Point(844, 56)
        Me.bnConsultar.Name = "bnConsultar"
        Me.bnConsultar.Size = New System.Drawing.Size(136, 36)
        Me.bnConsultar.TabIndex = 24
        Me.bnConsultar.Text = "&CONSULTA"
        Me.bnConsultar.UseVisualStyleBackColor = True
        '
        'dgvPortabilidad
        '
        Me.dgvPortabilidad.AllowUserToAddRows = False
        Me.dgvPortabilidad.AllowUserToDeleteRows = False
        Me.dgvPortabilidad.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPortabilidad.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle25
        Me.dgvPortabilidad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPortabilidad.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Folio, Me.Contrato, Me.Nombre})
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPortabilidad.DefaultCellStyle = DataGridViewCellStyle26
        Me.dgvPortabilidad.Location = New System.Drawing.Point(260, 12)
        Me.dgvPortabilidad.Name = "dgvPortabilidad"
        Me.dgvPortabilidad.ReadOnly = True
        Me.dgvPortabilidad.RowHeadersVisible = False
        Me.dgvPortabilidad.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPortabilidad.Size = New System.Drawing.Size(578, 640)
        Me.dgvPortabilidad.TabIndex = 18
        Me.dgvPortabilidad.TabStop = False
        '
        'Folio
        '
        Me.Folio.DataPropertyName = "Folio"
        Me.Folio.HeaderText = "Folio"
        Me.Folio.Name = "Folio"
        Me.Folio.ReadOnly = True
        Me.Folio.Width = 130
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        Me.Contrato.Width = 130
        '
        'Nombre
        '
        Me.Nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        '
        'CMBlabel200
        '
        Me.CMBlabel200.AutoSize = True
        Me.CMBlabel200.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlabel200.Location = New System.Drawing.Point(10, 368)
        Me.CMBlabel200.Name = "CMBlabel200"
        Me.CMBlabel200.Size = New System.Drawing.Size(58, 15)
        Me.CMBlabel200.TabIndex = 35
        Me.CMBlabel200.Text = "Nombre"
        '
        'bnBuscarNombre
        '
        Me.bnBuscarNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnBuscarNombre.Location = New System.Drawing.Point(13, 413)
        Me.bnBuscarNombre.Name = "bnBuscarNombre"
        Me.bnBuscarNombre.Size = New System.Drawing.Size(75, 23)
        Me.bnBuscarNombre.TabIndex = 32
        Me.bnBuscarNombre.Text = "Buscar"
        Me.bnBuscarNombre.UseVisualStyleBackColor = True
        '
        'tbBuscarNombre
        '
        Me.tbBuscarNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbBuscarNombre.Location = New System.Drawing.Point(13, 386)
        Me.tbBuscarNombre.Name = "tbBuscarNombre"
        Me.tbBuscarNombre.Size = New System.Drawing.Size(216, 21)
        Me.tbBuscarNombre.TabIndex = 31
        '
        'CMBlabel100
        '
        Me.CMBlabel100.AutoSize = True
        Me.CMBlabel100.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlabel100.Location = New System.Drawing.Point(10, 181)
        Me.CMBlabel100.Name = "CMBlabel100"
        Me.CMBlabel100.Size = New System.Drawing.Size(39, 15)
        Me.CMBlabel100.TabIndex = 34
        Me.CMBlabel100.Text = "Folio"
        '
        'bnBuscarFolio
        '
        Me.bnBuscarFolio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnBuscarFolio.Location = New System.Drawing.Point(13, 226)
        Me.bnBuscarFolio.Name = "bnBuscarFolio"
        Me.bnBuscarFolio.Size = New System.Drawing.Size(75, 23)
        Me.bnBuscarFolio.TabIndex = 28
        Me.bnBuscarFolio.Text = "Buscar"
        Me.bnBuscarFolio.UseVisualStyleBackColor = True
        '
        'tbBuscarFolio
        '
        Me.tbBuscarFolio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbBuscarFolio.Location = New System.Drawing.Point(13, 199)
        Me.tbBuscarFolio.Name = "tbBuscarFolio"
        Me.tbBuscarFolio.Size = New System.Drawing.Size(216, 21)
        Me.tbBuscarFolio.TabIndex = 27
        '
        'CMBlabel2
        '
        Me.CMBlabel2.AutoSize = True
        Me.CMBlabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlabel2.Location = New System.Drawing.Point(11, 276)
        Me.CMBlabel2.Name = "CMBlabel2"
        Me.CMBlabel2.Size = New System.Drawing.Size(61, 15)
        Me.CMBlabel2.TabIndex = 33
        Me.CMBlabel2.Text = "Contrato"
        '
        'bnBuscarContrato
        '
        Me.bnBuscarContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnBuscarContrato.Location = New System.Drawing.Point(14, 321)
        Me.bnBuscarContrato.Name = "bnBuscarContrato"
        Me.bnBuscarContrato.Size = New System.Drawing.Size(75, 23)
        Me.bnBuscarContrato.TabIndex = 30
        Me.bnBuscarContrato.Text = "Buscar"
        Me.bnBuscarContrato.UseVisualStyleBackColor = True
        '
        'tbBuscarContrato
        '
        Me.tbBuscarContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbBuscarContrato.Location = New System.Drawing.Point(14, 294)
        Me.tbBuscarContrato.Name = "tbBuscarContrato"
        Me.tbBuscarContrato.Size = New System.Drawing.Size(216, 21)
        Me.tbBuscarContrato.TabIndex = 29
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(844, 616)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 36
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'BrwPortabilidad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(990, 662)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.CMBlabel200)
        Me.Controls.Add(Me.bnBuscarNombre)
        Me.Controls.Add(Me.tbBuscarNombre)
        Me.Controls.Add(Me.CMBlabel100)
        Me.Controls.Add(Me.bnBuscarFolio)
        Me.Controls.Add(Me.tbBuscarFolio)
        Me.Controls.Add(Me.CMBlabel2)
        Me.Controls.Add(Me.bnBuscarContrato)
        Me.Controls.Add(Me.tbBuscarContrato)
        Me.Controls.Add(Me.bnImprimir)
        Me.Controls.Add(Me.gbStatus)
        Me.Controls.Add(Me.bnModificar)
        Me.Controls.Add(Me.bnNuevo)
        Me.Controls.Add(Me.CMBBuscarPor)
        Me.Controls.Add(Me.bnConsultar)
        Me.Controls.Add(Me.dgvPortabilidad)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1006, 700)
        Me.MinimumSize = New System.Drawing.Size(1006, 700)
        Me.Name = "BrwPortabilidad"
        Me.Text = "Portabilidad"
        Me.gbStatus.ResumeLayout(False)
        Me.gbStatus.PerformLayout()
        CType(Me.dgvPortabilidad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents bnImprimir As System.Windows.Forms.Button
    Private WithEvents gbStatus As System.Windows.Forms.GroupBox
    Private WithEvents rbPendientes As System.Windows.Forms.RadioButton
    Private WithEvents rbAmbos As System.Windows.Forms.RadioButton
    Private WithEvents rbActivos As System.Windows.Forms.RadioButton
    Private WithEvents bnModificar As System.Windows.Forms.Button
    Private WithEvents bnNuevo As System.Windows.Forms.Button
    Private WithEvents CMBBuscarPor As System.Windows.Forms.Label
    Private WithEvents bnConsultar As System.Windows.Forms.Button
    Private WithEvents dgvPortabilidad As System.Windows.Forms.DataGridView
    Friend WithEvents Folio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents CMBlabel200 As System.Windows.Forms.Label
    Private WithEvents bnBuscarNombre As System.Windows.Forms.Button
    Private WithEvents tbBuscarNombre As System.Windows.Forms.TextBox
    Private WithEvents CMBlabel100 As System.Windows.Forms.Label
    Private WithEvents bnBuscarFolio As System.Windows.Forms.Button
    Private WithEvents tbBuscarFolio As System.Windows.Forms.TextBox
    Private WithEvents CMBlabel2 As System.Windows.Forms.Label
    Private WithEvents bnBuscarContrato As System.Windows.Forms.Button
    Private WithEvents tbBuscarContrato As System.Windows.Forms.TextBox
    Private WithEvents bnSalir As System.Windows.Forms.Button
End Class
