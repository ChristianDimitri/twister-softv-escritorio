Imports System.Data.SqlClient
Public Class BrwRangos

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmRangos.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.MuestraCatalogoDeRangosDataGridView.RowCount > 0 Then
            eOpcion = "M"
            eCveRango = Me.CveRangoTextBox.Text
            eRangoInferior = Me.RangoInferiorTextBox.Text
            eRangoSuperior = Me.RangoSuperiorTextBox.Text
            FrmRangos.Show()
        Else
            MsgBox("No Existen Registros para Modificar.", , "Error")
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub BrwRangos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraCatalogoDeRangosTableAdapter.Connection = CON
        Me.MuestraCatalogoDeRangosTableAdapter.Fill(Me.DataSetEDGAR.MuestraCatalogoDeRangos, 0)
        CON.Close()

    End Sub

    Private Sub BrwRangos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraCatalogoDeRangosTableAdapter.Connection = CON
        Me.MuestraCatalogoDeRangosTableAdapter.Fill(Me.DataSetEDGAR.MuestraCatalogoDeRangos, 0)
        CON.Close()
    End Sub

End Class