﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPortabilidad
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbSuscriptor = New System.Windows.Forms.GroupBox()
        Me.lbPortabilidadActiva = New System.Windows.Forms.Label()
        Me.rbEntidadDeGobierno = New System.Windows.Forms.RadioButton()
        Me.rbPersonaMoral = New System.Windows.Forms.RadioButton()
        Me.rbPersonaFisica = New System.Windows.Forms.RadioButton()
        Me.bnBuscarClientes = New System.Windows.Forms.Button()
        Me.tbNombre = New System.Windows.Forms.TextBox()
        Me.tbContrato = New System.Windows.Forms.TextBox()
        Me.label3 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.label1 = New System.Windows.Forms.Label()
        Me.bnGuardar = New System.Windows.Forms.Button()
        Me.bnActivar = New System.Windows.Forms.Button()
        Me.gbNumeros = New System.Windows.Forms.GroupBox()
        Me.dgvTelefonos = New System.Windows.Forms.DataGridView()
        Me.KeyCol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TelefonoCol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bnEliminar = New System.Windows.Forms.Button()
        Me.bnAgregar = New System.Windows.Forms.Button()
        Me.tbAlTelefono = New System.Windows.Forms.TextBox()
        Me.label9 = New System.Windows.Forms.Label()
        Me.tbDelTelefono = New System.Windows.Forms.TextBox()
        Me.label8 = New System.Windows.Forms.Label()
        Me.gbProveedor = New System.Windows.Forms.GroupBox()
        Me.cbEmpresaAnterior = New System.Windows.Forms.ComboBox()
        Me.label7 = New System.Windows.Forms.Label()
        Me.gbTipoDeServicio = New System.Windows.Forms.GroupBox()
        Me.cbNumeroNoGeografico = New System.Windows.Forms.ComboBox()
        Me.cbMovil = New System.Windows.Forms.ComboBox()
        Me.cbFijo = New System.Windows.Forms.ComboBox()
        Me.label6 = New System.Windows.Forms.Label()
        Me.label5 = New System.Windows.Forms.Label()
        Me.label4 = New System.Windows.Forms.Label()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.gbSuscriptor.SuspendLayout()
        Me.gbNumeros.SuspendLayout()
        CType(Me.dgvTelefonos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbProveedor.SuspendLayout()
        Me.gbTipoDeServicio.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbSuscriptor
        '
        Me.gbSuscriptor.Controls.Add(Me.lbPortabilidadActiva)
        Me.gbSuscriptor.Controls.Add(Me.rbEntidadDeGobierno)
        Me.gbSuscriptor.Controls.Add(Me.rbPersonaMoral)
        Me.gbSuscriptor.Controls.Add(Me.rbPersonaFisica)
        Me.gbSuscriptor.Controls.Add(Me.bnBuscarClientes)
        Me.gbSuscriptor.Controls.Add(Me.tbNombre)
        Me.gbSuscriptor.Controls.Add(Me.tbContrato)
        Me.gbSuscriptor.Controls.Add(Me.label3)
        Me.gbSuscriptor.Controls.Add(Me.label2)
        Me.gbSuscriptor.Controls.Add(Me.label1)
        Me.gbSuscriptor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSuscriptor.Location = New System.Drawing.Point(12, 12)
        Me.gbSuscriptor.Name = "gbSuscriptor"
        Me.gbSuscriptor.Size = New System.Drawing.Size(593, 117)
        Me.gbSuscriptor.TabIndex = 1
        Me.gbSuscriptor.TabStop = False
        Me.gbSuscriptor.Text = "Datos del Suscriptor"
        '
        'lbPortabilidadActiva
        '
        Me.lbPortabilidadActiva.AutoSize = True
        Me.lbPortabilidadActiva.BackColor = System.Drawing.Color.Yellow
        Me.lbPortabilidadActiva.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbPortabilidadActiva.ForeColor = System.Drawing.Color.Red
        Me.lbPortabilidadActiva.Location = New System.Drawing.Point(429, 16)
        Me.lbPortabilidadActiva.Name = "lbPortabilidadActiva"
        Me.lbPortabilidadActiva.Size = New System.Drawing.Size(158, 20)
        Me.lbPortabilidadActiva.TabIndex = 4
        Me.lbPortabilidadActiva.Text = "Portabilidad Activa"
        Me.lbPortabilidadActiva.Visible = False
        '
        'rbEntidadDeGobierno
        '
        Me.rbEntidadDeGobierno.AutoSize = True
        Me.rbEntidadDeGobierno.Location = New System.Drawing.Point(355, 86)
        Me.rbEntidadDeGobierno.Name = "rbEntidadDeGobierno"
        Me.rbEntidadDeGobierno.Size = New System.Drawing.Size(157, 19)
        Me.rbEntidadDeGobierno.TabIndex = 3
        Me.rbEntidadDeGobierno.TabStop = True
        Me.rbEntidadDeGobierno.Text = "Entidad de Gobierno"
        Me.rbEntidadDeGobierno.UseVisualStyleBackColor = True
        '
        'rbPersonaMoral
        '
        Me.rbPersonaMoral.AutoSize = True
        Me.rbPersonaMoral.Location = New System.Drawing.Point(230, 86)
        Me.rbPersonaMoral.Name = "rbPersonaMoral"
        Me.rbPersonaMoral.Size = New System.Drawing.Size(119, 19)
        Me.rbPersonaMoral.TabIndex = 2
        Me.rbPersonaMoral.TabStop = True
        Me.rbPersonaMoral.Text = "Persona Moral"
        Me.rbPersonaMoral.UseVisualStyleBackColor = True
        '
        'rbPersonaFisica
        '
        Me.rbPersonaFisica.AutoSize = True
        Me.rbPersonaFisica.Checked = True
        Me.rbPersonaFisica.Location = New System.Drawing.Point(104, 86)
        Me.rbPersonaFisica.Name = "rbPersonaFisica"
        Me.rbPersonaFisica.Size = New System.Drawing.Size(120, 19)
        Me.rbPersonaFisica.TabIndex = 1
        Me.rbPersonaFisica.TabStop = True
        Me.rbPersonaFisica.Text = "Persona Física"
        Me.rbPersonaFisica.UseVisualStyleBackColor = True
        '
        'bnBuscarClientes
        '
        Me.bnBuscarClientes.Location = New System.Drawing.Point(210, 22)
        Me.bnBuscarClientes.Name = "bnBuscarClientes"
        Me.bnBuscarClientes.Size = New System.Drawing.Size(28, 23)
        Me.bnBuscarClientes.TabIndex = 0
        Me.bnBuscarClientes.Text = "..."
        Me.bnBuscarClientes.UseVisualStyleBackColor = True
        '
        'tbNombre
        '
        Me.tbNombre.Location = New System.Drawing.Point(104, 51)
        Me.tbNombre.Name = "tbNombre"
        Me.tbNombre.ReadOnly = True
        Me.tbNombre.Size = New System.Drawing.Size(408, 21)
        Me.tbNombre.TabIndex = 4
        Me.tbNombre.TabStop = False
        '
        'tbContrato
        '
        Me.tbContrato.Location = New System.Drawing.Point(104, 24)
        Me.tbContrato.Name = "tbContrato"
        Me.tbContrato.ReadOnly = True
        Me.tbContrato.Size = New System.Drawing.Size(100, 21)
        Me.tbContrato.TabIndex = 3
        Me.tbContrato.TabStop = False
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Location = New System.Drawing.Point(36, 30)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(65, 15)
        Me.label3.TabIndex = 2
        Me.label3.Text = "Contrato:"
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(62, 86)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(39, 15)
        Me.label2.TabIndex = 1
        Me.label2.Text = "Tipo:"
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Location = New System.Drawing.Point(39, 57)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(62, 15)
        Me.label1.TabIndex = 0
        Me.label1.Text = "Nombre:"
        '
        'bnGuardar
        '
        Me.bnGuardar.Enabled = False
        Me.bnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnGuardar.Location = New System.Drawing.Point(611, 12)
        Me.bnGuardar.Name = "bnGuardar"
        Me.bnGuardar.Size = New System.Drawing.Size(136, 36)
        Me.bnGuardar.TabIndex = 2
        Me.bnGuardar.Text = "&GUARDAR"
        Me.bnGuardar.UseVisualStyleBackColor = True
        '
        'bnActivar
        '
        Me.bnActivar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnActivar.Location = New System.Drawing.Point(611, 54)
        Me.bnActivar.Name = "bnActivar"
        Me.bnActivar.Size = New System.Drawing.Size(136, 36)
        Me.bnActivar.TabIndex = 3
        Me.bnActivar.Text = "&ACTIVAR"
        Me.bnActivar.UseVisualStyleBackColor = True
        '
        'gbNumeros
        '
        Me.gbNumeros.Controls.Add(Me.dgvTelefonos)
        Me.gbNumeros.Controls.Add(Me.bnEliminar)
        Me.gbNumeros.Controls.Add(Me.bnAgregar)
        Me.gbNumeros.Controls.Add(Me.tbAlTelefono)
        Me.gbNumeros.Controls.Add(Me.label9)
        Me.gbNumeros.Controls.Add(Me.tbDelTelefono)
        Me.gbNumeros.Controls.Add(Me.label8)
        Me.gbNumeros.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbNumeros.Location = New System.Drawing.Point(12, 320)
        Me.gbNumeros.Name = "gbNumeros"
        Me.gbNumeros.Size = New System.Drawing.Size(593, 183)
        Me.gbNumeros.TabIndex = 6
        Me.gbNumeros.TabStop = False
        Me.gbNumeros.Text = "Números Telefónicos a ser Portados"
        '
        'dgvTelefonos
        '
        Me.dgvTelefonos.AllowUserToAddRows = False
        Me.dgvTelefonos.AllowUserToDeleteRows = False
        Me.dgvTelefonos.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvTelefonos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTelefonos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.KeyCol, Me.TelefonoCol})
        Me.dgvTelefonos.Location = New System.Drawing.Point(115, 20)
        Me.dgvTelefonos.Name = "dgvTelefonos"
        Me.dgvTelefonos.ReadOnly = True
        Me.dgvTelefonos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTelefonos.Size = New System.Drawing.Size(448, 157)
        Me.dgvTelefonos.TabIndex = 7
        Me.dgvTelefonos.TabStop = False
        '
        'KeyCol
        '
        Me.KeyCol.DataPropertyName = "Key"
        Me.KeyCol.HeaderText = "Key"
        Me.KeyCol.Name = "KeyCol"
        Me.KeyCol.ReadOnly = True
        Me.KeyCol.Visible = False
        '
        'TelefonoCol
        '
        Me.TelefonoCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.TelefonoCol.DataPropertyName = "NumTelefono"
        Me.TelefonoCol.HeaderText = "Números Teléfonos"
        Me.TelefonoCol.Name = "TelefonoCol"
        Me.TelefonoCol.ReadOnly = True
        '
        'bnEliminar
        '
        Me.bnEliminar.Location = New System.Drawing.Point(34, 133)
        Me.bnEliminar.Name = "bnEliminar"
        Me.bnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.bnEliminar.TabIndex = 3
        Me.bnEliminar.Text = "&Eliminar"
        Me.bnEliminar.UseVisualStyleBackColor = True
        '
        'bnAgregar
        '
        Me.bnAgregar.Location = New System.Drawing.Point(34, 104)
        Me.bnAgregar.Name = "bnAgregar"
        Me.bnAgregar.Size = New System.Drawing.Size(75, 23)
        Me.bnAgregar.TabIndex = 2
        Me.bnAgregar.Text = "&Agregar"
        Me.bnAgregar.UseVisualStyleBackColor = True
        '
        'tbAlTelefono
        '
        Me.tbAlTelefono.Location = New System.Drawing.Point(9, 77)
        Me.tbAlTelefono.Name = "tbAlTelefono"
        Me.tbAlTelefono.Size = New System.Drawing.Size(100, 21)
        Me.tbAlTelefono.TabIndex = 1
        '
        'label9
        '
        Me.label9.AutoSize = True
        Me.label9.Location = New System.Drawing.Point(6, 59)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(83, 15)
        Me.label9.TabIndex = 2
        Me.label9.Text = "al Teléfono:"
        '
        'tbDelTelefono
        '
        Me.tbDelTelefono.Location = New System.Drawing.Point(9, 35)
        Me.tbDelTelefono.Name = "tbDelTelefono"
        Me.tbDelTelefono.Size = New System.Drawing.Size(100, 21)
        Me.tbDelTelefono.TabIndex = 0
        '
        'label8
        '
        Me.label8.AutoSize = True
        Me.label8.Location = New System.Drawing.Point(6, 17)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(67, 15)
        Me.label8.TabIndex = 0
        Me.label8.Text = "Teléfono:"
        '
        'gbProveedor
        '
        Me.gbProveedor.Controls.Add(Me.cbEmpresaAnterior)
        Me.gbProveedor.Controls.Add(Me.label7)
        Me.gbProveedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbProveedor.Location = New System.Drawing.Point(12, 233)
        Me.gbProveedor.Name = "gbProveedor"
        Me.gbProveedor.Size = New System.Drawing.Size(593, 81)
        Me.gbProveedor.TabIndex = 5
        Me.gbProveedor.TabStop = False
        Me.gbProveedor.Text = "Proveedor de Servicios de Telecomunicaciones"
        '
        'cbEmpresaAnterior
        '
        Me.cbEmpresaAnterior.DisplayMember = "Nombre"
        Me.cbEmpresaAnterior.FormattingEnabled = True
        Me.cbEmpresaAnterior.Location = New System.Drawing.Point(159, 31)
        Me.cbEmpresaAnterior.Name = "cbEmpresaAnterior"
        Me.cbEmpresaAnterior.Size = New System.Drawing.Size(404, 23)
        Me.cbEmpresaAnterior.TabIndex = 0
        Me.cbEmpresaAnterior.ValueMember = "Id"
        '
        'label7
        '
        Me.label7.AutoSize = True
        Me.label7.Location = New System.Drawing.Point(31, 39)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(122, 15)
        Me.label7.TabIndex = 7
        Me.label7.Text = "Empresa Anterior:"
        '
        'gbTipoDeServicio
        '
        Me.gbTipoDeServicio.Controls.Add(Me.cbNumeroNoGeografico)
        Me.gbTipoDeServicio.Controls.Add(Me.cbMovil)
        Me.gbTipoDeServicio.Controls.Add(Me.cbFijo)
        Me.gbTipoDeServicio.Controls.Add(Me.label6)
        Me.gbTipoDeServicio.Controls.Add(Me.label5)
        Me.gbTipoDeServicio.Controls.Add(Me.label4)
        Me.gbTipoDeServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTipoDeServicio.Location = New System.Drawing.Point(12, 135)
        Me.gbTipoDeServicio.Name = "gbTipoDeServicio"
        Me.gbTipoDeServicio.Size = New System.Drawing.Size(593, 92)
        Me.gbTipoDeServicio.TabIndex = 4
        Me.gbTipoDeServicio.TabStop = False
        Me.gbTipoDeServicio.Text = "Tipo de Servicio"
        '
        'cbNumeroNoGeografico
        '
        Me.cbNumeroNoGeografico.FormattingEnabled = True
        Me.cbNumeroNoGeografico.Items.AddRange(New Object() {"Ninguno", "800 + 7 Dígitos"})
        Me.cbNumeroNoGeografico.Location = New System.Drawing.Point(418, 20)
        Me.cbNumeroNoGeografico.Name = "cbNumeroNoGeografico"
        Me.cbNumeroNoGeografico.Size = New System.Drawing.Size(145, 23)
        Me.cbNumeroNoGeografico.TabIndex = 2
        Me.cbNumeroNoGeografico.Text = "Ninguno"
        '
        'cbMovil
        '
        Me.cbMovil.FormattingEnabled = True
        Me.cbMovil.Items.AddRange(New Object() {"Ninguno", "Postpago", "Prepago"})
        Me.cbMovil.Location = New System.Drawing.Point(79, 50)
        Me.cbMovil.Name = "cbMovil"
        Me.cbMovil.Size = New System.Drawing.Size(145, 23)
        Me.cbMovil.TabIndex = 1
        Me.cbMovil.Text = "Ninguno"
        '
        'cbFijo
        '
        Me.cbFijo.FormattingEnabled = True
        Me.cbFijo.Items.AddRange(New Object() {"Ninguno", "Postpago", "Prepago"})
        Me.cbFijo.Location = New System.Drawing.Point(79, 21)
        Me.cbFijo.Name = "cbFijo"
        Me.cbFijo.Size = New System.Drawing.Size(145, 23)
        Me.cbFijo.TabIndex = 0
        Me.cbFijo.Text = "Ninguno"
        '
        'label6
        '
        Me.label6.AutoSize = True
        Me.label6.Location = New System.Drawing.Point(256, 29)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(156, 15)
        Me.label6.TabIndex = 3
        Me.label6.Text = "Número no Geográfico:"
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.Location = New System.Drawing.Point(28, 58)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(45, 15)
        Me.label5.TabIndex = 2
        Me.label5.Text = "Móvil:"
        '
        'label4
        '
        Me.label4.AutoSize = True
        Me.label4.Location = New System.Drawing.Point(38, 29)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(35, 15)
        Me.label4.TabIndex = 1
        Me.label4.Text = "Fijo:"
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(611, 467)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 7
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'FrmPortabilidad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(759, 510)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.gbNumeros)
        Me.Controls.Add(Me.gbProveedor)
        Me.Controls.Add(Me.gbTipoDeServicio)
        Me.Controls.Add(Me.bnGuardar)
        Me.Controls.Add(Me.bnActivar)
        Me.Controls.Add(Me.gbSuscriptor)
        Me.Name = "FrmPortabilidad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmPortabilidad"
        Me.gbSuscriptor.ResumeLayout(False)
        Me.gbSuscriptor.PerformLayout()
        Me.gbNumeros.ResumeLayout(False)
        Me.gbNumeros.PerformLayout()
        CType(Me.dgvTelefonos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbProveedor.ResumeLayout(False)
        Me.gbProveedor.PerformLayout()
        Me.gbTipoDeServicio.ResumeLayout(False)
        Me.gbTipoDeServicio.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents gbSuscriptor As System.Windows.Forms.GroupBox
    Private WithEvents lbPortabilidadActiva As System.Windows.Forms.Label
    Private WithEvents rbEntidadDeGobierno As System.Windows.Forms.RadioButton
    Private WithEvents rbPersonaMoral As System.Windows.Forms.RadioButton
    Private WithEvents rbPersonaFisica As System.Windows.Forms.RadioButton
    Private WithEvents bnBuscarClientes As System.Windows.Forms.Button
    Private WithEvents tbNombre As System.Windows.Forms.TextBox
    Private WithEvents tbContrato As System.Windows.Forms.TextBox
    Private WithEvents label3 As System.Windows.Forms.Label
    Private WithEvents label2 As System.Windows.Forms.Label
    Private WithEvents label1 As System.Windows.Forms.Label
    Private WithEvents bnGuardar As System.Windows.Forms.Button
    Private WithEvents bnActivar As System.Windows.Forms.Button
    Private WithEvents gbNumeros As System.Windows.Forms.GroupBox
    Private WithEvents dgvTelefonos As System.Windows.Forms.DataGridView
    Friend WithEvents KeyCol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TelefonoCol As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents bnEliminar As System.Windows.Forms.Button
    Private WithEvents bnAgregar As System.Windows.Forms.Button
    Private WithEvents tbAlTelefono As System.Windows.Forms.TextBox
    Private WithEvents label9 As System.Windows.Forms.Label
    Private WithEvents tbDelTelefono As System.Windows.Forms.TextBox
    Private WithEvents label8 As System.Windows.Forms.Label
    Private WithEvents gbProveedor As System.Windows.Forms.GroupBox
    Private WithEvents cbEmpresaAnterior As System.Windows.Forms.ComboBox
    Private WithEvents label7 As System.Windows.Forms.Label
    Private WithEvents gbTipoDeServicio As System.Windows.Forms.GroupBox
    Private WithEvents cbNumeroNoGeografico As System.Windows.Forms.ComboBox
    Private WithEvents cbMovil As System.Windows.Forms.ComboBox
    Private WithEvents cbFijo As System.Windows.Forms.ComboBox
    Private WithEvents label6 As System.Windows.Forms.Label
    Private WithEvents label5 As System.Windows.Forms.Label
    Private WithEvents label4 As System.Windows.Forms.Label
    Private WithEvents bnSalir As System.Windows.Forms.Button
End Class
