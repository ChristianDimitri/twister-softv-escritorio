Imports System.Data.SqlClient

Public Class FrmCajasAdicionales

    Private Sub Guarda_CajasDigitales(ByVal contrato As Long, ByVal Cajas As Integer)
        Try
            Dim CON As New SqlClient.SqlConnection(MiConexion)
            Dim cmd As New SqlClient.SqlCommand()
            CON.Open()
            With cmd
                .CommandText = "Guarda_CajasDigitales"
                .Connection = CON
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim Prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = contrato
                .Parameters.Add(Prm)


                Dim prm2 As New SqlParameter("@NoCajas", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = Cajas
                .Parameters.Add(prm2)

                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            CON.Close()


        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmCajasAdicionales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CAJASDIG = 0
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        CAJASDIG = Me.CajasUpDown.Value
        If IsNumeric(LocContratoLog) = True Then
            Guarda_CajasDigitales(LocContratoLog, Me.CajasUpDown.Value)
            FrmContratacionCombo.Guarda_Clientes_Combo(LocContratoLog, GloClv_Combo, Tipo, 0, 0, CAJASDIG)
            uspObtenerClvUnicaNet(LocContratoLog)

            FrmClientes.CREAARBOLDIGITAL2()
        End If
        Me.Close()
    End Sub
    Private Sub GUARDARRel_ContDig_Usuarios(ByVal prmClvUnicaNet As Long, ByVal prmclvUsuario As Integer)
        Try

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_UnicaNet", SqlDbType.BigInt, prmClvUnicaNet)
        BaseII.CreateMyParameter("@Clv_Usuario_Num", SqlDbType.Int, prmclvUsuario)
        BaseII.Inserta("GUARDARRel_ContDig_Usuarios")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub GUARDARRel_ContNet_Usuarios(ByVal prmClvUnicaNet As Long, ByVal prmclvUsuario As Integer)
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_UnicaNet", SqlDbType.BigInt, prmClvUnicaNet)
            BaseII.CreateMyParameter("@Clv_Usuario_Num", SqlDbType.Int, prmclvUsuario)
            BaseII.Inserta("GUARDARRel_ContNet_Usuarios")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub uspObtenerClvUnicaNet(ByVal prmContrato As Long)
        Try
            Dim DT As New DataTable
            Dim I As Integer
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, prmContrato)
            DT = BaseII.ConsultaDT("uspObtenerClvUnicaNet")
            For I = 0 To DT.Rows.Count - 1
                If CInt(DT.Rows.Item(I)(1).ToString) = 3 Then
                    GloClvUnicaNet = CInt(DT.Rows.Item(I)(0).ToString)
                    GUARDARRel_ContDig_Usuarios(GloClvUnicaNet, GloClvUsuario)
                ElseIf CInt(DT.Rows.Item(I)(1).ToString) = 2 Then
                    GloClvUnicaNet = CInt(DT.Rows.Item(I)(0).ToString)
                    GUARDARRel_ContNet_Usuarios(GloClvUnicaNet, GloClvUsuario)
                End If
            Next


        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class