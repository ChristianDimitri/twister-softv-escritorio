Imports System.Data.SqlClient
Public Class FrmContratacionCombo

    Private Aplica_promocion As Integer = Nothing

    Private Sub CMBBtnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmContratacionCombo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlClient.SqlConnection(MiConexion)
        CON.Open()
        Me.Muestra_Combos_clContratanTableAdapter.Connection = CON
        Me.Muestra_Combos_clContratanTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_Combos_clContratan, 0)
        CON.Close()
    End Sub

    Private Sub CMBBtnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnAceptar.Click
        Dim op As Integer = 0
        GloClv_Combo = Me.Cmb1.SelectedValue
        If (Me.Cmb1.Text) <> "" Then
            Checa_si_ComboTieneInternet()
            If Tiene_Tel = 1 Then
                ChecaTelefonosDisponibles()
                If eResValida = 1 Then
                    MsgBox(eMsgValida, MsgBoxStyle.Information)
                    Exit Sub
                End If
                Equip_Int = False
                Equip_tel = True
                globndTel = True
                optTel = 5
                GloBndNum = False
            End If

            If Tiene_Internet > 0 Then
                op = MsgBox("�El Cablemodem Es Inal�mbrico?", MsgBoxStyle.YesNo)
                'si es no le ponemos un cablemodem al�mbrico
                If op = 7 Then
                    Tipo = 1 'Alambrico 
                Else
                    Tipo = 2 'Inalambrico
                End If
            ElseIf Tiene_Internet = 0 Then
                Tipo = 1
            End If
            If Tiene_Tv = 1 Then
                'Preguntar si Deseas Tv's Adicionales
                FrmTvAdicionales.Show()
            End If
            If Tiene_Dig = 1 Then
                FrmCajasAdicionales.Show()
                'Guarda_Clientes_Combo(LocContratoLog, CLITVSINPAGO, CLITVCONPAGO, CAJASDIG)
            End If

        End If

        'Guarda_Clientes_Combo(LocContratoLog, Me.Cmb1.SelectedValue, Tipo, CLITVSINPAGO, CLITVCONPAGO, CAJASDIG)

        'If Aplica_promocion = 0 Then
        '    op = MsgBox("Desea Asignarle Una Promoci�n Al Cliente", MsgBoxStyle.YesNo)
        '    If op = 6 Then 'si es si mostramos las promociones que existen 
        '        SeleccionPromocionCliente.Show()
        '    Else
        '        globndTel = True
        '        optTel = 5
        '        GloBndNum = False
        '        FrmSeleccionaTel.Show()
        '    End If
        'Else

        'FrmSeleccionaTel.Show()
        'End If

        LocbndContratacionCombo = True
        Me.Close()
    End Sub

    Private Sub Checa_si_ComboTieneInternet()
        Dim CON90 As New SqlClient.SqlConnection(MiConexion)
        Dim Cmd As New SqlClient.SqlCommand()
        Try
            Tiene_Internet = 0
            Tiene_Tv = 0
            Tiene_Dig = 0
            Tiene_Tel = 0
            CON90.Open()
            With Cmd
                .CommandText = "Checa_si_ComboTieneInternet"
                .Connection = CON90
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                '@clv_combo bigint,@error int output

                Dim prm As New SqlParameter("@clv_combo", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Me.Cmb1.SelectedValue
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@TieneTv", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@TieneDig", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@TieneNet", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@TieneTel", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Output
                prm4.Value = 0
                .Parameters.Add(prm4)
                Dim ia As Integer = .ExecuteNonQuery()
                Tiene_Tv = prm1.Value
                Tiene_Dig = prm2.Value
                Tiene_Internet = prm3.Value
                Tiene_Tel = prm4.Value
            End With
            CON90.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub Guarda_Clientes_Combo(ByVal contrato As Long, ByVal clv_combo As Integer, ByVal TipoModem As Integer, ByVal TvSinpago As Integer, ByVal TvConPAgo As Integer, ByVal Cajas As Integer)
        Try
            Dim CON78 As New SqlClient.SqlConnection(MiConexion)
            Dim cmd As New SqlClient.SqlCommand()
            CON78.Open()
            With cmd
                .CommandText = "Guarda_Cliente_combo"
                .Connection = CON78
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                '@contrato bigint,@clv_combo bigint,@Tipo_Cablemodem int

                Dim Prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                Prm.Direction = ParameterDirection.Input
                Prm.Value = contrato
                .Parameters.Add(Prm)

                Dim prm1 As New SqlParameter("@clv_combo", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = clv_combo
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Tipo_cablemodem", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = TipoModem
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@TvSinPago", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = TvSinpago
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@TvConPago", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Input
                prm4.Value = TvConPAgo
                .Parameters.Add(prm4)

                Dim prm5 As New SqlParameter("@NoCajas", SqlDbType.Int)
                prm5.Direction = ParameterDirection.Input
                prm5.Value = Cajas
                .Parameters.Add(prm5)

                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            CON78.Close()

            locGloClv_servicioPromocion = Me.Cmb1.SelectedValue

            Aplica_promocion = GCheca_si_Aplica_promocion(contrato, 0, 0, locGloClv_servicioPromocion)

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class