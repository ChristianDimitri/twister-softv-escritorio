﻿Imports System.Collections.Generic

Public Class FrmPortabilidad
    Dim activo As Boolean = False
    Dim tipoSuscriptor As String
    Dim eerror As String = ""
    Dim numerosRepetidos As String = ""
    Dim diccionarioTelefonos As New Dictionary(Of Integer, Telefonos)
    Dim listaTelefonos As List(Of Telefonos)
    Dim key As Integer = 0
    Dim tipo As String = ""

    Private Sub MUESTRAClientePortabilidad(ByVal Op As Integer, ByVal Id As Integer)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@ACTIVO", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@ID", SqlDbType.Int, Id)
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, "")
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("MUESTRAClientePortabilidad")
        tbContrato.Text = dt.Rows(0)("Contrato").ToString()
        tbNombre.Text = dt.Rows(0)("Nombre").ToString()
        cbFijo.Text = dt.Rows(0)("ServicioFijo").ToString()
        cbMovil.Text = dt.Rows(0)("ServicioMovil").ToString()
        cbNumeroNoGeografico.Text = dt.Rows(0)("NumNoGeografico").ToString()
        activo = Boolean.Parse(dt.Rows(0)("Activo").ToString())
        tipoSuscriptor = dt.Rows(0)("TipoSuscriptor").ToString()

        If tipoSuscriptor = "G" Then
            rbEntidadDeGobierno.Checked = True
        ElseIf tipoSuscriptor = "F" Then
            rbPersonaFisica.Checked = True
        ElseIf tipoSuscriptor = "M" Then
            rbPersonaMoral.Checked = True
        End If
    End Sub

    Private Sub MUESTRAProveedorPortabilidad(ByVal Op As Integer, ByVal Id As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@ID", SqlDbType.Int, Id)
        cbEmpresaAnterior.DataSource = BaseII.ConsultaDT("MUESTRAProveedorPortabilidad")

    End Sub

    Private Sub MUESTRARelClienTelPortabilidad(ByVal Id As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ID", SqlDbType.Int, Id)
        Dim dt As DataTable
        dt = BaseII.ConsultaDT("MUESTRARelClienTelPortabilidad")

        For Each row As DataRow In dt.Rows
            AgregarTelefono(row("NumTelefono").ToString())
        Next
    End Sub

    Private Sub NUEClientePortabilidad(ByVal TipoSuscriptor As String, ByVal Nombre As String, ByVal ServicioFijo As String, ByVal ServicioMovil As String, ByVal NumNoGeografico As String, ByVal EmpresaOld As Integer, ByVal Contrato As Integer, ByVal Activo As Boolean)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Id", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@TipoSuscriptor", SqlDbType.VarChar, TipoSuscriptor)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, Nombre)
        BaseII.CreateMyParameter("@ServicioFijo", SqlDbType.VarChar, ServicioFijo)
        BaseII.CreateMyParameter("@ServicioMovil", SqlDbType.VarChar, ServicioMovil)
        BaseII.CreateMyParameter("@NumNoGeografico", SqlDbType.VarChar, NumNoGeografico)
        BaseII.CreateMyParameter("@EmpresaOld", SqlDbType.VarChar, EmpresaOld)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.VarChar, Contrato)
        BaseII.CreateMyParameter("@Activo", SqlDbType.Bit, Activo)
        BaseII.ProcedimientoOutPut("NUEClientePortabilidad")
        IdPortabilidad = Convert.ToInt32(BaseII.dicoPar("@Id"))

    End Sub

    Private Sub MODClientePortabilidad(ByVal Id As Integer, ByVal TipoSuscriptor As String, ByVal Nombre As String, ByVal ServicioFijo As String, ByVal ServicioMovil As String, ByVal NumNoGeografico As String, ByVal EmpresaOld As Integer, ByVal Contrato As Integer, ByVal Activo As Boolean)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Id", SqlDbType.Int, Id)
        BaseII.CreateMyParameter("@TipoSuscriptor", SqlDbType.VarChar, TipoSuscriptor)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, Nombre)
        BaseII.CreateMyParameter("@ServicioFijo", SqlDbType.VarChar, ServicioFijo)
        BaseII.CreateMyParameter("@ServicioMovil", SqlDbType.VarChar, ServicioMovil)
        BaseII.CreateMyParameter("@NumNoGeografico", SqlDbType.VarChar, NumNoGeografico)
        BaseII.CreateMyParameter("@EmpresaOld", SqlDbType.VarChar, EmpresaOld)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.VarChar, Contrato)
        BaseII.CreateMyParameter("@Activo", SqlDbType.Bit, Activo)
        BaseII.Inserta("MODClientePortabilidad")
        IdPortabilidad = Id

    End Sub

    Private Sub NUERelClienTelPortabilidad(ByVal Id As Integer, ByVal Num_Telefono As String, ByVal Activo As Boolean)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Id", SqlDbType.Int, Id)
        BaseII.CreateMyParameter("@Num_Telefono", SqlDbType.VarChar, Num_Telefono)
        BaseII.CreateMyParameter("@Activo", SqlDbType.Bit, Activo)
        BaseII.CreateMyParameter("@Error", ParameterDirection.Output, SqlDbType.VarChar, 150)

        BaseII.ProcedimientoOutPut("NUERelClienTelPortabilidad")
        IdPortabilidad = Id

        eerror = BaseII.dicoPar("@Error").ToString()
    End Sub

    Private Sub BORRelClienTelPortabilidad(ByVal Id As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Id", SqlDbType.Int, Id)
        BaseII.Inserta("BORRelClienTelPortabilidad")

        IdPortabilidad = Id
    End Sub

    Private Sub FrmPortabilidad_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        If Bandera = True Then
            Bandera = False
            tbContrato.Text = GLOCONTRATOSEL
            tbNombre.Text = NombreCliente
        End If
    End Sub

    Private Sub FrmPortabilidad_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Bandera = False
        IdPortabilidad = gloClave
        If opcion = "N" Then
            MUESTRAProveedorPortabilidad(0, 0)
            bnActivar.Enabled = False
        ElseIf opcion = "C" Then
            MUESTRAClientePortabilidad(5, IdPortabilidad)
            MUESTRAProveedorPortabilidad(1, IdPortabilidad)
            MUESTRARelClienTelPortabilidad(IdPortabilidad)
            gbSuscriptor.Enabled = False
            gbTipoDeServicio.Enabled = False
            gbProveedor.Enabled = False
            gbNumeros.Enabled = False
            bnGuardar.Enabled = False
            bnActivar.Enabled = False
        ElseIf opcion = "M" Then
            MUESTRAClientePortabilidad(5, IdPortabilidad)
            MUESTRAProveedorPortabilidad(1, IdPortabilidad)
            MUESTRARelClienTelPortabilidad(IdPortabilidad)

        End If

        If opcion = "C" Or opcion = "M" Then
            If activo = False Then
                bnActivar.Enabled = True
            Else
                bnGuardar.Enabled = False
                bnActivar.Enabled = False
                lbPortabilidadActiva.Visible = True
            End If

        End If

    End Sub

    Private Sub bnBuscarClientes_Click(sender As Object, e As EventArgs) Handles bnBuscarClientes.Click
        GloClv_TipSer = 5
        Dim frmSel As New FrmSelCliente
        frmSel.ShowDialog()
        tbContrato.Text = GLOCONTRATOSEL
        tbNombre.Text = NombreCliente
    End Sub

    Private Sub bnAgregar_Click(sender As Object, e As EventArgs) Handles bnAgregar.Click
        Agregar()
    End Sub

    Private Sub tbDelTelefono_KeyDown(sender As Object, e As KeyEventArgs) Handles tbDelTelefono.KeyDown
        If (e.KeyValue = 13) Then
            Agregar()
        End If
    End Sub

    Private Sub tbAlTelefono_KeyDown(sender As Object, e As KeyEventArgs) Handles tbAlTelefono.KeyDown
        If (e.KeyValue = 13) Then
            Agregar()
        End If
    End Sub

    Private Sub Agregar()
        If tbDelTelefono.Text.Length = 0 Then
            MsgBox("Captura un Número Telefónico.")
            Exit Sub
        End If

        If tbDelTelefono.Text.Length > 0 And tbAlTelefono.Text.Length = 0 Then

            If IsNumeric(tbDelTelefono.Text) = False Then

                MsgBox("Captura un Número Telefónico válido.")
                Exit Sub
            End If
            If tbDelTelefono.Text.Length <> 10 Then

                MsgBox("El Número Telefónico debe contener 10 dígitos.")
                Exit Sub
            End If

            numerosRepetidos = ""
            AgregarTelefono(tbDelTelefono.Text)
            If numerosRepetidos.Length > 0 Then
                MessageBox.Show("El Número Telefónico " + numerosRepetidos + " ya se encuentra agregado.")

                tbDelTelefono.Clear()
                tbAlTelefono.Clear()

            End If
        End If
    End Sub

    Private Sub AgregarTelefono(ByVal NumTelefono As String)
        Dim telAux As New Telefonos
        For Each telAux In diccionarioTelefonos.Values
            If telAux.NumTelefono = NumTelefono Then
                If numerosRepetidos.Length = 0 Then
                    numerosRepetidos = NumTelefono
                Else
                    numerosRepetidos = numerosRepetidos + ", " + NumTelefono
                End If
                Return
            End If
        Next

        Dim Telefono As New Telefonos
        key += 1
        Telefono.Key = key
        Telefono.NumTelefono = NumTelefono
        diccionarioTelefonos.Add(key, Telefono)
        LlenadgvTelefonos()
    End Sub

    Private Sub LlenadgvTelefonos()
        listaTelefonos = New List(Of Telefonos)
        For Each telefono As Telefonos In diccionarioTelefonos.Values
            listaTelefonos.Add(telefono)
        Next
        dgvTelefonos.DataSource = listaTelefonos
    End Sub

    Private Sub bnEliminar_Click(sender As Object, e As EventArgs) Handles bnEliminar.Click
        If (dgvTelefonos.RowCount = 0) Then
            MsgBox("Selecciona un Número Telefónico a eliminar.")
            Exit Sub
        End If

        EliminarTelefono(Int32.Parse(dgvTelefonos.SelectedCells(0).Value.ToString()))
    End Sub

    Private Sub EliminarTelefono(ByVal Key As Integer)
        diccionarioTelefonos.Remove(Key)
        Key -= 1
        LlenadgvTelefonos()
    End Sub

    Private Sub bnGuardar_Click(sender As Object, e As EventArgs) Handles bnGuardar.Click
        If (tbContrato.Text.Length = 0) Then

            MsgBox("Selecciona un Contrato.")
            Return
        End If
        If (cbEmpresaAnterior.Text.Length = 0) Then

            MsgBox("Selecciona un Proveedor de Servicios de Telecomunicaciones.")
            Return
        End If
        If (dgvTelefonos.RowCount = 0) Then

            MsgBox("No se han agregado Números Telefónicos a ser Portados.")
            Return
        End If

        If (rbPersonaFisica.Checked = True) Then
            tipo = "F"
        ElseIf (rbPersonaMoral.Checked = True) Then
            tipo = "M"
        ElseIf (rbEntidadDeGobierno.Checked = True) Then
            tipo = "G"
        End If

        If (opcion = "N") Then
            NUEClientePortabilidad(tipo, tbNombre.Text, cbFijo.Text, cbMovil.Text, cbNumeroNoGeografico.Text, Int32.Parse(cbEmpresaAnterior.SelectedValue.ToString()), Int32.Parse(tbContrato.Text), False)
        ElseIf (opcion = "M") Then
            MODClientePortabilidad(IdPortabilidad, tipo, tbNombre.Text, cbFijo.Text, cbMovil.Text, cbNumeroNoGeografico.Text, Int32.Parse(cbEmpresaAnterior.SelectedValue.ToString()), Int32.Parse(tbContrato.Text), False)
        End If

        BORRelClienTelPortabilidad(IdPortabilidad)
        eerror = ""

        For y As Integer = 0 To dgvTelefonos.RowCount - 1
            NUERelClienTelPortabilidad(IdPortabilidad, dgvTelefonos(1, y).Value.ToString(), activo)
        Next
        MsgBox("Se guardó con éxito.")
        If (eerror.Length > 0) Then
            MsgBox("¡Atención! Los siguientes N+úmeros Telefónicos no se agregaron ya que actualmente se encuentran dados de alta en el Catálogo de Números Telefónicos: " + eerror)
        End If

        If (opcion = "N") Then

            'OpReporte = 13
            ' frm as FrmImprimir
            'frm.Show()
            gloClave = IdPortabilidad
            eOpPPE = 5
            FrmImprimirPPE.Show()
        End If
        Bandera = True
        Me.Close()
    End Sub

    Private Sub bnActivar_Click(sender As Object, e As EventArgs) Handles bnActivar.Click
        If (tbContrato.Text.Length = 0) Then

            MsgBox("Selecciona un Contrato.")
            Return
        End If
        If (cbEmpresaAnterior.Text.Length = 0) Then

            MsgBox("Selecciona un Proveedor de Servicios de Telecomunicaciones.")
            Return
        End If
        If (dgvTelefonos.RowCount = 0) Then

            MsgBox("No se han agregado Números Telefónicos a ser Portados.")
            Return
        End If

        If (rbPersonaFisica.Checked = True) Then
            tipo = "F"
        ElseIf (rbPersonaMoral.Checked = True) Then
            tipo = "M"
        ElseIf (rbEntidadDeGobierno.Checked = True) Then
            tipo = "G"
        End If

        MODClientePortabilidad(IdPortabilidad, tipo, tbNombre.Text, cbFijo.Text, cbMovil.Text, cbNumeroNoGeografico.Text, Int32.Parse(cbEmpresaAnterior.SelectedValue.ToString()), Int32.Parse(tbContrato.Text), True)
        BORRelClienTelPortabilidad(IdPortabilidad)
        eerror = ""

        For y As Integer = 0 To dgvTelefonos.RowCount - 1
            NUERelClienTelPortabilidad(IdPortabilidad, dgvTelefonos(1, y).Value.ToString(), activo)
        Next

        MsgBox("Se guardó con éxito.")
        If (eerror.Length > 0) Then
            MsgBox("¡Atención! Los siguientes Números Telefónicos no se agregaron ya que actualmente se encuentran dados de alta en el Catálogo de Números Telefónicos: " + eerror)
        End If

        Bandera = True
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(sender As Object, e As EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class