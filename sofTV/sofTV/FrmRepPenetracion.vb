Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class FrmRepPenetracion
    Private customersByCityReport As ReportDocument
    Dim princ As String = Nothing
    Dim oprep As String = Nothing
    Dim Titulo As String = Nothing
    Private Sub ConfigureCrystalReportsNew6()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim Contrataciones As Boolean = False
            Dim Instalaciones As Boolean = False
            Dim Fuera_Area As Boolean = False
            Dim Cancelaciones As Boolean = False
            Dim reportPath As String = Nothing

            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing



            mySelectFormula = "Penetraci�n Por Calle y Colonia Del "




            reportPath = RutaReportes + "\ReportePenetracion_CalleyColonia.rpt"

            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@op
            customersByCityReport.SetParameterValue(0, GloClv_tipser2)
            '@clv_colonia
            customersByCityReport.SetParameterValue(1, Locclv_colonia)
            '@clv_calle
            customersByCityReport.SetParameterValue(2, Locclv_calle)
            '@clv_txt
            customersByCityReport.SetParameterValue(3, Locclv_txt)





            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.ReportSource = customersByCityReport



            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & LocDesP & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia").Text = "' Colonia: " & LocDesPC & "'"
            customersByCityReport.DataDefinition.FormulaFields("Calle").Text = "'" & LocDesPCa & "'"






            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()

    End Sub

    Private Sub FrmRepPenetracion_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Locbndpen1 = True Then
            Locbndpen1 = False
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If oprep = 2 Then
                Me.Dame_Datos_Rep_PTableAdapter.Connection = CON
                Me.Dame_Datos_Rep_PTableAdapter.Fill(Me.DataSetarnoldo.Dame_Datos_Rep_P, GloClv_tipser2, Locclv_colonia, Locclv_calle, Locclv_txt)
                CON.Close()
                DameDatosPenetracion(GloClv_tipser2, Locclv_colonia, Locclv_calle, LocClv_session, eClv_Sector, eTipoPen)
                ReportePenetracionPorSector(eTipSer, 0, 0, LocClv_session, 0, 0)
                LocOp = 0
            Else
                Me.TextBox1.Text = ""
                Me.Dame_Datos_Rep_PTableAdapter.Connection = CON
                Me.Dame_Datos_Rep_PTableAdapter.Fill(Me.DataSetarnoldo.Dame_Datos_Rep_P, GloClv_tipser2, Locclv_colonia, Locclv_calle, Locclv_txt)
                CON.Close()

                If CInt(Me.SalidasTapsTextBox.Text) > 0 Then
                    Me.TextBox2.Text = Math.Round(((CDbl(Me.ITextBox.Text) + CDbl(Me.DTextBox.Text) + CDbl(Me.STextBox.Text)) / CDbl(Me.SalidasTapsTextBox.Text)) * 100, 2)
                    Me.TextBox3.Text = Math.Round((CDbl(Me.ITextBox.Text) / CDbl(Me.SalidasTapsTextBox.Text)) * 100, 2)
                    ConfigureCrystalReportsNew6()
                Else
                    MsgBox("En esa Colonia(s) y/o Calle(s) no existen salidas en los Taps", MsgBoxStyle.Information)
                End If
            End If
        End If
    End Sub
    Private Sub DameDatosPenetracion(ByVal Op As Integer, ByVal Clv_Colonia As Integer, ByVal Clv_Calle As Integer, ByVal Clv_Session As Long, ByVal Clv_Sector As Integer, ByVal Tipo As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameDatosPenetracion", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Op", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Op
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Colonia
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_Calle", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_Calle
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_Session
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Clv_Sector", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Clv_Sector
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Tipo", SqlDbType.Int)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = Tipo
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@Contratados", SqlDbType.BigInt)
        parametro7.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@Instalados", SqlDbType.BigInt)
        parametro8.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@Desconectados", SqlDbType.BigInt)
        parametro9.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@Suspendidos", SqlDbType.BigInt)
        parametro10.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro10)

        Dim parametro11 As New SqlParameter("@Baja", SqlDbType.BigInt)
        parametro11.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro11)

        Dim parametro12 As New SqlParameter("@FueraA", SqlDbType.BigInt)
        parametro12.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro12)

        Dim parametro13 As New SqlParameter("@SalidaTap", SqlDbType.BigInt)
        parametro13.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro13)

        Dim parametro14 As New SqlParameter("@TotClientes", SqlDbType.BigInt)
        parametro14.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro14)

        Dim parametro15 As New SqlParameter("@TotTotal", SqlDbType.BigInt)
        parametro15.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro15)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Me.CTextBox.Text = parametro7.Value.ToString
            Me.ITextBox.Text = parametro8.Value.ToString
            Me.DTextBox.Text = parametro9.Value.ToString
            Me.STextBox.Text = parametro10.Value.ToString
            Me.BTextBox.Text = parametro11.Value.ToString
            Me.FTextBox.Text = parametro12.Value.ToString
            Me.SalidasTapsTextBox.Text = parametro13.Value.ToString
            Me.TotalTextBox.Text = parametro15.Value.ToString

            If IsNumeric(Me.SalidasTapsTextBox.Text) = True Then
                If CLng(Me.SalidasTapsTextBox.Text) > 0 Then
                    Me.TextBox2.Text = Math.Round(((CDbl(Me.ITextBox.Text) + CDbl(Me.DTextBox.Text) + CDbl(Me.STextBox.Text)) / CDbl(Me.SalidasTapsTextBox.Text)) * 100, 2)
                    Me.TextBox3.Text = Math.Round((CDbl(Me.ITextBox.Text) / CDbl(Me.SalidasTapsTextBox.Text)) * 100, 2)
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub FrmRepPenetracion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)

        princ = Me.ComboBox4.SelectedValue.ToString
        GloClv_tipser2 = Me.ComboBox4.SelectedValue
        Select Case princ
            Case "1"
                Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
                Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
            Case "2"
                Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
                Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
            Case "3"
                Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
                Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
        End Select
        CON.Close()
        colorea(Me, Me.Name)
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
            GloClv_tipser2 = ComboBox4.SelectedValue
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
            Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
            CON.Close()
        End If
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If IsNumeric(Me.DataGridView1.SelectedCells(0).Value) = True Then
            oprep = Me.DataGridView1.SelectedCells(0).Value.ToString
            Titulo = Me.DataGridView1.SelectedCells(1).Value.ToString

            eTipSer = ComboBox4.SelectedValue
            LocClv_session = DAMESclv_Sessionporfavor()
            Locbndpen1 = True

            Locclv_colonia = 0
            Locclv_calle = 0

            If oprep = "1" Then
                My.Forms.FrmSelDatosP.Show()
            End If
            If oprep = "2" Then
                FrmSelServicioE.Show()
            End If
        End If
    End Sub
    Public Function DAMESclv_Sessionporfavor() As Long
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            'Regresa La Session
            DAMESclv_Sessionporfavor = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "DameClv_Session_Servicios"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm2 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
                DAMESclv_Sessionporfavor = prm2.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
        End Try
    End Function
    Private Sub ReportePenetracionPorSector(ByVal Op As Integer, ByVal Clv_Colonia As Integer, ByVal Clv_Calle As Integer, ByVal Clv_Session As Integer, ByVal Clv_Sector As Integer, ByVal Tipo As Integer)
        Try
            customersByCityReport = New ReportDocument

            Dim conexion As New SqlConnection(MiConexion)
            Dim reportPath As String = Nothing
            Dim sBuilder As New StringBuilder("EXEC ReportePenetracionPorSector " + Op.ToString() + ", " + Clv_Colonia.ToString() + ", " + Clv_Calle.ToString() + ", " + Clv_Session.ToString() + ", " + Clv_Sector.ToString + ", " + Tipo.ToString())
            Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
            Dim dTable As New DataTable

            dAdapter.Fill(dTable)
            Me.TotalTextBox.Text = dTable.Rows(0)(15).ToString
            Me.CTextBox.Text = dTable.Rows(0)(8).ToString
            Me.ITextBox.Text = dTable.Rows(0)(9).ToString
            Me.DTextBox.Text = dTable.Rows(0)(10).ToString
            Me.STextBox.Text = dTable.Rows(0)(11).ToString
            Me.FTextBox.Text = dTable.Rows(0)(12).ToString
            Me.BTextBox.Text = dTable.Rows(0)(13).ToString
            Me.SalidasTapsTextBox.Text = dTable.Rows(0)(5).ToString
            Me.TextBox3.Text = dTable.Rows(0)(6).ToString
            Me.TextBox2.Text = dTable.Rows(0)(7).ToString
            Me.TextBox1.Text = dTable.Rows(0)(16).ToString

            reportPath = RutaReportes + "\ReportPenetracionPorSector.rpt"
            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(dTable)

            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            CrystalReportViewer1.ReportSource = customersByCityReport

            customersByCityReport = Nothing

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

End Class