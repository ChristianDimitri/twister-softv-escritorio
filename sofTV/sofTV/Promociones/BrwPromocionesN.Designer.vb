﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwPromocionesN
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxListado = New System.Windows.Forms.GroupBox()
        Me.dgvPromociones = New System.Windows.Forms.DataGridView()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.gbxFiltros = New System.Windows.Forms.GroupBox()
        Me.lblFiltroNombre = New System.Windows.Forms.Label()
        Me.lblFiltroID = New System.Windows.Forms.Label()
        Me.txtFiltroNombre = New System.Windows.Forms.TextBox()
        Me.txtFiltroID = New System.Windows.Forms.TextBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.idPromocion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nombrePromocion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.contratacion = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.mensualidad = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.recontratacion = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.activa = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.gbxListado.SuspendLayout()
        CType(Me.dgvPromociones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFiltros.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxListado
        '
        Me.gbxListado.Controls.Add(Me.dgvPromociones)
        Me.gbxListado.Location = New System.Drawing.Point(235, 13)
        Me.gbxListado.Name = "gbxListado"
        Me.gbxListado.Size = New System.Drawing.Size(567, 635)
        Me.gbxListado.TabIndex = 0
        Me.gbxListado.TabStop = False
        Me.gbxListado.Text = "Listado de promociones"
        '
        'dgvPromociones
        '
        Me.dgvPromociones.AllowUserToAddRows = False
        Me.dgvPromociones.AllowUserToDeleteRows = False
        Me.dgvPromociones.AllowUserToOrderColumns = True
        Me.dgvPromociones.AllowUserToResizeColumns = False
        Me.dgvPromociones.AllowUserToResizeRows = False
        Me.dgvPromociones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPromociones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.idPromocion, Me.nombrePromocion, Me.contratacion, Me.mensualidad, Me.recontratacion, Me.activa})
        Me.dgvPromociones.Location = New System.Drawing.Point(11, 22)
        Me.dgvPromociones.Name = "dgvPromociones"
        Me.dgvPromociones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPromociones.Size = New System.Drawing.Size(545, 603)
        Me.dgvPromociones.TabIndex = 0
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(820, 603)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(100, 35)
        Me.btnSalir.TabIndex = 159
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'btnNuevo
        '
        Me.btnNuevo.BackColor = System.Drawing.Color.DarkOrange
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.ForeColor = System.Drawing.Color.Black
        Me.btnNuevo.Location = New System.Drawing.Point(820, 30)
        Me.btnNuevo.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(100, 35)
        Me.btnNuevo.TabIndex = 160
        Me.btnNuevo.Text = "Nueva"
        Me.btnNuevo.UseVisualStyleBackColor = False
        '
        'btnModificar
        '
        Me.btnModificar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModificar.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.ForeColor = System.Drawing.Color.Black
        Me.btnModificar.Location = New System.Drawing.Point(820, 82)
        Me.btnModificar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(100, 35)
        Me.btnModificar.TabIndex = 161
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = False
        '
        'btnEliminar
        '
        Me.btnEliminar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.ForeColor = System.Drawing.Color.Black
        Me.btnEliminar.Location = New System.Drawing.Point(820, 139)
        Me.btnEliminar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(100, 35)
        Me.btnEliminar.TabIndex = 162
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = False
        '
        'gbxFiltros
        '
        Me.gbxFiltros.Controls.Add(Me.lblFiltroNombre)
        Me.gbxFiltros.Controls.Add(Me.lblFiltroID)
        Me.gbxFiltros.Controls.Add(Me.txtFiltroNombre)
        Me.gbxFiltros.Controls.Add(Me.txtFiltroID)
        Me.gbxFiltros.Controls.Add(Me.btnBuscar)
        Me.gbxFiltros.Location = New System.Drawing.Point(13, 13)
        Me.gbxFiltros.Name = "gbxFiltros"
        Me.gbxFiltros.Size = New System.Drawing.Size(216, 635)
        Me.gbxFiltros.TabIndex = 163
        Me.gbxFiltros.TabStop = False
        Me.gbxFiltros.Text = "Filtros de búsqueda"
        '
        'lblFiltroNombre
        '
        Me.lblFiltroNombre.AutoSize = True
        Me.lblFiltroNombre.Location = New System.Drawing.Point(17, 86)
        Me.lblFiltroNombre.Name = "lblFiltroNombre"
        Me.lblFiltroNombre.Size = New System.Drawing.Size(53, 18)
        Me.lblFiltroNombre.TabIndex = 166
        Me.lblFiltroNombre.Text = "Nombre"
        '
        'lblFiltroID
        '
        Me.lblFiltroID.AutoSize = True
        Me.lblFiltroID.Location = New System.Drawing.Point(17, 34)
        Me.lblFiltroID.Name = "lblFiltroID"
        Me.lblFiltroID.Size = New System.Drawing.Size(25, 18)
        Me.lblFiltroID.TabIndex = 165
        Me.lblFiltroID.Text = "ID:"
        '
        'txtFiltroNombre
        '
        Me.txtFiltroNombre.Location = New System.Drawing.Point(17, 107)
        Me.txtFiltroNombre.Name = "txtFiltroNombre"
        Me.txtFiltroNombre.Size = New System.Drawing.Size(187, 23)
        Me.txtFiltroNombre.TabIndex = 164
        '
        'txtFiltroID
        '
        Me.txtFiltroID.Location = New System.Drawing.Point(17, 55)
        Me.txtFiltroID.Name = "txtFiltroID"
        Me.txtFiltroID.Size = New System.Drawing.Size(116, 23)
        Me.txtFiltroID.TabIndex = 163
        '
        'btnBuscar
        '
        Me.btnBuscar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscar.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscar.ForeColor = System.Drawing.Color.Black
        Me.btnBuscar.Location = New System.Drawing.Point(17, 137)
        Me.btnBuscar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(96, 35)
        Me.btnBuscar.TabIndex = 162
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = False
        '
        'idPromocion
        '
        Me.idPromocion.DataPropertyName = "idPromocion"
        Me.idPromocion.HeaderText = "ID"
        Me.idPromocion.Name = "idPromocion"
        Me.idPromocion.ReadOnly = True
        Me.idPromocion.Width = 50
        '
        'nombrePromocion
        '
        Me.nombrePromocion.DataPropertyName = "nombrePromocion"
        Me.nombrePromocion.HeaderText = "Nombre de la Promocion"
        Me.nombrePromocion.Name = "nombrePromocion"
        Me.nombrePromocion.ReadOnly = True
        Me.nombrePromocion.Width = 250
        '
        'contratacion
        '
        Me.contratacion.DataPropertyName = "contratacion"
        Me.contratacion.HeaderText = "Contratacion habilitada"
        Me.contratacion.Name = "contratacion"
        Me.contratacion.ReadOnly = True
        '
        'mensualidad
        '
        Me.mensualidad.DataPropertyName = "mensualidad"
        Me.mensualidad.HeaderText = "Mensualidad habilitada"
        Me.mensualidad.Name = "mensualidad"
        Me.mensualidad.ReadOnly = True
        '
        'recontratacion
        '
        Me.recontratacion.DataPropertyName = "recontratacion"
        Me.recontratacion.HeaderText = "Recontratacion"
        Me.recontratacion.Name = "recontratacion"
        Me.recontratacion.ReadOnly = True
        Me.recontratacion.Visible = False
        '
        'activa
        '
        Me.activa.DataPropertyName = "activa"
        Me.activa.HeaderText = "Activa"
        Me.activa.Name = "activa"
        Me.activa.ReadOnly = True
        Me.activa.Visible = False
        '
        'BrwPromocionesN
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(932, 660)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.gbxFiltros)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.gbxListado)
        Me.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.Name = "BrwPromocionesN"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Promociones"
        Me.gbxListado.ResumeLayout(False)
        CType(Me.dgvPromociones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFiltros.ResumeLayout(False)
        Me.gbxFiltros.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxListado As System.Windows.Forms.GroupBox
    Friend WithEvents dgvPromociones As System.Windows.Forms.DataGridView
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents gbxFiltros As System.Windows.Forms.GroupBox
    Friend WithEvents lblFiltroNombre As System.Windows.Forms.Label
    Friend WithEvents lblFiltroID As System.Windows.Forms.Label
    Friend WithEvents txtFiltroNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtFiltroID As System.Windows.Forms.TextBox
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents idPromocion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nombrePromocion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents contratacion As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents mensualidad As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents recontratacion As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents activa As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
