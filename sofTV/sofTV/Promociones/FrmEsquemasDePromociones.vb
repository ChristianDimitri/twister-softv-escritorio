﻿Imports System.Xml
Imports System.IO
Imports System.Text
Imports System.Data
Imports Softv.BAL
Imports System.Collections.Generic
Public Class FrmEsquemasDePromociones
    ''' <summary>
    ''' 
    ''' Forma que sirve para el CRUD y configuración de las promociones del sistema
    ''' 
    ''' Fecha de creación: 29/08/2011
    ''' Autor: Jorge Luis
    ''' Última Modificación:
    ''' Modificación: [N/A]
    ''' 
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Private dsCatalogos As New DataSet
    Public idPromocion As Integer = 0, idEsquemaContratacion As Integer = 0, idEsquemaMensualidad As Integer = 0
    Public Promocion As New Promociones()
    Dim Sericio As New Servicio()
    Dim listaServicio As New List(Of Servicio)


#Region "Controles y eventos"

    'btnSalir_Click
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    'btnGuardarPromocion_Click
    Private Sub btnGuardarPromocion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardarPromocion.Click
        If (ValidacionDatosPromocion() And ValidaFechas()) Then
            Try
                If idPromocion = 0 Then
                    NuevaPromocion()
                Else
                    ModificaPromocion()
                End If
                NUETBLDETPROMOCIONES(idPromocion, cbProporcionales.Checked)
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try
        End If
    End Sub

    'btnGuardarEsquemaContratacion_Click
    Private Sub btnGuardarEsquemaContratacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardarEsquemaContratacion.Click
        If (idEsquemaContratacion = 0) Then
            NuevoEsquemaContratacion()
        Else
            ModificaEsquemaContratacion()
        End If
    End Sub

    'bntGuardarEsquemaMensualidad_Click
    Private Sub bntGuardarEsquemaMensualidad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bntGuardarEsquemaMensualidad.Click

        If (nudMesesBonificados.Value > nudMesesDiferidos.Value) Then
            MsgBox("El número de meses a bonificar no puede ser superior al número de meses diferidos, ya que arrojaría cargos negativos.", MsgBoxStyle.Information)
            nudMesesBonificados.Focus()
            Exit Sub
        End If

        If (idEsquemaMensualidad = 0) Then
            NuevoEsquemaMensualidad()
        Else
            ModificaEsquemaMensualidad()
        End If
    End Sub

    'cbxServicio_SelectedValueChanged
    Private Sub cbxServicio_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxServicio.SelectedValueChanged
        Try
            If ((cbxTipoDeCobro.Items.Count > 0) And (cbxServicio.Items.Count > 0)) Then
                dgvRelTarifadoBonificacion.DataSource = Promocion.GetRelTarifadosEsquemaContratacionByIdTarIfado_Clv_Servicio(cbxTipoDeCobro.SelectedValue, cbxServicio.SelectedValue, idEsquemaContratacion)
                dgvRelTarifadoBonificacion.Columns(3).DefaultCellStyle.Format = "c"
                dgvRelTarifadoBonificacion.Columns(4).DefaultCellStyle.Format = "c"
                dgvRelTarifadoBonificacion.Refresh()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    'cbxTipoDeCobro_SelectedValueChanged
    Private Sub cbxTipoDeCobro_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoDeCobro.SelectedValueChanged
        Try
            If ((cbxTipoDeCobro.Items.Count > 0) And (cbxServicio.Items.Count > 0)) Then
                dgvRelTarifadoBonificacion.DataSource = Promocion.GetRelTarifadosEsquemaContratacionByIdTarIfado_Clv_Servicio(cbxTipoDeCobro.SelectedValue, cbxServicio.SelectedValue, idEsquemaContratacion)
                dgvRelTarifadoBonificacion.Columns(3).DefaultCellStyle.Format = "c"
                dgvRelTarifadoBonificacion.Columns(4).DefaultCellStyle.Format = "c"
                dgvRelTarifadoBonificacion.Refresh()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    'FrmEsquemasDePromociones_Load
    Private Sub FrmEsquemasDePromociones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        EnlazaDatos()
    End Sub

    'cbxServicio_Mensualidad_SelectedValueChanged
    Private Sub cbxServicio_Mensualidad_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxServicio_Mensualidad.SelectedValueChanged
        Try
            If ((cbxTipoDeCobro_Mensualidad.Items.Count > 0) And (cbxServicio_Mensualidad.Items.Count > 0)) Then
                EnlazadgvRelTarifadoBonificacion_Mensualidad(cbxTipoDeCobro_Mensualidad.SelectedValue, cbxServicio_Mensualidad.SelectedValue)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    'cbxTipoDeCobro_Mensualidad_SelectedValueChanged
    Private Sub cbxTipoDeCobro_Mensualidad_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoDeCobro_Mensualidad.SelectedValueChanged
        Try
            If ((cbxTipoDeCobro_Mensualidad.Items.Count > 0) And (cbxServicio_Mensualidad.Items.Count > 0)) Then
                EnlazadgvRelTarifadoBonificacion_Mensualidad(cbxTipoDeCobro_Mensualidad.SelectedValue, cbxServicio_Mensualidad.SelectedValue)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    'cbxTipoServicio_SelectedValueChanged
    Private Sub cbxTipoServicio_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoServicio.SelectedValueChanged

        Try
            If cbxTipoServicio.Items.Count > 0 Then
                EnlazaServiciosByClv_Tipser(cbxTipoServicio.SelectedValue)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    'cbxTipoServicio_Mensualidad_SelectedValueChanged
    Private Sub cbxTipoServicio_Mensualidad_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoServicio_Mensualidad.SelectedValueChanged
        Try
            If cbxTipoServicio_Mensualidad.Items.Count > 0 Then
                EnlazaServiciosByClv_Tipser_Mensualidad(cbxTipoServicio_Mensualidad.SelectedValue)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
	
#End Region

#Region "Funciones y métodos"

    'NuevaPromocion()
    Private Sub NuevaPromocion()
        Try
            Dim p As New Promociones()
            idPromocion = p.Add(0, txtNombrePromocion.Text, chbxContratacionActiva.Checked, chbxMensualidadActiva.Checked, chbxRecontratacion.Checked, dtpFechaInicialContratacion.Value, dtpFechaFinalContratacion.Value, "A")
            If (idPromocion) > 0 Then

                txtIDPromocion.Text = idPromocion.ToString()
                txtIDPromocion_Contratacion.Text = idPromocion.ToString()
                txtIDPromocion_Mensualidad.Text = idPromocion.ToString()
                HabilitaEsquemas(chbxContratacionActiva.Checked, chbxMensualidadActiva.Checked, chbxRecontratacion.Checked)

                MsgBox("Se ha agregado correctamente", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    'ModificaPromocion()
    Private Sub ModificaPromocion()
        Try
            Dim p As New Promociones()
            p.Edit(idPromocion, txtNombrePromocion.Text, chbxContratacionActiva.Checked, chbxMensualidadActiva.Checked, chbxRecontratacion.Checked, dtpFechaInicialContratacion.Value, dtpFechaFinalContratacion.Value, "A")
            HabilitaEsquemas(chbxContratacionActiva.Checked, chbxMensualidadActiva.Checked, chbxRecontratacion.Checked)
            Me.DialogResult = DialogResult.OK
            MsgBox("Se ha actualizado correctamente", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    'HabilitaEsquemas
    Private Sub HabilitaEsquemas(ByVal contratacion As Boolean, ByVal mensualidad As Boolean, ByVal Recontratacion As Boolean)
        Try
            'Contratacion
            gbxConfiguracionesPromocion_Contratacion.Enabled = contratacion
            gbxTarifadosALosQueAplica.Enabled = contratacion

            'Mensualidad
            gbxConfiguracionesPromocion_Mensualidad.Enabled = mensualidad
            gbxConfiguracionDeMesesBonificados.Enabled = mensualidad

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    'NuevoEsquemaContratacion()
    Private Sub NuevoEsquemaContratacion()

        Dim eC As New EsquemaContratacion()
        idEsquemaContratacion = eC.Add(0, idPromocion, cbxServicio.SelectedValue, 1, cbxTipoServicio.SelectedValue, 0, txtImporteBonificar.Text, True)
        GuardaRelEsquemaPromocionTarifados()
        If (idEsquemaContratacion > 0) Then
            MsgBox("Se ha agregado el esquema correctamente", MsgBoxStyle.Information)
        End If

    End Sub

    'ModificaEsquemaContratacion()
    Private Sub ModificaEsquemaContratacion()

        Dim eC As New EsquemaContratacion()
        eC.Edit(idEsquemaContratacion, idPromocion, cbxServicio.SelectedValue, 1, cbxTipoServicio.SelectedValue, 0, txtImporteBonificar.Text, True)
        GuardaRelEsquemaPromocionTarifados()
        If (idEsquemaContratacion > 0) Then
            MsgBox("Se ha actualizado el esquema correctamente", MsgBoxStyle.Information)
        End If

    End Sub

    'NuevoEsquemaMensualidad()
    Private Sub NuevoEsquemaMensualidad()

        Dim eM As New EsquemaMensualidad()
        idEsquemaMensualidad = eM.Add(0, idPromocion, cbxServicio_Mensualidad.SelectedValue, nudMesesBonificados.Value, nudMesesDiferidos.Value, txtImportePorMes.Text, txtImporteTotal.Text, True)

        If (idEsquemaMensualidad > 0) Then
            MsgBox("Se ha agregado el esquema correctamente", MsgBoxStyle.Information)
        End If

    End Sub

    'ModificaEsquemaMensualidad()
    Private Sub ModificaEsquemaMensualidad()

        Dim eM As New EsquemaMensualidad()
        eM.Edit(idEsquemaMensualidad, idPromocion, cbxServicio_Mensualidad.SelectedValue, nudMesesBonificados.Value, nudMesesDiferidos.Value, txtImportePorMes.Text, txtImporteTotal.Text, True)
        GuardaRelEsquemaMensualidadTarifados()
        EnlazaControlesEsquemaMensualidad(Promocion)
        If (idEsquemaMensualidad > 0) Then
            MsgBox("Se ha actualizado el esquema correctamente", MsgBoxStyle.Information)
        End If

    End Sub

    'EnlazaDatos()
    Private Sub EnlazaDatos()

        Try
            'Cargamos los catálogos que necesitan las prmociones
            Dim p As New Promociones()
            Dim bsTipoServicio As New BindingSource()

            dsCatalogos = p.GetCatalogosPromociones()

            dsCatalogos.Tables(0).TableName = "TipoServicio"
            dsCatalogos.Tables(1).TableName = "Servicios"
            dsCatalogos.Tables(2).TableName = "TipoClientes"
            dsCatalogos.Tables(3).TableName = "Tarifados"

            dsCatalogos.Relations.Add("FK_Clv_TipSer", dsCatalogos.Tables("TipoServicio").Columns("Clv_TipSer"), dsCatalogos.Tables("Servicios").Columns("Clv_TipSer"))
            dsCatalogos.Relations.Add("FK_RelTarifadosServicios", dsCatalogos.Tables("Servicios").Columns("Clv_Servicio"), dsCatalogos.Tables("Tarifados").Columns("Clv_Servicio"))

            bsTipoServicio.DataSource = dsCatalogos.Tables("TipoServicio")
            Dim bsServicios As New BindingSource(bsTipoServicio, "FK_Clv_TipSer")

            cbxTipoServicio.DisplayMember = "Concepto"
            cbxTipoServicio.ValueMember = "Clv_TipSer"
            cbxTipoServicio_Mensualidad.DisplayMember = "Concepto"
            cbxTipoServicio_Mensualidad.ValueMember = "Clv_TipSer"
            cbxTipoServicio.DataSource = dsCatalogos.Tables("TipoServicio")
            cbxTipoServicio_Mensualidad.DataSource = dsCatalogos.Tables("TipoServicio")

            cbxServicio.DisplayMember = "Descripcion"
            cbxServicio.ValueMember = "Clv_Txt"
            cbxServicio_Mensualidad.DisplayMember = "Descripcion"
            cbxServicio_Mensualidad.ValueMember = "Clv_Txt"
            cbxServicio.DataSource = dsCatalogos.Tables("Servicios")
            cbxServicio_Mensualidad.DataSource = dsCatalogos.Tables("Servicios")

            cbxTipoDeCobro.DisplayMember = "Descripcion"
            cbxTipoDeCobro.ValueMember = "Clv_TipoCliente"
            cbxTipoDeCobro.DataSource = dsCatalogos.Tables("TipoClientes")

            cbxTipoDeCobro_Mensualidad.DisplayMember = "Descripcion"
            cbxTipoDeCobro_Mensualidad.ValueMember = "Clv_TipoCliente"
            cbxTipoDeCobro_Mensualidad.DataSource = dsCatalogos.Tables("TipoClientes")

            dgvRelTarifadoBonificacion.DataSource = dsCatalogos.Tables("Tarifados")
            dgvRelTarifadoBonificacion.Columns(3).DefaultCellStyle.Format = "c"
            dgvRelTarifadoBonificacion.Columns(4).DefaultCellStyle.Format = "c"

            If idPromocion = 0 Then
                HabilitaEsquemas(False, False, False)
            Else
                HabilitaEsquemas(Promocion.contratacion, Promocion.mensualidad, Promocion.recontratacion)
                EnlazaControlesPromocion(Promocion)
                CONTBLDETPROMOCIONES(idPromocion)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    'EnlazaControlesPromocion
    Private Sub EnlazaControlesPromocion(ByRef Promocion As Promociones)
        Try

            idPromocion = Promocion.idPromocion
            txtIDPromocion.Text = Promocion.idPromocion
            txtNombrePromocion.Text = Promocion.nombrePromocion
            chbxContratacionActiva.Checked = Promocion.contratacion
            chbxMensualidadActiva.Checked = Promocion.mensualidad
            dtpFechaInicialContratacion.Value = Promocion.FechaInicial
            dtpFechaFinalContratacion.Value = Promocion.FechaFinal

            'Verificamos los esquemas de la promoción
            If (Promocion.contratacion) Then
                EnlazaControlesEsquemaContratacion(Promocion)
            End If

            If (Promocion.mensualidad) Then
                EnlazaControlesEsquemaMensualidad(Promocion)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    'EnlazaControlesEsquemaContratacion
    Private Sub EnlazaControlesEsquemaContratacion(ByRef Promocion As Promociones)

        Try
            Dim eC As New EsquemaContratacion()
            eC = eC.GetByIdPromocion(Promocion.idPromocion)

            If Not (eC Is Nothing) Then

                idEsquemaContratacion = eC.idEsquema
                txtIDPromocion_Contratacion.Text = Promocion.idPromocion
                cbxServicio.SelectedValue = eC.clv_txt
                txtImporteBonificar.Text = CType(eC.importeBonificado, Double)

            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        Try
            If ((cbxTipoDeCobro.Items.Count > 0) And (cbxServicio.Items.Count > 0)) Then
                dgvRelTarifadoBonificacion.DataSource = Promocion.GetRelTarifadosEsquemaContratacionByIdTarIfado_Clv_Servicio(cbxTipoDeCobro.SelectedValue, cbxServicio.SelectedValue, idEsquemaContratacion)
                dgvRelTarifadoBonificacion.Columns(3).DefaultCellStyle.Format = "c"
                dgvRelTarifadoBonificacion.Columns(4).DefaultCellStyle.Format = "c"
                dgvRelTarifadoBonificacion.Refresh()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    'EnlazaControlesEsquemaMensualidad
    Private Sub EnlazaControlesEsquemaMensualidad(ByRef Promocion As Promociones)
        Try

            Dim eM As New EsquemaMensualidad()
            eM = eM.GetByIdPromocion(Promocion.idPromocion)

            If Not (eM Is Nothing) Then

                idEsquemaMensualidad = eM.idEsquema
                txtIDPromocion_Mensualidad.Text = eM.idPromocion
                txtImportePorMes.Text = CType(eM.importePorMes, Double)
                txtImporteTotal.Text = CType(eM.importeTotal, Double)
                nudMesesBonificados.Value = eM.nMesesBonificados
                nudMesesDiferidos.Value = eM.nMesesDiferidos

            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        Try
            If ((cbxTipoDeCobro_Mensualidad.Items.Count > 0) And (cbxServicio_Mensualidad.Items.Count > 0)) Then
                EnlazadgvRelTarifadoBonificacion_Mensualidad(cbxTipoDeCobro_Mensualidad.SelectedValue, cbxServicio_Mensualidad.SelectedValue)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try


    End Sub

    'Convierte el detalle del grid en XML
    Private Function convierteRelTarifadosBonificacionXML() As String

        Dim sw As New StringWriter()
        Dim w As New XmlTextWriter(sw)
        Dim i As Integer
        Dim strXML As String = ""

        Try

            w.Formatting = Formatting.Indented
            w.WriteStartElement("tblRelEsquemaPromocionTarifados")
            'Barremos las Columnas de Se Retiro por Cada Detalle de la Orden
            For i = 0 To dgvRelTarifadoBonificacion.RowCount - 1

                w.WriteStartElement("Tarifado")
                w.WriteAttributeString("idEsquemaPromocion", idEsquemaContratacion)
                w.WriteAttributeString("CLV_LLAVE", dgvRelTarifadoBonificacion.Rows(i).Cells("CLV_LLAVE").Value.ToString())
                w.WriteAttributeString("Aplica", dgvRelTarifadoBonificacion.Rows(i).Cells("Aplica").Value.ToString())
                w.WriteAttributeString("importeBonificado", txtImporteBonificar.Text)

                w.WriteEndElement()

            Next

            w.WriteEndElement()
            w.Close()

            strXML = ""
            strXML = sw.ToString()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

        Return strXML
    End Function

    'Convierte el detalle del grid en XML
    Private Function convierteRelTarifadosMensualidadXML() As String

        Dim sw As New StringWriter()
        Dim w As New XmlTextWriter(sw)
        Dim i As Integer
        Dim strXML As String = ""

        Try

            w.Formatting = Formatting.Indented
            w.WriteStartElement("tblRelEsquemaMensualidadTarifados")
            'Barremos las Columnas de Se Retiro por Cada Detalle de la Orden
            For i = 0 To dgvRelTarifadoBonificacion_Mensualidad.RowCount - 1

                w.WriteStartElement("Tarifado")
                w.WriteAttributeString("idEsquemaMensualidad", idEsquemaMensualidad)
                w.WriteAttributeString("CLV_LLAVE", dgvRelTarifadoBonificacion_Mensualidad.Rows(i).Cells("CLV_LLAVEMensualidad").Value.ToString())
                w.WriteAttributeString("nMesesBonificados", nudMesesBonificados.Value.ToString)
                w.WriteAttributeString("nMesesDiferidos", nudMesesDiferidos.Value.ToString)
                w.WriteAttributeString("Mensualidad", dgvRelTarifadoBonificacion_Mensualidad.Rows(i).Cells("PRECIOMensualidad").Value.ToString())
                w.WriteAttributeString("RentaAparato", dgvRelTarifadoBonificacion_Mensualidad.Rows(i).Cells("RentaAparatoMensualidad").Value.ToString())
                w.WriteAttributeString("importeBonificadoMensualidad", 0)
                w.WriteAttributeString("importeBonificadoDeco", 0)
                w.WriteAttributeString("AplicaBonificacionDeco", dgvRelTarifadoBonificacion_Mensualidad.Rows(i).Cells("incluyeRentaDeco").Value.ToString())
                w.WriteAttributeString("Aplica", dgvRelTarifadoBonificacion_Mensualidad.Rows(i).Cells("AplicaMensualidad").Value.ToString())

                w.WriteEndElement()

            Next

            w.WriteEndElement()
            w.Close()

            strXML = ""
            strXML = sw.ToString()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

        Return strXML
    End Function

    Private Sub GuardaRelEsquemaPromocionTarifados()
        Try
            Promocion.AddRelEsquemaPromocionTarifados(convierteRelTarifadosBonificacionXML())
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub GuardaRelEsquemaMensualidadTarifados()
        Try
            Promocion.AddRelEsquemaMensualidadTarifados(convierteRelTarifadosMensualidadXML())
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub EnlazadgvRelTarifadoBonificacion_Mensualidad(ByVal idTrifado As Integer, ByVal clv_txt As String)
        Try
            If ((cbxTipoDeCobro_Mensualidad.Items.Count > 0) And (cbxServicio_Mensualidad.Items.Count > 0)) Then
                dgvRelTarifadoBonificacion_Mensualidad.DataSource = Promocion.GetRelTarifadosEsquemaMensualidadByIdTarIfado_Clv_Servicio(idTrifado, clv_txt, idEsquemaMensualidad)
                dgvRelTarifadoBonificacion_Mensualidad.Columns(3).DefaultCellStyle.Format = "c"
                dgvRelTarifadoBonificacion_Mensualidad.Columns(4).DefaultCellStyle.Format = "c"
                dgvRelTarifadoBonificacion_Mensualidad.Columns(5).DefaultCellStyle.Format = "c"
                dgvRelTarifadoBonificacion_Mensualidad.Columns(6).DefaultCellStyle.Format = "c"
                dgvRelTarifadoBonificacion_Mensualidad.Refresh()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    'Carga los serivios por tipo de servicio
    Public Sub EnlazaServiciosByClv_Tipser(ByVal Clv_TipSer As Integer)
        Try
            listaServicio = Sericio.GetAllServicioByClv_Tipser(Clv_TipSer)
            If listaServicio.Count > 0 Then

                cbxServicio.DisplayMember = "Descripcion"
                cbxServicio.ValueMember = "Clv_Txt"
                cbxServicio.DataSource = listaServicio

            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    'Carga los serivios por tipo de servicio
    Public Sub EnlazaServiciosByClv_Tipser_Mensualidad(ByVal Clv_TipSer As Integer)
        Try
            listaServicio = Sericio.GetAllServicioByClv_Tipser(Clv_TipSer)
            If listaServicio.Count > 0 Then

                cbxServicio_Mensualidad.DisplayMember = "Descripcion"
                cbxServicio_Mensualidad.ValueMember = "Clv_Txt"
                cbxServicio_Mensualidad.DataSource = listaServicio

            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Public Function ValidacionDatosPromocion() As Boolean

        Dim valido As Boolean = True

        If Len(txtNombrePromocion.Text) = 0 Then
            valido = False
        End If

        If Not (valido) Then
            MsgBox("Debes de colocar todos los campos que son marcados como obligatorios", MsgBoxStyle.Exclamation)
        End If

        Return valido
    End Function

    Public Function ValidaFechas() As Boolean
        Dim validas As Boolean = True

        If dtpFechaInicialContratacion.Value > dtpFechaFinalContratacion.Value Then
            MsgBox("La fecha inicial No puede ser superior a la fecha final", MsgBoxStyle.Exclamation)
            validas = False
        End If


        Return validas
    End Function

#End Region

    Private Sub txtImporteBonificar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtImporteBonificar.KeyPress
        ValidaDecimal(e, txtImporteBonificar)
    End Sub

    Private Sub CONTBLDETPROMOCIONES(ByVal IdPromocion As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdPromocion", SqlDbType.Int, IdPromocion)
        BaseII.CreateMyParameter("@Proporcionales", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("CONTBLDETPROMOCIONES")
        cbProporcionales.Checked = Boolean.Parse(BaseII.dicoPar("@Proporcionales").ToString)
    End Sub

    Private Sub NUETBLDETPROMOCIONES(ByVal IdPromocion As Integer, ByVal Proporcionales As Boolean)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdPromocion", SqlDbType.Int, IdPromocion)
        BaseII.CreateMyParameter("@Proporcionales", SqlDbType.Bit, Proporcionales)
        BaseII.Inserta("NUETBLDETPROMOCIONES")
    End Sub

End Class