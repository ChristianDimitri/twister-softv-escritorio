﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEsquemasDePromociones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tcEsquemasDePromociones = New System.Windows.Forms.TabControl()
        Me.tpPromocion = New System.Windows.Forms.TabPage()
        Me.btnGuardarPromocion = New System.Windows.Forms.Button()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.gbxGeneralesDePromocion = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dtpFechaFinalContratacion = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaInicialContratacion = New System.Windows.Forms.DateTimePicker()
        Me.chbxRecontratacion = New System.Windows.Forms.CheckBox()
        Me.lblFechaFinalContratacion = New System.Windows.Forms.Label()
        Me.dtpFechaModificacion = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaInicialContratacion = New System.Windows.Forms.Label()
        Me.dtpFechaCreacion = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.chbxContratacionActiva = New System.Windows.Forms.CheckBox()
        Me.chbxMensualidadActiva = New System.Windows.Forms.CheckBox()
        Me.txtNombrePromocion = New System.Windows.Forms.TextBox()
        Me.lblPromocion = New System.Windows.Forms.Label()
        Me.txtIDPromocion = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.rtbDescripcionPromocion = New System.Windows.Forms.RichTextBox()
        Me.tpEsquemaContratacion = New System.Windows.Forms.TabPage()
        Me.gbxTarifadosALosQueAplica = New System.Windows.Forms.GroupBox()
        Me.txtImporteBonificar = New System.Windows.Forms.TextBox()
        Me.lblListaDeTarifados = New System.Windows.Forms.Label()
        Me.lblAyuda01 = New System.Windows.Forms.Label()
        Me.chbxAplicaEnTodos = New System.Windows.Forms.CheckBox()
        Me.lblTipoDeCobro = New System.Windows.Forms.Label()
        Me.cbxTipoDeCobro = New System.Windows.Forms.ComboBox()
        Me.dgvRelTarifadoBonificacion = New System.Windows.Forms.DataGridView()
        Me.CLV_LLAVE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRECIO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bonificado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Rango = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Aplica = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.btnGuardarEsquemaContratacion = New System.Windows.Forms.Button()
        Me.lblImporteBonificar = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.gbxConfiguracionesPromocion_Contratacion = New System.Windows.Forms.GroupBox()
        Me.txtIDPromocion_Contratacion = New System.Windows.Forms.TextBox()
        Me.lblIDPromocion = New System.Windows.Forms.Label()
        Me.lblServicio = New System.Windows.Forms.Label()
        Me.cbxServicio = New System.Windows.Forms.ComboBox()
        Me.lblTipoServicio = New System.Windows.Forms.Label()
        Me.cbxTipoServicio = New System.Windows.Forms.ComboBox()
        Me.tpEsquemaMensualidades = New System.Windows.Forms.TabPage()
        Me.gbxConfiguracionDeMesesBonificados = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chbxIncluyeRentaDeco = New System.Windows.Forms.CheckBox()
        Me.lblTipoDeCobro_Mensualidad = New System.Windows.Forms.Label()
        Me.cbxTipoDeCobro_Mensualidad = New System.Windows.Forms.ComboBox()
        Me.lblListaDeTarifados_Mensualidad = New System.Windows.Forms.Label()
        Me.dgvRelTarifadoBonificacion_Mensualidad = New System.Windows.Forms.DataGridView()
        Me.CLV_LLAVEMensualidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_ServicioMensualidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionMensualidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRECIOMensualidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RentaAparatoMensualidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BonificadoMensualidadMensualidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BonificadoDecoMensualidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RangoMensualidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.incluyeRentaDeco = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.AplicaMensualidad = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.txtImporteTotal = New System.Windows.Forms.TextBox()
        Me.txtImportePorMes = New System.Windows.Forms.TextBox()
        Me.lblImportePorMes = New System.Windows.Forms.Label()
        Me.bntGuardarEsquemaMensualidad = New System.Windows.Forms.Button()
        Me.lblNMesesDiferidos = New System.Windows.Forms.Label()
        Me.lblImporteTotal = New System.Windows.Forms.Label()
        Me.nudMesesDiferidos = New System.Windows.Forms.NumericUpDown()
        Me.lblNMesesBonificados = New System.Windows.Forms.Label()
        Me.nudMesesBonificados = New System.Windows.Forms.NumericUpDown()
        Me.lblMoney01 = New System.Windows.Forms.Label()
        Me.lblMoney02 = New System.Windows.Forms.Label()
        Me.gbxConfiguracionesPromocion_Mensualidad = New System.Windows.Forms.GroupBox()
        Me.txtIDPromocion_Mensualidad = New System.Windows.Forms.TextBox()
        Me.lblIDPromocion_Mensualidad = New System.Windows.Forms.Label()
        Me.lblServicio_Mensualidad = New System.Windows.Forms.Label()
        Me.cbxServicio_Mensualidad = New System.Windows.Forms.ComboBox()
        Me.lblTipoServicio_Mensualidad = New System.Windows.Forms.Label()
        Me.cbxTipoServicio_Mensualidad = New System.Windows.Forms.ComboBox()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.cbProporcionales = New System.Windows.Forms.CheckBox()
        Me.tcEsquemasDePromociones.SuspendLayout()
        Me.tpPromocion.SuspendLayout()
        Me.gbxGeneralesDePromocion.SuspendLayout()
        Me.tpEsquemaContratacion.SuspendLayout()
        Me.gbxTarifadosALosQueAplica.SuspendLayout()
        CType(Me.dgvRelTarifadoBonificacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxConfiguracionesPromocion_Contratacion.SuspendLayout()
        Me.tpEsquemaMensualidades.SuspendLayout()
        Me.gbxConfiguracionDeMesesBonificados.SuspendLayout()
        CType(Me.dgvRelTarifadoBonificacion_Mensualidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMesesDiferidos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMesesBonificados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxConfiguracionesPromocion_Mensualidad.SuspendLayout()
        Me.SuspendLayout()
        '
        'tcEsquemasDePromociones
        '
        Me.tcEsquemasDePromociones.Controls.Add(Me.tpPromocion)
        Me.tcEsquemasDePromociones.Controls.Add(Me.tpEsquemaContratacion)
        Me.tcEsquemasDePromociones.Controls.Add(Me.tpEsquemaMensualidades)
        Me.tcEsquemasDePromociones.Location = New System.Drawing.Point(3, 2)
        Me.tcEsquemasDePromociones.Name = "tcEsquemasDePromociones"
        Me.tcEsquemasDePromociones.SelectedIndex = 0
        Me.tcEsquemasDePromociones.Size = New System.Drawing.Size(821, 664)
        Me.tcEsquemasDePromociones.TabIndex = 0
        '
        'tpPromocion
        '
        Me.tpPromocion.Controls.Add(Me.btnGuardarPromocion)
        Me.tpPromocion.Controls.Add(Me.lblDescripcion)
        Me.tpPromocion.Controls.Add(Me.gbxGeneralesDePromocion)
        Me.tpPromocion.Controls.Add(Me.rtbDescripcionPromocion)
        Me.tpPromocion.Location = New System.Drawing.Point(4, 27)
        Me.tpPromocion.Name = "tpPromocion"
        Me.tpPromocion.Padding = New System.Windows.Forms.Padding(3)
        Me.tpPromocion.Size = New System.Drawing.Size(813, 633)
        Me.tpPromocion.TabIndex = 2
        Me.tpPromocion.Text = "Detalles de la promoción"
        Me.tpPromocion.UseVisualStyleBackColor = True
        '
        'btnGuardarPromocion
        '
        Me.btnGuardarPromocion.BackColor = System.Drawing.Color.DarkOrange
        Me.btnGuardarPromocion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardarPromocion.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardarPromocion.ForeColor = System.Drawing.Color.Black
        Me.btnGuardarPromocion.Location = New System.Drawing.Point(692, 373)
        Me.btnGuardarPromocion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnGuardarPromocion.Name = "btnGuardarPromocion"
        Me.btnGuardarPromocion.Size = New System.Drawing.Size(113, 43)
        Me.btnGuardarPromocion.TabIndex = 158
        Me.btnGuardarPromocion.Text = "Guardar"
        Me.btnGuardarPromocion.UseVisualStyleBackColor = False
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescripcion.Location = New System.Drawing.Point(92, 412)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(177, 18)
        Me.lblDescripcion.TabIndex = 50
        Me.lblDescripcion.Text = "Descripción de la promoción:"
        Me.lblDescripcion.Visible = False
        '
        'gbxGeneralesDePromocion
        '
        Me.gbxGeneralesDePromocion.Controls.Add(Me.cbProporcionales)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.Label5)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.dtpFechaFinalContratacion)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.dtpFechaInicialContratacion)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.chbxRecontratacion)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.lblFechaFinalContratacion)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.dtpFechaModificacion)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.lblFechaInicialContratacion)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.dtpFechaCreacion)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.Label3)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.Label4)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.Label8)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.chbxContratacionActiva)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.chbxMensualidadActiva)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.txtNombrePromocion)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.lblPromocion)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.txtIDPromocion)
        Me.gbxGeneralesDePromocion.Controls.Add(Me.Label7)
        Me.gbxGeneralesDePromocion.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxGeneralesDePromocion.Location = New System.Drawing.Point(7, 7)
        Me.gbxGeneralesDePromocion.Name = "gbxGeneralesDePromocion"
        Me.gbxGeneralesDePromocion.Size = New System.Drawing.Size(798, 359)
        Me.gbxGeneralesDePromocion.TabIndex = 0
        Me.gbxGeneralesDePromocion.TabStop = False
        Me.gbxGeneralesDePromocion.Text = "Generales de la promoción"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(391, 177)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(318, 20)
        Me.Label5.TabIndex = 163
        Me.Label5.Text = "* Aplica para los Clientes que contraten en:"
        '
        'dtpFechaFinalContratacion
        '
        Me.dtpFechaFinalContratacion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFinalContratacion.Location = New System.Drawing.Point(589, 209)
        Me.dtpFechaFinalContratacion.Name = "dtpFechaFinalContratacion"
        Me.dtpFechaFinalContratacion.Size = New System.Drawing.Size(120, 23)
        Me.dtpFechaFinalContratacion.TabIndex = 162
        '
        'dtpFechaInicialContratacion
        '
        Me.dtpFechaInicialContratacion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaInicialContratacion.Location = New System.Drawing.Point(433, 210)
        Me.dtpFechaInicialContratacion.Name = "dtpFechaInicialContratacion"
        Me.dtpFechaInicialContratacion.Size = New System.Drawing.Size(120, 23)
        Me.dtpFechaInicialContratacion.TabIndex = 161
        '
        'chbxRecontratacion
        '
        Me.chbxRecontratacion.AutoSize = True
        Me.chbxRecontratacion.Location = New System.Drawing.Point(16, 241)
        Me.chbxRecontratacion.Name = "chbxRecontratacion"
        Me.chbxRecontratacion.Size = New System.Drawing.Size(120, 22)
        Me.chbxRecontratacion.TabIndex = 159
        Me.chbxRecontratacion.Text = "Recontratacion"
        Me.chbxRecontratacion.UseVisualStyleBackColor = True
        Me.chbxRecontratacion.Visible = False
        '
        'lblFechaFinalContratacion
        '
        Me.lblFechaFinalContratacion.AutoSize = True
        Me.lblFechaFinalContratacion.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaFinalContratacion.Location = New System.Drawing.Point(563, 212)
        Me.lblFechaFinalContratacion.Name = "lblFechaFinalContratacion"
        Me.lblFechaFinalContratacion.Size = New System.Drawing.Size(19, 18)
        Me.lblFechaFinalContratacion.TabIndex = 160
        Me.lblFechaFinalContratacion.Text = "al"
        '
        'dtpFechaModificacion
        '
        Me.dtpFechaModificacion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaModificacion.Location = New System.Drawing.Point(632, 53)
        Me.dtpFechaModificacion.Name = "dtpFechaModificacion"
        Me.dtpFechaModificacion.Size = New System.Drawing.Size(120, 23)
        Me.dtpFechaModificacion.TabIndex = 49
        Me.dtpFechaModificacion.Visible = False
        '
        'lblFechaInicialContratacion
        '
        Me.lblFechaInicialContratacion.AutoSize = True
        Me.lblFechaInicialContratacion.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaInicialContratacion.Location = New System.Drawing.Point(401, 213)
        Me.lblFechaInicialContratacion.Name = "lblFechaInicialContratacion"
        Me.lblFechaInicialContratacion.Size = New System.Drawing.Size(27, 18)
        Me.lblFechaInicialContratacion.TabIndex = 159
        Me.lblFechaInicialContratacion.Text = "Del"
        '
        'dtpFechaCreacion
        '
        Me.dtpFechaCreacion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaCreacion.Location = New System.Drawing.Point(435, 54)
        Me.dtpFechaCreacion.Name = "dtpFechaCreacion"
        Me.dtpFechaCreacion.Size = New System.Drawing.Size(120, 23)
        Me.dtpFechaCreacion.TabIndex = 48
        Me.dtpFechaCreacion.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(606, 33)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(162, 18)
        Me.Label3.TabIndex = 47
        Me.Label3.Text = "Fecha última modificación"
        Me.Label3.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(432, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(113, 18)
        Me.Label4.TabIndex = 46
        Me.Label4.Text = "Fecha de creación"
        Me.Label4.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(13, 177)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(105, 20)
        Me.Label8.TabIndex = 45
        Me.Label8.Text = "* Aplica para:"
        '
        'chbxContratacionActiva
        '
        Me.chbxContratacionActiva.AutoSize = True
        Me.chbxContratacionActiva.Location = New System.Drawing.Point(9, 212)
        Me.chbxContratacionActiva.Name = "chbxContratacionActiva"
        Me.chbxContratacionActiva.Size = New System.Drawing.Size(106, 22)
        Me.chbxContratacionActiva.TabIndex = 43
        Me.chbxContratacionActiva.Text = "Contratación"
        Me.chbxContratacionActiva.UseVisualStyleBackColor = True
        '
        'chbxMensualidadActiva
        '
        Me.chbxMensualidadActiva.AutoSize = True
        Me.chbxMensualidadActiva.Location = New System.Drawing.Point(138, 212)
        Me.chbxMensualidadActiva.Name = "chbxMensualidadActiva"
        Me.chbxMensualidadActiva.Size = New System.Drawing.Size(104, 22)
        Me.chbxMensualidadActiva.TabIndex = 42
        Me.chbxMensualidadActiva.Text = "Mensualidad"
        Me.chbxMensualidadActiva.UseVisualStyleBackColor = True
        '
        'txtNombrePromocion
        '
        Me.txtNombrePromocion.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombrePromocion.Location = New System.Drawing.Point(16, 117)
        Me.txtNombrePromocion.Name = "txtNombrePromocion"
        Me.txtNombrePromocion.Size = New System.Drawing.Size(765, 25)
        Me.txtNombrePromocion.TabIndex = 41
        Me.txtNombrePromocion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblPromocion
        '
        Me.lblPromocion.AutoSize = True
        Me.lblPromocion.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPromocion.Location = New System.Drawing.Point(13, 96)
        Me.lblPromocion.Name = "lblPromocion"
        Me.lblPromocion.Size = New System.Drawing.Size(164, 18)
        Me.lblPromocion.TabIndex = 40
        Me.lblPromocion.Text = "* Nombre de la promoción:"
        '
        'txtIDPromocion
        '
        Me.txtIDPromocion.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDPromocion.Location = New System.Drawing.Point(16, 53)
        Me.txtIDPromocion.Name = "txtIDPromocion"
        Me.txtIDPromocion.Size = New System.Drawing.Size(176, 26)
        Me.txtIDPromocion.TabIndex = 39
        Me.txtIDPromocion.Text = "0"
        Me.txtIDPromocion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(17, 32)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(90, 18)
        Me.Label7.TabIndex = 38
        Me.Label7.Text = "ID Promoción:"
        '
        'rtbDescripcionPromocion
        '
        Me.rtbDescripcionPromocion.Location = New System.Drawing.Point(91, 433)
        Me.rtbDescripcionPromocion.Name = "rtbDescripcionPromocion"
        Me.rtbDescripcionPromocion.Size = New System.Drawing.Size(215, 54)
        Me.rtbDescripcionPromocion.TabIndex = 44
        Me.rtbDescripcionPromocion.Text = ""
        Me.rtbDescripcionPromocion.Visible = False
        '
        'tpEsquemaContratacion
        '
        Me.tpEsquemaContratacion.Controls.Add(Me.gbxTarifadosALosQueAplica)
        Me.tpEsquemaContratacion.Controls.Add(Me.gbxConfiguracionesPromocion_Contratacion)
        Me.tpEsquemaContratacion.Location = New System.Drawing.Point(4, 27)
        Me.tpEsquemaContratacion.Name = "tpEsquemaContratacion"
        Me.tpEsquemaContratacion.Padding = New System.Windows.Forms.Padding(3)
        Me.tpEsquemaContratacion.Size = New System.Drawing.Size(813, 633)
        Me.tpEsquemaContratacion.TabIndex = 0
        Me.tpEsquemaContratacion.Text = "Esquema de contratación"
        Me.tpEsquemaContratacion.UseVisualStyleBackColor = True
        '
        'gbxTarifadosALosQueAplica
        '
        Me.gbxTarifadosALosQueAplica.Controls.Add(Me.txtImporteBonificar)
        Me.gbxTarifadosALosQueAplica.Controls.Add(Me.lblListaDeTarifados)
        Me.gbxTarifadosALosQueAplica.Controls.Add(Me.lblAyuda01)
        Me.gbxTarifadosALosQueAplica.Controls.Add(Me.chbxAplicaEnTodos)
        Me.gbxTarifadosALosQueAplica.Controls.Add(Me.lblTipoDeCobro)
        Me.gbxTarifadosALosQueAplica.Controls.Add(Me.cbxTipoDeCobro)
        Me.gbxTarifadosALosQueAplica.Controls.Add(Me.dgvRelTarifadoBonificacion)
        Me.gbxTarifadosALosQueAplica.Controls.Add(Me.btnGuardarEsquemaContratacion)
        Me.gbxTarifadosALosQueAplica.Controls.Add(Me.lblImporteBonificar)
        Me.gbxTarifadosALosQueAplica.Controls.Add(Me.Label1)
        Me.gbxTarifadosALosQueAplica.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxTarifadosALosQueAplica.Location = New System.Drawing.Point(7, 104)
        Me.gbxTarifadosALosQueAplica.Name = "gbxTarifadosALosQueAplica"
        Me.gbxTarifadosALosQueAplica.Size = New System.Drawing.Size(800, 529)
        Me.gbxTarifadosALosQueAplica.TabIndex = 162
        Me.gbxTarifadosALosQueAplica.TabStop = False
        Me.gbxTarifadosALosQueAplica.Text = "Selecciona los tarifados en los que aplica la bonificación"
        '
        'txtImporteBonificar
        '
        Me.txtImporteBonificar.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteBonificar.Location = New System.Drawing.Point(53, 48)
        Me.txtImporteBonificar.Name = "txtImporteBonificar"
        Me.txtImporteBonificar.Size = New System.Drawing.Size(147, 30)
        Me.txtImporteBonificar.TabIndex = 35
        Me.txtImporteBonificar.Text = "0.00"
        Me.txtImporteBonificar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblListaDeTarifados
        '
        Me.lblListaDeTarifados.AutoSize = True
        Me.lblListaDeTarifados.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblListaDeTarifados.Location = New System.Drawing.Point(25, 101)
        Me.lblListaDeTarifados.Name = "lblListaDeTarifados"
        Me.lblListaDeTarifados.Size = New System.Drawing.Size(328, 18)
        Me.lblListaDeTarifados.TabIndex = 165
        Me.lblListaDeTarifados.Text = "Lista de los tarifados en los que aplica la promoción"
        '
        'lblAyuda01
        '
        Me.lblAyuda01.AutoSize = True
        Me.lblAyuda01.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAyuda01.Location = New System.Drawing.Point(25, 421)
        Me.lblAyuda01.Name = "lblAyuda01"
        Me.lblAyuda01.Size = New System.Drawing.Size(319, 16)
        Me.lblAyuda01.TabIndex = 164
        Me.lblAyuda01.Text = "Selecciona los rangos que entran dentro de la promoción"
        '
        'chbxAplicaEnTodos
        '
        Me.chbxAplicaEnTodos.AutoSize = True
        Me.chbxAplicaEnTodos.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbxAplicaEnTodos.Location = New System.Drawing.Point(28, 439)
        Me.chbxAplicaEnTodos.Name = "chbxAplicaEnTodos"
        Me.chbxAplicaEnTodos.Size = New System.Drawing.Size(205, 22)
        Me.chbxAplicaEnTodos.TabIndex = 163
        Me.chbxAplicaEnTodos.Text = "Aplica para todos los tarifados"
        Me.chbxAplicaEnTodos.UseVisualStyleBackColor = True
        Me.chbxAplicaEnTodos.Visible = False
        '
        'lblTipoDeCobro
        '
        Me.lblTipoDeCobro.AutoSize = True
        Me.lblTipoDeCobro.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoDeCobro.Location = New System.Drawing.Point(613, 27)
        Me.lblTipoDeCobro.Name = "lblTipoDeCobro"
        Me.lblTipoDeCobro.Size = New System.Drawing.Size(88, 18)
        Me.lblTipoDeCobro.TabIndex = 161
        Me.lblTipoDeCobro.Text = "Tipo de cobro"
        '
        'cbxTipoDeCobro
        '
        Me.cbxTipoDeCobro.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxTipoDeCobro.FormattingEnabled = True
        Me.cbxTipoDeCobro.Location = New System.Drawing.Point(511, 48)
        Me.cbxTipoDeCobro.Name = "cbxTipoDeCobro"
        Me.cbxTipoDeCobro.Size = New System.Drawing.Size(273, 26)
        Me.cbxTipoDeCobro.TabIndex = 162
        '
        'dgvRelTarifadoBonificacion
        '
        Me.dgvRelTarifadoBonificacion.AllowUserToAddRows = False
        Me.dgvRelTarifadoBonificacion.AllowUserToDeleteRows = False
        Me.dgvRelTarifadoBonificacion.AllowUserToOrderColumns = True
        Me.dgvRelTarifadoBonificacion.AllowUserToResizeColumns = False
        Me.dgvRelTarifadoBonificacion.AllowUserToResizeRows = False
        Me.dgvRelTarifadoBonificacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRelTarifadoBonificacion.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CLV_LLAVE, Me.Clv_Servicio, Me.Descripcion, Me.PRECIO, Me.Bonificado, Me.Rango, Me.Aplica})
        Me.dgvRelTarifadoBonificacion.Location = New System.Drawing.Point(25, 122)
        Me.dgvRelTarifadoBonificacion.Name = "dgvRelTarifadoBonificacion"
        Me.dgvRelTarifadoBonificacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvRelTarifadoBonificacion.Size = New System.Drawing.Size(759, 296)
        Me.dgvRelTarifadoBonificacion.TabIndex = 0
        '
        'CLV_LLAVE
        '
        Me.CLV_LLAVE.DataPropertyName = "CLV_LLAVE"
        Me.CLV_LLAVE.HeaderText = "CLV_LLAVE"
        Me.CLV_LLAVE.Name = "CLV_LLAVE"
        Me.CLV_LLAVE.Visible = False
        '
        'Clv_Servicio
        '
        Me.Clv_Servicio.DataPropertyName = "Clv_Servicio"
        Me.Clv_Servicio.HeaderText = "Clv_Servicio"
        Me.Clv_Servicio.Name = "Clv_Servicio"
        Me.Clv_Servicio.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Servicio"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 210
        '
        'PRECIO
        '
        Me.PRECIO.DataPropertyName = "PRECIO"
        Me.PRECIO.HeaderText = "Importe"
        Me.PRECIO.Name = "PRECIO"
        Me.PRECIO.ReadOnly = True
        Me.PRECIO.Width = 105
        '
        'Bonificado
        '
        Me.Bonificado.DataPropertyName = "Bonificado"
        Me.Bonificado.HeaderText = "Importe Bonificado"
        Me.Bonificado.Name = "Bonificado"
        Me.Bonificado.ReadOnly = True
        Me.Bonificado.Width = 150
        '
        'Rango
        '
        Me.Rango.DataPropertyName = "Rango"
        Me.Rango.HeaderText = "Rango de fechas"
        Me.Rango.Name = "Rango"
        Me.Rango.ReadOnly = True
        Me.Rango.Width = 150
        '
        'Aplica
        '
        Me.Aplica.DataPropertyName = "Aplica"
        Me.Aplica.HeaderText = "Activo"
        Me.Aplica.Name = "Aplica"
        '
        'btnGuardarEsquemaContratacion
        '
        Me.btnGuardarEsquemaContratacion.BackColor = System.Drawing.Color.DarkOrange
        Me.btnGuardarEsquemaContratacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardarEsquemaContratacion.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardarEsquemaContratacion.ForeColor = System.Drawing.Color.Black
        Me.btnGuardarEsquemaContratacion.Location = New System.Drawing.Point(671, 426)
        Me.btnGuardarEsquemaContratacion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnGuardarEsquemaContratacion.Name = "btnGuardarEsquemaContratacion"
        Me.btnGuardarEsquemaContratacion.Size = New System.Drawing.Size(113, 43)
        Me.btnGuardarEsquemaContratacion.TabIndex = 160
        Me.btnGuardarEsquemaContratacion.Text = "Guardar"
        Me.btnGuardarEsquemaContratacion.UseVisualStyleBackColor = False
        '
        'lblImporteBonificar
        '
        Me.lblImporteBonificar.AutoSize = True
        Me.lblImporteBonificar.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImporteBonificar.Location = New System.Drawing.Point(51, 27)
        Me.lblImporteBonificar.Name = "lblImporteBonificar"
        Me.lblImporteBonificar.Size = New System.Drawing.Size(121, 18)
        Me.lblImporteBonificar.TabIndex = 33
        Me.lblImporteBonificar.Text = "Importe a bonificar"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(36, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 24)
        Me.Label1.TabIndex = 166
        Me.Label1.Text = "$"
        '
        'gbxConfiguracionesPromocion_Contratacion
        '
        Me.gbxConfiguracionesPromocion_Contratacion.Controls.Add(Me.txtIDPromocion_Contratacion)
        Me.gbxConfiguracionesPromocion_Contratacion.Controls.Add(Me.lblIDPromocion)
        Me.gbxConfiguracionesPromocion_Contratacion.Controls.Add(Me.lblServicio)
        Me.gbxConfiguracionesPromocion_Contratacion.Controls.Add(Me.cbxServicio)
        Me.gbxConfiguracionesPromocion_Contratacion.Controls.Add(Me.lblTipoServicio)
        Me.gbxConfiguracionesPromocion_Contratacion.Controls.Add(Me.cbxTipoServicio)
        Me.gbxConfiguracionesPromocion_Contratacion.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxConfiguracionesPromocion_Contratacion.Location = New System.Drawing.Point(6, 11)
        Me.gbxConfiguracionesPromocion_Contratacion.Name = "gbxConfiguracionesPromocion_Contratacion"
        Me.gbxConfiguracionesPromocion_Contratacion.Size = New System.Drawing.Size(804, 87)
        Me.gbxConfiguracionesPromocion_Contratacion.TabIndex = 4
        Me.gbxConfiguracionesPromocion_Contratacion.TabStop = False
        Me.gbxConfiguracionesPromocion_Contratacion.Text = "Detalle del servicio al que aplicará"
        '
        'txtIDPromocion_Contratacion
        '
        Me.txtIDPromocion_Contratacion.BackColor = System.Drawing.Color.White
        Me.txtIDPromocion_Contratacion.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDPromocion_Contratacion.Location = New System.Drawing.Point(25, 47)
        Me.txtIDPromocion_Contratacion.Name = "txtIDPromocion_Contratacion"
        Me.txtIDPromocion_Contratacion.ReadOnly = True
        Me.txtIDPromocion_Contratacion.Size = New System.Drawing.Size(176, 26)
        Me.txtIDPromocion_Contratacion.TabIndex = 37
        Me.txtIDPromocion_Contratacion.Text = "0"
        Me.txtIDPromocion_Contratacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblIDPromocion
        '
        Me.lblIDPromocion.AutoSize = True
        Me.lblIDPromocion.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIDPromocion.Location = New System.Drawing.Point(51, 26)
        Me.lblIDPromocion.Name = "lblIDPromocion"
        Me.lblIDPromocion.Size = New System.Drawing.Size(85, 18)
        Me.lblIDPromocion.TabIndex = 36
        Me.lblIDPromocion.Text = "ID Promoción"
        '
        'lblServicio
        '
        Me.lblServicio.AutoSize = True
        Me.lblServicio.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServicio.Location = New System.Drawing.Point(482, 26)
        Me.lblServicio.Name = "lblServicio"
        Me.lblServicio.Size = New System.Drawing.Size(55, 18)
        Me.lblServicio.TabIndex = 2
        Me.lblServicio.Text = "Servicio"
        '
        'cbxServicio
        '
        Me.cbxServicio.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxServicio.FormattingEnabled = True
        Me.cbxServicio.Location = New System.Drawing.Point(485, 47)
        Me.cbxServicio.Name = "cbxServicio"
        Me.cbxServicio.Size = New System.Drawing.Size(299, 26)
        Me.cbxServicio.TabIndex = 3
        '
        'lblTipoServicio
        '
        Me.lblTipoServicio.AutoSize = True
        Me.lblTipoServicio.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoServicio.Location = New System.Drawing.Point(257, 26)
        Me.lblTipoServicio.Name = "lblTipoServicio"
        Me.lblTipoServicio.Size = New System.Drawing.Size(102, 18)
        Me.lblTipoServicio.TabIndex = 0
        Me.lblTipoServicio.Text = "Tipo de servicio"
        '
        'cbxTipoServicio
        '
        Me.cbxTipoServicio.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxTipoServicio.FormattingEnabled = True
        Me.cbxTipoServicio.Location = New System.Drawing.Point(260, 47)
        Me.cbxTipoServicio.Name = "cbxTipoServicio"
        Me.cbxTipoServicio.Size = New System.Drawing.Size(219, 26)
        Me.cbxTipoServicio.TabIndex = 1
        '
        'tpEsquemaMensualidades
        '
        Me.tpEsquemaMensualidades.Controls.Add(Me.gbxConfiguracionDeMesesBonificados)
        Me.tpEsquemaMensualidades.Controls.Add(Me.gbxConfiguracionesPromocion_Mensualidad)
        Me.tpEsquemaMensualidades.Location = New System.Drawing.Point(4, 27)
        Me.tpEsquemaMensualidades.Name = "tpEsquemaMensualidades"
        Me.tpEsquemaMensualidades.Padding = New System.Windows.Forms.Padding(3)
        Me.tpEsquemaMensualidades.Size = New System.Drawing.Size(813, 633)
        Me.tpEsquemaMensualidades.TabIndex = 1
        Me.tpEsquemaMensualidades.Text = "Esquema de mensualidades"
        Me.tpEsquemaMensualidades.UseVisualStyleBackColor = True
        '
        'gbxConfiguracionDeMesesBonificados
        '
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.Label2)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.chbxIncluyeRentaDeco)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.lblTipoDeCobro_Mensualidad)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.cbxTipoDeCobro_Mensualidad)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.lblListaDeTarifados_Mensualidad)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.dgvRelTarifadoBonificacion_Mensualidad)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.txtImporteTotal)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.txtImportePorMes)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.lblImportePorMes)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.bntGuardarEsquemaMensualidad)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.lblNMesesDiferidos)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.lblImporteTotal)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.nudMesesDiferidos)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.lblNMesesBonificados)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.nudMesesBonificados)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.lblMoney01)
        Me.gbxConfiguracionDeMesesBonificados.Controls.Add(Me.lblMoney02)
        Me.gbxConfiguracionDeMesesBonificados.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxConfiguracionDeMesesBonificados.Location = New System.Drawing.Point(6, 100)
        Me.gbxConfiguracionDeMesesBonificados.Name = "gbxConfiguracionDeMesesBonificados"
        Me.gbxConfiguracionDeMesesBonificados.Size = New System.Drawing.Size(803, 530)
        Me.gbxConfiguracionDeMesesBonificados.TabIndex = 163
        Me.gbxConfiguracionDeMesesBonificados.TabStop = False
        Me.gbxConfiguracionDeMesesBonificados.Text = "Configura los meses a bonificar y los meses en que se diferirá el descuento"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(22, 469)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(319, 16)
        Me.Label2.TabIndex = 171
        Me.Label2.Text = "Selecciona los rangos que entran dentro de la promoción"
        '
        'chbxIncluyeRentaDeco
        '
        Me.chbxIncluyeRentaDeco.AutoSize = True
        Me.chbxIncluyeRentaDeco.Checked = True
        Me.chbxIncluyeRentaDeco.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chbxIncluyeRentaDeco.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbxIncluyeRentaDeco.Location = New System.Drawing.Point(25, 94)
        Me.chbxIncluyeRentaDeco.Name = "chbxIncluyeRentaDeco"
        Me.chbxIncluyeRentaDeco.Size = New System.Drawing.Size(214, 22)
        Me.chbxIncluyeRentaDeco.TabIndex = 170
        Me.chbxIncluyeRentaDeco.Text = "Incluye renta de decodificador"
        Me.chbxIncluyeRentaDeco.UseVisualStyleBackColor = True
        Me.chbxIncluyeRentaDeco.Visible = False
        '
        'lblTipoDeCobro_Mensualidad
        '
        Me.lblTipoDeCobro_Mensualidad.AutoSize = True
        Me.lblTipoDeCobro_Mensualidad.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoDeCobro_Mensualidad.Location = New System.Drawing.Point(613, 34)
        Me.lblTipoDeCobro_Mensualidad.Name = "lblTipoDeCobro_Mensualidad"
        Me.lblTipoDeCobro_Mensualidad.Size = New System.Drawing.Size(88, 18)
        Me.lblTipoDeCobro_Mensualidad.TabIndex = 168
        Me.lblTipoDeCobro_Mensualidad.Text = "Tipo de cobro"
        '
        'cbxTipoDeCobro_Mensualidad
        '
        Me.cbxTipoDeCobro_Mensualidad.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxTipoDeCobro_Mensualidad.FormattingEnabled = True
        Me.cbxTipoDeCobro_Mensualidad.Location = New System.Drawing.Point(511, 55)
        Me.cbxTipoDeCobro_Mensualidad.Name = "cbxTipoDeCobro_Mensualidad"
        Me.cbxTipoDeCobro_Mensualidad.Size = New System.Drawing.Size(273, 26)
        Me.cbxTipoDeCobro_Mensualidad.TabIndex = 169
        '
        'lblListaDeTarifados_Mensualidad
        '
        Me.lblListaDeTarifados_Mensualidad.AutoSize = True
        Me.lblListaDeTarifados_Mensualidad.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblListaDeTarifados_Mensualidad.Location = New System.Drawing.Point(456, 107)
        Me.lblListaDeTarifados_Mensualidad.Name = "lblListaDeTarifados_Mensualidad"
        Me.lblListaDeTarifados_Mensualidad.Size = New System.Drawing.Size(328, 18)
        Me.lblListaDeTarifados_Mensualidad.TabIndex = 167
        Me.lblListaDeTarifados_Mensualidad.Text = "Lista de los tarifados en los que aplica la promoción"
        '
        'dgvRelTarifadoBonificacion_Mensualidad
        '
        Me.dgvRelTarifadoBonificacion_Mensualidad.AllowUserToAddRows = False
        Me.dgvRelTarifadoBonificacion_Mensualidad.AllowUserToDeleteRows = False
        Me.dgvRelTarifadoBonificacion_Mensualidad.AllowUserToOrderColumns = True
        Me.dgvRelTarifadoBonificacion_Mensualidad.AllowUserToResizeColumns = False
        Me.dgvRelTarifadoBonificacion_Mensualidad.AllowUserToResizeRows = False
        Me.dgvRelTarifadoBonificacion_Mensualidad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRelTarifadoBonificacion_Mensualidad.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CLV_LLAVEMensualidad, Me.Clv_ServicioMensualidad, Me.DescripcionMensualidad, Me.PRECIOMensualidad, Me.RentaAparatoMensualidad, Me.BonificadoMensualidadMensualidad, Me.BonificadoDecoMensualidad, Me.RangoMensualidad, Me.incluyeRentaDeco, Me.AplicaMensualidad})
        Me.dgvRelTarifadoBonificacion_Mensualidad.Location = New System.Drawing.Point(25, 128)
        Me.dgvRelTarifadoBonificacion_Mensualidad.Name = "dgvRelTarifadoBonificacion_Mensualidad"
        Me.dgvRelTarifadoBonificacion_Mensualidad.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvRelTarifadoBonificacion_Mensualidad.Size = New System.Drawing.Size(759, 338)
        Me.dgvRelTarifadoBonificacion_Mensualidad.TabIndex = 166
        '
        'CLV_LLAVEMensualidad
        '
        Me.CLV_LLAVEMensualidad.DataPropertyName = "CLV_LLAVEMensualidad"
        Me.CLV_LLAVEMensualidad.HeaderText = "CLV_LLAVEMensualidad"
        Me.CLV_LLAVEMensualidad.Name = "CLV_LLAVEMensualidad"
        Me.CLV_LLAVEMensualidad.ReadOnly = True
        Me.CLV_LLAVEMensualidad.Visible = False
        '
        'Clv_ServicioMensualidad
        '
        Me.Clv_ServicioMensualidad.DataPropertyName = "Clv_ServicioMensualidad"
        Me.Clv_ServicioMensualidad.HeaderText = "Clv_ServicioMensualidad"
        Me.Clv_ServicioMensualidad.Name = "Clv_ServicioMensualidad"
        Me.Clv_ServicioMensualidad.ReadOnly = True
        Me.Clv_ServicioMensualidad.Visible = False
        '
        'DescripcionMensualidad
        '
        Me.DescripcionMensualidad.DataPropertyName = "DescripcionMensualidad"
        Me.DescripcionMensualidad.HeaderText = "Servicio"
        Me.DescripcionMensualidad.Name = "DescripcionMensualidad"
        Me.DescripcionMensualidad.ReadOnly = True
        Me.DescripcionMensualidad.Width = 220
        '
        'PRECIOMensualidad
        '
        Me.PRECIOMensualidad.DataPropertyName = "PRECIOMensualidad"
        Me.PRECIOMensualidad.HeaderText = "Costo mensual servicio"
        Me.PRECIOMensualidad.Name = "PRECIOMensualidad"
        Me.PRECIOMensualidad.ReadOnly = True
        Me.PRECIOMensualidad.Width = 150
        '
        'RentaAparatoMensualidad
        '
        Me.RentaAparatoMensualidad.DataPropertyName = "RentaAparatoMensualidad"
        Me.RentaAparatoMensualidad.HeaderText = "Costo renta del aparato"
        Me.RentaAparatoMensualidad.Name = "RentaAparatoMensualidad"
        Me.RentaAparatoMensualidad.ReadOnly = True
        Me.RentaAparatoMensualidad.Width = 200
        '
        'BonificadoMensualidadMensualidad
        '
        Me.BonificadoMensualidadMensualidad.DataPropertyName = "BonificadoMensualidadMensualidad"
        Me.BonificadoMensualidadMensualidad.HeaderText = "Bonificación a la mensualidad (por mes)"
        Me.BonificadoMensualidadMensualidad.Name = "BonificadoMensualidadMensualidad"
        Me.BonificadoMensualidadMensualidad.ReadOnly = True
        Me.BonificadoMensualidadMensualidad.Width = 200
        '
        'BonificadoDecoMensualidad
        '
        Me.BonificadoDecoMensualidad.DataPropertyName = "BonificadoDecoMensualidad"
        Me.BonificadoDecoMensualidad.HeaderText = "Bonificación al aparato (por mes)"
        Me.BonificadoDecoMensualidad.Name = "BonificadoDecoMensualidad"
        Me.BonificadoDecoMensualidad.ReadOnly = True
        Me.BonificadoDecoMensualidad.Width = 200
        '
        'RangoMensualidad
        '
        Me.RangoMensualidad.DataPropertyName = "RangoMensualidad"
        Me.RangoMensualidad.HeaderText = "Rango de fechas"
        Me.RangoMensualidad.Name = "RangoMensualidad"
        Me.RangoMensualidad.ReadOnly = True
        '
        'incluyeRentaDeco
        '
        Me.incluyeRentaDeco.DataPropertyName = "incluyeRentaDeco"
        Me.incluyeRentaDeco.HeaderText = "Incluye renta de aparato"
        Me.incluyeRentaDeco.Name = "incluyeRentaDeco"
        Me.incluyeRentaDeco.Width = 180
        '
        'AplicaMensualidad
        '
        Me.AplicaMensualidad.DataPropertyName = "AplicaMensualidad"
        Me.AplicaMensualidad.HeaderText = "Activo"
        Me.AplicaMensualidad.Name = "AplicaMensualidad"
        Me.AplicaMensualidad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.AplicaMensualidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'txtImporteTotal
        '
        Me.txtImporteTotal.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteTotal.Location = New System.Drawing.Point(567, 233)
        Me.txtImporteTotal.Name = "txtImporteTotal"
        Me.txtImporteTotal.Size = New System.Drawing.Size(157, 30)
        Me.txtImporteTotal.TabIndex = 31
        Me.txtImporteTotal.Text = "0.00"
        Me.txtImporteTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtImporteTotal.Visible = False
        '
        'txtImportePorMes
        '
        Me.txtImportePorMes.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImportePorMes.Location = New System.Drawing.Point(561, 233)
        Me.txtImportePorMes.Name = "txtImportePorMes"
        Me.txtImportePorMes.Size = New System.Drawing.Size(163, 30)
        Me.txtImportePorMes.TabIndex = 30
        Me.txtImportePorMes.Text = "0.00"
        Me.txtImportePorMes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtImportePorMes.Visible = False
        '
        'lblImportePorMes
        '
        Me.lblImportePorMes.AutoSize = True
        Me.lblImportePorMes.Location = New System.Drawing.Point(558, 212)
        Me.lblImportePorMes.Name = "lblImportePorMes"
        Me.lblImportePorMes.Size = New System.Drawing.Size(180, 18)
        Me.lblImportePorMes.TabIndex = 28
        Me.lblImportePorMes.Text = "Importe a bonificar por mes"
        Me.lblImportePorMes.Visible = False
        '
        'bntGuardarEsquemaMensualidad
        '
        Me.bntGuardarEsquemaMensualidad.BackColor = System.Drawing.Color.DarkOrange
        Me.bntGuardarEsquemaMensualidad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bntGuardarEsquemaMensualidad.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bntGuardarEsquemaMensualidad.ForeColor = System.Drawing.Color.Black
        Me.bntGuardarEsquemaMensualidad.Location = New System.Drawing.Point(671, 474)
        Me.bntGuardarEsquemaMensualidad.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.bntGuardarEsquemaMensualidad.Name = "bntGuardarEsquemaMensualidad"
        Me.bntGuardarEsquemaMensualidad.Size = New System.Drawing.Size(113, 43)
        Me.bntGuardarEsquemaMensualidad.TabIndex = 160
        Me.bntGuardarEsquemaMensualidad.Text = "Guardar"
        Me.bntGuardarEsquemaMensualidad.UseVisualStyleBackColor = False
        '
        'lblNMesesDiferidos
        '
        Me.lblNMesesDiferidos.AutoSize = True
        Me.lblNMesesDiferidos.Location = New System.Drawing.Point(155, 34)
        Me.lblNMesesDiferidos.Name = "lblNMesesDiferidos"
        Me.lblNMesesDiferidos.Size = New System.Drawing.Size(103, 18)
        Me.lblNMesesDiferidos.TabIndex = 33
        Me.lblNMesesDiferidos.Text = "Meses diferidos"
        '
        'lblImporteTotal
        '
        Me.lblImporteTotal.AutoSize = True
        Me.lblImporteTotal.Location = New System.Drawing.Point(559, 237)
        Me.lblImporteTotal.Name = "lblImporteTotal"
        Me.lblImporteTotal.Size = New System.Drawing.Size(159, 18)
        Me.lblImporteTotal.TabIndex = 29
        Me.lblImporteTotal.Text = "Importe total a bonificar"
        Me.lblImporteTotal.Visible = False
        '
        'nudMesesDiferidos
        '
        Me.nudMesesDiferidos.BackColor = System.Drawing.Color.White
        Me.nudMesesDiferidos.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMesesDiferidos.Location = New System.Drawing.Point(153, 55)
        Me.nudMesesDiferidos.Maximum = New Decimal(New Integer() {600, 0, 0, 0})
        Me.nudMesesDiferidos.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudMesesDiferidos.Name = "nudMesesDiferidos"
        Me.nudMesesDiferidos.ReadOnly = True
        Me.nudMesesDiferidos.Size = New System.Drawing.Size(111, 26)
        Me.nudMesesDiferidos.TabIndex = 27
        Me.nudMesesDiferidos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudMesesDiferidos.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'lblNMesesBonificados
        '
        Me.lblNMesesBonificados.AutoSize = True
        Me.lblNMesesBonificados.Location = New System.Drawing.Point(22, 34)
        Me.lblNMesesBonificados.Name = "lblNMesesBonificados"
        Me.lblNMesesBonificados.Size = New System.Drawing.Size(119, 18)
        Me.lblNMesesBonificados.TabIndex = 32
        Me.lblNMesesBonificados.Text = "Meses bonificados"
        '
        'nudMesesBonificados
        '
        Me.nudMesesBonificados.BackColor = System.Drawing.Color.White
        Me.nudMesesBonificados.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMesesBonificados.Location = New System.Drawing.Point(25, 55)
        Me.nudMesesBonificados.Maximum = New Decimal(New Integer() {600, 0, 0, 0})
        Me.nudMesesBonificados.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudMesesBonificados.Name = "nudMesesBonificados"
        Me.nudMesesBonificados.ReadOnly = True
        Me.nudMesesBonificados.Size = New System.Drawing.Size(111, 26)
        Me.nudMesesBonificados.TabIndex = 26
        Me.nudMesesBonificados.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudMesesBonificados.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblMoney01
        '
        Me.lblMoney01.AutoSize = True
        Me.lblMoney01.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoney01.Location = New System.Drawing.Point(540, 233)
        Me.lblMoney01.Name = "lblMoney01"
        Me.lblMoney01.Size = New System.Drawing.Size(21, 24)
        Me.lblMoney01.TabIndex = 161
        Me.lblMoney01.Text = "$"
        Me.lblMoney01.Visible = False
        '
        'lblMoney02
        '
        Me.lblMoney02.AutoSize = True
        Me.lblMoney02.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoney02.Location = New System.Drawing.Point(538, 233)
        Me.lblMoney02.Name = "lblMoney02"
        Me.lblMoney02.Size = New System.Drawing.Size(21, 24)
        Me.lblMoney02.TabIndex = 162
        Me.lblMoney02.Text = "$"
        Me.lblMoney02.Visible = False
        '
        'gbxConfiguracionesPromocion_Mensualidad
        '
        Me.gbxConfiguracionesPromocion_Mensualidad.Controls.Add(Me.txtIDPromocion_Mensualidad)
        Me.gbxConfiguracionesPromocion_Mensualidad.Controls.Add(Me.lblIDPromocion_Mensualidad)
        Me.gbxConfiguracionesPromocion_Mensualidad.Controls.Add(Me.lblServicio_Mensualidad)
        Me.gbxConfiguracionesPromocion_Mensualidad.Controls.Add(Me.cbxServicio_Mensualidad)
        Me.gbxConfiguracionesPromocion_Mensualidad.Controls.Add(Me.lblTipoServicio_Mensualidad)
        Me.gbxConfiguracionesPromocion_Mensualidad.Controls.Add(Me.cbxTipoServicio_Mensualidad)
        Me.gbxConfiguracionesPromocion_Mensualidad.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxConfiguracionesPromocion_Mensualidad.Location = New System.Drawing.Point(6, 11)
        Me.gbxConfiguracionesPromocion_Mensualidad.Name = "gbxConfiguracionesPromocion_Mensualidad"
        Me.gbxConfiguracionesPromocion_Mensualidad.Size = New System.Drawing.Size(804, 83)
        Me.gbxConfiguracionesPromocion_Mensualidad.TabIndex = 34
        Me.gbxConfiguracionesPromocion_Mensualidad.TabStop = False
        Me.gbxConfiguracionesPromocion_Mensualidad.Text = "Detalle del servicio al que aplicará"
        '
        'txtIDPromocion_Mensualidad
        '
        Me.txtIDPromocion_Mensualidad.BackColor = System.Drawing.Color.White
        Me.txtIDPromocion_Mensualidad.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDPromocion_Mensualidad.Location = New System.Drawing.Point(25, 47)
        Me.txtIDPromocion_Mensualidad.Name = "txtIDPromocion_Mensualidad"
        Me.txtIDPromocion_Mensualidad.ReadOnly = True
        Me.txtIDPromocion_Mensualidad.Size = New System.Drawing.Size(176, 26)
        Me.txtIDPromocion_Mensualidad.TabIndex = 37
        Me.txtIDPromocion_Mensualidad.Text = "0"
        Me.txtIDPromocion_Mensualidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblIDPromocion_Mensualidad
        '
        Me.lblIDPromocion_Mensualidad.AutoSize = True
        Me.lblIDPromocion_Mensualidad.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIDPromocion_Mensualidad.Location = New System.Drawing.Point(51, 26)
        Me.lblIDPromocion_Mensualidad.Name = "lblIDPromocion_Mensualidad"
        Me.lblIDPromocion_Mensualidad.Size = New System.Drawing.Size(85, 18)
        Me.lblIDPromocion_Mensualidad.TabIndex = 36
        Me.lblIDPromocion_Mensualidad.Text = "ID Promoción"
        '
        'lblServicio_Mensualidad
        '
        Me.lblServicio_Mensualidad.AutoSize = True
        Me.lblServicio_Mensualidad.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServicio_Mensualidad.Location = New System.Drawing.Point(482, 26)
        Me.lblServicio_Mensualidad.Name = "lblServicio_Mensualidad"
        Me.lblServicio_Mensualidad.Size = New System.Drawing.Size(55, 18)
        Me.lblServicio_Mensualidad.TabIndex = 2
        Me.lblServicio_Mensualidad.Text = "Servicio"
        '
        'cbxServicio_Mensualidad
        '
        Me.cbxServicio_Mensualidad.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxServicio_Mensualidad.FormattingEnabled = True
        Me.cbxServicio_Mensualidad.Location = New System.Drawing.Point(485, 47)
        Me.cbxServicio_Mensualidad.Name = "cbxServicio_Mensualidad"
        Me.cbxServicio_Mensualidad.Size = New System.Drawing.Size(299, 26)
        Me.cbxServicio_Mensualidad.TabIndex = 3
        '
        'lblTipoServicio_Mensualidad
        '
        Me.lblTipoServicio_Mensualidad.AutoSize = True
        Me.lblTipoServicio_Mensualidad.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoServicio_Mensualidad.Location = New System.Drawing.Point(257, 26)
        Me.lblTipoServicio_Mensualidad.Name = "lblTipoServicio_Mensualidad"
        Me.lblTipoServicio_Mensualidad.Size = New System.Drawing.Size(102, 18)
        Me.lblTipoServicio_Mensualidad.TabIndex = 0
        Me.lblTipoServicio_Mensualidad.Text = "Tipo de servicio"
        '
        'cbxTipoServicio_Mensualidad
        '
        Me.cbxTipoServicio_Mensualidad.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxTipoServicio_Mensualidad.FormattingEnabled = True
        Me.cbxTipoServicio_Mensualidad.Location = New System.Drawing.Point(260, 47)
        Me.cbxTipoServicio_Mensualidad.Name = "cbxTipoServicio_Mensualidad"
        Me.cbxTipoServicio_Mensualidad.Size = New System.Drawing.Size(219, 26)
        Me.cbxTipoServicio_Mensualidad.TabIndex = 1
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(684, 672)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(113, 43)
        Me.btnSalir.TabIndex = 157
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'cbProporcionales
        '
        Me.cbProporcionales.AutoSize = True
        Me.cbProporcionales.Location = New System.Drawing.Point(266, 212)
        Me.cbProporcionales.Name = "cbProporcionales"
        Me.cbProporcionales.Size = New System.Drawing.Size(119, 22)
        Me.cbProporcionales.TabIndex = 164
        Me.cbProporcionales.Text = "Proporcionales"
        Me.cbProporcionales.UseVisualStyleBackColor = True
        '
        'FrmEsquemasDePromociones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(824, 722)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.tcEsquemasDePromociones)
        Me.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmEsquemasDePromociones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Promociones"
        Me.tcEsquemasDePromociones.ResumeLayout(False)
        Me.tpPromocion.ResumeLayout(False)
        Me.tpPromocion.PerformLayout()
        Me.gbxGeneralesDePromocion.ResumeLayout(False)
        Me.gbxGeneralesDePromocion.PerformLayout()
        Me.tpEsquemaContratacion.ResumeLayout(False)
        Me.gbxTarifadosALosQueAplica.ResumeLayout(False)
        Me.gbxTarifadosALosQueAplica.PerformLayout()
        CType(Me.dgvRelTarifadoBonificacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxConfiguracionesPromocion_Contratacion.ResumeLayout(False)
        Me.gbxConfiguracionesPromocion_Contratacion.PerformLayout()
        Me.tpEsquemaMensualidades.ResumeLayout(False)
        Me.gbxConfiguracionDeMesesBonificados.ResumeLayout(False)
        Me.gbxConfiguracionDeMesesBonificados.PerformLayout()
        CType(Me.dgvRelTarifadoBonificacion_Mensualidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMesesDiferidos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMesesBonificados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxConfiguracionesPromocion_Mensualidad.ResumeLayout(False)
        Me.gbxConfiguracionesPromocion_Mensualidad.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tcEsquemasDePromociones As System.Windows.Forms.TabControl
    Friend WithEvents tpEsquemaContratacion As System.Windows.Forms.TabPage
    Friend WithEvents tpEsquemaMensualidades As System.Windows.Forms.TabPage
    Friend WithEvents btnGuardarPromocion As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents txtImporteTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtImportePorMes As System.Windows.Forms.TextBox
    Friend WithEvents lblImporteTotal As System.Windows.Forms.Label
    Friend WithEvents lblImportePorMes As System.Windows.Forms.Label
    Friend WithEvents lblNMesesDiferidos As System.Windows.Forms.Label
    Friend WithEvents lblNMesesBonificados As System.Windows.Forms.Label
    Friend WithEvents nudMesesDiferidos As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudMesesBonificados As System.Windows.Forms.NumericUpDown
    Friend WithEvents gbxConfiguracionesPromocion_Contratacion As System.Windows.Forms.GroupBox
    Friend WithEvents txtIDPromocion_Contratacion As System.Windows.Forms.TextBox
    Friend WithEvents lblIDPromocion As System.Windows.Forms.Label
    Friend WithEvents lblServicio As System.Windows.Forms.Label
    Friend WithEvents cbxServicio As System.Windows.Forms.ComboBox
    Friend WithEvents lblTipoServicio As System.Windows.Forms.Label
    Friend WithEvents cbxTipoServicio As System.Windows.Forms.ComboBox
    Friend WithEvents gbxTarifadosALosQueAplica As System.Windows.Forms.GroupBox
    Friend WithEvents lblAyuda01 As System.Windows.Forms.Label
    Friend WithEvents chbxAplicaEnTodos As System.Windows.Forms.CheckBox
    Friend WithEvents lblTipoDeCobro As System.Windows.Forms.Label
    Friend WithEvents cbxTipoDeCobro As System.Windows.Forms.ComboBox
    Friend WithEvents dgvRelTarifadoBonificacion As System.Windows.Forms.DataGridView
    Friend WithEvents txtImporteBonificar As System.Windows.Forms.TextBox
    Friend WithEvents btnGuardarEsquemaContratacion As System.Windows.Forms.Button
    Friend WithEvents lblImporteBonificar As System.Windows.Forms.Label
    Friend WithEvents tpPromocion As System.Windows.Forms.TabPage
    Friend WithEvents gbxGeneralesDePromocion As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents rtbDescripcionPromocion As System.Windows.Forms.RichTextBox
    Friend WithEvents chbxContratacionActiva As System.Windows.Forms.CheckBox
    Friend WithEvents chbxMensualidadActiva As System.Windows.Forms.CheckBox
    Friend WithEvents txtNombrePromocion As System.Windows.Forms.TextBox
    Friend WithEvents lblPromocion As System.Windows.Forms.Label
    Friend WithEvents txtIDPromocion As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents dtpFechaModificacion As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaCreacion As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents gbxConfiguracionesPromocion_Mensualidad As System.Windows.Forms.GroupBox
    Friend WithEvents txtIDPromocion_Mensualidad As System.Windows.Forms.TextBox
    Friend WithEvents lblIDPromocion_Mensualidad As System.Windows.Forms.Label
    Friend WithEvents lblServicio_Mensualidad As System.Windows.Forms.Label
    Friend WithEvents cbxServicio_Mensualidad As System.Windows.Forms.ComboBox
    Friend WithEvents lblTipoServicio_Mensualidad As System.Windows.Forms.Label
    Friend WithEvents cbxTipoServicio_Mensualidad As System.Windows.Forms.ComboBox
    Friend WithEvents gbxConfiguracionDeMesesBonificados As System.Windows.Forms.GroupBox
    Friend WithEvents bntGuardarEsquemaMensualidad As System.Windows.Forms.Button
    Friend WithEvents chbxRecontratacion As System.Windows.Forms.CheckBox
    Friend WithEvents lblListaDeTarifados As System.Windows.Forms.Label
    Friend WithEvents lblMoney01 As System.Windows.Forms.Label
    Friend WithEvents lblMoney02 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblListaDeTarifados_Mensualidad As System.Windows.Forms.Label
    Friend WithEvents dgvRelTarifadoBonificacion_Mensualidad As System.Windows.Forms.DataGridView
    Friend WithEvents lblTipoDeCobro_Mensualidad As System.Windows.Forms.Label
    Friend WithEvents cbxTipoDeCobro_Mensualidad As System.Windows.Forms.ComboBox
    Friend WithEvents chbxIncluyeRentaDeco As System.Windows.Forms.CheckBox
    Friend WithEvents CLV_LLAVEMensualidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_ServicioMensualidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionMensualidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRECIOMensualidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RentaAparatoMensualidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BonificadoMensualidadMensualidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BonificadoDecoMensualidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RangoMensualidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents incluyeRentaDeco As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents AplicaMensualidad As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents CLV_LLAVE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRECIO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Bonificado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Rango As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Aplica As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaFinalContratacion As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaInicialContratacion As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFechaFinalContratacion As System.Windows.Forms.Label
    Friend WithEvents lblFechaInicialContratacion As System.Windows.Forms.Label
    Friend WithEvents cbProporcionales As System.Windows.Forms.CheckBox
End Class
