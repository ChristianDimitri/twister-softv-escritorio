﻿Imports sofTV.BAL
Imports System.Collections.Generic
Public Class BrwPromocionesN
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private listaPromociones As New List(Of Promociones)
   

#Region "Controles y eventos"

    'BrwPromociones_Load
    Private Sub BrwPromociones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        EnlazaControles()

    End Sub

    'btnBuscar_Click
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try
            Dim p As New Promociones(), filtroId As Integer, filtroNombre As String

            If Len(txtFiltroID.Text) > 0 Then
                filtroId = txtFiltroID.Text
            Else
                filtroId = 0
            End If

            If Len(txtFiltroNombre.Text) > 0 Then
                filtroNombre = txtFiltroNombre.Text
            Else
                filtroNombre = ""
            End If
            listaPromociones.Clear()
            listaPromociones = p.GetAllByFilters(filtroId, filtroNombre)

            dgvPromociones.DataSource = listaPromociones

            If dgvPromociones.RowCount > 0 Then
                HabilitaBotones(True)
            Else
                HabilitaBotones(False)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
       

    End Sub

    'btnNuevo_Click
    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Try
            Dim esquemaPromociones As New FrmEsquemasDePromociones()
            esquemaPromociones.ShowDialog()
            EnlazaControles()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    'btnModificar_Click
    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Try
            Dim esquemaPromociones As New FrmEsquemasDePromociones()

            esquemaPromociones.idPromocion = dgvPromociones.CurrentRow.Cells(0).Value
            esquemaPromociones.Promocion = listaPromociones.Item(dgvPromociones.CurrentRow.Index)

            esquemaPromociones.ShowDialog()
            EnlazaControles()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    'btnEliminar_Click
    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        If (MsgBox("Deseas eliminar permanentemente la promoción", MsgBoxStyle.YesNo) = DialogResult.Yes) Then
            Try
                Dim p As New Promociones()
                p.Delete(dgvPromociones.SelectedCells(0).Value)
                MsgBox("Se ha eliminado correctamente", MsgBoxStyle.Information)
                EnlazaControles()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try
        End If
    End Sub

    'btnSalir_Click
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    

#End Region

#Region "Funciones y métodos"

    'EnlazaControles()
    Private Sub EnlazaControles()
        Try
            Dim p As New Promociones()
            listaPromociones = p.GetAll()
            dgvPromociones.DataSource = listaPromociones

            If dgvPromociones.RowCount > 0 Then
                HabilitaBotones(True)
            Else
                HabilitaBotones(False)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    'HabilitaBotones()
    Private Sub HabilitaBotones(ByVal habilitar As Boolean)

        btnModificar.Enabled = habilitar
        btnEliminar.Enabled = habilitar
        
    End Sub

#End Region



    

End Class