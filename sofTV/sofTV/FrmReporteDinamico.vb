Imports CrystalDecisions
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportAppServer
Imports System.Data.SqlClient

Public Class FrmReporteDinamico

    Private customersByCityReport As ReportDocument

    Private Sub ConfigureCrystalReports(ByVal op As String, ByVal Titulo As String)
        Try
            If GloClv_TipSer > 0 And GLOClv_Cartera > 0 Then
                customersByCityReport = New ReportDocument
                Dim connectionInfo As New ConnectionInfo
                '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
                '    "=True;User ID=DeSistema;Password=1975huli")

                connectionInfo.ServerName = GloServerName
                connectionInfo.DatabaseName = GloDatabaseName
                connectionInfo.UserID = GloUserID
                connectionInfo.Password = GloPassword

                Dim mySelectFormula As String = Titulo
                'customersByCityReport.ReportAppServer. 
                'Dim OpOrdenar As String = "0"
                'If Me.RadioButton1.Checked = True Then
                '    OpOrdenar = "0"
                'ElseIf Me.RadioButton2.Checked = True Then
                '    OpOrdenar = "1"
                'ElseIf Me.RadioButton3.Checked = True Then
                '    OpOrdenar = "2"
                'End If

                'Dim reportPath As String = nothing
                'Select Case op
                '    Case 4
                'reportPath = Application.StartupPath + "\Reportes\" + "ValorCarteras.rpt"
                '    Case Else
                'reportPath = Application.StartupPath + "\Reportes\" + "Carteras.rpt"
                'End Select
                
                SetDBLogonForReport(connectionInfo, customersByCityReport)


                CrystalReportViewer1.ReportSource = customersByCityReport
                SetDBLogonForReport2(connectionInfo)
                customersByCityReport = Nothing
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    Private Sub FrmReporteDinamico_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub
End Class