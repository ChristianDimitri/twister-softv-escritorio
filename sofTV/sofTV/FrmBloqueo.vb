Imports System.Data.SqlClient
Public Class FrmBloqueo
    Dim cambia As Integer = 0
    Private Sub FrmBloqueo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Timer1.Enabled = True
        Dim cone As New SqlClient.SqlConnection(MiConexion)
        cone.Open()
        Me.Consulta_ClientesBloqTableAdapter.Connection = cone
        Me.Consulta_ClientesBloqTableAdapter.Fill(Me.DataSetLidia.Consulta_ClientesBloq, eGloContrato)
        cone.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If cambia < 2 Then
            Me.Panel1.BackColor = Color.Yellow
            cambia = cambia + 1
        ElseIf cambia >= 2 Then
            Me.Panel1.BackColor = Color.WhiteSmoke
            cambia = cambia + 1
            If cambia = 5 Then
                cambia = 0
            End If
        End If
    End Sub
End Class