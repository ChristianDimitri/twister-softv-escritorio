﻿Imports System.Data.SqlClient

Public Class FrmSelSector

    Private Sub MueveSeleccion_TAbla(ByVal clv_Session As Long, ByVal clv_id As Integer, ByVal op As Integer)
        Dim cmd As New SqlClient.SqlCommand
        Dim CON4 As New SqlConnection(MiConexion)
        Try
            If IsNumeric(clv_Session) = False Then clv_Session = 0
            If clv_Session > 0 Then

                CON4.Open()
                With cmd
                    .CommandText = "MUEVE_Selecciona"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = CON4
                    Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_Periodo", SqlDbType.Int)
                    Dim prm3 As New SqlParameter("@op", SqlDbType.Int)
                    prm.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm.Value = clv_Session
                    prm2.Value = clv_id
                    prm3.Value = op
                    .Parameters.Add(prm)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    Dim i As Integer = .ExecuteNonQuery
                End With
                CON4.Close()
            End If


        Catch ex As System.Exception
            If CON4.State <> ConnectionState.Closed Then CON4.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Inicializa_Seleccion_TAbla(ByVal clv_Session As Long, ByVal query As String)
        Dim cmd As New SqlClient.SqlCommand
        Dim CON4 As New SqlConnection(MiConexion)
        Try
            If IsNumeric(clv_Session) = False Then clv_Session = 0
            If clv_Session > 0 Then

                CON4.Open()
                With cmd
                    .CommandText = "Llena_Tabla_Selecciona1"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = CON4
                    Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@QueryTablaOrigen", SqlDbType.VarChar, 250)
                    prm.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm.Value = clv_Session
                    prm2.Value = query
                    .Parameters.Add(prm)
                    .Parameters.Add(prm2)
                    Dim i As Integer = .ExecuteNonQuery
                End With
                CON4.Close()
            End If


        Catch ex As System.Exception
            If CON4.State <> ConnectionState.Closed Then CON4.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Guarda_Seleccion_TAbla(ByVal clv_Session As Long, ByVal Tabla As String)
        Dim cmd As New SqlClient.SqlCommand
        Dim CON4 As New SqlConnection(MiConexion)
        Try
            If IsNumeric(clv_Session) = False Then clv_Session = 0
            If clv_Session > 0 Then

                CON4.Open()
                With cmd
                    .CommandText = "GUARDA_TABLA_Selecciona"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = CON4
                    Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Tabla_Guardar", SqlDbType.VarChar, 150)
                    prm.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm.Value = clv_Session
                    prm2.Value = Tabla
                    .Parameters.Add(prm)
                    .Parameters.Add(prm2)
                    Dim i As Integer = .ExecuteNonQuery
                End With
                CON4.Close()
            End If


        Catch ex As System.Exception
            If CON4.State <> ConnectionState.Closed Then CON4.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Private Sub LLENA_LISBOX2(ByVal CLV_SESSION As Long)

        Dim sw As Integer = 0
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("MUESTRATabla_Selecciona2", con)
        Me.ListBox2.Items.Clear()
        Me.ListBox4.Items.Clear()
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New Data.SqlClient.SqlParameter( _
                "@Clv_Session", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CLV_SESSION
        cmd.Parameters.Add(prm)

        con.Open()

        Dim reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Try
            While reader.Read()

                'Llenamos los TxtBox con los datos respectivos
                'Me.ListBox1.ItemsInsert(reader(0).ToString, reader(1).ToString)
                Me.ListBox2.Items.Add(reader(1).ToString)
                Me.ListBox4.Items.Add(reader(0).ToString)
                sw = 1
            End While
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            reader.Close()
        End Try
        con.Close()
        'If sw = 0 Then
        '    Me.ComboBox2.SelectedValue = 0
        '    Me.ComboBox3.SelectedValue = 0
        'End If

    End Sub


    Private Sub LLENA_LISBOX1(ByVal CLV_SESSION As Long)

        Dim sw As Integer = 0
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("MUESTRATabla_Selecciona1", con)
        Me.ListBox1.Items.Clear()
        Me.ListBox3.Items.Clear()
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New Data.SqlClient.SqlParameter( _
                "@Clv_Session", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CLV_SESSION
        cmd.Parameters.Add(prm)

        con.Open()

        Dim reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Try
            While reader.Read()

                'Llenamos los TxtBox con los datos respectivos
                'Me.ListBox1.ItemsInsert(reader(0).ToString, reader(1).ToString)
                Me.ListBox1.Items.Add(reader(1).ToString)
                Me.ListBox3.Items.Add(reader(0).ToString)
                sw = 1
            End While
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            reader.Close()
        End Try
        con.Close()
        'If sw = 0 Then
        '    Me.ComboBox2.SelectedValue = 0
        '    Me.ComboBox3.SelectedValue = 0
        'End If

    End Sub

    Private Sub FrmSelSector_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'Inicializa_Seleccion_TAbla(LocClv_session, "SELECT Clv_colonia,nombre FROM Colonias")
        Inicializa_Seleccion_TAbla(LocClv_session, "select Clv_Sector ,Descripcion from dbo.Sector Order by Descripcion ")
        Me.LLENA_LISBOX1(LocClv_session)
        LocPeriodo1 = False
        LocPeriodo2 = False
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If Me.ListBox2.Items.Count = 0 Then
            MsgBox("Seleccione al menos un Sector", MsgBoxStyle.Information)
            Exit Sub
        Else
            Guarda_Seleccion_TAbla(LocClv_session, "Tabla_Seleccion_Sector")

            If LocOp = 22 Or LocOp = 8 Then
                FrmSelCiudad.Show()
            ElseIf LocOp = 2001 Then
                FrmSelColonia.Show()
            ElseIf LocOp = 2002 Then
                OBndReport = True
            ElseIf LocOp = 2007 Then
                FrmSelRepAgendaTecnico.Show()
            ElseIf LocOp = 2006 Then
                FrmSelTecnico_Rep.Show()
            ElseIf LocOp = 2004 Then
                eBndAtenTelGraf = True
                FrmRepAtenTel.Show()
            ElseIf LocOp = 1 Then
                FrmSelServRep.Show()
            ElseIf LocOp = 2003 Then
                OBndReport = True
            ElseIf LocOp = 1001 Then
                OBndReport = True
            ElseIf LocOp = 2301 Then
                FrmSelCd_Cartera.Show()
            ElseIf LocOp = 2501 Then
                FrmSelContratoRango.Show()
            ElseIf LocOp = 2023 Then
                My.Forms.FrmSelDatosP.Show()
            ElseIf LocOp = 30 Then
                FrmTipoClientes.Show()
            ElseIf LocOp = 90 Then
                FrmTipoClientes.Show()
            ElseIf eOpVentas = 63 Then
                FrmImprimirComision.Show()
            ElseIf eOpVentas = 65 Then
                FrmImprimirComision.Show()
            ElseIf eOpVentas = 66 Then
                eOpVentas = 66
                FrmImprimirComision.Show()
            ElseIf eOpVentas = 67 Then
                FrmImprimirComision.Show()
            ElseIf eOpVentas = 69 And eOpPPE = 0 Then
                eOpPPE = 0
                eOpVentas = 69
                FrmRepPermanencia.Show()
            ElseIf LocOp = 2722 Then
                Locbndrep = 1
                FrmImprimirContrato.Show()
            ElseIf LocOp = 2723 Then
                eOpPPE = 0
                eOpVentas = 77
                FrmSelFechasPPE.Show()
            ElseIf LocOp = 2724 Then
                eOpPPE = 0
                eOpVentas = 62
                eOpCNRCNRDIG = 1
                eServicio = "Digital"
                FrmSelFechasPPE.Show()
            ElseIf LocOp = 2725 Then
                eOpPPE = 0
                eOpVentas = 62
                eOpCNRCNRDIG = 0
                eServicio = "de Cablemodems"
                FrmSelFechasPPE.Show()
            ElseIf LocOp = 2726 Then
                eOpPPE = 0
                eOpVentas = 100
                FrmSelFechasPPE.Show()
            ElseIf LocOp = 77777 Then
                FrmTipoClientes.Show()
            ElseIf LocOp = 1000 Then
                Locbndrep = 1000
                eOpVentas = 1000
                FrmImprimirComision.Show()
            ElseIf LocOp = 1001 Then
                LocOp = 1001
                FrmLlamadasTelefonicasReporte.Show()
            ElseIf OpcionReporteVentas = 1 Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
                OpcionReporteVentas = 0
            ElseIf eOpPPE = 3 Then
                FrmSelFechasPPE.Show()
            ElseIf eOpPPE = 4 Then
                FrmSelFechasPPE.Show()
            ElseIf opciongraficas = 1 Then
                FrmGraficas.Show()
            ElseIf eOpVentas = 55 Then
                FrmSelFechasPPE.Show()
            ElseIf eOpVentas = 56 Then
                FrmSelFechasPPE.Show()
            ElseIf eOpVentas = 61 Then
                FrmSelFechasPPE.Show()
            ElseIf eOpVentas = 68 Then
                FrmSelFechasPPE.Show()
            ElseIf eOpVentas = 70 Then
                FrmRepPermanencia.Show()
                'SAUL reporte Listado de Clientes Combo
            ElseIf LocOp = 66661 Or LocOp = 66662 Or LocOp = 66663 Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
                'SAUL reporte Listado de Clientes Combo(FIN)
            Else
                FrmTipoClientes.Show()
            End If
            Me.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Locbndpen1 = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.ListBox3.SelectedIndex = Me.ListBox1.SelectedIndex
        If IsNumeric(Me.ListBox3.Text) = True Then
            Me.MueveSeleccion_TAbla(LocClv_session, Me.ListBox3.Text, 2)
            Me.LLENA_LISBOX2(LocClv_session)
            Me.LLENA_LISBOX1(LocClv_session)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.ListBox3.SelectedIndex = Me.ListBox1.SelectedIndex
        If Me.ListBox3.Items.Count > 0 Then
            Me.MueveSeleccion_TAbla(LocClv_session, 0, 22)
            Me.LLENA_LISBOX2(LocClv_session)
            Me.LLENA_LISBOX1(LocClv_session)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.ListBox4.SelectedIndex = Me.ListBox2.SelectedIndex
        If Me.ListBox4.Items.Count > 0 Then
            Me.MueveSeleccion_TAbla(LocClv_session, 0, 11)
            Me.LLENA_LISBOX2(LocClv_session)
            Me.LLENA_LISBOX1(LocClv_session)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.ListBox4.SelectedIndex = Me.ListBox2.SelectedIndex
        If IsNumeric(Me.ListBox4.Text) = True Then
            Me.MueveSeleccion_TAbla(LocClv_session, Me.ListBox4.Text, 1)
            Me.LLENA_LISBOX2(LocClv_session)
            Me.LLENA_LISBOX1(LocClv_session)
        End If
    End Sub

End Class