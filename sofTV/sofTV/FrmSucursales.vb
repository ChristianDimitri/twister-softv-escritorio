﻿Imports System.Data.SqlClient
Public Class FrmSucursales

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Public Sub UPS_Mod_SUCURSAL_DIGITAL(ByVal eClv_Sucursal As Long, ByVal eSerieDigital As String, ByVal eUltimoFolioUsadoDigital As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("UPS_Mod_SUCURSAL_DIGITAL", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@CLV_SUCURSAL", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = eClv_Sucursal
        command.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@SerieDigital", SqlDbType.VarChar, 15)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = eSerieDigital
        command.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@UltimoFolioUsadoDigital", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = eUltimoFolioUsadoDigital
        command.Parameters.Add(parametro3)

        Try
            conexion.Open()
            command.ExecuteNonQuery()
            
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub



    Public Sub UPS_CON_SUCURSAL_DIGITAL(ByVal eClv_Sucursal As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("UPS_CON_SUCURSAL_DIGITAL", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@CLV_SUCURSAL", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = eClv_Sucursal
        command.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@SerieDigital", SqlDbType.VarChar, 15)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = ""
        command.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@UltimoFolioUsadoDigital", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        command.Parameters.Add(parametro3)

        Try
            conexion.Open()
            command.ExecuteNonQuery()
            SerieDigitalTxt.Text = parametro2.Value
            UltimoFolioUsadoDigitalTxt.Text = parametro3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub


    Private Sub CONSUCURSALESBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONSUCURSALESBindingNavigatorSaveItem.Click
        Dim locerror As Integer
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(Me.No_folioTextBox.Text) = False Then Me.No_folioTextBox.Text = 0
            Me.Inserta_Generales_FacturaGlobalTableAdapter.Connection = CON
            Me.Inserta_Generales_FacturaGlobalTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Generales_FacturaGlobal, CInt(Me.Clv_SucursalTextBox.Text), Me.SerieTextBox1.Text, CInt(Me.No_folioTextBox.Text), locerror)
            If locerror = 0 Then
                If IsNumeric(Me.UltimoFolioUsadoTextBox.Text) = False Then Me.UltimoFolioUsadoTextBox.Text = 0
                Me.Validate()
                Me.CONSUCURSALESBindingSource.EndEdit()
                Me.CONSUCURSALESTableAdapter.Connection = CON
                Me.CONSUCURSALESTableAdapter.Update(Me.NewSofTvDataSet.CONSUCURSALES)
                Me.Inserta_impresora_sucursalTableAdapter1.Connection = CON
                Me.Inserta_impresora_sucursalTableAdapter1.Fill(Me.ProcedimientosArnoldo2.inserta_impresora_sucursal, CInt(Me.Clv_SucursalTextBox.Text), Me.Impresora_ContratosTextBox.Text, Me.Impresora_TarjetasTextBox.Text, Me.TextBox1.Text)
                Me.Inserta_Generales_FacturaGlobalTableAdapter.Connection = CON
                Me.Inserta_Generales_FacturaGlobalTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Generales_FacturaGlobal, CInt(Me.Clv_SucursalTextBox.Text), Me.SerieTextBox1.Text, CInt(Me.No_folioTextBox.Text), locerror)
                CON.Close()
                UPS_Mod_SUCURSAL_DIGITAL(CInt(Me.Clv_SucursalTextBox.Text), SerieDigitalTxt.Text, UltimoFolioUsadoDigitalTxt.Text)
                bitsist(GloUsuario, 0, LocGloSistema, "Catálogo de Sucursales ", "", "Creación Sucursal", "Clave de la Sucursal: " + Me.Clv_SucursalTextBox.Text, LocClv_Ciudad)
                MsgBox(mensaje5)
                GloBnd = True

                Me.Close()
            Else
                MsgBox("La Serie de Factura Global ya Existe no se puede Guardar", MsgBoxStyle.Information)
            End If

        Catch ex As System.Exception
            'MsgBox("La Serie ya Existe no se Puede Guardar")
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONSUCURSALESTableAdapter.Connection = CON
            Me.CONSUCURSALESTableAdapter.Fill(Me.NewSofTvDataSet.CONSUCURSALES, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            Me.Consulta_Impresora_SucursalTableAdapter1.Connection = CON
            Me.Consulta_Impresora_SucursalTableAdapter1.Fill(Me.ProcedimientosArnoldo2.Consulta_Impresora_Sucursal, CLAVE)
            Me.Consulta_Generales_FacturasGlobalesTableAdapter.Connection = CON
            Me.Consulta_Generales_FacturasGlobalesTableAdapter.Fill(Me.DataSetarnoldo.Consulta_Generales_FacturasGlobales, CLAVE)
            CON.Close()
            UPS_CON_SUCURSAL_DIGITAL(CLAVE)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmSucursales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "N" Then
            Me.CONSUCURSALESBindingSource.AddNew()
            Panel1.Enabled = True
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            BUSCA(gloClave)
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            BUSCA(gloClave)
        End If
    End Sub

    Private Sub Clv_SucursalTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_SucursalTextBox.TextChanged
        gloClave = Me.Clv_SucursalTextBox.Text
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONSUCURSALESTableAdapter.Connection = CON
        Me.CONSUCURSALESTableAdapter.Delete(gloClave)
        Me.Borra_Impresora_SucursalesTableAdapter.Connection = CON
        Me.Borra_Impresora_SucursalesTableAdapter.Fill(Me.DataSetarnoldo.Borra_Impresora_Sucursales, gloClave)
        Me.Borra_Generales_FacturasGlobalesTableAdapter.Connection = CON
        Me.Borra_Generales_FacturasGlobalesTableAdapter.Fill(Me.DataSetarnoldo.Borra_Generales_FacturasGlobales, gloClave)
        bitsist(GloUsuario, 0, LocGloSistema, "Catálogo de Sucursales ", "", "Eliminó Sucursal", "Clave de la Sucursal: " + Me.Clv_SucursalTextBox.Text, LocClv_Ciudad)
        CON.Close()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub NombreTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NombreTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(NombreTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub



   
End Class