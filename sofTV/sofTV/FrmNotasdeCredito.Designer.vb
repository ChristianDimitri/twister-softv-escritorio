<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmNotasdeCredito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmNotasdeCredito))
        Dim Clv_NotadeCreditoLabel As System.Windows.Forms.Label
        Dim SerieLabel As System.Windows.Forms.Label
        Dim Nota_CreditoLabel As System.Windows.Forms.Label
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim FacturaLabel As System.Windows.Forms.Label
        Dim Fecha_deGeneracionLabel As System.Windows.Forms.Label
        Dim Usuario_CapturaLabel As System.Windows.Forms.Label
        Dim Usuario_AutorizoLabel As System.Windows.Forms.Label
        Dim Fecha_CaducidadLabel As System.Windows.Forms.Label
        Dim MontoLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim ObservacionesLabel As System.Windows.Forms.Label
        Me.DataSetLidia = New sofTV.DataSetLidia
        Me.Consulta_NotaCreditoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_NotaCreditoTableAdapter = New sofTV.DataSetLidiaTableAdapters.Consulta_NotaCreditoTableAdapter
        Me.Consulta_NotaCreditoBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.Consulta_NotaCreditoBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.Clv_NotadeCreditoTextBox = New System.Windows.Forms.TextBox
        Me.SerieTextBox = New System.Windows.Forms.TextBox
        Me.Nota_CreditoTextBox = New System.Windows.Forms.TextBox
        Me.ContratoTextBox = New System.Windows.Forms.TextBox
        Me.FacturaTextBox = New System.Windows.Forms.TextBox
        Me.Fecha_deGeneracionDateTimePicker = New System.Windows.Forms.DateTimePicker
        Me.Usuario_CapturaTextBox = New System.Windows.Forms.TextBox
        Me.Usuario_AutorizoTextBox = New System.Windows.Forms.TextBox
        Me.Fecha_CaducidadDateTimePicker = New System.Windows.Forms.DateTimePicker
        Me.MontoTextBox = New System.Windows.Forms.TextBox
        Me.StatusTextBox = New System.Windows.Forms.TextBox
        Me.ObservacionesTextBox = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Button1 = New System.Windows.Forms.Button
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo
        Me.Dame_fecha_hora_servBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_fecha_hora_servTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Dame_fecha_hora_servTableAdapter
        Clv_NotadeCreditoLabel = New System.Windows.Forms.Label
        SerieLabel = New System.Windows.Forms.Label
        Nota_CreditoLabel = New System.Windows.Forms.Label
        ContratoLabel = New System.Windows.Forms.Label
        FacturaLabel = New System.Windows.Forms.Label
        Fecha_deGeneracionLabel = New System.Windows.Forms.Label
        Usuario_CapturaLabel = New System.Windows.Forms.Label
        Usuario_AutorizoLabel = New System.Windows.Forms.Label
        Fecha_CaducidadLabel = New System.Windows.Forms.Label
        MontoLabel = New System.Windows.Forms.Label
        StatusLabel = New System.Windows.Forms.Label
        ObservacionesLabel = New System.Windows.Forms.Label
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_NotaCreditoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_NotaCreditoBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_NotaCreditoBindingNavigator.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_fecha_hora_servBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Consulta_NotaCreditoBindingSource
        '
        Me.Consulta_NotaCreditoBindingSource.DataMember = "Consulta_NotaCredito"
        Me.Consulta_NotaCreditoBindingSource.DataSource = Me.DataSetLidia
        '
        'Consulta_NotaCreditoTableAdapter
        '
        Me.Consulta_NotaCreditoTableAdapter.ClearBeforeFill = True
        '
        'Consulta_NotaCreditoBindingNavigator
        '
        Me.Consulta_NotaCreditoBindingNavigator.AddNewItem = Nothing
        Me.Consulta_NotaCreditoBindingNavigator.BindingSource = Me.Consulta_NotaCreditoBindingSource
        Me.Consulta_NotaCreditoBindingNavigator.CountItem = Nothing
        Me.Consulta_NotaCreditoBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Consulta_NotaCreditoBindingNavigator.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_NotaCreditoBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Consulta_NotaCreditoBindingNavigatorSaveItem})
        Me.Consulta_NotaCreditoBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_NotaCreditoBindingNavigator.MoveFirstItem = Nothing
        Me.Consulta_NotaCreditoBindingNavigator.MoveLastItem = Nothing
        Me.Consulta_NotaCreditoBindingNavigator.MoveNextItem = Nothing
        Me.Consulta_NotaCreditoBindingNavigator.MovePreviousItem = Nothing
        Me.Consulta_NotaCreditoBindingNavigator.Name = "Consulta_NotaCreditoBindingNavigator"
        Me.Consulta_NotaCreditoBindingNavigator.PositionItem = Nothing
        Me.Consulta_NotaCreditoBindingNavigator.Size = New System.Drawing.Size(770, 25)
        Me.Consulta_NotaCreditoBindingNavigator.TabIndex = 0
        Me.Consulta_NotaCreditoBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(76, 22)
        Me.BindingNavigatorDeleteItem.Text = "Cancelar"
        '
        'Consulta_NotaCreditoBindingNavigatorSaveItem
        '
        Me.Consulta_NotaCreditoBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Consulta_NotaCreditoBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_NotaCreditoBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_NotaCreditoBindingNavigatorSaveItem.Name = "Consulta_NotaCreditoBindingNavigatorSaveItem"
        Me.Consulta_NotaCreditoBindingNavigatorSaveItem.Size = New System.Drawing.Size(108, 22)
        Me.Consulta_NotaCreditoBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'Clv_NotadeCreditoLabel
        '
        Clv_NotadeCreditoLabel.AutoSize = True
        Clv_NotadeCreditoLabel.Location = New System.Drawing.Point(81, 554)
        Clv_NotadeCreditoLabel.Name = "Clv_NotadeCreditoLabel"
        Clv_NotadeCreditoLabel.Size = New System.Drawing.Size(99, 13)
        Clv_NotadeCreditoLabel.TabIndex = 1
        Clv_NotadeCreditoLabel.Text = "Clv Notade Credito:"
        '
        'Clv_NotadeCreditoTextBox
        '
        Me.Clv_NotadeCreditoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Clv_NotadeCredito", True))
        Me.Clv_NotadeCreditoTextBox.Location = New System.Drawing.Point(200, 551)
        Me.Clv_NotadeCreditoTextBox.Name = "Clv_NotadeCreditoTextBox"
        Me.Clv_NotadeCreditoTextBox.Size = New System.Drawing.Size(200, 20)
        Me.Clv_NotadeCreditoTextBox.TabIndex = 2
        '
        'SerieLabel
        '
        SerieLabel.AutoSize = True
        SerieLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SerieLabel.Location = New System.Drawing.Point(113, 11)
        SerieLabel.Name = "SerieLabel"
        SerieLabel.Size = New System.Drawing.Size(45, 15)
        SerieLabel.TabIndex = 3
        SerieLabel.Text = "Serie:"
        '
        'SerieTextBox
        '
        Me.SerieTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Serie", True))
        Me.SerieTextBox.Location = New System.Drawing.Point(189, 6)
        Me.SerieTextBox.Name = "SerieTextBox"
        Me.SerieTextBox.ReadOnly = True
        Me.SerieTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SerieTextBox.TabIndex = 4
        '
        'Nota_CreditoLabel
        '
        Nota_CreditoLabel.AutoSize = True
        Nota_CreditoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Nota_CreditoLabel.Location = New System.Drawing.Point(67, 33)
        Nota_CreditoLabel.Name = "Nota_CreditoLabel"
        Nota_CreditoLabel.Size = New System.Drawing.Size(91, 15)
        Nota_CreditoLabel.TabIndex = 5
        Nota_CreditoLabel.Text = "Nota Credito:"
        '
        'Nota_CreditoTextBox
        '
        Me.Nota_CreditoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Nota_Credito", True))
        Me.Nota_CreditoTextBox.Location = New System.Drawing.Point(189, 32)
        Me.Nota_CreditoTextBox.Name = "Nota_CreditoTextBox"
        Me.Nota_CreditoTextBox.ReadOnly = True
        Me.Nota_CreditoTextBox.Size = New System.Drawing.Size(200, 20)
        Me.Nota_CreditoTextBox.TabIndex = 6
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratoLabel.Location = New System.Drawing.Point(93, 79)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(65, 15)
        ContratoLabel.TabIndex = 7
        ContratoLabel.Text = "Contrato:"
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Contrato", True))
        Me.ContratoTextBox.Location = New System.Drawing.Point(189, 78)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(114, 20)
        Me.ContratoTextBox.TabIndex = 8
        '
        'FacturaLabel
        '
        FacturaLabel.AutoSize = True
        FacturaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FacturaLabel.Location = New System.Drawing.Point(99, 105)
        FacturaLabel.Name = "FacturaLabel"
        FacturaLabel.Size = New System.Drawing.Size(59, 15)
        FacturaLabel.TabIndex = 9
        FacturaLabel.Text = "Factura:"
        '
        'FacturaTextBox
        '
        Me.FacturaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Factura", True))
        Me.FacturaTextBox.Location = New System.Drawing.Point(189, 104)
        Me.FacturaTextBox.Name = "FacturaTextBox"
        Me.FacturaTextBox.Size = New System.Drawing.Size(114, 20)
        Me.FacturaTextBox.TabIndex = 10
        '
        'Fecha_deGeneracionLabel
        '
        Fecha_deGeneracionLabel.AutoSize = True
        Fecha_deGeneracionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_deGeneracionLabel.Location = New System.Drawing.Point(10, 132)
        Fecha_deGeneracionLabel.Name = "Fecha_deGeneracionLabel"
        Fecha_deGeneracionLabel.Size = New System.Drawing.Size(148, 15)
        Fecha_deGeneracionLabel.TabIndex = 11
        Fecha_deGeneracionLabel.Text = "Fecha de Generacion:"
        '
        'Fecha_deGeneracionDateTimePicker
        '
        Me.Fecha_deGeneracionDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.Consulta_NotaCreditoBindingSource, "Fecha_deGeneracion", True))
        Me.Fecha_deGeneracionDateTimePicker.Location = New System.Drawing.Point(189, 130)
        Me.Fecha_deGeneracionDateTimePicker.Name = "Fecha_deGeneracionDateTimePicker"
        Me.Fecha_deGeneracionDateTimePicker.Size = New System.Drawing.Size(221, 20)
        Me.Fecha_deGeneracionDateTimePicker.TabIndex = 12
        '
        'Usuario_CapturaLabel
        '
        Usuario_CapturaLabel.AutoSize = True
        Usuario_CapturaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Usuario_CapturaLabel.Location = New System.Drawing.Point(43, 157)
        Usuario_CapturaLabel.Name = "Usuario_CapturaLabel"
        Usuario_CapturaLabel.Size = New System.Drawing.Size(115, 15)
        Usuario_CapturaLabel.TabIndex = 13
        Usuario_CapturaLabel.Text = "Usuario Captura:"
        '
        'Usuario_CapturaTextBox
        '
        Me.Usuario_CapturaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Usuario_Captura", True))
        Me.Usuario_CapturaTextBox.Location = New System.Drawing.Point(189, 156)
        Me.Usuario_CapturaTextBox.Name = "Usuario_CapturaTextBox"
        Me.Usuario_CapturaTextBox.Size = New System.Drawing.Size(200, 20)
        Me.Usuario_CapturaTextBox.TabIndex = 14
        '
        'Usuario_AutorizoLabel
        '
        Usuario_AutorizoLabel.AutoSize = True
        Usuario_AutorizoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Usuario_AutorizoLabel.Location = New System.Drawing.Point(41, 183)
        Usuario_AutorizoLabel.Name = "Usuario_AutorizoLabel"
        Usuario_AutorizoLabel.Size = New System.Drawing.Size(117, 15)
        Usuario_AutorizoLabel.TabIndex = 15
        Usuario_AutorizoLabel.Text = "Usuario Autorizo:"
        '
        'Usuario_AutorizoTextBox
        '
        Me.Usuario_AutorizoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Usuario_Autorizo", True))
        Me.Usuario_AutorizoTextBox.Location = New System.Drawing.Point(189, 182)
        Me.Usuario_AutorizoTextBox.Name = "Usuario_AutorizoTextBox"
        Me.Usuario_AutorizoTextBox.Size = New System.Drawing.Size(200, 20)
        Me.Usuario_AutorizoTextBox.TabIndex = 16
        '
        'Fecha_CaducidadLabel
        '
        Fecha_CaducidadLabel.AutoSize = True
        Fecha_CaducidadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_CaducidadLabel.Location = New System.Drawing.Point(36, 210)
        Fecha_CaducidadLabel.Name = "Fecha_CaducidadLabel"
        Fecha_CaducidadLabel.Size = New System.Drawing.Size(122, 15)
        Fecha_CaducidadLabel.TabIndex = 17
        Fecha_CaducidadLabel.Text = "Fecha Caducidad:"
        '
        'Fecha_CaducidadDateTimePicker
        '
        Me.Fecha_CaducidadDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.Consulta_NotaCreditoBindingSource, "Fecha_Caducidad", True))
        Me.Fecha_CaducidadDateTimePicker.Location = New System.Drawing.Point(189, 208)
        Me.Fecha_CaducidadDateTimePicker.Name = "Fecha_CaducidadDateTimePicker"
        Me.Fecha_CaducidadDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.Fecha_CaducidadDateTimePicker.TabIndex = 18
        '
        'MontoLabel
        '
        MontoLabel.AutoSize = True
        MontoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MontoLabel.Location = New System.Drawing.Point(107, 235)
        MontoLabel.Name = "MontoLabel"
        MontoLabel.Size = New System.Drawing.Size(51, 15)
        MontoLabel.TabIndex = 19
        MontoLabel.Text = "Monto:"
        '
        'MontoTextBox
        '
        Me.MontoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Monto", True))
        Me.MontoTextBox.Location = New System.Drawing.Point(189, 234)
        Me.MontoTextBox.Name = "MontoTextBox"
        Me.MontoTextBox.Size = New System.Drawing.Size(200, 20)
        Me.MontoTextBox.TabIndex = 20
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel.Location = New System.Drawing.Point(107, 261)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(51, 15)
        StatusLabel.TabIndex = 21
        StatusLabel.Text = "Status:"
        '
        'StatusTextBox
        '
        Me.StatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Status", True))
        Me.StatusTextBox.Location = New System.Drawing.Point(189, 260)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.Size = New System.Drawing.Size(200, 20)
        Me.StatusTextBox.TabIndex = 22
        '
        'ObservacionesLabel
        '
        ObservacionesLabel.AutoSize = True
        ObservacionesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ObservacionesLabel.Location = New System.Drawing.Point(53, 287)
        ObservacionesLabel.Name = "ObservacionesLabel"
        ObservacionesLabel.Size = New System.Drawing.Size(105, 15)
        ObservacionesLabel.TabIndex = 23
        ObservacionesLabel.Text = "Observaciones:"
        '
        'ObservacionesTextBox
        '
        Me.ObservacionesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Observaciones", True))
        Me.ObservacionesTextBox.Location = New System.Drawing.Point(189, 286)
        Me.ObservacionesTextBox.Multiline = True
        Me.ObservacionesTextBox.Name = "ObservacionesTextBox"
        Me.ObservacionesTextBox.Size = New System.Drawing.Size(490, 76)
        Me.ObservacionesTextBox.TabIndex = 24
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Nota_CreditoTextBox)
        Me.Panel1.Controls.Add(Me.ObservacionesTextBox)
        Me.Panel1.Controls.Add(ObservacionesLabel)
        Me.Panel1.Controls.Add(SerieLabel)
        Me.Panel1.Controls.Add(Me.StatusTextBox)
        Me.Panel1.Controls.Add(Me.SerieTextBox)
        Me.Panel1.Controls.Add(StatusLabel)
        Me.Panel1.Controls.Add(Nota_CreditoLabel)
        Me.Panel1.Controls.Add(Me.MontoTextBox)
        Me.Panel1.Controls.Add(MontoLabel)
        Me.Panel1.Controls.Add(ContratoLabel)
        Me.Panel1.Controls.Add(Me.Fecha_CaducidadDateTimePicker)
        Me.Panel1.Controls.Add(Me.ContratoTextBox)
        Me.Panel1.Controls.Add(Fecha_CaducidadLabel)
        Me.Panel1.Controls.Add(FacturaLabel)
        Me.Panel1.Controls.Add(Me.Usuario_AutorizoTextBox)
        Me.Panel1.Controls.Add(Me.FacturaTextBox)
        Me.Panel1.Controls.Add(Usuario_AutorizoLabel)
        Me.Panel1.Controls.Add(Fecha_deGeneracionLabel)
        Me.Panel1.Controls.Add(Me.Usuario_CapturaTextBox)
        Me.Panel1.Controls.Add(Me.Fecha_deGeneracionDateTimePicker)
        Me.Panel1.Controls.Add(Usuario_CapturaLabel)
        Me.Panel1.Location = New System.Drawing.Point(17, 41)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(734, 393)
        Me.Panel1.TabIndex = 25
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(580, 451)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(171, 34)
        Me.Button1.TabIndex = 26
        Me.Button1.Text = "SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_fecha_hora_servBindingSource
        '
        Me.Dame_fecha_hora_servBindingSource.DataMember = "Dame_fecha_hora_serv"
        Me.Dame_fecha_hora_servBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Dame_fecha_hora_servTableAdapter
        '
        Me.Dame_fecha_hora_servTableAdapter.ClearBeforeFill = True
        '
        'FrmNotasdeCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(770, 506)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Clv_NotadeCreditoLabel)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Consulta_NotaCreditoBindingNavigator)
        Me.Controls.Add(Me.Clv_NotadeCreditoTextBox)
        Me.MaximizeBox = False
        Me.Name = "FrmNotasdeCredito"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Notas de Crédito"
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_NotaCreditoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_NotaCreditoBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_NotaCreditoBindingNavigator.ResumeLayout(False)
        Me.Consulta_NotaCreditoBindingNavigator.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_fecha_hora_servBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents Consulta_NotaCreditoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_NotaCreditoTableAdapter As sofTV.DataSetLidiaTableAdapters.Consulta_NotaCreditoTableAdapter
    Friend WithEvents Consulta_NotaCreditoBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_NotaCreditoBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_NotadeCreditoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SerieTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Nota_CreditoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FacturaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_deGeneracionDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Usuario_CapturaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Usuario_AutorizoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_CaducidadDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents MontoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ObservacionesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Dame_fecha_hora_servBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_fecha_hora_servTableAdapter As sofTV.DataSetarnoldoTableAdapters.Dame_fecha_hora_servTableAdapter
End Class
