﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRoboDeSeñal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRoboDeSeñal))
        Me.DataSetEric = New sofTV.DataSetEric
        Me.ConRoboDeSeñalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConRoboDeSeñalTableAdapter = New sofTV.DataSetEricTableAdapters.ConRoboDeSeñalTableAdapter
        Me.ConRoboDeSeñalBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.ConRoboDeSeñalBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Button1 = New System.Windows.Forms.Button
        DescripcionLabel = New System.Windows.Forms.Label
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRoboDeSeñalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRoboDeSeñalBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConRoboDeSeñalBindingNavigator.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.Red
        DescripcionLabel.Location = New System.Drawing.Point(160, 6)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(187, 29)
        DescripcionLabel.TabIndex = 4
        DescripcionLabel.Text = "Robo de Señal"
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConRoboDeSeñalBindingSource
        '
        Me.ConRoboDeSeñalBindingSource.DataMember = "ConRoboDeSeñal"
        Me.ConRoboDeSeñalBindingSource.DataSource = Me.DataSetEric
        '
        'ConRoboDeSeñalTableAdapter
        '
        Me.ConRoboDeSeñalTableAdapter.ClearBeforeFill = True
        '
        'ConRoboDeSeñalBindingNavigator
        '
        Me.ConRoboDeSeñalBindingNavigator.AddNewItem = Nothing
        Me.ConRoboDeSeñalBindingNavigator.BindingSource = Me.ConRoboDeSeñalBindingSource
        Me.ConRoboDeSeñalBindingNavigator.CountItem = Nothing
        Me.ConRoboDeSeñalBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.ConRoboDeSeñalBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConRoboDeSeñalBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConRoboDeSeñalBindingNavigatorSaveItem})
        Me.ConRoboDeSeñalBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConRoboDeSeñalBindingNavigator.MoveFirstItem = Nothing
        Me.ConRoboDeSeñalBindingNavigator.MoveLastItem = Nothing
        Me.ConRoboDeSeñalBindingNavigator.MoveNextItem = Nothing
        Me.ConRoboDeSeñalBindingNavigator.MovePreviousItem = Nothing
        Me.ConRoboDeSeñalBindingNavigator.Name = "ConRoboDeSeñalBindingNavigator"
        Me.ConRoboDeSeñalBindingNavigator.PositionItem = Nothing
        Me.ConRoboDeSeñalBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConRoboDeSeñalBindingNavigator.Size = New System.Drawing.Size(512, 25)
        Me.ConRoboDeSeñalBindingNavigator.TabIndex = 1
        Me.ConRoboDeSeñalBindingNavigator.TabStop = True
        Me.ConRoboDeSeñalBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(88, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ConRoboDeSeñalBindingNavigatorSaveItem
        '
        Me.ConRoboDeSeñalBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConRoboDeSeñalBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConRoboDeSeñalBindingNavigatorSaveItem.Name = "ConRoboDeSeñalBindingNavigatorSaveItem"
        Me.ConRoboDeSeñalBindingNavigatorSaveItem.Size = New System.Drawing.Size(79, 22)
        Me.ConRoboDeSeñalBindingNavigatorSaveItem.Text = "&GRABAR"
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRoboDeSeñalBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(12, 34)
        Me.DescripcionTextBox.Multiline = True
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DescripcionTextBox.Size = New System.Drawing.Size(475, 170)
        Me.DescripcionTextBox.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.DescripcionTextBox)
        Me.Panel1.Controls.Add(DescripcionLabel)
        Me.Panel1.Location = New System.Drawing.Point(0, 24)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(515, 280)
        Me.Panel1.TabIndex = 6
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(354, 225)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FrmRoboDeSeñal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(512, 302)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ConRoboDeSeñalBindingNavigator)
        Me.MaximizeBox = False
        Me.Name = "FrmRoboDeSeñal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Robo de Señal"
        Me.TopMost = True
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRoboDeSeñalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRoboDeSeñalBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConRoboDeSeñalBindingNavigator.ResumeLayout(False)
        Me.ConRoboDeSeñalBindingNavigator.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConRoboDeSeñalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRoboDeSeñalTableAdapter As sofTV.DataSetEricTableAdapters.ConRoboDeSeñalTableAdapter
    Friend WithEvents ConRoboDeSeñalBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConRoboDeSeñalBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
