﻿Imports System.Data.SqlClient

Public Class FrmSelCliente

    'Private Sub BUSCACLIENTES(ByVal OP As Integer)
    '    Try
    '        Dim CON3 As New SqlConnection(MiConexion)
    '        CON3.Open()
    '        If OP = 0 Then
    '            If GloClv_TipSer = 1000 Or GloClv_TipSer = 1001 Then
    '                OP = OP + 10
    '            End If
    '            If IsNumeric(Me.bcONTRATO.Text) = True Then
    '                Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
    '                Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(bcONTRATO.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer, Me.txtTelefono.Text)
    '            Else
    '                Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
    '                Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer, Me.txtTelefono.Text)
    '            End If
    '            Me.bcONTRATO.Clear()
    '        ElseIf OP = 1 Then
    '            If GloClv_TipSer = 1000 Or GloClv_TipSer = 1001 Then
    '                OP = OP + 10
    '            End If
    '            Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
    '            Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), (CType(Me.BNOMBRE.Text, String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer, Me.txtTelefono.Text)
    '            'Me.BNOMBRE.Clear()
    '        ElseIf OP = 2 Then
    '            If GloClv_TipSer = 1000 Or GloClv_TipSer = 1001 Then
    '                OP = OP + 10
    '            End If
    '            Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
    '            Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType(Me.BCALLE.Text, String)), (CType(Me.BNUMERO.Text, String)), (CType(Me.BCIUDAD.Text, String)), New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer, Me.txtTelefono.Text)
    '            'Me.BCALLE.Clear()
    '            'Me.BNUMERO.Clear()
    '            'Me.BCIUDAD.Clear()
    '        ElseIf OP = 3 Then
    '            If GloClv_TipSer = 1000 Or GloClv_TipSer = 1001 Then
    '                OP = OP + 10
    '            End If
    '            Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
    '            Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), "", "", "", "", New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer, Me.txtTelefono.Text)

    '            'Me.BCALLE.Clear()
    '            'Me.BNUMERO.Clear()
    '            'Me.BCIUDAD.Clear()
    '        ElseIf OP = 4 Then
    '            If GloClv_TipSer = 1000 Or GloClv_TipSer = 1001 Then
    '                OP = OP + 10
    '            End If
    '            Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON3
    '            Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), "", "", "", "", New System.Nullable(Of Integer)(CType(OP, Integer)), GloClv_TipSer, Me.txtTelefono.Text)
    '            Me.txtTelefono.Text = ""
    '        End If
    '        CON3.Close()
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Dim contrato1 As Long
        Dim CON As New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("BUSCLIPORCONTRATO_2", CON)
        Dim tabla As DataTable = New DataTable
        Dim bs As BindingSource = New BindingSource
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        If OP = 0 Then
            If IsNumeric(Me.bcONTRATO.Text) = True Then
                contrato1 = Long.Parse(Me.bcONTRATO.Text)
            Else
                MsgBox("El contrato introducido no es correcto, favor de verificarlo", MsgBoxStyle.Information)
            End If
        End If
        If GloClv_TipSer = 1000 Or GloClv_TipSer = 1001 Then
            OP = OP + 10
        End If
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@CONTRATO", contrato1))
        com.Parameters.Add(New SqlParameter("@NOMBRE", Me.BNOMBRE.Text.ToString))
        com.Parameters.Add(New SqlParameter("@CALLE", Me.BCALLE.Text.ToString))
        com.Parameters.Add(New SqlParameter("@NUMERO", Me.BNUMERO.Text.ToString))
        com.Parameters.Add(New SqlParameter("@CIUDAD", Me.BCIUDAD.Text.ToString))
        com.Parameters.Add(New SqlParameter("@OP", OP))
        com.Parameters.Add(New SqlParameter("@Clv_TipSer", GloClv_TipSer))
        com.Parameters.Add(New SqlParameter("@Telefono", Me.txtTelefono.Text.ToString))

        Try
            CON.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
            Me.DataGridView1.DataSource = bs
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            CON.Close()
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If IsNumeric(Me.DataGridView1.SelectedCells.Item(0).Value) = True Then
            GLOCONTRATOSEL = Me.DataGridView1.SelectedCells.Item(0).Value
            NombreCliente = Me.DataGridView1.SelectedCells.Item(1).Value
            If GloClv_TipSer = 1000 Or 1001 Then
                LocbndProceso = True
            End If
            If Locformulario = 1 Then
                Locbndcontrato1 = True
            ElseIf Locformulario = 2 Then
                Locbndcontrato2 = True
            ElseIf Locformulario = 3 Then
                Locbndcontrato3 = True
            End If
            Me.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        LocbndProceso1 = True
        Me.Close()
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.BUSCACLIENTES(0)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Me.BUSCACLIENTES(1)
    End Sub

    Private Sub BrwClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If IdSistema = "VA" Or IdSistema = "LO" Or IdSistema = "YU" Then
            FrmSelCliente2.Show()
            Me.Close()
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla según sea necesario.
        'Me.MUESTRACALLESTableAdapter.Connection = CON
        'Me.MUESTRACALLESTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACALLES)
        CON.Close()
        Me.BUSCACLIENTES(3)
    End Sub

    Private Sub bcONTRATO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles bcONTRATO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(0)
        End If
    End Sub

    Private Sub bcONTRATO_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bcONTRATO.TextChanged

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Me.BUSCACLIENTES(2)
    End Sub

    Private Sub BNOMBRE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNOMBRE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(1)
            'Me.BNOMBRE.Text = ""
        End If
    End Sub

    Private Sub BNOMBRE_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BNOMBRE.TextChanged

    End Sub

    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub

    Private Sub BCALLE_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCALLE.TextChanged

    End Sub

    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub

    Private Sub BNUMERO_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BNUMERO.TextChanged

    End Sub

    Private Sub BCIUDAD_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCIUDAD.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub



    Private Sub CONTRATOLabel1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CONTRATOLabel1.TextChanged
        Try
            CREAARBOL()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub SOLOINTERNETCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SOLOINTERNETCheckBox.CheckedChanged
        If Me.SOLOINTERNETCheckBox.Checked = False Then
            Me.SOLOINTERNETCheckBox.Enabled = False
        Else
            Me.SOLOINTERNETCheckBox.Enabled = True
        End If
    End Sub

    Private Sub ESHOTELCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ESHOTELCheckBox.CheckedChanged
        If Me.ESHOTELCheckBox.Checked = False Then
            Me.ESHOTELCheckBox.Enabled = False
        Else
            Me.ESHOTELCheckBox.Enabled = True
        End If
    End Sub


    Private Sub ServicioListBox_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Me.bcONTRATO.Focus()
    End Sub




    Private Sub CREAARBOL()

        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.CONTRATOLabel1.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisión Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next
            CON.Close()
            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub


    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If IsNumeric(Me.DataGridView1.SelectedCells.Item(0).Value) = True Then
            GLOCONTRATOSEL = Me.DataGridView1.SelectedCells.Item(0).Value
            If GloClv_TipSer = 1000 Or 1001 Then
                LocbndProceso = True
            End If
            If Locformulario = 1 Then
                Locbndcontrato1 = True
            ElseIf Locformulario = 2 Then
                Locbndcontrato2 = True
            End If
            Me.Close()
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub btnBtelefono_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBtelefono.Click
        Me.BUSCACLIENTES(4)
    End Sub

    Private Sub txtTelefono_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTelefono.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(4)
        End If
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Try
            Dim row As Integer = Convert.ToInt32(Me.DataGridView1.CurrentRow.Index.ToString)
            Me.NOMBRELabel1.Text = Me.DataGridView1.Rows(row).Cells(1).Value.ToString
            Me.CALLELabel1.Text = Me.DataGridView1.Rows(row).Cells(2).Value.ToString
            Me.NUMEROLabel1.Text = Me.DataGridView1.Rows(row).Cells(3).Value.ToString
            Me.CALLELabel1.Text = Me.DataGridView1.Rows(row).Cells(4).Value.ToString
            Me.COLONIALabel1.Text = Me.DataGridView1.Rows(row).Cells(5).Value.ToString
            Me.SOLOINTERNETCheckBox.Checked = Convert.ToBoolean(Convert.ToInt32(Me.DataGridView1.Rows(row).Cells(6).Value.ToString))
            Me.ESHOTELCheckBox.Checked = Convert.ToBoolean(Convert.ToInt32(Me.DataGridView1.Rows(row).Cells(7).Value.ToString))
        Catch

        End Try
    End Sub
End Class