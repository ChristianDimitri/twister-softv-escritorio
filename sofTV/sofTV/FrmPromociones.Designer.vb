﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPromociones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim CLAVELabel As System.Windows.Forms.Label
        Dim DESCRIPCIONLabel As System.Windows.Forms.Label
        Dim Clv_TipSerLabel As System.Windows.Forms.Label
        Dim MesesAplicaLabel As System.Windows.Forms.Label
        Dim DescuentoLabel As System.Windows.Forms.Label
        Dim ActivoLabel As System.Windows.Forms.Label
        Dim Fecha2Label As System.Windows.Forms.Label
        Dim Fecha1Label As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPromociones))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Fecha1DateTimePicker = New System.Windows.Forms.DateTimePicker
        Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2
        Me.Fecha2DateTimePicker = New System.Windows.Forms.DateTimePicker
        Me.MesesAplicaNumericUpDown = New System.Windows.Forms.NumericUpDown
        Me.DescuentoTextBox = New System.Windows.Forms.TextBox
        Me.ActivoCheckBox = New System.Windows.Forms.CheckBox
        Me.CONTipoPromocionBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        Me.CONTipoPromocionBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.CLAVETextBox = New System.Windows.Forms.TextBox
        Me.DESCRIPCIONTextBox = New System.Windows.Forms.TextBox
        Me.Clv_TipSerTextBox = New System.Windows.Forms.TextBox
        Me.Button5 = New System.Windows.Forms.Button
        Me.Consulta_Rel_Promocion_Meses_AplicaTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Rel_Promocion_Meses_AplicaTableAdapter
        Me.Inserta_Rel_Promocion_Meses_AplicaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Rel_Promocion_Meses_AplicaTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Rel_Promocion_Meses_AplicaTableAdapter
        Me.CONTipoPromocionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet
        Me.CONTipoPromocionTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONTipoPromocionTableAdapter
        CLAVELabel = New System.Windows.Forms.Label
        DESCRIPCIONLabel = New System.Windows.Forms.Label
        Clv_TipSerLabel = New System.Windows.Forms.Label
        MesesAplicaLabel = New System.Windows.Forms.Label
        DescuentoLabel = New System.Windows.Forms.Label
        ActivoLabel = New System.Windows.Forms.Label
        Fecha2Label = New System.Windows.Forms.Label
        Fecha1Label = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MesesAplicaNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONTipoPromocionBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONTipoPromocionBindingNavigator.SuspendLayout()
        CType(Me.Inserta_Rel_Promocion_Meses_AplicaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONTipoPromocionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CLAVELabel
        '
        CLAVELabel.AutoSize = True
        CLAVELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CLAVELabel.Location = New System.Drawing.Point(106, 69)
        CLAVELabel.Name = "CLAVELabel"
        CLAVELabel.Size = New System.Drawing.Size(50, 15)
        CLAVELabel.TabIndex = 0
        CLAVELabel.Text = "Clave :"
        '
        'DESCRIPCIONLabel
        '
        DESCRIPCIONLabel.AutoSize = True
        DESCRIPCIONLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DESCRIPCIONLabel.Location = New System.Drawing.Point(86, 96)
        DESCRIPCIONLabel.Name = "DESCRIPCIONLabel"
        DESCRIPCIONLabel.Size = New System.Drawing.Size(91, 15)
        DESCRIPCIONLabel.TabIndex = 2
        DESCRIPCIONLabel.Text = "Descripción :"
        'AddHandler DESCRIPCIONLabel.Click, AddressOf Me.DESCRIPCIONLabel_Click
        '
        'Clv_TipSerLabel
        '
        Clv_TipSerLabel.AutoSize = True
        Clv_TipSerLabel.ForeColor = System.Drawing.Color.WhiteSmoke
        Clv_TipSerLabel.Location = New System.Drawing.Point(12, 273)
        Clv_TipSerLabel.Name = "Clv_TipSerLabel"
        Clv_TipSerLabel.Size = New System.Drawing.Size(62, 13)
        Clv_TipSerLabel.TabIndex = 20
        Clv_TipSerLabel.Text = "Clv Tip Ser:"
        '
        'MesesAplicaLabel
        '
        MesesAplicaLabel.AutoSize = True
        MesesAplicaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        MesesAplicaLabel.Location = New System.Drawing.Point(3, 125)
        MesesAplicaLabel.Name = "MesesAplicaLabel"
        MesesAplicaLabel.Size = New System.Drawing.Size(174, 15)
        MesesAplicaLabel.TabIndex = 5
        MesesAplicaLabel.Text = "No. De Meses Que Aplica:"
        '
        'DescuentoLabel
        '
        DescuentoLabel.AutoSize = True
        DescuentoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescuentoLabel.Location = New System.Drawing.Point(405, 125)
        DescuentoLabel.Name = "DescuentoLabel"
        DescuentoLabel.Size = New System.Drawing.Size(101, 15)
        DescuentoLabel.TabIndex = 11
        DescuentoLabel.Text = "Descuento ($):"
        '
        'ActivoLabel
        '
        ActivoLabel.AutoSize = True
        ActivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ActivoLabel.Location = New System.Drawing.Point(405, 186)
        ActivoLabel.Name = "ActivoLabel"
        ActivoLabel.Size = New System.Drawing.Size(59, 15)
        ActivoLabel.TabIndex = 13
        ActivoLabel.Text = "Vigente:"
        '
        'Fecha2Label
        '
        Fecha2Label.AutoSize = True
        Fecha2Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha2Label.Location = New System.Drawing.Point(224, 16)
        Fecha2Label.Name = "Fecha2Label"
        Fecha2Label.Size = New System.Drawing.Size(86, 15)
        Fecha2Label.TabIndex = 9
        Fecha2Label.Text = "Fecha Final:"
        '
        'Fecha1Label
        '
        Fecha1Label.AutoSize = True
        Fecha1Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha1Label.Location = New System.Drawing.Point(34, 16)
        Fecha1Label.Name = "Fecha1Label"
        Fecha1Label.Size = New System.Drawing.Size(93, 15)
        Fecha1Label.TabIndex = 7
        Fecha1Label.Text = "Fecha Inicial:"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(MesesAplicaLabel)
        Me.Panel1.Controls.Add(Me.MesesAplicaNumericUpDown)
        Me.Panel1.Controls.Add(DescuentoLabel)
        Me.Panel1.Controls.Add(Me.DescuentoTextBox)
        Me.Panel1.Controls.Add(ActivoLabel)
        Me.Panel1.Controls.Add(Me.ActivoCheckBox)
        Me.Panel1.Controls.Add(Me.CONTipoPromocionBindingNavigator)
        Me.Panel1.Controls.Add(CLAVELabel)
        Me.Panel1.Controls.Add(Me.CLAVETextBox)
        Me.Panel1.Controls.Add(DESCRIPCIONLabel)
        Me.Panel1.Controls.Add(Me.DESCRIPCIONTextBox)
        Me.Panel1.Controls.Add(Clv_TipSerLabel)
        Me.Panel1.Controls.Add(Me.Clv_TipSerTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(676, 292)
        Me.Panel1.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Fecha1DateTimePicker)
        Me.GroupBox1.Controls.Add(Me.Fecha2DateTimePicker)
        Me.GroupBox1.Controls.Add(Fecha2Label)
        Me.GroupBox1.Controls.Add(Fecha1Label)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(31, 170)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(343, 100)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Clientes Con Fecha de Contratación Entre:"
        '
        'Fecha1DateTimePicker
        '
        Me.Fecha1DateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource, "Fecha1", True))
        Me.Fecha1DateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha1DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha1DateTimePicker.Location = New System.Drawing.Point(19, 43)
        Me.Fecha1DateTimePicker.Name = "Fecha1DateTimePicker"
        Me.Fecha1DateTimePicker.Size = New System.Drawing.Size(118, 21)
        Me.Fecha1DateTimePicker.TabIndex = 8
        '
        'Consulta_Rel_Promocion_Meses_AplicaBindingSource
        '
        Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource.DataMember = "Consulta_Rel_Promocion_Meses_Aplica"
        Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Fecha2DateTimePicker
        '
        Me.Fecha2DateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource, "Fecha2", True))
        Me.Fecha2DateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha2DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha2DateTimePicker.Location = New System.Drawing.Point(201, 43)
        Me.Fecha2DateTimePicker.Name = "Fecha2DateTimePicker"
        Me.Fecha2DateTimePicker.Size = New System.Drawing.Size(115, 21)
        Me.Fecha2DateTimePicker.TabIndex = 10
        '
        'MesesAplicaNumericUpDown
        '
        Me.MesesAplicaNumericUpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MesesAplicaNumericUpDown.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource, "MesesAplica", True))
        Me.MesesAplicaNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MesesAplicaNumericUpDown.Location = New System.Drawing.Point(183, 125)
        Me.MesesAplicaNumericUpDown.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.MesesAplicaNumericUpDown.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.MesesAplicaNumericUpDown.Name = "MesesAplicaNumericUpDown"
        Me.MesesAplicaNumericUpDown.Size = New System.Drawing.Size(54, 21)
        Me.MesesAplicaNumericUpDown.TabIndex = 6
        Me.MesesAplicaNumericUpDown.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'DescuentoTextBox
        '
        Me.DescuentoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescuentoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource, "Descuento", True))
        Me.DescuentoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescuentoTextBox.Location = New System.Drawing.Point(518, 123)
        Me.DescuentoTextBox.MaxLength = 6
        Me.DescuentoTextBox.Name = "DescuentoTextBox"
        Me.DescuentoTextBox.Size = New System.Drawing.Size(64, 21)
        Me.DescuentoTextBox.TabIndex = 12
        '
        'ActivoCheckBox
        '
        Me.ActivoCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource, "Activo", True))
        Me.ActivoCheckBox.Location = New System.Drawing.Point(470, 183)
        Me.ActivoCheckBox.Name = "ActivoCheckBox"
        Me.ActivoCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.ActivoCheckBox.TabIndex = 14
        '
        'CONTipoPromocionBindingNavigator
        '
        Me.CONTipoPromocionBindingNavigator.AddNewItem = Nothing
        Me.CONTipoPromocionBindingNavigator.BindingSource = Me.CONTipoPromocionBindingSource
        Me.CONTipoPromocionBindingNavigator.CountItem = Nothing
        Me.CONTipoPromocionBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONTipoPromocionBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorDeleteItem, Me.CONTipoPromocionBindingNavigatorSaveItem})
        Me.CONTipoPromocionBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONTipoPromocionBindingNavigator.MoveFirstItem = Nothing
        Me.CONTipoPromocionBindingNavigator.MoveLastItem = Nothing
        Me.CONTipoPromocionBindingNavigator.MoveNextItem = Nothing
        Me.CONTipoPromocionBindingNavigator.MovePreviousItem = Nothing
        Me.CONTipoPromocionBindingNavigator.Name = "CONTipoPromocionBindingNavigator"
        Me.CONTipoPromocionBindingNavigator.PositionItem = Nothing
        Me.CONTipoPromocionBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONTipoPromocionBindingNavigator.Size = New System.Drawing.Size(676, 25)
        Me.CONTipoPromocionBindingNavigator.TabIndex = 1
        Me.CONTipoPromocionBindingNavigator.TabStop = True
        Me.CONTipoPromocionBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONTipoPromocionBindingNavigatorSaveItem
        '
        Me.CONTipoPromocionBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTipoPromocionBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONTipoPromocionBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONTipoPromocionBindingNavigatorSaveItem.Name = "CONTipoPromocionBindingNavigatorSaveItem"
        Me.CONTipoPromocionBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONTipoPromocionBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'CLAVETextBox
        '
        Me.CLAVETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CLAVETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTipoPromocionBindingSource, "CLAVE", True))
        Me.CLAVETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CLAVETextBox.Location = New System.Drawing.Point(183, 67)
        Me.CLAVETextBox.Name = "CLAVETextBox"
        Me.CLAVETextBox.ReadOnly = True
        Me.CLAVETextBox.Size = New System.Drawing.Size(100, 21)
        Me.CLAVETextBox.TabIndex = 1
        Me.CLAVETextBox.TabStop = False
        '
        'DESCRIPCIONTextBox
        '
        Me.DESCRIPCIONTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DESCRIPCIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTipoPromocionBindingSource, "DESCRIPCION", True))
        Me.DESCRIPCIONTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DESCRIPCIONTextBox.Location = New System.Drawing.Point(183, 94)
        Me.DESCRIPCIONTextBox.Name = "DESCRIPCIONTextBox"
        Me.DESCRIPCIONTextBox.Size = New System.Drawing.Size(418, 21)
        Me.DESCRIPCIONTextBox.TabIndex = 0
        '
        'Clv_TipSerTextBox
        '
        Me.Clv_TipSerTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipSerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTipoPromocionBindingSource, "Clv_TipSer", True))
        Me.Clv_TipSerTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_TipSerTextBox.Location = New System.Drawing.Point(80, 273)
        Me.Clv_TipSerTextBox.Name = "Clv_TipSerTextBox"
        Me.Clv_TipSerTextBox.Size = New System.Drawing.Size(100, 13)
        Me.Clv_TipSerTextBox.TabIndex = 21
        Me.Clv_TipSerTextBox.TabStop = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(552, 317)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 2
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Consulta_Rel_Promocion_Meses_AplicaTableAdapter
        '
        Me.Consulta_Rel_Promocion_Meses_AplicaTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Rel_Promocion_Meses_AplicaBindingSource
        '
        Me.Inserta_Rel_Promocion_Meses_AplicaBindingSource.DataMember = "Inserta_Rel_Promocion_Meses_Aplica"
        Me.Inserta_Rel_Promocion_Meses_AplicaBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_Rel_Promocion_Meses_AplicaTableAdapter
        '
        Me.Inserta_Rel_Promocion_Meses_AplicaTableAdapter.ClearBeforeFill = True
        '
        'CONTipoPromocionBindingSource
        '
        Me.CONTipoPromocionBindingSource.DataMember = "CONTipoPromocion"
        Me.CONTipoPromocionBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONTipoPromocionTableAdapter
        '
        Me.CONTipoPromocionTableAdapter.ClearBeforeFill = True
        '
        'FrmPromociones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(695, 358)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.Name = "FrmPromociones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Promociones"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.Consulta_Rel_Promocion_Meses_AplicaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MesesAplicaNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONTipoPromocionBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONTipoPromocionBindingNavigator.ResumeLayout(False)
        Me.CONTipoPromocionBindingNavigator.PerformLayout()
        CType(Me.Inserta_Rel_Promocion_Meses_AplicaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONTipoPromocionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONTipoPromocionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONTipoPromocionTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONTipoPromocionTableAdapter
    Friend WithEvents CLAVETextBox As System.Windows.Forms.TextBox
    Friend WithEvents DESCRIPCIONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONTipoPromocionBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONTipoPromocionBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Consulta_Rel_Promocion_Meses_AplicaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Rel_Promocion_Meses_AplicaTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Rel_Promocion_Meses_AplicaTableAdapter
    Friend WithEvents MesesAplicaNumericUpDown As System.Windows.Forms.NumericUpDown
    Friend WithEvents Fecha1DateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Fecha2DateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents DescuentoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ActivoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Inserta_Rel_Promocion_Meses_AplicaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Rel_Promocion_Meses_AplicaTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_Rel_Promocion_Meses_AplicaTableAdapter
End Class
