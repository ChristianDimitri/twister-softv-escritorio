﻿Imports System.Data.SqlClient
Public Class FrmPromociones

    Private Descripcion As String = Nothing
    Private nomesesaplica As String = Nothing
    Private descuento As String = Nothing
    Private activa As String = Nothing
    Private fechaini As String = Nothing
    Private fechafin As String = Nothing

    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                Descripcion = Me.DESCRIPCIONTextBox.Text
                nomesesaplica = CStr(Me.MesesAplicaNumericUpDown.Value)
                descuento = Me.DescuentoTextBox.Text
                If Me.ActivoCheckBox.CheckState = CheckState.Checked Then
                    activa = "True"
                Else
                    activa = "False"
                End If
                fechaini = Me.Fecha1DateTimePicker.Text
                fechafin = Me.Fecha2DateTimePicker.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora(ByVal op As Integer)
        Try
            Dim validacion1 As String = Nothing

            Select Case op
                Case 0
                    If opcion = "M" Then
                        'Descripcion = Me.DESCRIPCIONTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.DESCRIPCIONTextBox.Name, Descripcion, Me.DESCRIPCIONTextBox.Text, LocClv_Ciudad)
                        'nomesesaplica = CStr(Me.MesesAplicaNumericUpDown.Value)
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.MesesAplicaNumericUpDown.Name, nomesesaplica, CStr(Me.MesesAplicaNumericUpDown.Value), LocClv_Ciudad)
                        'descuento = Me.DescuentoTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.DescuentoTextBox.Name, descuento, Me.DescuentoTextBox.Text, LocClv_Ciudad)
                        'Activo 
                        If Me.ActivoCheckBox.CheckState = CheckState.Checked Then
                            'activa = "True"
                            validacion1 = "True"
                        Else
                            'activa = "False"
                            validacion1 = "False"
                        End If
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "PromocionActiva", activa, validacion1, LocClv_Ciudad)

                        'fechaini = Me.Fecha1DateTimePicker.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.Fecha1DateTimePicker.Name, fechaini, Me.Fecha1DateTimePicker.Text, LocClv_Ciudad)
                        'fechafin = Me.Fecha2DateTimePicker.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.Fecha2DateTimePicker.Name, fechafin, Me.Fecha2DateTimePicker.Text, LocClv_Ciudad)

                    ElseIf opcion = "N" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Agregó Una Promoción", "", "Se Agregó Una Promoción: " + Me.DESCRIPCIONTextBox.Text, LocClv_Ciudad)
                    End If
                Case 1
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Una Promoción", "", "Se Elimino Una Promoción: " + Me.DESCRIPCIONTextBox.Text, LocClv_Ciudad)
            End Select

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub busca(ByVal clave As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONTipoPromocionTableAdapter.Connection = CON
            Me.CONTipoPromocionTableAdapter.Fill(Me.NewSofTvDataSet.CONTipoPromocion, New System.Nullable(Of Integer)(CType(clave, Integer)))
            Me.Consulta_Rel_Promocion_Meses_AplicaTableAdapter.Connection = CON
            Me.Consulta_Rel_Promocion_Meses_AplicaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Consulta_Rel_Promocion_Meses_Aplica, clave)
            damedatosbitacora()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONTipoPromocionBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONTipoPromocionTableAdapter.Connection = CON
        Me.CONTipoPromocionTableAdapter.Delete(gloClave)
        CON.Close()
        guardabitacora(0)
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub CLAVETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CLAVETextBox.TextChanged
        gloClave = Me.CLAVETextBox.Text
    End Sub

    Private Sub FrmPromociones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "N" Then
            Me.CONTipoPromocionBindingSource.AddNew()
            Panel1.Enabled = True
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            busca(gloClave)
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            busca(gloClave)
        End If
        Me.Clv_TipSerTextBox.Text = GloClv_TipSer
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub CONTipoPromocionBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONTipoPromocionBindingNavigatorSaveItem.Click
        Dim activo As Boolean = False
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONTipoPromocionBindingSource.EndEdit()
        Me.CONTipoPromocionTableAdapter.Connection = CON
        Me.CONTipoPromocionTableAdapter.Update(Me.NewSofTvDataSet.CONTipoPromocion)
        If Me.ActivoCheckBox.CheckState = CheckState.Checked Then
            activo = True
        ElseIf Me.ActivoCheckBox.CheckState = CheckState.Unchecked Then
            activo = False
        End If
        Me.Inserta_Rel_Promocion_Meses_AplicaTableAdapter.Connection = CON
        Me.Inserta_Rel_Promocion_Meses_AplicaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_Rel_Promocion_Meses_Aplica, Me.CLAVETextBox.Text, Me.MesesAplicaNumericUpDown.Value, Me.Fecha1DateTimePicker.Text, Me.Fecha2DateTimePicker.Text, Me.DescuentoTextBox.Text, activo)
        CON.Close()
        guardabitacora(0)
        MsgBox(mensaje5)
        GloBnd = True
        Me.Close()
    End Sub
    Private Sub DESCRIPCIONTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DESCRIPCIONTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(DESCRIPCIONTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub
    Private Sub DescuentoTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DescuentoTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.DescuentoTextBox, Asc(LCase(e.KeyChar)), "L")))
    End Sub
    Private Sub Fecha1DateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha1DateTimePicker.ValueChanged
        Me.Fecha2DateTimePicker.MinDate = Me.Fecha1DateTimePicker.Value
    End Sub
End Class