﻿Imports System.Collections.Generic
Imports sofTV.BAL
Public Class FrmPeriodosDeInstalacion
    ''' <summary>
    ''' Alta de periodos de instalación pertenecientes a un periodo de cobro
    ''' Fecha de creación: 01/10/2011
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

#Region "Atributos y propiedades"
    Public idPeriodoDeCorte As Integer = 0
    Public pInst As New PeriodoDeInstalacion
    Public idPeriodoInstalacion As Int32 = 0
    Public lsPeriodosInstalacion As New List(Of PeriodoDeInstalacion)
#End Region
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub



    Private Sub FrmPeriodosDeInstalacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If idPeriodoInstalacion > 0 Then
            BindPeriodoInstalacion(idPeriodoInstalacion)
        Else
            txtIDPeriodoCorte.Text = idPeriodoDeCorte
        End If
    End Sub


    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        'Validamos la longitud de los campos
        If Not (ValidaCampos()) Then
            Exit Sub
        End If

        If (ValidaRangos()) Then
            Try
                If idPeriodoInstalacion = 0 Then
                    idPeriodoInstalacion = AddPeriodoInstalacion()
                Else
                    EditPeriodoInstalacion()
                End If
                Me.DialogResult = DialogResult.OK
                Me.Close()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try
        End If

    End Sub

    Private Sub txtDiaCierreDeSeñal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDiaCierreDeSeñal.KeyPress
        If e.KeyChar.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtDiaFinal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDiaFinal.KeyPress
        If e.KeyChar.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtDiaInicial_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDiaInicial.KeyPress
        If e.KeyChar.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtDiaLimiteDePago_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDiaLimiteDePago.KeyPress
        If e.KeyChar.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtEnvioECFisicos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEnvioECFisicos.KeyPress
        If e.KeyChar.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtIDPeriodoCorte_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtIDPeriodoCorte.KeyPress
        If e.KeyChar.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub



    Private Sub BindPeriodoInstalacion(ByVal idPeriodoInstalacion As Int32)
        Try
            pInst = pInst.GetOne(idPeriodoInstalacion)
            txtIDPeriodoInstalacion.Text = pInst.id.ToString()
            txtIDPeriodoCorte.Text = pInst.idPeriodoCorte.ToString()
            txtDiaInicial.Text = pInst.diaInicial.ToString()
            txtDiaFinal.Text = pInst.diaFinal.ToString()
            txtEnvioECFisicos.Text = pInst.diaGeneracionEC.ToString()
            txtDiaEnvioDecos.Text = pInst.diaEnvioECDecos.ToString()
            txtDiaLimiteDePago.Text = pInst.diaLimiteDePago.ToString()
            txtDiaCierreDeSeñal.Text = pInst.diaCierreDeSeñal.ToString()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Function AddPeriodoInstalacion() As Int32
        Dim idGenerado As Int32 = 0
        Try
            idGenerado = pInst.Add(0, idPeriodoDeCorte, txtDiaInicial.Text, txtDiaFinal.Text, txtEnvioECFisicos.Text, txtDiaEnvioDecos.Text, txtDiaLimiteDePago.Text, txtDiaCierreDeSeñal.Text, GloUsuario, Nothing, Nothing)
            MsgBox("Se ha agregado correctamente un nuevo periodo de instalación. ID generado: " + idGenerado.ToString())
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Function
    Public Sub EditPeriodoInstalacion()
        Try
            pInst.Edit(idPeriodoInstalacion, idPeriodoDeCorte, txtDiaInicial.Text, txtDiaFinal.Text, txtEnvioECFisicos.Text, txtDiaEnvioDecos.Text, txtDiaLimiteDePago.Text, txtDiaCierreDeSeñal.Text, "", Nothing, Nothing)
            MsgBox("Se ha actualizado correctamente.", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Private Function ValidaRangos() As Boolean
        Dim validos As Boolean = True
        If (lsPeriodosInstalacion.Count = 0) Then
            Return validos
        End If

        For Each periodo As PeriodoDeInstalacion In lsPeriodosInstalacion
            If periodo.id <> idPeriodoInstalacion Then
                If CInt(txtDiaInicial.Text) >= periodo.diaInicial And CInt(txtDiaInicial.Text) <= periodo.diaFinal Then
                    MsgBox("Ya hay un periodo de instalación con que cuenta con el rango que se especifica para la fecha inicial. ID Periodo: " + periodo.id.ToString(), MsgBoxStyle.Information)
                    validos = False
                    Exit For
                End If
                If CInt(txtDiaFinal.Text) >= periodo.diaInicial And CInt(txtDiaFinal.Text) <= periodo.diaFinal Then
                    MsgBox("Ya hay un periodo de instalación con que cuenta con el rango que se especifica para la fecha final. ID Periodo: " + periodo.id.ToString(), MsgBoxStyle.Information)
                    validos = False
                    Exit For
                End If
            End If
        Next

        Return validos
    End Function
    Private Function ValidaCampos() As Boolean
        Dim v As Boolean = True

        If Not ((txtDiaCierreDeSeñal.Text) >= 1 And (txtDiaCierreDeSeñal.Text) <= 31) Then
            v = False
        End If
        If Not ((txtDiaEnvioDecos.Text) >= 1 And (txtDiaEnvioDecos.Text) <= 31) Then
            v = False
        End If
        If Not ((txtDiaFinal.Text) >= 1 And (txtDiaFinal.Text) <= 31) Then
            v = False
        End If
        If Not ((txtDiaInicial.Text) >= 1 And (txtDiaInicial.Text) <= 31) Then
            v = False
        End If
        If Not ((txtDiaLimiteDePago.Text) >= 1 And (txtDiaLimiteDePago.Text) <= 31) Then
            v = False
        End If
        If Not ((txtEnvioECFisicos.Text) >= 1 And (txtEnvioECFisicos.Text) <= 31) Then
            v = False
        End If
        If Not (v) Then
            MsgBox("El rango válido de días es del 1 al 31", MsgBoxStyle.Exclamation)
        End If
        Return v
    End Function
End Class