﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPeriodosDeInstalacion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.gbxDetallePeriodoInstalacion = New System.Windows.Forms.GroupBox()
        Me.txtDiaEnvioDecos = New System.Windows.Forms.TextBox()
        Me.lblDiaEnvioDecos = New System.Windows.Forms.Label()
        Me.txtIDPeriodoInstalacion = New System.Windows.Forms.TextBox()
        Me.lblIDPeriodoInstalacion = New System.Windows.Forms.Label()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblRangoDeFechas = New System.Windows.Forms.Label()
        Me.lblPeriodoDeCorte = New System.Windows.Forms.Label()
        Me.txtIDPeriodoCorte = New System.Windows.Forms.TextBox()
        Me.txtDiaCierreDeSeñal = New System.Windows.Forms.TextBox()
        Me.lblDiaCierreDeSeñal = New System.Windows.Forms.Label()
        Me.txtDiaLimiteDePago = New System.Windows.Forms.TextBox()
        Me.lblDiaLimiteDePago = New System.Windows.Forms.Label()
        Me.txtEnvioECFisicos = New System.Windows.Forms.TextBox()
        Me.lblEnvioECFisicos = New System.Windows.Forms.Label()
        Me.txtDiaFinal = New System.Windows.Forms.TextBox()
        Me.lblDiaFinal = New System.Windows.Forms.Label()
        Me.txtDiaInicial = New System.Windows.Forms.TextBox()
        Me.lblDiaInicial = New System.Windows.Forms.Label()
        Me.gbxDetallePeriodoInstalacion.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.Black
        Me.btnGuardar.Location = New System.Drawing.Point(436, 427)
        Me.btnGuardar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(118, 43)
        Me.btnGuardar.TabIndex = 0
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(588, 427)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(116, 43)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'gbxDetallePeriodoInstalacion
        '
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.txtDiaEnvioDecos)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.lblDiaEnvioDecos)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.txtIDPeriodoInstalacion)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.lblIDPeriodoInstalacion)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.lblPeriodo)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.Label1)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.lblRangoDeFechas)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.lblPeriodoDeCorte)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.txtIDPeriodoCorte)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.txtDiaCierreDeSeñal)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.lblDiaCierreDeSeñal)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.txtDiaLimiteDePago)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.lblDiaLimiteDePago)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.txtEnvioECFisicos)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.lblEnvioECFisicos)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.txtDiaFinal)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.lblDiaFinal)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.txtDiaInicial)
        Me.gbxDetallePeriodoInstalacion.Controls.Add(Me.lblDiaInicial)
        Me.gbxDetallePeriodoInstalacion.Location = New System.Drawing.Point(12, 12)
        Me.gbxDetallePeriodoInstalacion.Name = "gbxDetallePeriodoInstalacion"
        Me.gbxDetallePeriodoInstalacion.Size = New System.Drawing.Size(692, 408)
        Me.gbxDetallePeriodoInstalacion.TabIndex = 105
        Me.gbxDetallePeriodoInstalacion.TabStop = False
        Me.gbxDetallePeriodoInstalacion.Text = "Detalle Periodo Instalación"
        '
        'txtDiaEnvioDecos
        '
        Me.txtDiaEnvioDecos.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiaEnvioDecos.Location = New System.Drawing.Point(219, 340)
        Me.txtDiaEnvioDecos.Name = "txtDiaEnvioDecos"
        Me.txtDiaEnvioDecos.Size = New System.Drawing.Size(133, 25)
        Me.txtDiaEnvioDecos.TabIndex = 3
        Me.txtDiaEnvioDecos.Text = "0"
        Me.txtDiaEnvioDecos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblDiaEnvioDecos
        '
        Me.lblDiaEnvioDecos.AutoSize = True
        Me.lblDiaEnvioDecos.Location = New System.Drawing.Point(232, 319)
        Me.lblDiaEnvioDecos.Name = "lblDiaEnvioDecos"
        Me.lblDiaEnvioDecos.Size = New System.Drawing.Size(101, 18)
        Me.lblDiaEnvioDecos.TabIndex = 22
        Me.lblDiaEnvioDecos.Text = "Dia Envio Decos"
        '
        'txtIDPeriodoInstalacion
        '
        Me.txtIDPeriodoInstalacion.BackColor = System.Drawing.Color.White
        Me.txtIDPeriodoInstalacion.Enabled = False
        Me.txtIDPeriodoInstalacion.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDPeriodoInstalacion.Location = New System.Drawing.Point(424, 110)
        Me.txtIDPeriodoInstalacion.Name = "txtIDPeriodoInstalacion"
        Me.txtIDPeriodoInstalacion.ReadOnly = True
        Me.txtIDPeriodoInstalacion.Size = New System.Drawing.Size(133, 25)
        Me.txtIDPeriodoInstalacion.TabIndex = 21
        Me.txtIDPeriodoInstalacion.Text = "0"
        Me.txtIDPeriodoInstalacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblIDPeriodoInstalacion
        '
        Me.lblIDPeriodoInstalacion.AutoSize = True
        Me.lblIDPeriodoInstalacion.Location = New System.Drawing.Point(411, 89)
        Me.lblIDPeriodoInstalacion.Name = "lblIDPeriodoInstalacion"
        Me.lblIDPeriodoInstalacion.Size = New System.Drawing.Size(153, 18)
        Me.lblIDPeriodoInstalacion.TabIndex = 20
        Me.lblIDPeriodoInstalacion.Text = "ID Periodo de Instalacion"
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(186, 89)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(52, 18)
        Me.lblPeriodo.TabIndex = 19
        Me.lblPeriodo.Text = "Periodo"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Black
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(107, 284)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(481, 20)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "                       Corte, envío y cierre de señal                         "
        '
        'lblRangoDeFechas
        '
        Me.lblRangoDeFechas.AutoSize = True
        Me.lblRangoDeFechas.BackColor = System.Drawing.Color.Black
        Me.lblRangoDeFechas.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRangoDeFechas.ForeColor = System.Drawing.Color.White
        Me.lblRangoDeFechas.Location = New System.Drawing.Point(101, 163)
        Me.lblRangoDeFechas.Name = "lblRangoDeFechas"
        Me.lblRangoDeFechas.Size = New System.Drawing.Size(479, 20)
        Me.lblRangoDeFechas.TabIndex = 17
        Me.lblRangoDeFechas.Text = "                                  Rango De Fechas                               "
        '
        'lblPeriodoDeCorte
        '
        Me.lblPeriodoDeCorte.AutoSize = True
        Me.lblPeriodoDeCorte.BackColor = System.Drawing.Color.Black
        Me.lblPeriodoDeCorte.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodoDeCorte.ForeColor = System.Drawing.Color.White
        Me.lblPeriodoDeCorte.Location = New System.Drawing.Point(107, 51)
        Me.lblPeriodoDeCorte.Name = "lblPeriodoDeCorte"
        Me.lblPeriodoDeCorte.Size = New System.Drawing.Size(472, 20)
        Me.lblPeriodoDeCorte.TabIndex = 16
        Me.lblPeriodoDeCorte.Text = "                                Periodo De Corte                                 " & _
            ""
        '
        'txtIDPeriodoCorte
        '
        Me.txtIDPeriodoCorte.BackColor = System.Drawing.Color.White
        Me.txtIDPeriodoCorte.Enabled = False
        Me.txtIDPeriodoCorte.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDPeriodoCorte.Location = New System.Drawing.Point(155, 110)
        Me.txtIDPeriodoCorte.Name = "txtIDPeriodoCorte"
        Me.txtIDPeriodoCorte.ReadOnly = True
        Me.txtIDPeriodoCorte.Size = New System.Drawing.Size(132, 25)
        Me.txtIDPeriodoCorte.TabIndex = 13
        Me.txtIDPeriodoCorte.Text = "0"
        Me.txtIDPeriodoCorte.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtDiaCierreDeSeñal
        '
        Me.txtDiaCierreDeSeñal.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiaCierreDeSeñal.Location = New System.Drawing.Point(513, 340)
        Me.txtDiaCierreDeSeñal.Name = "txtDiaCierreDeSeñal"
        Me.txtDiaCierreDeSeñal.Size = New System.Drawing.Size(133, 25)
        Me.txtDiaCierreDeSeñal.TabIndex = 5
        Me.txtDiaCierreDeSeñal.Text = "0"
        Me.txtDiaCierreDeSeñal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblDiaCierreDeSeñal
        '
        Me.lblDiaCierreDeSeñal.AutoSize = True
        Me.lblDiaCierreDeSeñal.Location = New System.Drawing.Point(517, 319)
        Me.lblDiaCierreDeSeñal.Name = "lblDiaCierreDeSeñal"
        Me.lblDiaCierreDeSeñal.Size = New System.Drawing.Size(98, 18)
        Me.lblDiaCierreDeSeñal.TabIndex = 8
        Me.lblDiaCierreDeSeñal.Text = "Cierre De Señal"
        '
        'txtDiaLimiteDePago
        '
        Me.txtDiaLimiteDePago.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiaLimiteDePago.Location = New System.Drawing.Point(365, 340)
        Me.txtDiaLimiteDePago.Name = "txtDiaLimiteDePago"
        Me.txtDiaLimiteDePago.Size = New System.Drawing.Size(133, 25)
        Me.txtDiaLimiteDePago.TabIndex = 4
        Me.txtDiaLimiteDePago.Text = "0"
        Me.txtDiaLimiteDePago.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblDiaLimiteDePago
        '
        Me.lblDiaLimiteDePago.AutoSize = True
        Me.lblDiaLimiteDePago.Location = New System.Drawing.Point(378, 319)
        Me.lblDiaLimiteDePago.Name = "lblDiaLimiteDePago"
        Me.lblDiaLimiteDePago.Size = New System.Drawing.Size(97, 18)
        Me.lblDiaLimiteDePago.TabIndex = 6
        Me.lblDiaLimiteDePago.Text = "Limite De Pago"
        '
        'txtEnvioECFisicos
        '
        Me.txtEnvioECFisicos.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEnvioECFisicos.Location = New System.Drawing.Point(71, 340)
        Me.txtEnvioECFisicos.Name = "txtEnvioECFisicos"
        Me.txtEnvioECFisicos.Size = New System.Drawing.Size(133, 25)
        Me.txtEnvioECFisicos.TabIndex = 2
        Me.txtEnvioECFisicos.Text = "0"
        Me.txtEnvioECFisicos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblEnvioECFisicos
        '
        Me.lblEnvioECFisicos.AutoSize = True
        Me.lblEnvioECFisicos.Location = New System.Drawing.Point(68, 319)
        Me.lblEnvioECFisicos.Name = "lblEnvioECFisicos"
        Me.lblEnvioECFisicos.Size = New System.Drawing.Size(122, 18)
        Me.lblEnvioECFisicos.TabIndex = 4
        Me.lblEnvioECFisicos.Text = "Envio E.C. en papel"
        '
        'txtDiaFinal
        '
        Me.txtDiaFinal.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiaFinal.Location = New System.Drawing.Point(424, 215)
        Me.txtDiaFinal.Name = "txtDiaFinal"
        Me.txtDiaFinal.Size = New System.Drawing.Size(133, 25)
        Me.txtDiaFinal.TabIndex = 1
        Me.txtDiaFinal.Text = "0"
        Me.txtDiaFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblDiaFinal
        '
        Me.lblDiaFinal.AutoSize = True
        Me.lblDiaFinal.Location = New System.Drawing.Point(439, 194)
        Me.lblDiaFinal.Name = "lblDiaFinal"
        Me.lblDiaFinal.Size = New System.Drawing.Size(59, 18)
        Me.lblDiaFinal.TabIndex = 2
        Me.lblDiaFinal.Text = "Dia Final"
        '
        'txtDiaInicial
        '
        Me.txtDiaInicial.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiaInicial.Location = New System.Drawing.Point(155, 215)
        Me.txtDiaInicial.Name = "txtDiaInicial"
        Me.txtDiaInicial.Size = New System.Drawing.Size(132, 25)
        Me.txtDiaInicial.TabIndex = 0
        Me.txtDiaInicial.Text = "0"
        Me.txtDiaInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblDiaInicial
        '
        Me.lblDiaInicial.AutoSize = True
        Me.lblDiaInicial.Location = New System.Drawing.Point(186, 194)
        Me.lblDiaInicial.Name = "lblDiaInicial"
        Me.lblDiaInicial.Size = New System.Drawing.Size(66, 18)
        Me.lblDiaInicial.TabIndex = 0
        Me.lblDiaInicial.Text = "Dia Inicial"
        '
        'FrmPeriodosDeInstalacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(718, 479)
        Me.Controls.Add(Me.gbxDetallePeriodoInstalacion)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnSalir)
        Me.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(734, 517)
        Me.MinimumSize = New System.Drawing.Size(734, 517)
        Me.Name = "FrmPeriodosDeInstalacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Periodo De Instalacion"
        Me.gbxDetallePeriodoInstalacion.ResumeLayout(False)
        Me.gbxDetallePeriodoInstalacion.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents gbxDetallePeriodoInstalacion As System.Windows.Forms.GroupBox
    Friend WithEvents txtDiaCierreDeSeñal As System.Windows.Forms.TextBox
    Friend WithEvents lblDiaCierreDeSeñal As System.Windows.Forms.Label
    Friend WithEvents txtDiaLimiteDePago As System.Windows.Forms.TextBox
    Friend WithEvents lblDiaLimiteDePago As System.Windows.Forms.Label
    Friend WithEvents txtEnvioECFisicos As System.Windows.Forms.TextBox
    Friend WithEvents lblEnvioECFisicos As System.Windows.Forms.Label
    Friend WithEvents txtDiaFinal As System.Windows.Forms.TextBox
    Friend WithEvents lblDiaFinal As System.Windows.Forms.Label
    Friend WithEvents txtDiaInicial As System.Windows.Forms.TextBox
    Friend WithEvents lblDiaInicial As System.Windows.Forms.Label
    Friend WithEvents txtIDPeriodoCorte As System.Windows.Forms.TextBox
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblRangoDeFechas As System.Windows.Forms.Label
    Friend WithEvents lblPeriodoDeCorte As System.Windows.Forms.Label
    Friend WithEvents txtIDPeriodoInstalacion As System.Windows.Forms.TextBox
    Friend WithEvents lblIDPeriodoInstalacion As System.Windows.Forms.Label
    Friend WithEvents txtDiaEnvioDecos As System.Windows.Forms.TextBox
    Friend WithEvents lblDiaEnvioDecos As System.Windows.Forms.Label
End Class
