Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic


Public Class FrmImprimirContratoBrwCte

    Private Sub FrmImprimirContratoBrwCte_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Dim dSet As New DataSet
        dSet = ConContratoGiga(eContrato)
        rDocument.Load(RutaReportes + "\ReportConContrato.rpt")
        rDocument.SetDataSource(dSet)
        CrystalReportViewer1.ReportSource = rDocument
    End Sub

    Private Function ConContratoGiga(ByVal Contrato As Integer) As DataSet
        Dim listaTablas As New List(Of String)
        listaTablas.Add("ConContratoGiga")
        listaTablas.Add("DameDesglose_Contrato")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, CObj(Contrato))
        Return BaseII.ConsultaDS("ConContratoGiga", listaTablas)
    End Function
End Class
