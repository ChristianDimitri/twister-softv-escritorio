﻿Public Class BrwPortabilidad
    Dim Activo As Integer = 0
    Private Sub BrwPortabilidad_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'VariablesGlobales.Bandera = false;
        MUESTRAClientePortabilidad(0, 0, 0, 0, "")
    End Sub
    Private Sub MUESTRAClientePortabilidad(ByVal Op As Integer, ByVal Activo As Integer, ByVal Id As Integer, ByVal Contrato As Integer, ByVal Nombre As String)
        '[MUESTRAClientePortabilidad](@OP INT
        '			,@ACTIVO INT
        '			,@ID INT
        '			,@CONTRATO INT
        '			,@NOMBRE VARCHAR(250))
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@ACTIVO", SqlDbType.Int, Activo)
        BaseII.CreateMyParameter("@ID", SqlDbType.Int, Id)
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@NOMBRE", SqlDbType.VarChar, Nombre)
        dgvPortabilidad.DataSource = BaseII.ConsultaDT("MUESTRAClientePortabilidad")
    End Sub

    Private Sub rbAmbos_CheckedChanged(sender As Object, e As EventArgs) Handles rbAmbos.CheckedChanged
        If rbAmbos.Checked = True Then
            Activo = 0
            MUESTRAClientePortabilidad(1, Activo, 0, 0, "")
        End If
    End Sub

    Private Sub rbActivos_CheckedChanged(sender As Object, e As EventArgs) Handles rbActivos.CheckedChanged
        If rbActivos.Checked = True Then
            Activo = 1
            MUESTRAClientePortabilidad(1, Activo, 0, 0, "")
        End If
    End Sub

    Private Sub rbPendientes_CheckedChanged(sender As Object, e As EventArgs) Handles rbPendientes.CheckedChanged
        If rbPendientes.Checked = True Then
            Activo = 2
            MUESTRAClientePortabilidad(1, Activo, 0, 0, "")
        End If
    End Sub

    Private Sub bnBuscarFolio_Click(sender As Object, e As EventArgs) Handles bnBuscarFolio.Click
        If tbBuscarFolio.Text.Length > 0 Then
            If IsNumeric(tbBuscarFolio.Text) = False Then
                MessageBox.Show("Captura un Folio válido.")
                tbBuscarFolio.Text = ""
            End If
            MUESTRAClientePortabilidad(2, Activo, Int32.Parse(tbBuscarFolio.Text), 0, "")
        End If
    End Sub

    Private Sub bnBuscarContrato_Click(sender As Object, e As EventArgs) Handles bnBuscarContrato.Click
        If tbBuscarContrato.Text.Length > 0 Then
            If IsNumeric(tbBuscarContrato.Text) = False Then
                MessageBox.Show("Captura un Contrato válido.")
            End If
            MUESTRAClientePortabilidad(3, Activo, 0, Int32.Parse(tbBuscarContrato.Text), "")
        End If
    End Sub

    Private Sub bnBuscarNombre_Click(sender As Object, e As EventArgs) Handles bnBuscarNombre.Click
        If tbBuscarNombre.Text.Length > 0 Then
            MUESTRAClientePortabilidad(4, Activo, 0, 0, tbBuscarNombre.Text)
        End If
    End Sub

    Private Sub bnNuevo_Click(sender As Object, e As EventArgs) Handles bnNuevo.Click
        opcion = "N"
        gloClave = 0
        FrmPortabilidad.Show()
    End Sub

    Private Sub bnConsultar_Click(sender As Object, e As EventArgs) Handles bnConsultar.Click
        If dgvPortabilidad.Rows.Count = 0 Then
            MessageBox.Show("Selecciona un registro a Consultar.")
        Else
            opcion = "C"
            gloClave = Int32.Parse(dgvPortabilidad.SelectedCells(0).Value.ToString())
            FrmPortabilidad.Show()
        End If
    End Sub

    Private Sub bnModificar_Click(sender As Object, e As EventArgs) Handles bnModificar.Click
        If dgvPortabilidad.Rows.Count = 0 Then
            MessageBox.Show("Selecciona un registro a Modificar.")
        Else
            opcion = "M"
            gloClave = Int32.Parse(dgvPortabilidad.SelectedCells(0).Value.ToString())
            FrmPortabilidad.Show()
        End If
    End Sub

    Private Sub bnImprimir_Click(sender As Object, e As EventArgs) Handles bnImprimir.Click
        If dgvPortabilidad.Rows.Count = 0 Then
            MessageBox.Show("Selecciona un registro para imprimir el Formato de Solicitud de Portabilidad Numérica.")
        Else
            gloClave = Int32.Parse(dgvPortabilidad.SelectedCells(0).Value.ToString())
            eOpPPE = 5
            FrmImprimirPPE.Show()
        End If
    End Sub

    Private Sub bnSalir_Click(sender As Object, e As EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class