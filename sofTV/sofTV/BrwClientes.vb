﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Reflection
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System
Imports System.IO.StreamReader
Imports System.IO.File

Public Class BrwClientes
    Private customersByCityReport As ReportDocument
    Private LocNomImpresora_Contratos As String = Nothing
    Private LocNomImpresora_Tarjetas As String = Nothing
    Private errorfactura As Integer = 0
    Private clv_empresa As String = Nothing

    Private eClv_Id As Integer = 0
    Private eNombreBD As String = Nothing
    Private eClave_Txt As String = Nothing
    Private eCiudad As String = Nothing



    Private Sub valida_factura(ByVal contrato As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(contrato) = True And contrato > 0 Then
            Me.Valida_si_facturoTableAdapter.Connection = CON

            Me.Valida_si_facturoTableAdapter.Fill(Me.DataSetarnoldo.Valida_si_facturo, contrato, errorfactura)
            If IdSistema = "TO" Then
                If errorfactura > 0 Then
                    Me.Button10.Visible = True
                Else
                    Me.Button10.Visible = False
                End If
            End If
        End If
        CON.Close()
    End Sub

    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Dim contrato1 As Long
        Dim CON As New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("BUSCLIPORCONTRATO2", CON)
        com.CommandTimeout = 0
        Dim tabla As DataTable = New DataTable
        Dim bs As BindingSource = New BindingSource
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        If OP = 0 Then
            If IsNumeric(Me.bcONTRATO.Text) = True Then
                contrato1 = Long.Parse(Me.bcONTRATO.Text)
            Else
                MsgBox("El contrato introducido no es correcto, favor de verificarlo", MsgBoxStyle.Information)
            End If
        End If
        Try
            'CON.Open()
            'If OP = 0 Then
            '    If IsNumeric(Me.bcONTRATO.Text) = True Then
            '        Me.BUSCLIPORCONTRATO2TableAdapter1.Connection = CON
            '        Me.BUSCLIPORCONTRATO2TableAdapter1.Fill(Me.Procedimientosarnoldo4.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(bcONTRATO.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
            '    Else
            '        Me.BUSCLIPORCONTRATO2TableAdapter1.Connection = CON
            '        Me.BUSCLIPORCONTRATO2TableAdapter1.Fill(Me.Procedimientosarnoldo4.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
            '    End If
            '    Me.bcONTRATO.Clear()
            'ElseIf OP = 1 Then
            '    Me.BUSCLIPORCONTRATO2TableAdapter1.Connection = CON
            '    Me.BUSCLIPORCONTRATO2TableAdapter1.Fill(Me.Procedimientosarnoldo4.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(0, Long)), (CType(Me.BNOMBRE.Text, String)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
            '    'Me.BNOMBRE.Clear()
            'ElseIf OP = 2 Then
            '    Me.BUSCLIPORCONTRATO2TableAdapter1.Connection = CON
            '    Me.BUSCLIPORCONTRATO2TableAdapter1.Fill(Me.Procedimientosarnoldo4.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType(Me.BCALLE.Text, String)), (CType(Me.BNUMERO.Text, String)), (CType(Me.BCIUDAD.Text, String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
            '    'Me.BCALLE.Clear()
            '    'Me.BNUMERO.Clear()
            '    'Me.BCIUDAD.Clear()
            'Else
            '    Me.BUSCLIPORCONTRATO2TableAdapter1.Connection = CON
            '    Me.BUSCLIPORCONTRATO2TableAdapter1.Fill(Me.Procedimientosarnoldo4.BUSCLIPORCONTRATO2, New System.Nullable(Of Long)(CType(0, Long)), "", "", "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)))
            '    'Me.BCALLE.Clear()
            '    'Me.BNUMERO.Clear()
            '    'Me.BCIUDAD.Clear()
            'End If
            com.CommandType = CommandType.StoredProcedure
            com.Parameters.Add(New SqlParameter("@CONTRATO", contrato1))
            com.Parameters.Add(New SqlParameter("@NOMBRE", Me.BNOMBRE.Text.ToString))
            com.Parameters.Add(New SqlParameter("@CALLE", Me.BCALLE.Text.ToString))
            com.Parameters.Add(New SqlParameter("@NUMERO", Me.BNUMERO.Text.ToString))
            com.Parameters.Add(New SqlParameter("@CIUDAD", Me.BCIUDAD.Text.ToString))
            com.Parameters.Add(New SqlParameter("@Telefono", Me.txtTelefono.Text.ToString))
            com.Parameters.Add(New SqlParameter("@OP", OP))
            Try
                CON.Open()
                da.Fill(tabla)
                bs.DataSource = tabla
                Me.DataGridView1.DataSource = bs
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            Finally
                CON.Close()
            End Try
            'CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        Me.Refresh()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            Contrato = 0
            OpcionCli = "N"
            FrmClientes.Show()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.BUSCACLIENTES(0)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Me.BUSCACLIENTES(1)
    End Sub

    Private Sub BrwClientes_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim valida As Integer = 0
        Dim clave As Long = 0
        Dim comando As New SqlClient.SqlCommand
        Dim comando2 As New SqlClient.SqlCommand
        Dim comando3 As New SqlClient.SqlCommand
        Dim ConLidia As New SqlClient.SqlConnection(MiConexion)
        Dim Contrato As Integer = Nothing
        If Me.DataGridView1.Rows.Count > 0 Then
            Me.CONTRATOLabel1.Text = Me.DataGridView1.SelectedCells.Item(0).Value()
            Contrato = Me.DataGridView1.SelectedCells.Item(0).Value()
        End If

        

        'Me.CONTRATOLabel1.Text = Me.DataGridView1.SelectedCells.Item(0).Value()
        'Contrato = Me.DataGridView1.SelectedCells.Item(0).Value()
        If GloBnd = True Then
            GloBnd = False
            Me.BUSCACLIENTES(3)
            If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
                valida_factura(CInt(Me.CONTRATOLabel1.Text))
            End If
        End If
        If bndcontt = True Then
            bndcontt = False
            ConfigureCrystalReportsContratoTomatlan(clv_empresa)
            valida = MsgBox("Voltee la hoja Para Continuar la Impresión", MsgBoxStyle.YesNo, "Pausa")
            If valida = 6 Then
                ConfigureCrystalReportsContratoTomatlan2(clv_empresa)
            ElseIf valida = 7 Then
                MsgBox("No se continuo con la Impresion", MsgBoxStyle.Information)
            End If
            MsgBox("Se Imprimio con Exito", MsgBoxStyle.Information)
        End If
        Dim Clv_Cita As Long
        If Len(Trim(FechaAgenda)) > 0 And Len(Trim(HoraAgenda)) Then
            'Me.NUE_CITASTableAdapter.Connection = CON
            'Me.NUE_CITASTableAdapter.Fill(Me.NewSofTvDataSet.NUE_CITAS, Me.Tecnicos.SelectedValue, New System.Nullable(Of Date)(CType(FechaAgenda, Date)), New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), "", "", "Q", Clv_Cita)
            'Me.CONDetCitasTableAdapter.Connection = CON
            'Me.CONDetCitasTableAdapter.Delete(Clv_Cita)
            'Me.CONDetCitasTableAdapter.Connection = CON
            'Me.CONDetCitasTableAdapter.Insert(Clv_HoraAgenda, Clv_Cita, clave)
            'Me.NUEREL_CITAS_QUEJASTableAdapter.Connection = CON
            'Me.NUEREL_CITAS_QUEJASTableAdapter.Fill(Me.NewSofTvDataSet.NUEREL_CITAS_QUEJAS, New System.Nullable(Of Long)(CType(Clv_Cita, Long)), New System.Nullable(Of Long)(CType(Me.Clv_quejaTextBox.Text, Long)))
            '[NUE_CITAS] (@Clv_Tecnico INT, @Fecha DATETIME, @Contrato BIGINT, @Descripcion_corta VARCHAR(50), @Descripcion VARCHAR(300), @Queja_o_Orden_o_Otro VARCHAR(1),@Clv_Cita BIGINT OUTPUT)	AS
            ConLidia.Open()
            With comando2
                .CommandText = "Nue_Citas"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = ConLidia

                Dim prm As New SqlParameter("@Clv_tecnico", SqlDbType.Int)
                Dim prm2 As New SqlParameter("@Fecha", SqlDbType.DateTime)
                Dim prm3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
                Dim prm4 As New SqlParameter("@Descripcion_corta", SqlDbType.VarChar, 50)
                Dim prm5 As New SqlParameter("@Descripcion", SqlDbType.VarChar, 300)
                Dim prm6 As New SqlParameter("@Queja_o_Orden_o_Otro", SqlDbType.VarChar, 1)
                Dim prm8 As New SqlParameter("@Clv_cita", SqlDbType.BigInt)

                prm.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Input
                prm4.Direction = ParameterDirection.Input
                prm5.Direction = ParameterDirection.Input
                prm6.Direction = ParameterDirection.Input
                prm8.Direction = ParameterDirection.Output

                prm.Value = GloClv_tecnico
                prm2.Value = FechaAgenda
                prm3.Value = CLng(Me.DataGridView1.SelectedCells.Item(0).Value())
                prm4.Value = "Acometida"
                prm5.Value = "Instalación Acometida"
                prm6.Value = "O"
                prm8.Value = 0
                .Parameters.Add(prm)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                .Parameters.Add(prm4)
                .Parameters.Add(prm5)
                .Parameters.Add(prm6)
                .Parameters.Add(prm8)
                Dim i As Integer = .ExecuteNonQuery
                Clv_Cita = prm8.Value

            End With
            With comando
                .CommandText = "NUEDETCITAS"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = ConLidia
                '(@CLV_HORA INT,@CLV_CITA INT ,@CLAVE INT OUTPUT) 
                Dim prm As New SqlParameter("@Clv_Hora", SqlDbType.Int)
                Dim Prm2 As New SqlParameter("@Clv_Cita", SqlDbType.Int)
                Dim Prm3 As New SqlParameter("Clave", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                Prm2.Direction = ParameterDirection.Input
                Prm3.Direction = ParameterDirection.Output
                prm.Value = Clv_HoraAgenda
                Prm2.Value = Clv_Cita
                Prm3.Value = 0
                .Parameters.Add(prm)
                .Parameters.Add(Prm2)
                .Parameters.Add(Prm3)
                Dim i As Integer = .ExecuteNonQuery
                clave = Prm3.Value
            End With
            NUEMOVREL_CITAS(Clv_Cita, ComentarioAgenda)
            'With comando3
            '    .CommandText = "NUEREL_CITAS"
            '    .CommandTimeout = 0
            '    .CommandType = CommandType.StoredProcedure
            '    .Connection = ConLidia
            '    Dim prm As New SqlParameter("@Clv_Cita", SqlDbType.Int)
            '    Dim Prm2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
            '    prm.Direction = ParameterDirection.Input
            '    Prm2.Direction = ParameterDirection.Input
            '    prm.Value = Clv_Cita
            '    prm.Value = 0 'clv_orden
            '    .Parameters.Add(prm)
            '    .Parameters.Add(Prm2)

            'End With
            '[NUEREL_CITAS_ORDENES](@CLV_CITA BIGINT,@CLV_ORDEN BIGINT)
            ConLidia.Close()
            FechaAgenda = ""
            HoraAgenda = ""
            ComentarioAgenda = ""
            If LocBndAgendaClientes = False Then
                Impresion_Contrato()
            ElseIf LocBndAgendaClientes = True Then
                LocBndAgendaClientes = False
            End If
        End If
    End Sub

    Private Sub BrwClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        Dim CON As New SqlConnection(MiConexion)
        Dim Comando As SqlClient.SqlCommand
        CON.Open()

        'Me.ConGeneralRelBDTableAdapter.Connection = CON
        'Me.ConGeneralRelBDTableAdapter.Fill(Me.DataSetEric.ConGeneralRelBD)

        Try
            Comando = New SqlClient.SqlCommand
            With Comando
                .Connection = CON
                .CommandText = "ConGeneralRelBD"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                '' Create a SqlParameter for each parameter in the stored procedure.
                Dim prm As New SqlParameter("@Clv_Id", SqlDbType.Int)
                Dim prm1 As New SqlParameter("@NombreBD", SqlDbType.VarChar, 250)
                Dim prm2 As New SqlParameter("@Clave_Txt", SqlDbType.VarChar, 50)
                Dim prm3 As New SqlParameter("@Ciudad", SqlDbType.VarChar, 250)

                prm.Direction = ParameterDirection.Output
                prm1.Direction = ParameterDirection.Output
                prm2.Direction = ParameterDirection.Output
                prm3.Direction = ParameterDirection.Output

                prm.Value = 0
                prm1.Value = " "
                prm2.Value = " "
                prm3.Value = " "
                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                Dim i As Integer = Comando.ExecuteNonQuery()
                eClv_Id = prm.Value
                eNombreBD = prm1.Value
                eClave_Txt = prm2.Value
                eCiudad = prm3.Value
            End With
        Catch
        End Try


        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)

        'MsgBox(Me.DataGridView1.SelectedCells.ToString)




        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla según sea necesario.
        Me.MUESTRACALLESTableAdapter.Connection = CON
        Me.MUESTRACALLESTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACALLES)
        Me.BUSCACLIENTES(3)
        Me.Selecciona_Impresora_SucursalTableAdapter.Connection = CON
        Me.Selecciona_Impresora_SucursalTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora_Sucursal, GloClv_Sucursal, LocNomImpresora_Tarjetas, LocNomImpresora_Contratos)
        LocNomImpresora_Contratos = "ORDENES"              'recuerda comentra esta linea y descomentar la de abajo
        'ImpresoraContatos = LocNomImpresora_Contratos
        If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
            valida_factura(CInt(Me.CONTRATOLabel1.Text))
        End If
        Me.Button11.Visible = False
        If IdSistema = "SA" Or IdSistema = "VA" Then
            Me.Button11.Visible = True
        End If
        If IdSistema = "VA" Then
            Me.Button13.Visible = True
        End If
        CON.Close()
        frmctr = New FrmCtrl_ServiciosCli
        frmTelefonia = New FrmClientesTel
        frmInternet2 = New FrmInternet
        frmctr.MdiParent = FrmClientes
        frmInternet2.MdiParent = FrmClientes
        'frmctr.WindowState = FormWindowState.Normal
        frmInternet2.Show()
        frmInternet2.Hide()
        frmctr.Show()
        frmctr.Hide()





    End Sub

    Private Sub bcONTRATO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles bcONTRATO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(0)
        End If
    End Sub

    Private Sub bcONTRATO_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bcONTRATO.TextChanged

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Me.BUSCACLIENTES(2)
    End Sub

    Private Sub BNOMBRE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNOMBRE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(1)
            'Me.BNOMBRE.Text = ""
        End If
    End Sub

    Private Sub BNOMBRE_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BNOMBRE.TextChanged

    End Sub

    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub

    Private Sub BCALLE_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCALLE.TextChanged

    End Sub

    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub

    Private Sub BNUMERO_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BNUMERO.TextChanged

    End Sub

    Private Sub BCIUDAD_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCIUDAD.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub




    Private Sub CONTRATOLabel1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CONTRATOLabel1.TextChanged
        Try
            CREAARBOL()

            If Me.CONTRATOLabel1.Text <> "" Then
                valida_factura(CInt(Me.CONTRATOLabel1.Text))
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub SOLOINTERNETCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SOLOINTERNETCheckBox.CheckedChanged
        If Me.SOLOINTERNETCheckBox.Checked = False Then
            Me.SOLOINTERNETCheckBox.Enabled = False
        Else
            Me.SOLOINTERNETCheckBox.Enabled = True
        End If
    End Sub

    Private Sub ESHOTELCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ESHOTELCheckBox.CheckedChanged
        If Me.ESHOTELCheckBox.Checked = False Then
            Me.ESHOTELCheckBox.Enabled = False
        Else
            Me.ESHOTELCheckBox.Enabled = True
        End If
    End Sub

    Private Sub ServicioListBox_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)

    End Sub

    Private Sub ServicioListBox_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Me.bcONTRATO.Focus()
    End Sub

    Private Sub ServicioListBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ESHOTELLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub SOLOINTERNETLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub

    Private Sub consultar()
        Try
            'Me.DataGridView1.SelectedCells.Item(0).Value()
            If IsNumeric(Me.DataGridView1.SelectedCells.Item(0).Value()) = True Then
                Contrato = Me.DataGridView1.SelectedCells.Item(0).Value()
                OpcionCli = "C"
                FrmClientes.Show()
            Else
                MsgBox("Seleccione un Cliente para poder Consultar ", MsgBoxStyle.Information)
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        consultar()
    End Sub

    Private Sub modificar()
        Try
            If IsNumeric(Me.DataGridView1.SelectedCells.Item(0).Value()) = True Then
                Contrato = Me.DataGridView1.SelectedCells.Item(0).Value()
                OpcionCli = "M"
                Locbndvalcliente = True
                FrmClientes.Show()

            Else
                MsgBox("Seleccione un Cliente para poder Modificar ", MsgBoxStyle.Information)
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        modificar()
    End Sub

    Private Sub CREAARBOL()

        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.CONTRATOLabel1.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False

            Dim PasaJNet As Boolean = False
            Dim jNet As Integer = -1
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisión Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1

                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Teléfonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then
                    I = I + 1
                    pasa = False
                End If

            Next
            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
            CON.Close()
            'Me.TreeView1.Nodes(0).ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub CREAARBOL2()

        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' Console.WriteLine(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.CONTRATOLabel1.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If



            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                Me.TreeView1.Nodes.Add(Trim(FilaRow("Servicio").ToString()))
                I += 1
            Next



            CON.Close()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Me.CONTRATOLabel1.Text = Me.DataGridView1.SelectedCells.Item(0).Value()
    End Sub




    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        If LocNomImpresora_Tarjetas = "" Then
            MsgBox("No se ha asignado una Impresora de Tarjetas a esta Sucursal", MsgBoxStyle.Information)
        Else
            LocTarjNo_Contrato = CLng(Me.CONTRATOLabel1.Text)
            'LocGloOpRep = 1
            ConfigureCrystalReports1(LocTarjNo_Contrato)
            MsgBox("Se Imprimio con Exito", MsgBoxStyle.Information)
            'FrmImprimirFac.Show()
        End If
    End Sub

    Private Sub ConfigureCrystalReports1(ByVal No_Contrato As Integer)

        ' Dim impresora As String = nothing

        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Contrato_txt As String = Nothing
        Dim reportPath As String = Nothing


        reportPath = RutaReportes + "\Tarjetas.rpt"

        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        '@No_Contrato 
        Contrato_txt = CStr(No_Contrato)
        If Len(Contrato_txt) = 1 Then
            Contrato_txt = "00" & Contrato_txt
        ElseIf Len(Contrato_txt) = 2 Then
            Contrato_txt = "0" & Contrato_txt

        End If
        customersByCityReport.SetParameterValue(0, No_Contrato)

        mySelectFormula = "*" & Contrato_txt & "*"
        customersByCityReport.DataDefinition.FormulaFields("codigo").Text = "'" & mySelectFormula & "'"
        ' Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 3, impresora)

        customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Tarjetas
        customersByCityReport.PrintToPrinter(1, True, 1, 1)


        'customersByCityReport.PrintOptions.PrinterName = "Datacard Printer"






    End Sub

    Private Sub ConfigureCrystalReportsBoleta(ByVal No_Contrato As Long)

        ' Dim impresora As String = nothing

        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Contrato_txt As String = Nothing
        Dim reportPath As String = Nothing


        reportPath = RutaReportes + "\Boletas_de_Pago.rpt"

        customersByCityReport.Load(reportPath)

        SetDBLogonForReport(connectionInfo, customersByCityReport)
        '@No_Contrato 
        customersByCityReport.SetParameterValue(0, No_Contrato)
        Dim MIcIUDAD As String = Nothing
        MIcIUDAD = GloSucursal
        customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

        customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos

        customersByCityReport.PrintToPrinter(1, True, 1, 0)

    End Sub
    Private Sub ConfigureCrystalReportsBoletaTVS(ByVal No_Contrato As Long)
        ' Dim impresora As String = nothing

        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Contrato_txt As String = Nothing
        Dim reportPath As String = Nothing


        reportPath = RutaReportes + "\ReportTalonPagosTVS.rpt"

        customersByCityReport.Load(reportPath)

        SetDBLogonForReport(connectionInfo, customersByCityReport)
        '@No_Contrato 
        customersByCityReport.SetParameterValue(0, No_Contrato)

        customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos

        customersByCityReport.PrintToPrinter(1, True, 1, 0)
    End Sub



    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub
    Private Sub Imprime_Contrato()

    End Sub
    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Dim CON As New SqlConnection(MiConexion)
        Dim contador As Integer = 0
        eContrato = Me.DataGridView1.SelectedCells.Item(0).Value()
        CON.Open()
        'AQUI DEBE DE IR LO DE LA AGENDA
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        Dim comando As New SqlClient.SqlCommand
        conlidia.Open()
        With comando
            .CommandText = "Checar Citas"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = conlidia
            Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = Me.DataGridView1.SelectedCells.Item(0).Value()
            .Parameters.Add(prm)
            Dim prm1 As New SqlParameter("@Cont", SqlDbType.Int)
            prm1.Direction = ParameterDirection.Output
            prm1.Value = 0
            .Parameters.Add(prm1)
            Dim i As Integer = comando.ExecuteNonQuery
            contador = prm1.Value
        End With
        conlidia.Close()
        If contador = 0 Then
            FrmAgendaRapida.Show()
        ElseIf contador > 0 Then
            Me.Impresion_Contrato()
        End If




    End Sub
    Private Sub Impresion_Contrato()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim valida As Integer = 0
        Dim valida2 As Integer = 0
        Dim yes As Integer = 0
        Dim tv As Integer = 0, Int As Integer = 0, Dig As Integer = 0
        If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
            eContrato = Me.CONTRATOLabel1.Text
            Me.Dame_clv_EmpresaTableAdapter.Connection = CON
            Me.Dame_clv_EmpresaTableAdapter.Fill(Me.DataSetarnoldo.Dame_clv_Empresa, clv_empresa)



            If LocNomImpresora_Contratos = "" Then
                MsgBox("No se ha asignado una Impresora de Contratos a esta Sucursal", MsgBoxStyle.Information)
            Else
                If clv_empresa = "AG" Or clv_empresa = "SA" Or clv_empresa = "VA" Then
                    Me.ChecaServicioContratadoTableAdapter.Connection = CON
                    Me.ChecaServicioContratadoTableAdapter.Fill(Me.DataSetEDGAR.ChecaServicioContratado, CInt(Me.CONTRATOLabel1.Text), eErrorContrato)
                    If eErrorContrato = 0 Then

                        
                        FrmImprimirContratoBrwCte.Show()
                        
                    Else
                        MsgBox("El Cliente no cuenta con Servicios Asignados", , "Advertencia")
                    End If
                ElseIf clv_empresa = "TO" Then
                    Me.Valida_Contrato_ServTableAdapter.Connection = CON
                    Me.Valida_Contrato_ServTableAdapter.Fill(Me.DataSetarnoldo.Valida_Contrato_Serv, eContrato, eErrorContrato)
                    If eErrorContrato = 0 Then
                        My.Forms.FrmHorasInst.Show()
                    Else
                        MsgBox("El cliente no cuenta con servicios Asignados", , "Advertencia")
                    End If
                 
                End If
            End If
        Else
            MsgBox("No Se Ha Seleccionado Un Contrato ", MsgBoxStyle.Information)
        End If
        CON.Close()
    End Sub
    Private Sub ConfigureCrystalReportsContrato(ByVal Clv_Factura As Long)
        Dim impresora As String = Nothing
        'customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        Dim CON As New SqlConnection(MiConexion)
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        customersByCityReport = New ReportDocument
        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReportConContrato.rpt"
        'customersByCityReport.Load(reportPath)

        Dim CMD As New SqlCommand("ConContratoGiga", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@CONTRATO", eContrato)

        Dim DA As New SqlDataAdapter(cmd)
        Dim DS As New DataSet()

        DA.Fill(DS)

        DS.Tables(0).TableName = "ConContratoGiga"
        DS.Tables(1).TableName = "DameDesglose_Contrato"

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(ds)


        'Select Case Tipo
        '    Case 1
        '        LocTipo = "SERVICIOS DE TV POR CABLE"
        '    Case 2
        '        LocTipo = "SERVICIOS DE INTERNET"
        '    Case 3
        '        LocTipo = "SERVICIOS DE CANALES ADICIONALES"
        'End Select
        'customersByCityReport.DataDefinition.FormulaFields("Titulo2").Text = "'" & LocTipo & "'"
        customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
        ' customersByCityReport.PrintOptions.PrinterName = ""
        'customersByCityReport.PrintOptions.PrinterName = "TICKET"
        customersByCityReport.PrintToPrinter(2, True, 1, 1)
        'customersByCityReport = Nothing



        'customersByCityReport = Nothing


    End Sub
    Private Sub ConfigureCrystalReportsContratoTomatlan(ByVal clv_empresa As String)
        Try
            Dim impresora As String = Nothing
            Dim cmd As SqlClient.SqlCommand
            Dim horaini As String = Nothing
            Dim horafin As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\ContratoTomatlan.rpt"
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, eContrato)
            '@clv_empresa
            customersByCityReport.SetParameterValue(1, clv_empresa)


            'horaini = InputBox("Apartir de ", "Captura Hora")
            'horafin = InputBox("Capture la hora de la Instalación Final", "Captura Hora")



            'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)
            'If horaini = "" Then horaini = "0"
            'If horafin = "" Then horafin = "0"

            customersByCityReport.DataDefinition.FormulaFields("horaini").Text = "'" & horaini & "'"
            customersByCityReport.DataDefinition.FormulaFields("horafin").Text = "'" & horafin & "'"


            customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(1, True, 1, 1)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            With cmd
                .CommandText = "Hora_ins"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = CON
                Dim prm As New SqlParameter("@Hora_ini", SqlDbType.VarChar, 10)
                Dim prm1 As New SqlParameter("@Hora_fin", SqlDbType.VarChar, 10)
                Dim prm2 As New SqlParameter("@contrato", SqlDbType.BigInt)
                Dim prm3 As New SqlParameter("@observaciones", SqlDbType.VarChar)

                '@Hora_ini varchar(10),@Hora_fin varchar(10),@contrato bigint,@observaciones varchar(max)
                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Input

                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)

                prm.Value = horaini
                prm1.Value = horafin
                prm2.Value = eContrato
                prm3.Value = ""

                Dim i As Integer = .ExecuteNonQuery

            End With

            'Me.Hora_insTableAdapter.Connection = CON
            'Me.Hora_insTableAdapter.Fill(Me.DataSetarnoldo.Hora_ins, horaini, horafin, eContrato)
            Me.Inserta_Comentario2TableAdapter.Connection = CON
            Me.Inserta_Comentario2TableAdapter.Fill(Me.DataSetarnoldo.Inserta_Comentario2, eContrato)
            CON.Close()
            'customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ConfigureCrystalReportsContratoTomatlan2(ByVal clv_empresa As String)
        Try
            Dim impresora As String = Nothing
            Dim horaini As String = Nothing
            Dim horafin As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\TomatlanAtras.rpt"
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, eContrato)
            '@clv_empresa
            customersByCityReport.SetParameterValue(1, clv_empresa)





            customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(1, True, 1, 1)
            'customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReportsContratoSAComun(ByVal Tipo As Integer)
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            Dim LocTipo As String = Nothing
            If clv_empresa = "SA" Then
                reportPath = RutaReportes + "\ReportConContratoSA.rpt"
            ElseIf clv_empresa = "VA" Then
                reportPath = RutaReportes + "\ReportConContratoVA.rpt"
            End If
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, eContrato)
            '@TIpo
            customersByCityReport.SetParameterValue(1, Tipo)
            'CrystalReportViewer1.ReportSource = customersByCityReport
            'CrystalReportViewer1.ShowPrintButton = True

            'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)

            Select Case Tipo
                Case 1
                    LocTipo = "SERVICIOS DE TV POR CABLE"
                Case 2
                    LocTipo = "SERVICIOS DE INTERNET"
                Case 3
                    LocTipo = "SERVICIOS DE CANALES ADICIONALES"
            End Select
            customersByCityReport.DataDefinition.FormulaFields("Titulo2").Text = "'" & LocTipo & "'"

            customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(1, True, 1, 1)
            'customersByCityReport = Nothing


        Catch ex As Exception
            MsgBox("La Impresora Especificada no es válida (" & LocNomImpresora_Contratos & ")")
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsContratoSAAtras(ByVal op As Integer)
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing

            If clv_empresa = "SA" Then
                Select Case op
                    Case 1
                        reportPath = RutaReportes + "\ParteAtrasSATvcable.rpt"
                    Case 2
                        reportPath = RutaReportes + "\ParteAtrasIntRey.rpt"
                    Case 3
                        reportPath = RutaReportes + "\ParteAtrasTvDigRey.rpt"
                End Select
            ElseIf clv_empresa = "VA" Then
                'Select Case op
                'Case 1
                reportPath = RutaReportes + "\ParteAtrasVA.rpt"
                'Case 2
                'reportPath = RutaReportes + "\ParteAtrasIntVA.rpt"
                'Case 3
                'reportPath = RutaReportes + "\ParteAtrasDigVA.rpt"
                'End Select
            End If


            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, eContrato)
            'CrystalReportViewer1.ReportSource = customersByCityReport
            'CrystalReportViewer1.ShowPrintButton = True

            'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)


            customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(1, True, 1, 1)
            'customersByCityReport = Nothing


        Catch ex As Exception
            MsgBox("La Impresora Especificada no es válida (" & LocNomImpresora_Contratos & ")")
        End Try

    End Sub





    Private Sub CONTRATOLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONTRATOLabel1.Click

    End Sub


    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        Dim CON As New SqlConnection(MiConexion)

        CON.Open()
        Dim Status As String = Nothing
        Me.Dime_Si_BoletasTableAdapter.Connection = CON
        Me.Dime_Si_BoletasTableAdapter.Fill(Me.DataSetEdgarRev2.Dime_Si_Boletas, CLng(Me.CONTRATOLabel1.Text), Status)
        If Status = "I" Then
            If eClave_Txt = "TV" Then
                ConfigureCrystalReportsBoletaTVS(CLng(Me.CONTRATOLabel1.Text))
            Else
                ConfigureCrystalReportsBoleta(CLng(Me.CONTRATOLabel1.Text))
            End If

        Else
            MsgBox("El Cliente Tiene que estar con Status de Instalado ", MsgBoxStyle.Information)
        End If
        CON.Close()
    End Sub


    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        Dim I As Integer = 0
        Dim X As Integer = 0
        Dim Txt As String = Nothing
        Dim GLOBND As Boolean = True
        'Me.DameGeneralesBancosTableAdapter.Fill(Me.NewsoftvDataSet2.DameGeneralesBancos, "PR")

        If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
            Dim Nom_Archivo As String = Nothing
            Dim Encabezado As String = Nothing
            Dim imp1 As String = Nothing
            Dim Rutatxt As String = Nothing

            Dim Nom_ArchivoBat As String = Nothing
            Nom_ArchivoBat = "C:" + "\" + "ImprimeEtiqueta.bat"
            Dim fileExists2 As Boolean
            fileExists2 = My.Computer.FileSystem.FileExists(Nom_ArchivoBat)
            If fileExists2 = True Then
                File.Delete(Nom_ArchivoBat)
            End If
            Using sw2 As StreamWriter = File.CreateText(Nom_ArchivoBat)
                sw2.WriteLine("cd c:\")
                sw2.WriteLine("Print Etiqueta_1.txt > lpt1")
                sw2.Close()
            End Using
            'If (result = DialogResult.OK) Then
            Nom_Archivo = "C:" + "\" + "Etiqueta_1.txt"

            Dim fileExists As Boolean
            fileExists = My.Computer.FileSystem.FileExists(Nom_Archivo)
            If fileExists = True Then
                File.Delete(Nom_Archivo)
            End If
            Using sw As StreamWriter = File.CreateText(Nom_Archivo)
                'Dim FilaRow As DataRow
                'Me.CONSULTACNRTableAdapter.Fill(Me.DataSetLidia.CONSULTACNR)
                Dim NumeroAfiliacion As String = Nothing
                Dim ClaveBanco As String = Nothing
                Dim ReferenciaCliente As String = Nothing
                Dim NumeroTarjeta As String = Nothing
                Dim StDetalle As String = Nothing
                Dim StMonto As String = Nothing

                'Me.Reporte_TiposCliente_CiudadTableAdapter.Fill(Me.DataSetEdgarRev2.Reporte_TiposCliente_Ciudad, LocClv_session, GloClv_tipser2, LocBndC, LocBndB, LocBndI, LocBndD, LocBndS, LocBndF, LocBndDT, 1, LocValidaHab, LocPeriodo1, LocPeriodo2)
                'For Each FilaRow In Me.DataSetEdgarRev2.Reporte_TiposCliente_Ciudad.Rows
                'If FilaRow("Contrato".ToString()) Is Nothing Then
                'Exit For
                'End If
                sw.WriteLine("Q400,025")
                sw.WriteLine("q1200")
                sw.WriteLine("rN")
                sw.WriteLine("S4")
                sw.WriteLine("D7")
                sw.WriteLine("ZT")
                sw.WriteLine("JB")
                sw.WriteLine("OD")
                sw.WriteLine("R50,50")
                sw.WriteLine("N")
                sw.WriteLine("A0,0,0,5,1,1,N," & Chr(34) & Trim(Me.CONTRATOLabel1.Text) & Chr(34))
                sw.WriteLine("A500,0,0,5,1,1,N," & Chr(34) & Trim(Me.CONTRATOLabel1.Text) & Chr(34))
                sw.WriteLine("P1")
                sw.WriteLine("^@")

                'Next

                '    Txt = "save"
                '    sw.Write(Txt)
                sw.Close()
            End Using
            Dim myProcess As New Process()
            Dim myProcessStartInfo As New ProcessStartInfo("C:\\ImprimeEtiqueta.bat")
            'Dim myProcessStartInfo As New ProcessStartInfo("C:\\Print Etiqueta2.txt ", "> lpt1")
            myProcessStartInfo.WorkingDirectory = "C:\\"
            'C:\Program Files\Network Registrar\Local\bin
            myProcessStartInfo.UseShellExecute = False
            myProcessStartInfo.RedirectStandardOutput = True
            myProcess.StartInfo = myProcessStartInfo
            myProcess.Start()



        End If

    End Sub
    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        Dim ConLidia As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand
        Dim Status As String = Nothing
        ConLidia.Open()
        With cmd
            .CommandText = "Dime_si_Estado"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConLidia
            Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = Me.CONTRATOLabel1.Text
            .Parameters.Add(prm)

            Dim Prm2 As New SqlParameter("@Status", SqlDbType.VarChar, 1)
            Prm2.Direction = ParameterDirection.Output
            Prm2.Value = ""
            .Parameters.Add(Prm2)

            Dim i As Integer = .ExecuteNonQuery
            Status = Prm2.Value
        End With

        ConLidia.Close()
        If Status = "I" Then
            LiContrato = Me.CONTRATOLabel1.Text
            ImpresoraEstado = LocNomImpresora_Contratos
            FrmNumMeses.Show()
        Else
            MsgBox("El Cliente tiene que Tener Status Instalado", MsgBoxStyle.Information)
        End If

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub btnBtelefono_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBtelefono.Click
        Me.BUSCACLIENTES(4)
    End Sub

    Private Sub txtTelefono_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTelefono.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(4)
        End If
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Try
            Dim row As Integer = Convert.ToInt32(Me.DataGridView1.CurrentRow.Index.ToString)
            Me.NOMBRELabel1.Text = Me.DataGridView1.Rows(row).Cells(1).Value.ToString
            Me.CALLELabel1.Text = Me.DataGridView1.Rows(row).Cells(2).Value.ToString
            Me.NUMEROLabel1.Text = Me.DataGridView1.Rows(row).Cells(3).Value.ToString
            Me.CALLELabel1.Text = Me.DataGridView1.Rows(row).Cells(4).Value.ToString
            Me.COLONIALabel1.Text = Me.DataGridView1.Rows(row).Cells(5).Value.ToString
            Me.SOLOINTERNETCheckBox.Checked = Convert.ToBoolean(Convert.ToInt32(Me.DataGridView1.Rows(row).Cells(6).Value.ToString))
            Me.ESHOTELCheckBox.Checked = Convert.ToBoolean(Convert.ToInt32(Me.DataGridView1.Rows(row).Cells(7).Value.ToString))
        Catch

        End Try
    End Sub

    Private Sub DataGridView1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.LostFocus
        Me.CONTRATOLabel1.Text = Me.DataGridView1.SelectedCells.Item(0).Value()
    End Sub
End Class