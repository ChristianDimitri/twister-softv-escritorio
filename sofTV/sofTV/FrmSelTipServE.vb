Imports System.Data.SqlClient
Public Class FrmSelTipServE


    Private Sub FrmSelTipServE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraTipServEricTableAdapter.Connection = CON
        Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric.MuestraTipServEric, 0, 0)
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.ConceptoComboBox.SelectedValue > 0 Then
            eTipSer = Me.ConceptoComboBox.SelectedValue
            eServicio = Me.ConceptoComboBox.Text
            'If eBndContratoF = True Then
            '    eBndContratoF = False
            '    FrmImprimirComision.Show()
            'Else
            If eOpVentas = 61 And eOpPPE = 0 Then
                FrmImprimirComision.Show()
            Else
                FrmSelServicioE.Show()
            End If

            'End If
            Me.Close()
        End If
    End Sub
End Class