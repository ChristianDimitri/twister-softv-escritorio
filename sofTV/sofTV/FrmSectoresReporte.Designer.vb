﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSectoresReporte
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.DescripcionListBox1 = New System.Windows.Forms.ListBox()
        Me.DescripcionListBox = New System.Windows.Forms.ListBox()
        Me.Aceptar = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(621, 389)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 23
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'DescripcionListBox1
        '
        Me.DescripcionListBox1.DisplayMember = "Descripcion"
        Me.DescripcionListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionListBox1.FormattingEnabled = True
        Me.DescripcionListBox1.ItemHeight = 15
        Me.DescripcionListBox1.Location = New System.Drawing.Point(412, 20)
        Me.DescripcionListBox1.Name = "DescripcionListBox1"
        Me.DescripcionListBox1.Size = New System.Drawing.Size(276, 319)
        Me.DescripcionListBox1.TabIndex = 22
        Me.DescripcionListBox1.ValueMember = "ClvSector"
        '
        'DescripcionListBox
        '
        Me.DescripcionListBox.DisplayMember = "Descripcion"
        Me.DescripcionListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionListBox.FormattingEnabled = True
        Me.DescripcionListBox.ItemHeight = 15
        Me.DescripcionListBox.Location = New System.Drawing.Point(28, 20)
        Me.DescripcionListBox.Name = "DescripcionListBox"
        Me.DescripcionListBox.Size = New System.Drawing.Size(276, 319)
        Me.DescripcionListBox.TabIndex = 21
        Me.DescripcionListBox.ValueMember = "ClvSector"
        '
        'Aceptar
        '
        Me.Aceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Aceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Aceptar.Location = New System.Drawing.Point(479, 389)
        Me.Aceptar.Name = "Aceptar"
        Me.Aceptar.Size = New System.Drawing.Size(136, 36)
        Me.Aceptar.TabIndex = 20
        Me.Aceptar.Text = "&ACEPTAR"
        Me.Aceptar.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(329, 274)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(64, 25)
        Me.Button4.TabIndex = 19
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(329, 89)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(64, 25)
        Me.Button1.TabIndex = 16
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(329, 120)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(64, 25)
        Me.Button2.TabIndex = 17
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(329, 243)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(64, 25)
        Me.Button3.TabIndex = 18
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'FrmSectoresReporte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 445)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.DescripcionListBox1)
        Me.Controls.Add(Me.DescripcionListBox)
        Me.Controls.Add(Me.Aceptar)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button3)
        Me.Name = "FrmSectoresReporte"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmSectoresReporte"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents DescripcionListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents DescripcionListBox As System.Windows.Forms.ListBox
    Friend WithEvents Aceptar As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
End Class
