﻿Imports System.Data.SqlClient
Imports System.Text

Public Class FrmPaqueteServicios

    Private Sub ConPaqueteServicios()

        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ConPaqueteServicios")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            dgw.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub NuePaqueteServicios(ByVal Numero As Integer, ByVal LetraPaquete As String, ByVal TvAdicional As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NuePaqueteServicios", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Numero", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Numero
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@LETRAPAQUETE", SqlDbType.VarChar, 1)
        par2.Direction = ParameterDirection.Input
        par2.Value = LetraPaquete
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@TVADICIONAL", SqlDbType.Int)
        par3.Direction = ParameterDirection.Input
        par3.Value = TvAdicional
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@IdPaquete", SqlDbType.Int)
        par4.Direction = ParameterDirection.Output
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@Res", SqlDbType.Int)
        par5.Direction = ParameterDirection.Output
        comando.Parameters.Add(par5)

        Dim par6 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par6.Direction = ParameterDirection.Output
        comando.Parameters.Add(par6)

        Try
            eIdPaquete = 0
            eRes = 0
            eMsj = ""

            conexion.Open()
            comando.ExecuteNonQuery()

            eIdPaquete = par4.Value
            eRes = par5.Value
            eMsj = par6.Value

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueDetPaqueteServicios(ByVal IdPaquete As Integer, ByVal Clv_Servicio As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDetPaqueteServicios", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@IdPaquete", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = IdPaquete
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Servicio
        comando.Parameters.Add(par2)


        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub BorPaqueteServicios(ByVal IdPaquete As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorPaqueteServicios", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@IdPaquete", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = IdPaquete
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Res", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            eRes = 0
            eMsj = ""
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = par2.Value
            eMsj = par3.Value


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ValidaPaqueteServicios(ByVal IdPaquete)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaPaqueteServicios", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@IdPaquete", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = IdPaquete
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Res", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            eRes = 0
            eMsj = ""
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = par2.Value
            eMsj = par3.Value


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub MuestraServiciosDigital()
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC MuestraServiciosEric 3,0,3")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            cbServicioDig.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub MuestraServiciosInternet()
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC MuestraServiciosEric 2,0,3")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            cbServicioNet.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub MuestraServiciosDigitalAdi()
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC MuestraServiciosEric 3,0,4")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            dgwAdi.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub FrmPaqueteServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        MuestraServiciosDigital()
        MuestraServiciosDigitalAdi()
        MuestraServiciosInternet()
        ConPaqueteServicios()

    End Sub

    Private Sub tsbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbGuardar.Click

        If tbNumero.Text.Length = 0 Then
            MsgBox("Captura el número", MsgBoxStyle.Information)
            Exit Sub
        End If

        If tbLetraPaquete.Text.Length = 0 Then
            MsgBox("Captura la letra de paquete.", MsgBoxStyle.Information)
            Exit Sub
        End If

        NuePaqueteServicios(tbNumero.Text, tbLetraPaquete.Text, nudTvAdic.Value)

        If eRes = 1 Then
            MsgBox(eMsj, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If cbServicioDig.SelectedValue > 0 Then NueDetPaqueteServicios(eIdPaquete, cbServicioDig.SelectedValue)
        If cbServicioNet.SelectedValue > 0 Then NueDetPaqueteServicios(eIdPaquete, cbServicioNet.SelectedValue)

        Dim i As Integer

        For i = 0 To dgwAdi.Rows.Count - 1
            If dgwAdi.Rows(i).Cells(2).Value = True Then
                NueDetPaqueteServicios(eIdPaquete, dgwAdi.Rows(i).Cells(0).Value)
            End If
        Next

        ValidaPaqueteServicios(eIdPaquete)

        If eRes = 1 Then
            MsgBox(eMsj, MsgBoxStyle.Exclamation)
        Else
            MsgBox(mensaje5, MsgBoxStyle.Information)
        End If


        tbNumero.Text = ""
        tbLetraPaquete.Text = ""
        nudTvAdic.Value = 0

        MuestraServiciosDigital()
        MuestraServiciosDigitalAdi()
        MuestraServiciosInternet()
        ConPaqueteServicios()

    End Sub

    Private Sub bnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnEliminar.Click

        If dgw.Rows.Count = 0 Then
            MsgBox("Selecciona un paquete.", MsgBoxStyle.Information)
            Exit Sub
        End If

        BorPaqueteServicios(dgw.SelectedCells(0).Value)

        If eRes = 1 Then
            MsgBox(eMsj, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        ConPaqueteServicios()

    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class