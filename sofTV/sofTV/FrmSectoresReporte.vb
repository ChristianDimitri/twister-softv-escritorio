﻿Public Class FrmSectoresReporte

    Private Sub FrmSectoresReporte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        UspNuevosSectores()
        UspMostrarSectoresTMP()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        UspInsertaUnSector(Me.DescripcionListBox.SelectedValue)
        UspMostrarSectoresTMP()
        UspMostrarSectores()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        UspInsertaTodosSectores()
        UspMostrarSectoresTMP()
        UspMostrarSectores()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        UspDeseleccionarUnSector(Me.DescripcionListBox1.SelectedValue)
        UspMostrarSectoresTMP()
        UspMostrarSectores()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        UspDeseleccionarTodosSectores()
        UspMostrarSectoresTMP()
        UspMostrarSectores()
    End Sub

    Private Sub UspNuevosSectores()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, eClv_Session)
            BaseII.Inserta("UspNuevosSectores")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMostrarSectoresTMP()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, eClv_Session)
            DescripcionListBox.DataSource = BaseII.ConsultaDT("UspMostrarSectoresTMP")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMostrarSectores()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, eClv_Session)
            DescripcionListBox1.DataSource = BaseII.ConsultaDT("UspMostrarSectores")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspInsertaTodosSectores()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, eClv_Session)
            BaseII.Inserta("UspInsertaTodosSectores")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub UspDeseleccionarTodosSectores()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, eClv_Session)
            BaseII.Inserta("UspDeseleccionarTodosSectores")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspInsertaUnSector(ByVal PRMCLVSECTOR As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, eClv_Session)
            BaseII.CreateMyParameter("@CLVSECTOR", SqlDbType.Int, PRMCLVSECTOR)
            BaseII.Inserta("UspInsertaUnSector")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspDeseleccionarUnSector(ByVal PRMCLVSECTOR As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, eClv_Session)
            BaseII.CreateMyParameter("@CLVSECTOR", SqlDbType.Int, PRMCLVSECTOR)
            BaseII.Inserta("UspDeseleccionarUnSector")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub




    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Locbndpen1 = False
        Me.Close()
    End Sub

    Private Sub Aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Aceptar.Click
        Me.Close()
    End Sub
End Class