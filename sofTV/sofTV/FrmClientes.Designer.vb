﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CONTRATOLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim CALLELabel As System.Windows.Forms.Label
        Dim NUMEROLabel As System.Windows.Forms.Label
        Dim ENTRECALLESLabel As System.Windows.Forms.Label
        Dim COLONIALabel As System.Windows.Forms.Label
        Dim CodigoPostalLabel As System.Windows.Forms.Label
        Dim TELEFONOLabel As System.Windows.Forms.Label
        Dim CELULARLabel As System.Windows.Forms.Label
        Dim CIUDADLabel As System.Windows.Forms.Label
        Dim EmailLabel As System.Windows.Forms.Label
        Dim Label30 As System.Windows.Forms.Label
        Dim Label31 As System.Windows.Forms.Label
        Dim Label34 As System.Windows.Forms.Label
        Dim Label35 As System.Windows.Forms.Label
        Dim CMBLabel44 As System.Windows.Forms.Label
        Dim CMBLabel45 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim RespuestaLabel As System.Windows.Forms.Label
        Dim ValidaLabel As System.Windows.Forms.Label
        Dim ValidaLabel1 As System.Windows.Forms.Label
        Dim BasicoLabel As System.Windows.Forms.Label
        Dim InternetLabel As System.Windows.Forms.Label
        Dim DigitalLabel As System.Windows.Forms.Label
        Dim CONTRATONETLabel As System.Windows.Forms.Label
        Dim MACCABLEMODEMLabel As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim Label33 As System.Windows.Forms.Label
        Dim Label36 As System.Windows.Forms.Label
        Dim Label28 As System.Windows.Forms.Label
        Dim Label27 As System.Windows.Forms.Label
        Dim Label26 As System.Windows.Forms.Label
        Dim Label25 As System.Windows.Forms.Label
        Dim Label24 As System.Windows.Forms.Label
        Dim Label23 As System.Windows.Forms.Label
        Dim Label22 As System.Windows.Forms.Label
        Dim Label21 As System.Windows.Forms.Label
        Dim Label18 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label16 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim CortesiaLabel1 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label50 As System.Windows.Forms.Label
        Dim Label51 As System.Windows.Forms.Label
        Dim Label52 As System.Windows.Forms.Label
        Dim Label53 As System.Windows.Forms.Label
        Dim LblNumInt As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmClientes))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TxtNumeroInt = New System.Windows.Forms.TextBox()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.LabelComboYRenta = New System.Windows.Forms.Label()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.TextBox28 = New System.Windows.Forms.TextBox()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.TextBox27 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.CONSULTARCLIENTEBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONSULTARCLIENTEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.CONSULTARCLIENTEBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.CALLEComboBox = New System.Windows.Forms.ComboBox()
        Me.MUESTRACALLESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.COLONIAComboBox = New System.Windows.Forms.ComboBox()
        Me.DAMECOLONIACALLEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CIUDADComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraCVECOLCIUBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button15 = New System.Windows.Forms.Button()
        Me.ComboBox15 = New System.Windows.Forms.ComboBox()
        Me.MUESTRACatalogoPeriodosCorteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR()
        Me.ComboBox11 = New System.Windows.Forms.ComboBox()
        Me.MUESTRATABSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.ComboBox7 = New System.Windows.Forms.ComboBox()
        Me.CONRelClientesTiposClientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRATIPOCLIENTESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DigitalTextBox = New System.Windows.Forms.TextBox()
        Me.HaberServicios_CliBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InternetTextBox = New System.Windows.Forms.TextBox()
        Me.BasicoTextBox = New System.Windows.Forms.TextBox()
        Me.EmailTextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.NUMEROTextBox = New System.Windows.Forms.TextBox()
        Me.ENTRECALLESTextBox = New System.Windows.Forms.TextBox()
        Me.CodigoPostalTextBox = New System.Windows.Forms.TextBox()
        Me.TELEFONOTextBox = New System.Windows.Forms.TextBox()
        Me.CELULARTextBox = New System.Windows.Forms.TextBox()
        Me.DESGLOSA_IvaCheckBox = New System.Windows.Forms.CheckBox()
        Me.SoloInternetCheckBox = New System.Windows.Forms.CheckBox()
        Me.EshotelCheckBox = New System.Windows.Forms.CheckBox()
        Me.Clv_CiudadTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_CalleTextBox = New System.Windows.Forms.TextBox()
        Me.CONTRATOTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.CONCLIENTETVBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConRelCteDescuentoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.ConRelClientesTvVendedorBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraPromotoresTvBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConRelCtePlacaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTiposServicioTvBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.StatusBasicoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraMotivoCancelacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NOMBRELabel3 = New System.Windows.Forms.Label()
        Me.CONRel_ClientesTv_UsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VerAparatodelClienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONSULTACLIENTESNETBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.StatusCableModemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TipoCablemodemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTipSerInternetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONRel_ContNet_UsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONSULTACONTNETBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.StatusNetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraPromotoresNetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.CMBLabel54 = New System.Windows.Forms.Label()
        Me.CMBLabel53 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.TextBox()
        Me.VerAparatodelClientedigBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingNavigator4 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONSULTACLIENTESDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton11 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton12 = New System.Windows.Forms.ToolStripButton()
        Me.TextBox29 = New System.Windows.Forms.TextBox()
        Me.CMB2Label5 = New System.Windows.Forms.Label()
        Me.ComboBox13 = New System.Windows.Forms.ComboBox()
        Me.TextBox23 = New System.Windows.Forms.TextBox()
        Me.TextBox24 = New System.Windows.Forms.TextBox()
        Me.TextBox25 = New System.Windows.Forms.TextBox()
        Me.TextBox26 = New System.Windows.Forms.TextBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CMBTextBox28 = New System.Windows.Forms.TextBox()
        Me.TextBox30 = New System.Windows.Forms.TextBox()
        Me.TextBox32 = New System.Windows.Forms.TextBox()
        Me.TextBox33 = New System.Windows.Forms.TextBox()
        Me.TextBox34 = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.ComboBox12 = New System.Windows.Forms.ComboBox()
        Me.MuestraServiciosdigitalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox14 = New System.Windows.Forms.ComboBox()
        Me.MUESTRADIGITALDELCLIBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MACCABLEMODEMTextBox = New System.Windows.Forms.TextBox()
        Me.MUESTRADIGITALDELCLI_porAparatoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONTRATONETTextBox2 = New System.Windows.Forms.TextBox()
        Me.RespuestaTextBox = New System.Windows.Forms.TextBox()
        Me.ValidaDigitalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBLabel32 = New System.Windows.Forms.Label()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.CMBLabel29 = New System.Windows.Forms.Label()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.TreeView3 = New System.Windows.Forms.TreeView()
        Me.CMBLabel10 = New System.Windows.Forms.Label()
        Me.BindingNavigator5 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton13 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton14 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigator6 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripSeparator13 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton15 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton16 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator14 = New System.Windows.Forms.ToolStripSeparator()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.ComboBox20 = New System.Windows.Forms.ComboBox()
        Me.ComboBox19 = New System.Windows.Forms.ComboBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Button27 = New System.Windows.Forms.Button()
        Me.DescuentoLabel1 = New System.Windows.Forms.Label()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.BindingNavigator3 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONSULTACONTDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton9 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton10 = New System.Windows.Forms.ToolStripButton()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.ComboBox8 = New System.Windows.Forms.ComboBox()
        Me.ComboBox9 = New System.Windows.Forms.ComboBox()
        Me.ComboBox10 = New System.Windows.Forms.ComboBox()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.CortesiaCheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.CMBTextBox5 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.TextBox20 = New System.Windows.Forms.TextBox()
        Me.TextBox21 = New System.Windows.Forms.TextBox()
        Me.TextBox22 = New System.Windows.Forms.TextBox()
        Me.CONRel_ContDig_UsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button11 = New System.Windows.Forms.Button()
        Me.ValidaTextBox = New System.Windows.Forms.TextBox()
        Me.Valida_SiahiOrdSerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaTextBox1 = New System.Windows.Forms.TextBox()
        Me.Valida_SiahiQuejasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button10 = New System.Windows.Forms.Button()
        Me.MUESTRA_TIPOCLIENTESTableAdapter = New sofTV.DataSetEDGARTableAdapters.MUESTRA_TIPOCLIENTESTableAdapter()
        Me.CONRel_Clientes_TiposClientesTableAdapter = New sofTV.DataSetEDGARTableAdapters.CONRel_Clientes_TiposClientesTableAdapter()
        Me.GUARDARRel_Clientes_TiposClientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GUARDARRel_Clientes_TiposClientesTableAdapter = New sofTV.DataSetEDGARTableAdapters.GUARDARRel_Clientes_TiposClientesTableAdapter()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.MUESTRATABSTableAdapter = New sofTV.DataSetarnoldoTableAdapters.MUESTRATABSTableAdapter()
        Me.MUESTRACatalogoPeriodosCorteTableAdapter = New sofTV.DataSetEDGARTableAdapters.MUESTRACatalogoPeriodosCorteTableAdapter()
        Me.Valida_facturasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Valida_facturasTableAdapter = New sofTV.DataSetarnoldoTableAdapters.valida_facturasTableAdapter()
        Me.CONRel_ContDig_UsuariosTableAdapter = New sofTV.DataSetEDGARTableAdapters.CONRel_ContDig_UsuariosTableAdapter()
        Me.GUARDARRel_ContDig_UsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GUARDARRel_ContDig_UsuariosTableAdapter = New sofTV.DataSetEDGARTableAdapters.GUARDARRel_ContDig_UsuariosTableAdapter()
        Me.CONRel_ContNet_UsuariosTableAdapter = New sofTV.DataSetEDGARTableAdapters.CONRel_ContNet_UsuariosTableAdapter()
        Me.CONRel_ClientesTv_UsuariosTableAdapter = New sofTV.DataSetEDGARTableAdapters.CONRel_ClientesTv_UsuariosTableAdapter()
        Me.GUARDARRel_ClientesTv_UsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GUARDARRel_ClientesTv_UsuariosTableAdapter = New sofTV.DataSetEDGARTableAdapters.GUARDARRel_ClientesTv_UsuariosTableAdapter()
        Me.GUARDARRel_ContNet_UsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GUARDARRel_ContNet_UsuariosTableAdapter = New sofTV.DataSetEDGARTableAdapters.GUARDARRel_ContNet_UsuariosTableAdapter()
        Me.MuestraPromotoresNetTableAdapter = New sofTV.DataSetEDGARTableAdapters.MuestraPromotoresNetTableAdapter()
        Me.MuestraPromotoresTvTableAdapter = New sofTV.DataSetEDGARTableAdapters.MuestraPromotoresTvTableAdapter()
        Me.ConRel_ClientesTv_VendedorTableAdapter = New sofTV.DataSetEDGARTableAdapters.ConRel_ClientesTv_VendedorTableAdapter()
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2()
        Me.DAMEFECHADELSERVIDOR_2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMEFECHADELSERVIDOR_2TableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.DAMEFECHADELSERVIDOR_2TableAdapter()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.ConRelCtePlacaTableAdapter = New sofTV.DataSetEricTableAdapters.ConRelCtePlacaTableAdapter()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.ChecaRoboDeSeñalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ChecaRoboDeSeñalTableAdapter = New sofTV.DataSetEricTableAdapters.ChecaRoboDeSeñalTableAdapter()
        Me.Dime_Si_ESMiniBasicoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dime_Si_ESMiniBasicoTableAdapter = New sofTV.DataSetEDGARTableAdapters.Dime_Si_ESMiniBasicoTableAdapter()
        Me.ChecaRelCteDescuentoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ChecaRelCteDescuentoTableAdapter = New sofTV.DataSetEricTableAdapters.ChecaRelCteDescuentoTableAdapter()
        Me.ConRelCteDescuentoTableAdapter = New sofTV.DataSetEricTableAdapters.ConRelCteDescuentoTableAdapter()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Valida_Direccion1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Valida_Direccion1TableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Valida_Direccion1TableAdapter()
        Me.Dame_clv_session_clientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clv_session_clientesTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Dame_clv_session_clientesTableAdapter()
        Me.Valida_servicioTvBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Valida_servicioTvTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter()
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.Inserta_Rel_cortesia_FechaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Rel_cortesia_FechaTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Inserta_Rel_cortesia_FechaTableAdapter()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.Button29 = New System.Windows.Forms.Button()
        Me.StatusBasicoBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAADIGITALBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraPromotoresBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameFechaHabilitarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.MuestraServiciosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONSULTARCLIENTETableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONSULTARCLIENTETableAdapter()
        Me.CALLESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CIUDADESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CALLESTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CALLESTableAdapter()
        Me.DAMECOLONIA_CALLETableAdapter = New sofTV.NewSofTvDataSetTableAdapters.DAMECOLONIA_CALLETableAdapter()
        Me.CIUDADESTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CIUDADESTableAdapter()
        Me.MUESTRACALLESTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACALLESTableAdapter()
        Me.CONCLIENTETVTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONCLIENTETVTableAdapter()
        Me.MuestraTiposServicioTvTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraTiposServicioTvTableAdapter()
        Me.MuestraMotivoCancelacionTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraMotivoCancelacionTableAdapter()
        Me.StatusBasicoTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.StatusBasicoTableAdapter()
        Me.MUESTRACABLEMODEMSDELCLIBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraCVECOLCIUTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraCVECOLCIUTableAdapter()
        Me.CONSULTACLIENTESNETTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONSULTACLIENTESNETTableAdapter()
        Me.MUESTRACABLEMODEMSDELCLITableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLITableAdapter()
        Me.MUESTRACONTNETBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTNETTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTNETTableAdapter()
        Me.VerAparatodelClienteTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.VerAparatodelClienteTableAdapter()
        Me.StatusNetTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.StatusNetTableAdapter()
        Me.MuestraTipSerInternetTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerInternetTableAdapter()
        Me.TipoCablemodemTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.TipoCablemodemTableAdapter()
        Me.StatusCableModemTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.StatusCableModemTableAdapter()
        Me.CONSULTACONTNETTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONSULTACONTNETTableAdapter()
        Me.MuestraPromotoresTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraPromotoresTableAdapter()
        Me.HABILITACABLEMODEMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.HABILITACABLEMODEMTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.HABILITACABLEMODEMTableAdapter()
        Me.MUESTRADIGITALDELCLITableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRADIGITALDELCLITableAdapter()
        Me.MUESTRACONTDIGBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACONTDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTDIGTableAdapter()
        Me.CONSULTACLIENTESDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONSULTACLIENTESDIGTableAdapter()
        Me.CONSULTACONTDIGTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONSULTACONTDIGTableAdapter()
        Me.VerAparatodelClientedigTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.VerAparatodelClientedigTableAdapter()
        Me.MUESTRA_A_DIGITALTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRA_A_DIGITALTableAdapter()
        Me.MuestraServiciosTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraServiciosTableAdapter()
        Me.MuestraServicios_digitalTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraServicios_digitalTableAdapter()
        Me.ValidaDigitalTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.ValidaDigitalTableAdapter()
        Me.Valida_SiahiOrdSerTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.Valida_SiahiOrdSerTableAdapter()
        Me.Valida_SiahiQuejasTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.Valida_SiahiQuejasTableAdapter()
        Me.HaberServicios_CliTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.HaberServicios_CliTableAdapter()
        Me.MUESTRADIGITALDELCLI_porAparatoTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRADIGITALDELCLI_porAparatoTableAdapter()
        Me.BorraNetPor_NoGRaboBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorraNetPor_NoGRaboTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BorraNetPor_NoGRaboTableAdapter()
        Me.BorraDigPor_NoGRaboBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorraDigPor_NoGRaboTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BorraDigPor_NoGRaboTableAdapter()
        Me.DAMESTATUSHABBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMESTATUSHABTableAdapter = New sofTV.DataSetLidiaTableAdapters.DAMESTATUSHABTableAdapter()
        Me.AsignaPeriodoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AsignaPeriodoTableAdapter = New sofTV.DataSetLidiaTableAdapters.AsignaPeriodoTableAdapter()
        Me.CONTARCLIENTESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONTARCLIENTESTableAdapter = New sofTV.DataSetLidiaTableAdapters.CONTARCLIENTESTableAdapter()
        Me.PrimerMesCLIENTESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PrimerMesCLIENTESTableAdapter = New sofTV.DataSetLidiaTableAdapters.PrimerMesCLIENTESTableAdapter()
        Me.DameFechaHabilitarTableAdapter = New sofTV.DataSetLidiaTableAdapters.DameFechaHabilitarTableAdapter()
        Me.DIMEQUEPERIODODECORTEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DIMEQUEPERIODODECORTETableAdapter = New sofTV.DataSetLidiaTableAdapters.DIMEQUEPERIODODECORTETableAdapter()
        Me.BuscaBloqueadoTableAdapter = New sofTV.DataSetLidiaTableAdapters.BuscaBloqueadoTableAdapter()
        Me.BuscaBloqueadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button30 = New System.Windows.Forms.Button()
        Me.BtnEstadoDeCuenta = New System.Windows.Forms.Button()
        Me.Button31 = New System.Windows.Forms.Button()
        Me.btnHisDes = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CONTRATOLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        CALLELabel = New System.Windows.Forms.Label()
        NUMEROLabel = New System.Windows.Forms.Label()
        ENTRECALLESLabel = New System.Windows.Forms.Label()
        COLONIALabel = New System.Windows.Forms.Label()
        CodigoPostalLabel = New System.Windows.Forms.Label()
        TELEFONOLabel = New System.Windows.Forms.Label()
        CELULARLabel = New System.Windows.Forms.Label()
        CIUDADLabel = New System.Windows.Forms.Label()
        EmailLabel = New System.Windows.Forms.Label()
        Label30 = New System.Windows.Forms.Label()
        Label31 = New System.Windows.Forms.Label()
        Label34 = New System.Windows.Forms.Label()
        Label35 = New System.Windows.Forms.Label()
        CMBLabel44 = New System.Windows.Forms.Label()
        CMBLabel45 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        RespuestaLabel = New System.Windows.Forms.Label()
        ValidaLabel = New System.Windows.Forms.Label()
        ValidaLabel1 = New System.Windows.Forms.Label()
        BasicoLabel = New System.Windows.Forms.Label()
        InternetLabel = New System.Windows.Forms.Label()
        DigitalLabel = New System.Windows.Forms.Label()
        CONTRATONETLabel = New System.Windows.Forms.Label()
        MACCABLEMODEMLabel = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Label33 = New System.Windows.Forms.Label()
        Label36 = New System.Windows.Forms.Label()
        Label28 = New System.Windows.Forms.Label()
        Label27 = New System.Windows.Forms.Label()
        Label26 = New System.Windows.Forms.Label()
        Label25 = New System.Windows.Forms.Label()
        Label24 = New System.Windows.Forms.Label()
        Label23 = New System.Windows.Forms.Label()
        Label22 = New System.Windows.Forms.Label()
        Label21 = New System.Windows.Forms.Label()
        Label18 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label16 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        CortesiaLabel1 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label50 = New System.Windows.Forms.Label()
        Label51 = New System.Windows.Forms.Label()
        Label52 = New System.Windows.Forms.Label()
        Label53 = New System.Windows.Forms.Label()
        LblNumInt = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel13.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.Panel11.SuspendLayout()
        CType(Me.CONSULTARCLIENTEBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONSULTARCLIENTEBindingNavigator.SuspendLayout()
        CType(Me.CONSULTARCLIENTEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMECOLONIACALLEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraCVECOLCIUBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACatalogoPeriodosCorteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATABSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONRelClientesTiposClientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATIPOCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HaberServicios_CliBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONCLIENTETVBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRelClientesTvVendedorBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraPromotoresTvBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRelCtePlacaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTiposServicioTvBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBasicoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraMotivoCancelacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONRel_ClientesTv_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VerAparatodelClienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSULTACLIENTESNETBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusCableModemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TipoCablemodemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipSerInternetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONRel_ContNet_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSULTACONTNETBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusNetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraPromotoresNetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        Me.Panel9.SuspendLayout()
        CType(Me.VerAparatodelClientedigBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator4.SuspendLayout()
        CType(Me.CONSULTACLIENTESDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        CType(Me.MuestraServiciosdigitalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRADIGITALDELCLIBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRADIGITALDELCLI_porAparatoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaDigitalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        CType(Me.BindingNavigator5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator5.SuspendLayout()
        CType(Me.BindingNavigator6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator6.SuspendLayout()
        Me.Panel8.SuspendLayout()
        CType(Me.BindingNavigator3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator3.SuspendLayout()
        CType(Me.CONSULTACONTDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONRel_ContDig_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_SiahiOrdSerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_SiahiQuejasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GUARDARRel_Clientes_TiposClientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_facturasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GUARDARRel_ContDig_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GUARDARRel_ClientesTv_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GUARDARRel_ContNet_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMEFECHADELSERVIDOR_2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChecaRoboDeSeñalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dime_Si_ESMiniBasicoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChecaRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_Direccion1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clv_session_clientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_servicioTvBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Rel_cortesia_FechaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBasicoBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAADIGITALBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraPromotoresBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameFechaHabilitarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraServiciosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CALLESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CIUDADESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACABLEMODEMSDELCLIBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTNETBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HABILITACABLEMODEMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACONTDIGBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorraNetPor_NoGRaboBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorraDigPor_NoGRaboBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMESTATUSHABBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AsignaPeriodoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONTARCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrimerMesCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DIMEQUEPERIODODECORTEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BuscaBloqueadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CONTRATOLabel
        '
        CONTRATOLabel.AutoSize = True
        CONTRATOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CONTRATOLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CONTRATOLabel.Location = New System.Drawing.Point(27, 43)
        CONTRATOLabel.Name = "CONTRATOLabel"
        CONTRATOLabel.Size = New System.Drawing.Size(61, 15)
        CONTRATOLabel.TabIndex = 76
        CONTRATOLabel.Text = "Contrato"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel.ForeColor = System.Drawing.Color.LightSlateGray
        NOMBRELabel.Location = New System.Drawing.Point(299, 43)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(62, 15)
        NOMBRELabel.TabIndex = 77
        NOMBRELabel.Text = "Nombre "
        '
        'CALLELabel
        '
        CALLELabel.AutoSize = True
        CALLELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CALLELabel.ForeColor = System.Drawing.Color.LightSlateGray
        CALLELabel.Location = New System.Drawing.Point(125, 93)
        CALLELabel.Name = "CALLELabel"
        CALLELabel.Size = New System.Drawing.Size(44, 15)
        CALLELabel.TabIndex = 42
        CALLELabel.Text = "Calle "
        '
        'NUMEROLabel
        '
        NUMEROLabel.AutoSize = True
        NUMEROLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NUMEROLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NUMEROLabel.Location = New System.Drawing.Point(314, 93)
        NUMEROLabel.Name = "NUMEROLabel"
        NUMEROLabel.Size = New System.Drawing.Size(86, 15)
        NUMEROLabel.TabIndex = 44
        NUMEROLabel.Text = "Numero Ext."
        '
        'ENTRECALLESLabel
        '
        ENTRECALLESLabel.AutoSize = True
        ENTRECALLESLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ENTRECALLESLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ENTRECALLESLabel.Location = New System.Drawing.Point(168, 141)
        ENTRECALLESLabel.Name = "ENTRECALLESLabel"
        ENTRECALLESLabel.Size = New System.Drawing.Size(83, 15)
        ENTRECALLESLabel.TabIndex = 46
        ENTRECALLESLabel.Text = "Entrecalles "
        '
        'COLONIALabel
        '
        COLONIALabel.AutoSize = True
        COLONIALabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        COLONIALabel.ForeColor = System.Drawing.Color.LightSlateGray
        COLONIALabel.Location = New System.Drawing.Point(138, 182)
        COLONIALabel.Name = "COLONIALabel"
        COLONIALabel.Size = New System.Drawing.Size(56, 15)
        COLONIALabel.TabIndex = 50
        COLONIALabel.Text = "Colonia"
        '
        'CodigoPostalLabel
        '
        CodigoPostalLabel.AutoSize = True
        CodigoPostalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CodigoPostalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CodigoPostalLabel.Location = New System.Drawing.Point(431, 138)
        CodigoPostalLabel.Name = "CodigoPostalLabel"
        CodigoPostalLabel.Size = New System.Drawing.Size(96, 15)
        CodigoPostalLabel.TabIndex = 52
        CodigoPostalLabel.Text = "Codigo Postal"
        '
        'TELEFONOLabel
        '
        TELEFONOLabel.AutoSize = True
        TELEFONOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TELEFONOLabel.ForeColor = System.Drawing.Color.LightSlateGray
        TELEFONOLabel.Location = New System.Drawing.Point(663, 143)
        TELEFONOLabel.Name = "TELEFONOLabel"
        TELEFONOLabel.Size = New System.Drawing.Size(67, 15)
        TELEFONOLabel.TabIndex = 54
        TELEFONOLabel.Text = "Teléfono "
        '
        'CELULARLabel
        '
        CELULARLabel.AutoSize = True
        CELULARLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CELULARLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CELULARLabel.Location = New System.Drawing.Point(668, 185)
        CELULARLabel.Name = "CELULARLabel"
        CELULARLabel.Size = New System.Drawing.Size(57, 15)
        CELULARLabel.TabIndex = 56
        CELULARLabel.Text = "Celular "
        '
        'CIUDADLabel
        '
        CIUDADLabel.AutoSize = True
        CIUDADLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CIUDADLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CIUDADLabel.Location = New System.Drawing.Point(75, 227)
        CIUDADLabel.Name = "CIUDADLabel"
        CIUDADLabel.Size = New System.Drawing.Size(173, 15)
        CIUDADLabel.TabIndex = 64
        CIUDADLabel.Text = "Ciudad/Region/Municipio "
        '
        'EmailLabel
        '
        EmailLabel.AutoSize = True
        EmailLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        EmailLabel.ForeColor = System.Drawing.Color.LightSlateGray
        EmailLabel.Location = New System.Drawing.Point(674, 225)
        EmailLabel.Name = "EmailLabel"
        EmailLabel.Size = New System.Drawing.Size(44, 15)
        EmailLabel.TabIndex = 67
        EmailLabel.Text = "Email"
        '
        'Label30
        '
        Label30.AutoSize = True
        Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label30.ForeColor = System.Drawing.Color.LightSlateGray
        Label30.Location = New System.Drawing.Point(370, 282)
        Label30.Name = "Label30"
        Label30.Size = New System.Drawing.Size(74, 15)
        Label30.TabIndex = 46
        Label30.Text = "Se Renta :"
        '
        'Label31
        '
        Label31.AutoSize = True
        Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label31.ForeColor = System.Drawing.Color.LightSlateGray
        Label31.Location = New System.Drawing.Point(18, 129)
        Label31.Name = "Label31"
        Label31.Size = New System.Drawing.Size(109, 15)
        Label31.TabIndex = 23
        Label31.Text = "Observaciones :"
        '
        'Label34
        '
        Label34.AutoSize = True
        Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label34.ForeColor = System.Drawing.Color.LightSlateGray
        Label34.Location = New System.Drawing.Point(340, 118)
        Label34.Name = "Label34"
        Label34.Size = New System.Drawing.Size(86, 15)
        Label34.TabIndex = 17
        Label34.Text = "Suspención:"
        '
        'Label35
        '
        Label35.AutoSize = True
        Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label35.ForeColor = System.Drawing.Color.LightSlateGray
        Label35.Location = New System.Drawing.Point(351, 92)
        Label35.Name = "Label35"
        Label35.Size = New System.Drawing.Size(75, 15)
        Label35.TabIndex = 15
        Label35.Text = "Activación:"
        '
        'CMBLabel44
        '
        CMBLabel44.AutoSize = True
        CMBLabel44.BackColor = System.Drawing.Color.WhiteSmoke
        CMBLabel44.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel44.ForeColor = System.Drawing.Color.LightSlateGray
        CMBLabel44.Location = New System.Drawing.Point(65, 197)
        CMBLabel44.Name = "CMBLabel44"
        CMBLabel44.Size = New System.Drawing.Size(55, 15)
        CMBLabel44.TabIndex = 2
        CMBLabel44.Text = "Marca :"
        '
        'CMBLabel45
        '
        CMBLabel45.AutoSize = True
        CMBLabel45.BackColor = System.Drawing.Color.WhiteSmoke
        CMBLabel45.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabel45.ForeColor = System.Drawing.Color.LightSlateGray
        CMBLabel45.Location = New System.Drawing.Point(25, 228)
        CMBLabel45.Name = "CMBLabel45"
        CMBLabel45.Size = New System.Drawing.Size(96, 15)
        CMBLabel45.TabIndex = 4
        CMBLabel45.Text = "Tipo Aparato :"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Label9.Location = New System.Drawing.Point(386, 145)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(40, 15)
        Label9.TabIndex = 56
        Label9.Text = "Baja:"
        '
        'RespuestaLabel
        '
        RespuestaLabel.AutoSize = True
        RespuestaLabel.ForeColor = System.Drawing.Color.WhiteSmoke
        RespuestaLabel.Location = New System.Drawing.Point(133, 157)
        RespuestaLabel.Name = "RespuestaLabel"
        RespuestaLabel.Size = New System.Drawing.Size(61, 13)
        RespuestaLabel.TabIndex = 8
        RespuestaLabel.Text = "Respuesta:"
        '
        'ValidaLabel
        '
        ValidaLabel.AutoSize = True
        ValidaLabel.Location = New System.Drawing.Point(841, 130)
        ValidaLabel.Name = "ValidaLabel"
        ValidaLabel.Size = New System.Drawing.Size(39, 13)
        ValidaLabel.TabIndex = 25
        ValidaLabel.Text = "Valida:"
        '
        'ValidaLabel1
        '
        ValidaLabel1.AutoSize = True
        ValidaLabel1.Location = New System.Drawing.Point(849, 154)
        ValidaLabel1.Name = "ValidaLabel1"
        ValidaLabel1.Size = New System.Drawing.Size(39, 13)
        ValidaLabel1.TabIndex = 27
        ValidaLabel1.Text = "Valida:"
        '
        'BasicoLabel
        '
        BasicoLabel.AutoSize = True
        BasicoLabel.Location = New System.Drawing.Point(206, 3)
        BasicoLabel.Name = "BasicoLabel"
        BasicoLabel.Size = New System.Drawing.Size(42, 13)
        BasicoLabel.TabIndex = 68
        BasicoLabel.Text = "Basico:"
        '
        'InternetLabel
        '
        InternetLabel.AutoSize = True
        InternetLabel.Location = New System.Drawing.Point(198, 0)
        InternetLabel.Name = "InternetLabel"
        InternetLabel.Size = New System.Drawing.Size(46, 13)
        InternetLabel.TabIndex = 69
        InternetLabel.Text = "Internet:"
        '
        'DigitalLabel
        '
        DigitalLabel.AutoSize = True
        DigitalLabel.Location = New System.Drawing.Point(209, 0)
        DigitalLabel.Name = "DigitalLabel"
        DigitalLabel.Size = New System.Drawing.Size(39, 13)
        DigitalLabel.TabIndex = 70
        DigitalLabel.Text = "Digital:"
        '
        'CONTRATONETLabel
        '
        CONTRATONETLabel.AutoSize = True
        CONTRATONETLabel.Location = New System.Drawing.Point(33, 39)
        CONTRATONETLabel.Name = "CONTRATONETLabel"
        CONTRATONETLabel.Size = New System.Drawing.Size(92, 13)
        CONTRATONETLabel.TabIndex = 9
        CONTRATONETLabel.Text = "CONTRATONET:"
        '
        'MACCABLEMODEMLabel
        '
        MACCABLEMODEMLabel.AutoSize = True
        MACCABLEMODEMLabel.Location = New System.Drawing.Point(177, 42)
        MACCABLEMODEMLabel.Name = "MACCABLEMODEMLabel"
        MACCABLEMODEMLabel.Size = New System.Drawing.Size(108, 13)
        MACCABLEMODEMLabel.TabIndex = 10
        MACCABLEMODEMLabel.Text = "MACCABLEMODEM:"
        '
        'Label15
        '
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Label15.Location = New System.Drawing.Point(562, 36)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(198, 16)
        Label15.TabIndex = 73
        Label15.Text = "Tipo Cobro"
        Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label33
        '
        Label33.AutoSize = True
        Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label33.ForeColor = System.Drawing.Color.LightSlateGray
        Label33.Location = New System.Drawing.Point(413, 228)
        Label33.Name = "Label33"
        Label33.Size = New System.Drawing.Size(45, 15)
        Label33.TabIndex = 75
        Label33.Text = "CMTS"
        '
        'Label36
        '
        Label36.AutoSize = True
        Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label36.ForeColor = System.Drawing.Color.LightSlateGray
        Label36.Location = New System.Drawing.Point(380, 181)
        Label36.Name = "Label36"
        Label36.Size = New System.Drawing.Size(115, 15)
        Label36.TabIndex = 77
        Label36.Text = "Periodo de Corte"
        '
        'Label28
        '
        Label28.AutoSize = True
        Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label28.ForeColor = System.Drawing.Color.LightSlateGray
        Label28.Location = New System.Drawing.Point(21, 61)
        Label28.Name = "Label28"
        Label28.Size = New System.Drawing.Size(55, 15)
        Label28.TabIndex = 6
        Label28.Text = "Status :"
        '
        'Label27
        '
        Label27.AutoSize = True
        Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label27.ForeColor = System.Drawing.Color.LightSlateGray
        Label27.Location = New System.Drawing.Point(338, 89)
        Label27.Name = "Label27"
        Label27.Size = New System.Drawing.Size(96, 15)
        Label27.TabIndex = 8
        Label27.Text = "Contratación :"
        '
        'Label26
        '
        Label26.AutoSize = True
        Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label26.ForeColor = System.Drawing.Color.LightSlateGray
        Label26.Location = New System.Drawing.Point(346, 114)
        Label26.Name = "Label26"
        Label26.Size = New System.Drawing.Size(85, 15)
        Label26.TabIndex = 10
        Label26.Text = "Instalación :"
        '
        'Label25
        '
        Label25.AutoSize = True
        Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label25.ForeColor = System.Drawing.Color.LightSlateGray
        Label25.Location = New System.Drawing.Point(341, 141)
        Label25.Name = "Label25"
        Label25.Size = New System.Drawing.Size(90, 15)
        Label25.TabIndex = 12
        Label25.Text = "Suspención :"
        '
        'Label24
        '
        Label24.AutoSize = True
        Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label24.ForeColor = System.Drawing.Color.LightSlateGray
        Label24.Location = New System.Drawing.Point(372, 166)
        Label24.Name = "Label24"
        Label24.Size = New System.Drawing.Size(44, 15)
        Label24.TabIndex = 14
        Label24.Text = "Baja :"
        '
        'Label23
        '
        Label23.AutoSize = True
        Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label23.ForeColor = System.Drawing.Color.LightSlateGray
        Label23.Location = New System.Drawing.Point(345, 193)
        Label23.Name = "Label23"
        Label23.Size = New System.Drawing.Size(85, 15)
        Label23.TabIndex = 16
        Label23.Text = "Fuera Area :"
        '
        'Label22
        '
        Label22.AutoSize = True
        Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label22.ForeColor = System.Drawing.Color.LightSlateGray
        Label22.Location = New System.Drawing.Point(340, 219)
        Label22.Name = "Label22"
        Label22.Size = New System.Drawing.Size(94, 15)
        Label22.TabIndex = 18
        Label22.Text = "Ultimo Pago :"
        '
        'Label21
        '
        Label21.AutoSize = True
        Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label21.ForeColor = System.Drawing.Color.LightSlateGray
        Label21.Location = New System.Drawing.Point(25, 115)
        Label21.Name = "Label21"
        Label21.Size = New System.Drawing.Size(141, 15)
        Label21.TabIndex = 20
        Label21.Text = "Primer Mensualidad:"
        '
        'Label18
        '
        Label18.AutoSize = True
        Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label18.ForeColor = System.Drawing.Color.LightSlateGray
        Label18.Location = New System.Drawing.Point(6, 90)
        Label18.Name = "Label18"
        Label18.Size = New System.Drawing.Size(75, 15)
        Label18.TabIndex = 26
        Label18.Text = "# Factura :"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.LightSlateGray
        Label17.Location = New System.Drawing.Point(15, 168)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(76, 15)
        Label17.TabIndex = 28
        Label17.Text = "Vendedor :"
        '
        'Label16
        '
        Label16.AutoSize = True
        Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Label16.Location = New System.Drawing.Point(8, 196)
        Label16.Name = "Label16"
        Label16.Size = New System.Drawing.Size(84, 15)
        Label16.TabIndex = 30
        Label16.Text = "Promocion :"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Label14.Location = New System.Drawing.Point(18, 244)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(109, 15)
        Label14.TabIndex = 34
        Label14.Text = "Observaciones :"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.BackColor = System.Drawing.Color.Transparent
        Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label13.ForeColor = System.Drawing.Color.OrangeRed
        Label13.Location = New System.Drawing.Point(5, 29)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(146, 18)
        Label13.TabIndex = 36
        Label13.Text = "Datos del Paquete"
        '
        'CortesiaLabel1
        '
        CortesiaLabel1.AutoSize = True
        CortesiaLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CortesiaLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        CortesiaLabel1.Location = New System.Drawing.Point(31, 140)
        CortesiaLabel1.Name = "CortesiaLabel1"
        CortesiaLabel1.Size = New System.Drawing.Size(64, 15)
        CortesiaLabel1.TabIndex = 56
        CortesiaLabel1.Text = "Cortesia:"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Label6.Location = New System.Drawing.Point(29, 220)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(65, 15)
        Label6.TabIndex = 81
        Label6.Text = "Capturo :"
        '
        'Label50
        '
        Label50.AutoSize = True
        Label50.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label50.ForeColor = System.Drawing.Color.LightSlateGray
        Label50.Location = New System.Drawing.Point(10, 7)
        Label50.Name = "Label50"
        Label50.Size = New System.Drawing.Size(113, 15)
        Label50.TabIndex = 0
        Label50.Text = "Apellido Paterno"
        '
        'Label51
        '
        Label51.AutoSize = True
        Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label51.ForeColor = System.Drawing.Color.LightSlateGray
        Label51.Location = New System.Drawing.Point(142, 7)
        Label51.Name = "Label51"
        Label51.Size = New System.Drawing.Size(116, 15)
        Label51.TabIndex = 0
        Label51.Text = "Apellido Materno"
        '
        'Label52
        '
        Label52.AutoSize = True
        Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label52.ForeColor = System.Drawing.Color.LightSlateGray
        Label52.Location = New System.Drawing.Point(288, 7)
        Label52.Name = "Label52"
        Label52.Size = New System.Drawing.Size(58, 15)
        Label52.TabIndex = 0
        Label52.Text = "Nombre"
        '
        'Label53
        '
        Label53.AutoSize = True
        Label53.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label53.ForeColor = System.Drawing.Color.LightSlateGray
        Label53.Location = New System.Drawing.Point(77, 15)
        Label53.Name = "Label53"
        Label53.Size = New System.Drawing.Size(52, 15)
        Label53.TabIndex = 55
        Label53.Text = "Cuenta"
        '
        'LblNumInt
        '
        LblNumInt.AutoSize = True
        LblNumInt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        LblNumInt.ForeColor = System.Drawing.Color.LightSlateGray
        LblNumInt.Location = New System.Drawing.Point(451, 93)
        LblNumInt.Name = "LblNumInt"
        LblNumInt.Size = New System.Drawing.Size(82, 15)
        LblNumInt.TabIndex = 81
        LblNumInt.Text = "Numero Int."
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.TxtNumeroInt)
        Me.Panel1.Controls.Add(LblNumInt)
        Me.Panel1.Controls.Add(Me.Panel13)
        Me.Panel1.Controls.Add(Me.Panel12)
        Me.Panel1.Controls.Add(Me.Panel11)
        Me.Panel1.Controls.Add(Me.CONSULTARCLIENTEBindingNavigator)
        Me.Panel1.Controls.Add(Me.CALLEComboBox)
        Me.Panel1.Controls.Add(Me.COLONIAComboBox)
        Me.Panel1.Controls.Add(Me.CIUDADComboBox)
        Me.Panel1.Controls.Add(Me.Button15)
        Me.Panel1.Controls.Add(Label36)
        Me.Panel1.Controls.Add(Me.ComboBox15)
        Me.Panel1.Controls.Add(Label33)
        Me.Panel1.Controls.Add(Me.ComboBox11)
        Me.Panel1.Controls.Add(Label15)
        Me.Panel1.Controls.Add(Me.ComboBox7)
        Me.Panel1.Controls.Add(DigitalLabel)
        Me.Panel1.Controls.Add(Me.DigitalTextBox)
        Me.Panel1.Controls.Add(InternetLabel)
        Me.Panel1.Controls.Add(Me.InternetTextBox)
        Me.Panel1.Controls.Add(BasicoLabel)
        Me.Panel1.Controls.Add(Me.BasicoTextBox)
        Me.Panel1.Controls.Add(Me.EmailTextBox)
        Me.Panel1.Controls.Add(CONTRATOLabel)
        Me.Panel1.Controls.Add(NOMBRELabel)
        Me.Panel1.Controls.Add(Me.NOMBRETextBox)
        Me.Panel1.Controls.Add(CALLELabel)
        Me.Panel1.Controls.Add(NUMEROLabel)
        Me.Panel1.Controls.Add(Me.NUMEROTextBox)
        Me.Panel1.Controls.Add(ENTRECALLESLabel)
        Me.Panel1.Controls.Add(Me.ENTRECALLESTextBox)
        Me.Panel1.Controls.Add(COLONIALabel)
        Me.Panel1.Controls.Add(CodigoPostalLabel)
        Me.Panel1.Controls.Add(Me.CodigoPostalTextBox)
        Me.Panel1.Controls.Add(Me.TELEFONOTextBox)
        Me.Panel1.Controls.Add(Me.CELULARTextBox)
        Me.Panel1.Controls.Add(Me.DESGLOSA_IvaCheckBox)
        Me.Panel1.Controls.Add(Me.SoloInternetCheckBox)
        Me.Panel1.Controls.Add(Me.EshotelCheckBox)
        Me.Panel1.Controls.Add(CIUDADLabel)
        Me.Panel1.Controls.Add(Me.Clv_CiudadTextBox)
        Me.Panel1.Controls.Add(Me.Clv_ColoniaTextBox)
        Me.Panel1.Controls.Add(Me.Clv_CalleTextBox)
        Me.Panel1.Controls.Add(CELULARLabel)
        Me.Panel1.Controls.Add(TELEFONOLabel)
        Me.Panel1.Controls.Add(EmailLabel)
        Me.Panel1.Controls.Add(Me.CONTRATOTextBox)
        Me.Panel1.Location = New System.Drawing.Point(2, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Panel1.Size = New System.Drawing.Size(819, 281)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'TxtNumeroInt
        '
        Me.TxtNumeroInt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtNumeroInt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtNumeroInt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TxtNumeroInt.Location = New System.Drawing.Point(430, 111)
        Me.TxtNumeroInt.MaxLength = 50
        Me.TxtNumeroInt.Multiline = True
        Me.TxtNumeroInt.Name = "TxtNumeroInt"
        Me.TxtNumeroInt.Size = New System.Drawing.Size(121, 22)
        Me.TxtNumeroInt.TabIndex = 5
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.LabelComboYRenta)
        Me.Panel13.Location = New System.Drawing.Point(327, 225)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(274, 49)
        Me.Panel13.TabIndex = 80
        Me.Panel13.Visible = False
        '
        'LabelComboYRenta
        '
        Me.LabelComboYRenta.BackColor = System.Drawing.Color.WhiteSmoke
        Me.LabelComboYRenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelComboYRenta.ForeColor = System.Drawing.Color.Red
        Me.LabelComboYRenta.Location = New System.Drawing.Point(3, 4)
        Me.LabelComboYRenta.Name = "LabelComboYRenta"
        Me.LabelComboYRenta.Size = New System.Drawing.Size(265, 40)
        Me.LabelComboYRenta.TabIndex = 0
        Me.LabelComboYRenta.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Label53)
        Me.Panel12.Controls.Add(Me.TextBox28)
        Me.Panel12.Location = New System.Drawing.Point(591, 84)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(205, 61)
        Me.Panel12.TabIndex = 6
        Me.Panel12.Visible = False
        '
        'TextBox28
        '
        Me.TextBox28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox28.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox28.Enabled = False
        Me.TextBox28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox28.Location = New System.Drawing.Point(16, 34)
        Me.TextBox28.MaxLength = 50
        Me.TextBox28.Name = "TextBox28"
        Me.TextBox28.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextBox28.Size = New System.Drawing.Size(178, 22)
        Me.TextBox28.TabIndex = 15
        Me.TextBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Label52)
        Me.Panel11.Controls.Add(Label51)
        Me.Panel11.Controls.Add(Label50)
        Me.Panel11.Controls.Add(Me.TextBox27)
        Me.Panel11.Controls.Add(Me.TextBox7)
        Me.Panel11.Controls.Add(Me.TextBox4)
        Me.Panel11.Location = New System.Drawing.Point(108, 36)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(443, 54)
        Me.Panel11.TabIndex = 0
        Me.Panel11.Visible = False
        '
        'TextBox27
        '
        Me.TextBox27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox27.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox27.Location = New System.Drawing.Point(291, 28)
        Me.TextBox27.Name = "TextBox27"
        Me.TextBox27.Size = New System.Drawing.Size(143, 21)
        Me.TextBox27.TabIndex = 2
        '
        'TextBox7
        '
        Me.TextBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox7.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(145, 28)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(140, 21)
        Me.TextBox7.TabIndex = 1
        '
        'TextBox4
        '
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(3, 28)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(136, 21)
        Me.TextBox4.TabIndex = 0
        '
        'CONSULTARCLIENTEBindingNavigator
        '
        Me.CONSULTARCLIENTEBindingNavigator.AddNewItem = Nothing
        Me.CONSULTARCLIENTEBindingNavigator.BindingSource = Me.CONSULTARCLIENTEBindingSource
        Me.CONSULTARCLIENTEBindingNavigator.CountItem = Nothing
        Me.CONSULTARCLIENTEBindingNavigator.DeleteItem = Nothing
        Me.CONSULTARCLIENTEBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorSeparator, Me.ToolStripButton1, Me.BindingNavigatorSeparator2, Me.CONSULTARCLIENTEBindingNavigatorSaveItem})
        Me.CONSULTARCLIENTEBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONSULTARCLIENTEBindingNavigator.MoveFirstItem = Nothing
        Me.CONSULTARCLIENTEBindingNavigator.MoveLastItem = Nothing
        Me.CONSULTARCLIENTEBindingNavigator.MoveNextItem = Nothing
        Me.CONSULTARCLIENTEBindingNavigator.MovePreviousItem = Nothing
        Me.CONSULTARCLIENTEBindingNavigator.Name = "CONSULTARCLIENTEBindingNavigator"
        Me.CONSULTARCLIENTEBindingNavigator.PositionItem = Nothing
        Me.CONSULTARCLIENTEBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.CONSULTARCLIENTEBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONSULTARCLIENTEBindingNavigator.Size = New System.Drawing.Size(819, 25)
        Me.CONSULTARCLIENTEBindingNavigator.TabIndex = 12
        Me.CONSULTARCLIENTEBindingNavigator.Text = "BindingNavigator1"
        '
        'CONSULTARCLIENTEBindingSource
        '
        Me.CONSULTARCLIENTEBindingSource.DataMember = "CONSULTARCLIENTE"
        Me.CONSULTARCLIENTEBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton1.Size = New System.Drawing.Size(76, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        Me.ToolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'CONSULTARCLIENTEBindingNavigatorSaveItem
        '
        Me.CONSULTARCLIENTEBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONSULTARCLIENTEBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONSULTARCLIENTEBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONSULTARCLIENTEBindingNavigatorSaveItem.Name = "CONSULTARCLIENTEBindingNavigatorSaveItem"
        Me.CONSULTARCLIENTEBindingNavigatorSaveItem.Size = New System.Drawing.Size(88, 22)
        Me.CONSULTARCLIENTEBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'CALLEComboBox
        '
        Me.CALLEComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONSULTARCLIENTEBindingSource, "Clv_Calle", True))
        Me.CALLEComboBox.DataSource = Me.MUESTRACALLESBindingSource
        Me.CALLEComboBox.DisplayMember = "NOMBRE"
        Me.CALLEComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CALLEComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLEComboBox.FormattingEnabled = True
        Me.CALLEComboBox.Location = New System.Drawing.Point(10, 111)
        Me.CALLEComboBox.Name = "CALLEComboBox"
        Me.CALLEComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CALLEComboBox.Size = New System.Drawing.Size(281, 24)
        Me.CALLEComboBox.TabIndex = 3
        Me.CALLEComboBox.ValueMember = "Clv_Calle"
        '
        'MUESTRACALLESBindingSource
        '
        Me.MUESTRACALLESBindingSource.DataMember = "MUESTRACALLES"
        Me.MUESTRACALLESBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'COLONIAComboBox
        '
        Me.COLONIAComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONSULTARCLIENTEBindingSource, "Clv_Colonia", True))
        Me.COLONIAComboBox.DataSource = Me.DAMECOLONIACALLEBindingSource
        Me.COLONIAComboBox.DisplayMember = "COLONIA"
        Me.COLONIAComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.COLONIAComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIAComboBox.FormattingEnabled = True
        Me.COLONIAComboBox.Location = New System.Drawing.Point(10, 200)
        Me.COLONIAComboBox.Name = "COLONIAComboBox"
        Me.COLONIAComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.COLONIAComboBox.Size = New System.Drawing.Size(317, 24)
        Me.COLONIAComboBox.TabIndex = 8
        Me.COLONIAComboBox.ValueMember = "CLV_COLONIA"
        '
        'DAMECOLONIACALLEBindingSource
        '
        Me.DAMECOLONIACALLEBindingSource.DataMember = "DAMECOLONIA_CALLE"
        Me.DAMECOLONIACALLEBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CIUDADComboBox
        '
        Me.CIUDADComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONSULTARCLIENTEBindingSource, "clv_Ciudad", True))
        Me.CIUDADComboBox.DataSource = Me.MuestraCVECOLCIUBindingSource
        Me.CIUDADComboBox.DisplayMember = "Nombre"
        Me.CIUDADComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CIUDADComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADComboBox.FormattingEnabled = True
        Me.CIUDADComboBox.Location = New System.Drawing.Point(9, 245)
        Me.CIUDADComboBox.Name = "CIUDADComboBox"
        Me.CIUDADComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CIUDADComboBox.Size = New System.Drawing.Size(312, 24)
        Me.CIUDADComboBox.TabIndex = 10
        Me.CIUDADComboBox.ValueMember = "Clv_Ciudad"
        '
        'MuestraCVECOLCIUBindingSource
        '
        Me.MuestraCVECOLCIUBindingSource.DataMember = "MuestraCVECOLCIU"
        Me.MuestraCVECOLCIUBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button15
        '
        Me.Button15.BackColor = System.Drawing.Color.DarkOrange
        Me.Button15.ForeColor = System.Drawing.Color.White
        Me.Button15.Location = New System.Drawing.Point(772, 55)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(24, 23)
        Me.Button15.TabIndex = 10
        Me.Button15.Text = "..."
        Me.Button15.UseVisualStyleBackColor = False
        '
        'ComboBox15
        '
        Me.ComboBox15.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONSULTARCLIENTEBindingSource, "Clv_Periodo", True))
        Me.ComboBox15.DataSource = Me.MUESTRACatalogoPeriodosCorteBindingSource
        Me.ComboBox15.DisplayMember = "Descripcion"
        Me.ComboBox15.Enabled = False
        Me.ComboBox15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox15.FormattingEnabled = True
        Me.ComboBox15.Location = New System.Drawing.Point(333, 199)
        Me.ComboBox15.Name = "ComboBox15"
        Me.ComboBox15.Size = New System.Drawing.Size(212, 24)
        Me.ComboBox15.TabIndex = 9
        Me.ComboBox15.ValueMember = "Clv_Periodo"
        '
        'MUESTRACatalogoPeriodosCorteBindingSource
        '
        Me.MUESTRACatalogoPeriodosCorteBindingSource.DataMember = "MUESTRACatalogoPeriodosCorte"
        Me.MUESTRACatalogoPeriodosCorteBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComboBox11
        '
        Me.ComboBox11.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONSULTARCLIENTEBindingSource, "clv_sector", True))
        Me.ComboBox11.DataSource = Me.MUESTRATABSBindingSource
        Me.ComboBox11.DisplayMember = "Sector"
        Me.ComboBox11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox11.FormattingEnabled = True
        Me.ComboBox11.Location = New System.Drawing.Point(333, 246)
        Me.ComboBox11.Name = "ComboBox11"
        Me.ComboBox11.Size = New System.Drawing.Size(213, 24)
        Me.ComboBox11.TabIndex = 7
        Me.ComboBox11.ValueMember = "Clv_Sector"
        '
        'MUESTRATABSBindingSource
        '
        Me.MUESTRATABSBindingSource.DataMember = "MUESTRATABS"
        Me.MUESTRATABSBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComboBox7
        '
        Me.ComboBox7.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONRelClientesTiposClientesBindingSource, "Clv_TipoCliente", True))
        Me.ComboBox7.DataSource = Me.MUESTRATIPOCLIENTESBindingSource
        Me.ComboBox7.DisplayMember = "DESCRIPCION"
        Me.ComboBox7.Enabled = False
        Me.ComboBox7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox7.ForeColor = System.Drawing.Color.Red
        Me.ComboBox7.FormattingEnabled = True
        Me.ComboBox7.Location = New System.Drawing.Point(563, 55)
        Me.ComboBox7.Name = "ComboBox7"
        Me.ComboBox7.Size = New System.Drawing.Size(204, 23)
        Me.ComboBox7.TabIndex = 14
        Me.ComboBox7.ValueMember = "CLV_TIPOCLIENTE"
        '
        'CONRelClientesTiposClientesBindingSource
        '
        Me.CONRelClientesTiposClientesBindingSource.DataMember = "CONRel_Clientes_TiposClientes"
        Me.CONRelClientesTiposClientesBindingSource.DataSource = Me.DataSetEDGAR
        '
        'MUESTRATIPOCLIENTESBindingSource
        '
        Me.MUESTRATIPOCLIENTESBindingSource.DataMember = "MUESTRA_TIPOCLIENTES"
        Me.MUESTRATIPOCLIENTESBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DigitalTextBox
        '
        Me.DigitalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.HaberServicios_CliBindingSource, "Digital", True))
        Me.DigitalTextBox.Location = New System.Drawing.Point(261, 0)
        Me.DigitalTextBox.Name = "DigitalTextBox"
        Me.DigitalTextBox.Size = New System.Drawing.Size(100, 20)
        Me.DigitalTextBox.TabIndex = 71
        Me.DigitalTextBox.TabStop = False
        '
        'HaberServicios_CliBindingSource
        '
        Me.HaberServicios_CliBindingSource.DataMember = "HaberServicios_Cli"
        Me.HaberServicios_CliBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'InternetTextBox
        '
        Me.InternetTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.HaberServicios_CliBindingSource, "Internet", True))
        Me.InternetTextBox.Location = New System.Drawing.Point(262, 0)
        Me.InternetTextBox.Name = "InternetTextBox"
        Me.InternetTextBox.Size = New System.Drawing.Size(100, 20)
        Me.InternetTextBox.TabIndex = 70
        Me.InternetTextBox.TabStop = False
        '
        'BasicoTextBox
        '
        Me.BasicoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.HaberServicios_CliBindingSource, "Basico", True))
        Me.BasicoTextBox.Location = New System.Drawing.Point(262, 3)
        Me.BasicoTextBox.Name = "BasicoTextBox"
        Me.BasicoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.BasicoTextBox.TabIndex = 69
        Me.BasicoTextBox.TabStop = False
        '
        'EmailTextBox
        '
        Me.EmailTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EmailTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmailTextBox.Location = New System.Drawing.Point(607, 245)
        Me.EmailTextBox.MaxLength = 150
        Me.EmailTextBox.Name = "EmailTextBox"
        Me.EmailTextBox.Size = New System.Drawing.Size(178, 21)
        Me.EmailTextBox.TabIndex = 13
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NOMBRETextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NOMBRETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTARCLIENTEBindingSource, "NOMBRE", True))
        Me.NOMBRETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(115, 63)
        Me.NOMBRETextBox.MaxLength = 200
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.NOMBRETextBox.Size = New System.Drawing.Size(432, 22)
        Me.NOMBRETextBox.TabIndex = 0
        '
        'NUMEROTextBox
        '
        Me.NUMEROTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NUMEROTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NUMEROTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTARCLIENTEBindingSource, "NUMERO", True))
        Me.NUMEROTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROTextBox.Location = New System.Drawing.Point(297, 111)
        Me.NUMEROTextBox.MaxLength = 50
        Me.NUMEROTextBox.Name = "NUMEROTextBox"
        Me.NUMEROTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.NUMEROTextBox.Size = New System.Drawing.Size(121, 22)
        Me.NUMEROTextBox.TabIndex = 4
        '
        'ENTRECALLESTextBox
        '
        Me.ENTRECALLESTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ENTRECALLESTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ENTRECALLESTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTARCLIENTEBindingSource, "ENTRECALLES", True))
        Me.ENTRECALLESTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ENTRECALLESTextBox.Location = New System.Drawing.Point(11, 158)
        Me.ENTRECALLESTextBox.MaxLength = 150
        Me.ENTRECALLESTextBox.Name = "ENTRECALLESTextBox"
        Me.ENTRECALLESTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ENTRECALLESTextBox.Size = New System.Drawing.Size(407, 22)
        Me.ENTRECALLESTextBox.TabIndex = 6
        '
        'CodigoPostalTextBox
        '
        Me.CodigoPostalTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CodigoPostalTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CodigoPostalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DAMECOLONIACALLEBindingSource, "cp", True))
        Me.CodigoPostalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CodigoPostalTextBox.Location = New System.Drawing.Point(424, 158)
        Me.CodigoPostalTextBox.Name = "CodigoPostalTextBox"
        Me.CodigoPostalTextBox.ReadOnly = True
        Me.CodigoPostalTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CodigoPostalTextBox.Size = New System.Drawing.Size(121, 22)
        Me.CodigoPostalTextBox.TabIndex = 7
        Me.CodigoPostalTextBox.TabStop = False
        '
        'TELEFONOTextBox
        '
        Me.TELEFONOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TELEFONOTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TELEFONOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTARCLIENTEBindingSource, "TELEFONO", True))
        Me.TELEFONOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TELEFONOTextBox.Location = New System.Drawing.Point(607, 162)
        Me.TELEFONOTextBox.MaxLength = 30
        Me.TELEFONOTextBox.Name = "TELEFONOTextBox"
        Me.TELEFONOTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TELEFONOTextBox.Size = New System.Drawing.Size(178, 22)
        Me.TELEFONOTextBox.TabIndex = 11
        Me.TELEFONOTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CELULARTextBox
        '
        Me.CELULARTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CELULARTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CELULARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTARCLIENTEBindingSource, "CELULAR", True))
        Me.CELULARTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CELULARTextBox.Location = New System.Drawing.Point(607, 202)
        Me.CELULARTextBox.MaxLength = 30
        Me.CELULARTextBox.Name = "CELULARTextBox"
        Me.CELULARTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CELULARTextBox.Size = New System.Drawing.Size(178, 22)
        Me.CELULARTextBox.TabIndex = 12
        Me.CELULARTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DESGLOSA_IvaCheckBox
        '
        Me.DESGLOSA_IvaCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSULTARCLIENTEBindingSource, "DESGLOSA_Iva", True))
        Me.DESGLOSA_IvaCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.DESGLOSA_IvaCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DESGLOSA_IvaCheckBox.ForeColor = System.Drawing.Color.RoyalBlue
        Me.DESGLOSA_IvaCheckBox.Location = New System.Drawing.Point(595, 117)
        Me.DESGLOSA_IvaCheckBox.Name = "DESGLOSA_IvaCheckBox"
        Me.DESGLOSA_IvaCheckBox.Size = New System.Drawing.Size(200, 24)
        Me.DESGLOSA_IvaCheckBox.TabIndex = 11
        Me.DESGLOSA_IvaCheckBox.Text = "Desglosa I.V.A.  "
        '
        'SoloInternetCheckBox
        '
        Me.SoloInternetCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSULTARCLIENTEBindingSource, "SoloInternet", True))
        Me.SoloInternetCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.SoloInternetCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SoloInternetCheckBox.ForeColor = System.Drawing.Color.RoyalBlue
        Me.SoloInternetCheckBox.Location = New System.Drawing.Point(594, 87)
        Me.SoloInternetCheckBox.Name = "SoloInternetCheckBox"
        Me.SoloInternetCheckBox.Size = New System.Drawing.Size(202, 24)
        Me.SoloInternetCheckBox.TabIndex = 10
        Me.SoloInternetCheckBox.Text = "Solo Internet"
        '
        'EshotelCheckBox
        '
        Me.EshotelCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSULTARCLIENTEBindingSource, "eshotel", True))
        Me.EshotelCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.EshotelCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EshotelCheckBox.ForeColor = System.Drawing.Color.RoyalBlue
        Me.EshotelCheckBox.Location = New System.Drawing.Point(430, 63)
        Me.EshotelCheckBox.Name = "EshotelCheckBox"
        Me.EshotelCheckBox.Size = New System.Drawing.Size(115, 24)
        Me.EshotelCheckBox.TabIndex = 79
        Me.EshotelCheckBox.TabStop = False
        Me.EshotelCheckBox.Text = "Es un Hotel"
        Me.EshotelCheckBox.Visible = False
        '
        'Clv_CiudadTextBox
        '
        Me.Clv_CiudadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTARCLIENTEBindingSource, "clv_Ciudad", True))
        Me.Clv_CiudadTextBox.Location = New System.Drawing.Point(137, 246)
        Me.Clv_CiudadTextBox.Name = "Clv_CiudadTextBox"
        Me.Clv_CiudadTextBox.Size = New System.Drawing.Size(32, 20)
        Me.Clv_CiudadTextBox.TabIndex = 67
        Me.Clv_CiudadTextBox.TabStop = False
        '
        'Clv_ColoniaTextBox
        '
        Me.Clv_ColoniaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTARCLIENTEBindingSource, "Clv_Colonia", True))
        Me.Clv_ColoniaTextBox.Location = New System.Drawing.Point(236, 201)
        Me.Clv_ColoniaTextBox.Name = "Clv_ColoniaTextBox"
        Me.Clv_ColoniaTextBox.Size = New System.Drawing.Size(45, 20)
        Me.Clv_ColoniaTextBox.TabIndex = 49
        Me.Clv_ColoniaTextBox.TabStop = False
        '
        'Clv_CalleTextBox
        '
        Me.Clv_CalleTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTARCLIENTEBindingSource, "Clv_Calle", True))
        Me.Clv_CalleTextBox.Location = New System.Drawing.Point(223, 112)
        Me.Clv_CalleTextBox.Name = "Clv_CalleTextBox"
        Me.Clv_CalleTextBox.Size = New System.Drawing.Size(39, 20)
        Me.Clv_CalleTextBox.TabIndex = 79
        Me.Clv_CalleTextBox.TabStop = False
        '
        'CONTRATOTextBox
        '
        Me.CONTRATOTextBox.BackColor = System.Drawing.Color.LightSteelBlue
        Me.CONTRATOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CONTRATOTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CONTRATOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTARCLIENTEBindingSource, "CONTRATO", True))
        Me.CONTRATOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTRATOTextBox.ForeColor = System.Drawing.Color.Black
        Me.CONTRATOTextBox.Location = New System.Drawing.Point(10, 63)
        Me.CONTRATOTextBox.Name = "CONTRATOTextBox"
        Me.CONTRATOTextBox.ReadOnly = True
        Me.CONTRATOTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CONTRATOTextBox.Size = New System.Drawing.Size(99, 22)
        Me.CONTRATOTextBox.TabIndex = 37
        Me.CONTRATOTextBox.TabStop = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(866, 688)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 30
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Enabled = False
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(826, 2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(181, 26)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "&Historial de Pagos"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Enabled = False
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(827, 31)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(181, 26)
        Me.Button2.TabIndex = 14
        Me.Button2.Text = "Datos &Fiscales"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Enabled = False
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(827, 60)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(181, 26)
        Me.Button3.TabIndex = 15
        Me.Button3.Text = "Datos &Bancarios"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.Enabled = False
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(827, 89)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(181, 26)
        Me.Button4.TabIndex = 16
        Me.Button4.Text = "Ord&enes de Servicio"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Enabled = False
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.White
        Me.Button6.Location = New System.Drawing.Point(827, 118)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(181, 26)
        Me.Button6.TabIndex = 17
        Me.Button6.Text = "Que&jas"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.Enabled = False
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.White
        Me.Button7.Location = New System.Drawing.Point(137, 289)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(127, 36)
        Me.Button7.TabIndex = 21
        Me.Button7.Text = "&Internet"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.DarkOrange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.White
        Me.Button8.Location = New System.Drawing.Point(3, 289)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(128, 36)
        Me.Button8.TabIndex = 20
        Me.Button8.Text = "&Television"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'CONCLIENTETVBindingSource
        '
        Me.CONCLIENTETVBindingSource.DataMember = "CONCLIENTETV"
        Me.CONCLIENTETVBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ConRelCteDescuentoBindingSource
        '
        Me.ConRelCteDescuentoBindingSource.DataMember = "ConRelCteDescuento"
        Me.ConRelCteDescuentoBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConRelClientesTvVendedorBindingSource
        '
        Me.ConRelClientesTvVendedorBindingSource.DataMember = "ConRel_ClientesTv_Vendedor"
        Me.ConRelClientesTvVendedorBindingSource.DataSource = Me.DataSetEDGAR
        '
        'MuestraPromotoresTvBindingSource
        '
        Me.MuestraPromotoresTvBindingSource.DataMember = "MuestraPromotoresTv"
        Me.MuestraPromotoresTvBindingSource.DataSource = Me.DataSetEDGAR
        '
        'ConRelCtePlacaBindingSource
        '
        Me.ConRelCtePlacaBindingSource.DataMember = "ConRelCtePlaca"
        Me.ConRelCtePlacaBindingSource.DataSource = Me.DataSetEric
        '
        'MuestraTiposServicioTvBindingSource
        '
        Me.MuestraTiposServicioTvBindingSource.DataMember = "MuestraTiposServicioTv"
        Me.MuestraTiposServicioTvBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'StatusBasicoBindingSource
        '
        Me.StatusBasicoBindingSource.DataMember = "StatusBasico"
        Me.StatusBasicoBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MuestraMotivoCancelacionBindingSource
        '
        Me.MuestraMotivoCancelacionBindingSource.DataMember = "MuestraMotivoCancelacion"
        Me.MuestraMotivoCancelacionBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NOMBRELabel3
        '
        Me.NOMBRELabel3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONRel_ClientesTv_UsuariosBindingSource, "NOMBRE", True))
        Me.NOMBRELabel3.Enabled = False
        Me.NOMBRELabel3.Location = New System.Drawing.Point(232, 239)
        Me.NOMBRELabel3.Name = "NOMBRELabel3"
        Me.NOMBRELabel3.Size = New System.Drawing.Size(326, 23)
        Me.NOMBRELabel3.TabIndex = 31
        '
        'CONRel_ClientesTv_UsuariosBindingSource
        '
        Me.CONRel_ClientesTv_UsuariosBindingSource.DataMember = "CONRel_ClientesTv_Usuarios"
        Me.CONRel_ClientesTv_UsuariosBindingSource.DataSource = Me.DataSetEDGAR
        '
        'VerAparatodelClienteBindingSource
        '
        Me.VerAparatodelClienteBindingSource.DataMember = "VerAparatodelCliente"
        Me.VerAparatodelClienteBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONSULTACLIENTESNETBindingSource
        '
        Me.CONSULTACLIENTESNETBindingSource.DataMember = "CONSULTACLIENTESNET"
        Me.CONSULTACLIENTESNETBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'StatusCableModemBindingSource
        '
        Me.StatusCableModemBindingSource.DataMember = "StatusCableModem"
        Me.StatusCableModemBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'TipoCablemodemBindingSource
        '
        Me.TipoCablemodemBindingSource.DataMember = "TipoCablemodem"
        Me.TipoCablemodemBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MuestraTipSerInternetBindingSource
        '
        Me.MuestraTipSerInternetBindingSource.DataMember = "MuestraTipSerInternet"
        Me.MuestraTipSerInternetBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONRel_ContNet_UsuariosBindingSource
        '
        Me.CONRel_ContNet_UsuariosBindingSource.DataMember = "CONRel_ContNet_Usuarios"
        Me.CONRel_ContNet_UsuariosBindingSource.DataSource = Me.DataSetEDGAR
        '
        'CONSULTACONTNETBindingSource
        '
        Me.CONSULTACONTNETBindingSource.DataMember = "CONSULTACONTNET"
        Me.CONSULTACONTNETBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'StatusNetBindingSource
        '
        Me.StatusNetBindingSource.DataMember = "StatusNet"
        Me.StatusNetBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MuestraPromotoresNetBindingSource
        '
        Me.MuestraPromotoresNetBindingSource.DataMember = "MuestraPromotoresNet"
        Me.MuestraPromotoresNetBindingSource.DataSource = Me.DataSetEDGAR
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel7.Controls.Add(Me.Panel9)
        Me.Panel7.Controls.Add(Me.Panel10)
        Me.Panel7.Controls.Add(Me.SplitContainer2)
        Me.Panel7.Enabled = False
        Me.Panel7.Location = New System.Drawing.Point(3, 331)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(1010, 347)
        Me.Panel7.TabIndex = 23
        Me.Panel7.TabStop = True
        Me.Panel7.Visible = False
        '
        'Panel9
        '
        Me.Panel9.AutoScroll = True
        Me.Panel9.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel9.Controls.Add(Me.CMBLabel54)
        Me.Panel9.Controls.Add(Me.CMBLabel53)
        Me.Panel9.Controls.Add(Me.Label41)
        Me.Panel9.Controls.Add(Me.BindingNavigator4)
        Me.Panel9.Controls.Add(Me.TextBox29)
        Me.Panel9.Controls.Add(Me.CMB2Label5)
        Me.Panel9.Controls.Add(Me.ComboBox13)
        Me.Panel9.Controls.Add(Label9)
        Me.Panel9.Controls.Add(Me.TextBox23)
        Me.Panel9.Controls.Add(Me.TextBox24)
        Me.Panel9.Controls.Add(Me.TextBox25)
        Me.Panel9.Controls.Add(Me.TextBox26)
        Me.Panel9.Controls.Add(Label30)
        Me.Panel9.Controls.Add(Me.CheckBox2)
        Me.Panel9.Controls.Add(Me.CMBTextBox28)
        Me.Panel9.Controls.Add(Label31)
        Me.Panel9.Controls.Add(Me.TextBox30)
        Me.Panel9.Controls.Add(Label34)
        Me.Panel9.Controls.Add(Me.TextBox32)
        Me.Panel9.Controls.Add(Label35)
        Me.Panel9.Controls.Add(Me.TextBox33)
        Me.Panel9.Controls.Add(Me.TextBox34)
        Me.Panel9.Controls.Add(Me.Label42)
        Me.Panel9.Controls.Add(Me.Label43)
        Me.Panel9.Controls.Add(CMBLabel44)
        Me.Panel9.Controls.Add(CMBLabel45)
        Me.Panel9.Enabled = False
        Me.Panel9.Location = New System.Drawing.Point(445, 3)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(548, 311)
        Me.Panel9.TabIndex = 26
        Me.Panel9.TabStop = True
        '
        'CMBLabel54
        '
        Me.CMBLabel54.AutoSize = True
        Me.CMBLabel54.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel54.ForeColor = System.Drawing.Color.OrangeRed
        Me.CMBLabel54.Location = New System.Drawing.Point(12, 31)
        Me.CMBLabel54.Name = "CMBLabel54"
        Me.CMBLabel54.Size = New System.Drawing.Size(203, 18)
        Me.CMBLabel54.TabIndex = 60
        Me.CMBLabel54.Text = "Datos de la Tarjeta Digital"
        Me.CMBLabel54.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CMBLabel53
        '
        Me.CMBLabel53.AutoSize = True
        Me.CMBLabel53.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel53.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CMBLabel53.Location = New System.Drawing.Point(36, 95)
        Me.CMBLabel53.Name = "CMBLabel53"
        Me.CMBLabel53.Size = New System.Drawing.Size(104, 15)
        Me.CMBLabel53.TabIndex = 59
        Me.CMBLabel53.Text = "Status Tarjeta :"
        '
        'Label41
        '
        Me.Label41.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Label41.CausesValidation = False
        Me.Label41.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VerAparatodelClientedigBindingSource, "MACCABLEMODEM", True))
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(221, 31)
        Me.Label41.Multiline = True
        Me.Label41.Name = "Label41"
        Me.Label41.ReadOnly = True
        Me.Label41.Size = New System.Drawing.Size(173, 20)
        Me.Label41.TabIndex = 58
        '
        'VerAparatodelClientedigBindingSource
        '
        Me.VerAparatodelClientedigBindingSource.DataMember = "VerAparatodelClientedig"
        Me.VerAparatodelClientedigBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BindingNavigator4
        '
        Me.BindingNavigator4.AddNewItem = Nothing
        Me.BindingNavigator4.BindingSource = Me.CONSULTACLIENTESDIGBindingSource
        Me.BindingNavigator4.CountItem = Nothing
        Me.BindingNavigator4.DeleteItem = Nothing
        Me.BindingNavigator4.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSeparator9, Me.ToolStripButton11, Me.ToolStripSeparator10, Me.ToolStripButton12})
        Me.BindingNavigator4.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator4.MoveFirstItem = Nothing
        Me.BindingNavigator4.MoveLastItem = Nothing
        Me.BindingNavigator4.MoveNextItem = Nothing
        Me.BindingNavigator4.MovePreviousItem = Nothing
        Me.BindingNavigator4.Name = "BindingNavigator4"
        Me.BindingNavigator4.PositionItem = Nothing
        Me.BindingNavigator4.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.BindingNavigator4.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator4.Size = New System.Drawing.Size(548, 25)
        Me.BindingNavigator4.TabIndex = 27
        Me.BindingNavigator4.TabStop = True
        Me.BindingNavigator4.Text = "BindingNavigator4"
        '
        'CONSULTACLIENTESDIGBindingSource
        '
        Me.CONSULTACLIENTESDIGBindingSource.DataMember = "CONSULTACLIENTESDIG"
        Me.CONSULTACLIENTESDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton11
        '
        Me.ToolStripButton11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton11.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton11.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton11.Name = "ToolStripButton11"
        Me.ToolStripButton11.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton11.Size = New System.Drawing.Size(76, 22)
        Me.ToolStripButton11.Text = "CA&NCELAR"
        Me.ToolStripButton11.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton12
        '
        Me.ToolStripButton12.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton12.Image = CType(resources.GetObject("ToolStripButton12.Image"), System.Drawing.Image)
        Me.ToolStripButton12.Name = "ToolStripButton12"
        Me.ToolStripButton12.Size = New System.Drawing.Size(88, 22)
        Me.ToolStripButton12.Text = "GUA&RDAR"
        '
        'TextBox29
        '
        Me.TextBox29.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACLIENTESDIGBindingSource, "Obs", True))
        Me.TextBox29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox29.Location = New System.Drawing.Point(19, 146)
        Me.TextBox29.MaxLength = 250
        Me.TextBox29.Multiline = True
        Me.TextBox29.Name = "TextBox29"
        Me.TextBox29.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox29.Size = New System.Drawing.Size(304, 107)
        Me.TextBox29.TabIndex = 26
        '
        'CMB2Label5
        '
        Me.CMB2Label5.AutoSize = True
        Me.CMB2Label5.BackColor = System.Drawing.Color.Gold
        Me.CMB2Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMB2Label5.ForeColor = System.Drawing.Color.Red
        Me.CMB2Label5.Location = New System.Drawing.Point(7, 256)
        Me.CMB2Label5.Name = "CMB2Label5"
        Me.CMB2Label5.Size = New System.Drawing.Size(273, 15)
        Me.CMB2Label5.TabIndex = 57
        Me.CMB2Label5.Text = """El Servicio es de Prueba y Vence el Dia """
        Me.CMB2Label5.Visible = False
        '
        'ComboBox13
        '
        Me.ComboBox13.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONSULTACLIENTESDIGBindingSource, "Status", True))
        Me.ComboBox13.DataSource = Me.StatusCableModemBindingSource
        Me.ComboBox13.DisplayMember = "Concepto"
        Me.ComboBox13.Enabled = False
        Me.ComboBox13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox13.FormattingEnabled = True
        Me.ComboBox13.Location = New System.Drawing.Point(138, 88)
        Me.ComboBox13.Name = "ComboBox13"
        Me.ComboBox13.Size = New System.Drawing.Size(145, 23)
        Me.ComboBox13.TabIndex = 50
        Me.ComboBox13.TabStop = False
        Me.ComboBox13.ValueMember = "Clv_StatusCableModem"
        '
        'TextBox23
        '
        Me.TextBox23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox23.CausesValidation = False
        Me.TextBox23.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACLIENTESDIGBindingSource, "Fecha_Baja", True))
        Me.TextBox23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox23.Location = New System.Drawing.Point(432, 143)
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.ReadOnly = True
        Me.TextBox23.Size = New System.Drawing.Size(100, 21)
        Me.TextBox23.TabIndex = 55
        Me.TextBox23.TabStop = False
        '
        'TextBox24
        '
        Me.TextBox24.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACLIENTESDIGBindingSource, "ContratoNet", True))
        Me.TextBox24.Location = New System.Drawing.Point(378, 253)
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.Size = New System.Drawing.Size(100, 20)
        Me.TextBox24.TabIndex = 54
        Me.TextBox24.TabStop = False
        Me.TextBox24.Visible = False
        '
        'TextBox25
        '
        Me.TextBox25.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACLIENTESDIGBindingSource, "Contrato", True))
        Me.TextBox25.Location = New System.Drawing.Point(378, 229)
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Size = New System.Drawing.Size(100, 20)
        Me.TextBox25.TabIndex = 53
        Me.TextBox25.TabStop = False
        Me.TextBox25.Visible = False
        '
        'TextBox26
        '
        Me.TextBox26.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox26.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox26.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACLIENTESDIGBindingSource, "Status", True))
        Me.TextBox26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox26.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox26.Location = New System.Drawing.Point(231, 120)
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Size = New System.Drawing.Size(38, 14)
        Me.TextBox26.TabIndex = 8
        Me.TextBox26.TabStop = False
        '
        'CheckBox2
        '
        Me.CheckBox2.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSULTACLIENTESDIGBindingSource, "SeRenta", True))
        Me.CheckBox2.Location = New System.Drawing.Point(450, 282)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(16, 24)
        Me.CheckBox2.TabIndex = 47
        Me.CheckBox2.TabStop = False
        '
        'CMBTextBox28
        '
        Me.CMBTextBox28.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox28.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox28.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox28.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox28.Location = New System.Drawing.Point(347, 58)
        Me.CMBTextBox28.Name = "CMBTextBox28"
        Me.CMBTextBox28.ReadOnly = True
        Me.CMBTextBox28.Size = New System.Drawing.Size(189, 19)
        Me.CMBTextBox28.TabIndex = 46
        Me.CMBTextBox28.TabStop = False
        Me.CMBTextBox28.Text = "Fechas de "
        Me.CMBTextBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox30
        '
        Me.TextBox30.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox30.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox30.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACLIENTESNETBindingSource, "Tipo_Cablemodem", True))
        Me.TextBox30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox30.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox30.Location = New System.Drawing.Point(163, 117)
        Me.TextBox30.Name = "TextBox30"
        Me.TextBox30.Size = New System.Drawing.Size(31, 13)
        Me.TextBox30.TabIndex = 22
        Me.TextBox30.TabStop = False
        '
        'TextBox32
        '
        Me.TextBox32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox32.CausesValidation = False
        Me.TextBox32.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACLIENTESDIGBindingSource, "Fecha_Suspencion", True))
        Me.TextBox32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox32.Location = New System.Drawing.Point(432, 116)
        Me.TextBox32.Name = "TextBox32"
        Me.TextBox32.ReadOnly = True
        Me.TextBox32.Size = New System.Drawing.Size(100, 21)
        Me.TextBox32.TabIndex = 18
        Me.TextBox32.TabStop = False
        '
        'TextBox33
        '
        Me.TextBox33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox33.CausesValidation = False
        Me.TextBox33.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACLIENTESDIGBindingSource, "Fecha_Activacion", True))
        Me.TextBox33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox33.Location = New System.Drawing.Point(432, 90)
        Me.TextBox33.Name = "TextBox33"
        Me.TextBox33.ReadOnly = True
        Me.TextBox33.Size = New System.Drawing.Size(100, 21)
        Me.TextBox33.TabIndex = 16
        Me.TextBox33.TabStop = False
        '
        'TextBox34
        '
        Me.TextBox34.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox34.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox34.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACLIENTESNETBindingSource, "Clv_TipoServicio", True))
        Me.TextBox34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox34.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.TextBox34.Location = New System.Drawing.Point(199, 121)
        Me.TextBox34.Name = "TextBox34"
        Me.TextBox34.Size = New System.Drawing.Size(31, 14)
        Me.TextBox34.TabIndex = 14
        Me.TextBox34.TabStop = False
        '
        'Label42
        '
        Me.Label42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label42.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VerAparatodelClientedigBindingSource, "TIPOAPARATO", True))
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(126, 225)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(123, 23)
        Me.Label42.TabIndex = 5
        '
        'Label43
        '
        Me.Label43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label43.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VerAparatodelClientedigBindingSource, "MARCA", True))
        Me.Label43.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(126, 196)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(100, 23)
        Me.Label43.TabIndex = 3
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.ComboBox12)
        Me.Panel10.Controls.Add(Me.ComboBox14)
        Me.Panel10.Controls.Add(MACCABLEMODEMLabel)
        Me.Panel10.Controls.Add(Me.MACCABLEMODEMTextBox)
        Me.Panel10.Controls.Add(CONTRATONETLabel)
        Me.Panel10.Controls.Add(Me.CONTRATONETTextBox2)
        Me.Panel10.Controls.Add(RespuestaLabel)
        Me.Panel10.Controls.Add(Me.RespuestaTextBox)
        Me.Panel10.Controls.Add(Me.CMBLabel32)
        Me.Panel10.Controls.Add(Me.Button14)
        Me.Panel10.Controls.Add(Me.CMBLabel29)
        Me.Panel10.Controls.Add(Me.Button13)
        Me.Panel10.Controls.Add(Me.NOMBRELabel3)
        Me.Panel10.Enabled = False
        Me.Panel10.Location = New System.Drawing.Point(6, 0)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(433, 320)
        Me.Panel10.TabIndex = 23
        Me.Panel10.TabStop = True
        Me.Panel10.Visible = False
        '
        'ComboBox12
        '
        Me.ComboBox12.DataSource = Me.MuestraServiciosdigitalBindingSource
        Me.ComboBox12.DisplayMember = "Descripcion"
        Me.ComboBox12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox12.FormattingEnabled = True
        Me.ComboBox12.Location = New System.Drawing.Point(18, 118)
        Me.ComboBox12.Name = "ComboBox12"
        Me.ComboBox12.Size = New System.Drawing.Size(363, 23)
        Me.ComboBox12.TabIndex = 24
        Me.ComboBox12.ValueMember = "Clv_Servicio"
        '
        'MuestraServiciosdigitalBindingSource
        '
        Me.MuestraServiciosdigitalBindingSource.DataMember = "MuestraServicios_digital"
        Me.MuestraServiciosdigitalBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ComboBox14
        '
        Me.ComboBox14.DataSource = Me.MUESTRADIGITALDELCLIBindingSource
        Me.ComboBox14.DisplayMember = "MACCABLEMODEM"
        Me.ComboBox14.Enabled = False
        Me.ComboBox14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox14.ForeColor = System.Drawing.Color.Red
        Me.ComboBox14.FormattingEnabled = True
        Me.ComboBox14.Location = New System.Drawing.Point(18, 37)
        Me.ComboBox14.Name = "ComboBox14"
        Me.ComboBox14.Size = New System.Drawing.Size(371, 24)
        Me.ComboBox14.TabIndex = 23
        Me.ComboBox14.ValueMember = "CONTRATONET"
        '
        'MUESTRADIGITALDELCLIBindingSource
        '
        Me.MUESTRADIGITALDELCLIBindingSource.DataMember = "MUESTRADIGITALDELCLI"
        Me.MUESTRADIGITALDELCLIBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MACCABLEMODEMTextBox
        '
        Me.MACCABLEMODEMTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MUESTRADIGITALDELCLI_porAparatoBindingSource, "MACCABLEMODEM", True))
        Me.MACCABLEMODEMTextBox.Location = New System.Drawing.Point(239, 39)
        Me.MACCABLEMODEMTextBox.Name = "MACCABLEMODEMTextBox"
        Me.MACCABLEMODEMTextBox.Size = New System.Drawing.Size(100, 20)
        Me.MACCABLEMODEMTextBox.TabIndex = 11
        Me.MACCABLEMODEMTextBox.TabStop = False
        '
        'MUESTRADIGITALDELCLI_porAparatoBindingSource
        '
        Me.MUESTRADIGITALDELCLI_porAparatoBindingSource.DataMember = "MUESTRADIGITALDELCLI_porAparato"
        Me.MUESTRADIGITALDELCLI_porAparatoBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONTRATONETTextBox2
        '
        Me.CONTRATONETTextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MUESTRADIGITALDELCLI_porAparatoBindingSource, "CONTRATONET", True))
        Me.CONTRATONETTextBox2.Location = New System.Drawing.Point(118, 37)
        Me.CONTRATONETTextBox2.Name = "CONTRATONETTextBox2"
        Me.CONTRATONETTextBox2.Size = New System.Drawing.Size(100, 20)
        Me.CONTRATONETTextBox2.TabIndex = 10
        Me.CONTRATONETTextBox2.TabStop = False
        '
        'RespuestaTextBox
        '
        Me.RespuestaTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.RespuestaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RespuestaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ValidaDigitalBindingSource, "Respuesta", True))
        Me.RespuestaTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.RespuestaTextBox.Location = New System.Drawing.Point(200, 154)
        Me.RespuestaTextBox.Name = "RespuestaTextBox"
        Me.RespuestaTextBox.Size = New System.Drawing.Size(100, 13)
        Me.RespuestaTextBox.TabIndex = 9
        Me.RespuestaTextBox.TabStop = False
        '
        'ValidaDigitalBindingSource
        '
        Me.ValidaDigitalBindingSource.DataMember = "ValidaDigital"
        Me.ValidaDigitalBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CMBLabel32
        '
        Me.CMBLabel32.AutoSize = True
        Me.CMBLabel32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel32.ForeColor = System.Drawing.Color.DarkOrange
        Me.CMBLabel32.Location = New System.Drawing.Point(6, 16)
        Me.CMBLabel32.Name = "CMBLabel32"
        Me.CMBLabel32.Size = New System.Drawing.Size(369, 16)
        Me.CMBLabel32.TabIndex = 8
        Me.CMBLabel32.Text = "Seleccione la Tarjeta que le va Asignar el Paquete :"
        '
        'Button14
        '
        Me.Button14.BackColor = System.Drawing.Color.DarkOrange
        Me.Button14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button14.ForeColor = System.Drawing.Color.Black
        Me.Button14.Location = New System.Drawing.Point(154, 193)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(139, 33)
        Me.Button14.TabIndex = 26
        Me.Button14.Text = "C&ANCELAR"
        Me.Button14.UseVisualStyleBackColor = False
        '
        'CMBLabel29
        '
        Me.CMBLabel29.AutoSize = True
        Me.CMBLabel29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel29.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CMBLabel29.Location = New System.Drawing.Point(4, 97)
        Me.CMBLabel29.Name = "CMBLabel29"
        Me.CMBLabel29.Size = New System.Drawing.Size(243, 16)
        Me.CMBLabel29.TabIndex = 4
        Me.CMBLabel29.Text = "Seleccione el Paquete a Asignar :"
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.Color.DarkOrange
        Me.Button13.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.ForeColor = System.Drawing.Color.Black
        Me.Button13.Location = New System.Drawing.Point(9, 193)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(139, 33)
        Me.Button13.TabIndex = 25
        Me.Button13.Text = "G&UARDAR"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.AutoScroll = True
        Me.SplitContainer2.Panel1.Controls.Add(Me.TreeView3)
        Me.SplitContainer2.Panel1.Controls.Add(Me.CMBLabel10)
        Me.SplitContainer2.Panel1.Controls.Add(Me.BindingNavigator5)
        Me.SplitContainer2.Panel1.Controls.Add(Me.BindingNavigator6)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.AutoScroll = True
        Me.SplitContainer2.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer2.Panel2.Controls.Add(Me.Panel8)
        Me.SplitContainer2.Size = New System.Drawing.Size(1001, 324)
        Me.SplitContainer2.SplitterDistance = 436
        Me.SplitContainer2.TabIndex = 23
        Me.SplitContainer2.TabStop = False
        '
        'TreeView3
        '
        Me.TreeView3.BackColor = System.Drawing.Color.Gainsboro
        Me.TreeView3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView3.ForeColor = System.Drawing.Color.Navy
        Me.TreeView3.Location = New System.Drawing.Point(12, 85)
        Me.TreeView3.Name = "TreeView3"
        Me.TreeView3.Size = New System.Drawing.Size(383, 217)
        Me.TreeView3.TabIndex = 25
        '
        'CMBLabel10
        '
        Me.CMBLabel10.AutoSize = True
        Me.CMBLabel10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel10.ForeColor = System.Drawing.Color.DarkOrange
        Me.CMBLabel10.Location = New System.Drawing.Point(9, 63)
        Me.CMBLabel10.Name = "CMBLabel10"
        Me.CMBLabel10.Size = New System.Drawing.Size(287, 16)
        Me.CMBLabel10.TabIndex = 4
        Me.CMBLabel10.Text = "Tarjetas Digitales Asignados al Cliente :"
        '
        'BindingNavigator5
        '
        Me.BindingNavigator5.AddNewItem = Nothing
        Me.BindingNavigator5.BindingSource = Me.CONSULTACLIENTESNETBindingSource
        Me.BindingNavigator5.CountItem = Nothing
        Me.BindingNavigator5.DeleteItem = Nothing
        Me.BindingNavigator5.Enabled = False
        Me.BindingNavigator5.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSeparator11, Me.ToolStripButton13, Me.ToolStripButton14, Me.ToolStripSeparator12})
        Me.BindingNavigator5.Location = New System.Drawing.Point(0, 25)
        Me.BindingNavigator5.MoveFirstItem = Nothing
        Me.BindingNavigator5.MoveLastItem = Nothing
        Me.BindingNavigator5.MoveNextItem = Nothing
        Me.BindingNavigator5.MovePreviousItem = Nothing
        Me.BindingNavigator5.Name = "BindingNavigator5"
        Me.BindingNavigator5.PositionItem = Nothing
        Me.BindingNavigator5.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator5.Size = New System.Drawing.Size(436, 25)
        Me.BindingNavigator5.TabIndex = 24
        Me.BindingNavigator5.TabStop = True
        Me.BindingNavigator5.Text = "BindingNavigator5"
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton13
        '
        Me.ToolStripButton13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton13.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton13.ForeColor = System.Drawing.Color.DodgerBlue
        Me.ToolStripButton13.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton13.Name = "ToolStripButton13"
        Me.ToolStripButton13.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton13.Size = New System.Drawing.Size(104, 22)
        Me.ToolStripButton13.Text = "Q&uitar Paquete"
        Me.ToolStripButton13.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        '
        'ToolStripButton14
        '
        Me.ToolStripButton14.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton14.ForeColor = System.Drawing.Color.DodgerBlue
        Me.ToolStripButton14.Image = CType(resources.GetObject("ToolStripButton14.Image"), System.Drawing.Image)
        Me.ToolStripButton14.Name = "ToolStripButton14"
        Me.ToolStripButton14.Size = New System.Drawing.Size(131, 22)
        Me.ToolStripButton14.Text = "Agregar &Paquete"
        '
        'ToolStripSeparator12
        '
        Me.ToolStripSeparator12.Name = "ToolStripSeparator12"
        Me.ToolStripSeparator12.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigator6
        '
        Me.BindingNavigator6.AddNewItem = Nothing
        Me.BindingNavigator6.BindingSource = Me.CONSULTACLIENTESNETBindingSource
        Me.BindingNavigator6.CountItem = Nothing
        Me.BindingNavigator6.DeleteItem = Nothing
        Me.BindingNavigator6.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSeparator13, Me.ToolStripButton15, Me.ToolStripButton16, Me.ToolStripSeparator14})
        Me.BindingNavigator6.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator6.MoveFirstItem = Nothing
        Me.BindingNavigator6.MoveLastItem = Nothing
        Me.BindingNavigator6.MoveNextItem = Nothing
        Me.BindingNavigator6.MovePreviousItem = Nothing
        Me.BindingNavigator6.Name = "BindingNavigator6"
        Me.BindingNavigator6.PositionItem = Nothing
        Me.BindingNavigator6.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator6.Size = New System.Drawing.Size(436, 25)
        Me.BindingNavigator6.TabIndex = 23
        Me.BindingNavigator6.TabStop = True
        Me.BindingNavigator6.Text = "BindingNavigator6"
        '
        'ToolStripSeparator13
        '
        Me.ToolStripSeparator13.Name = "ToolStripSeparator13"
        Me.ToolStripSeparator13.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton15
        '
        Me.ToolStripButton15.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton15.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton15.ForeColor = System.Drawing.Color.Red
        Me.ToolStripButton15.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton15.Name = "ToolStripButton15"
        Me.ToolStripButton15.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton15.Size = New System.Drawing.Size(139, 22)
        Me.ToolStripButton15.Text = "&Quitar Tarjeta Digital"
        Me.ToolStripButton15.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        '
        'ToolStripButton16
        '
        Me.ToolStripButton16.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton16.ForeColor = System.Drawing.Color.Red
        Me.ToolStripButton16.Image = CType(resources.GetObject("ToolStripButton16.Image"), System.Drawing.Image)
        Me.ToolStripButton16.Name = "ToolStripButton16"
        Me.ToolStripButton16.Size = New System.Drawing.Size(166, 22)
        Me.ToolStripButton16.Text = "Agregar Tarjeta &Digital"
        '
        'ToolStripSeparator14
        '
        Me.ToolStripSeparator14.Name = "ToolStripSeparator14"
        Me.ToolStripSeparator14.Size = New System.Drawing.Size(6, 25)
        '
        'Panel8
        '
        Me.Panel8.AutoScroll = True
        Me.Panel8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel8.Controls.Add(Me.ComboBox20)
        Me.Panel8.Controls.Add(Me.ComboBox19)
        Me.Panel8.Controls.Add(Me.Label48)
        Me.Panel8.Controls.Add(Me.Label39)
        Me.Panel8.Controls.Add(Me.Button27)
        Me.Panel8.Controls.Add(Me.DescuentoLabel1)
        Me.Panel8.Controls.Add(Me.Button24)
        Me.Panel8.Controls.Add(Me.BindingNavigator3)
        Me.Panel8.Controls.Add(Me.TextBox3)
        Me.Panel8.Controls.Add(Me.ComboBox8)
        Me.Panel8.Controls.Add(Me.ComboBox9)
        Me.Panel8.Controls.Add(Me.ComboBox10)
        Me.Panel8.Controls.Add(Label6)
        Me.Panel8.Controls.Add(Me.Button17)
        Me.Panel8.Controls.Add(CortesiaLabel1)
        Me.Panel8.Controls.Add(Me.Label38)
        Me.Panel8.Controls.Add(Me.Label46)
        Me.Panel8.Controls.Add(Me.CortesiaCheckBox1)
        Me.Panel8.Controls.Add(Me.Label47)
        Me.Panel8.Controls.Add(Me.CMBTextBox5)
        Me.Panel8.Controls.Add(Me.Label12)
        Me.Panel8.Controls.Add(Label13)
        Me.Panel8.Controls.Add(Label14)
        Me.Panel8.Controls.Add(Me.TextBox6)
        Me.Panel8.Controls.Add(Label16)
        Me.Panel8.Controls.Add(Label17)
        Me.Panel8.Controls.Add(Label18)
        Me.Panel8.Controls.Add(Me.TextBox8)
        Me.Panel8.Controls.Add(Me.TextBox9)
        Me.Panel8.Controls.Add(Me.TextBox10)
        Me.Panel8.Controls.Add(Label21)
        Me.Panel8.Controls.Add(Me.CheckBox1)
        Me.Panel8.Controls.Add(Label22)
        Me.Panel8.Controls.Add(Me.TextBox11)
        Me.Panel8.Controls.Add(Label23)
        Me.Panel8.Controls.Add(Me.TextBox12)
        Me.Panel8.Controls.Add(Label24)
        Me.Panel8.Controls.Add(Me.TextBox13)
        Me.Panel8.Controls.Add(Label25)
        Me.Panel8.Controls.Add(Me.TextBox14)
        Me.Panel8.Controls.Add(Label26)
        Me.Panel8.Controls.Add(Me.TextBox15)
        Me.Panel8.Controls.Add(Label27)
        Me.Panel8.Controls.Add(Me.TextBox16)
        Me.Panel8.Controls.Add(Label28)
        Me.Panel8.Controls.Add(Me.TextBox17)
        Me.Panel8.Controls.Add(Me.TextBox18)
        Me.Panel8.Controls.Add(Me.TextBox19)
        Me.Panel8.Controls.Add(Me.TextBox20)
        Me.Panel8.Controls.Add(Me.TextBox21)
        Me.Panel8.Controls.Add(Me.TextBox22)
        Me.Panel8.Location = New System.Drawing.Point(3, 0)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(549, 318)
        Me.Panel8.TabIndex = 27
        Me.Panel8.TabStop = True
        Me.Panel8.Visible = False
        '
        'ComboBox20
        '
        Me.ComboBox20.DisplayMember = "nombre"
        Me.ComboBox20.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox20.FormattingEnabled = True
        Me.ComboBox20.Location = New System.Drawing.Point(100, 223)
        Me.ComboBox20.Name = "ComboBox20"
        Me.ComboBox20.Size = New System.Drawing.Size(218, 21)
        Me.ComboBox20.TabIndex = 88
        Me.ComboBox20.ValueMember = "clave"
        '
        'ComboBox19
        '
        Me.ComboBox19.DisplayMember = "nombre"
        Me.ComboBox19.Enabled = False
        Me.ComboBox19.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox19.FormattingEnabled = True
        Me.ComboBox19.Location = New System.Drawing.Point(95, 220)
        Me.ComboBox19.Name = "ComboBox19"
        Me.ComboBox19.Size = New System.Drawing.Size(223, 21)
        Me.ComboBox19.TabIndex = 87
        Me.ComboBox19.ValueMember = "clave"
        Me.ComboBox19.Visible = False
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label48.Location = New System.Drawing.Point(327, 289)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(94, 15)
        Me.Label48.TabIndex = 86
        Me.Label48.Text = "Ultimo Pago :"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(316, 136)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(19, 15)
        Me.Label39.TabIndex = 34
        Me.Label39.Text = "%"
        Me.Label39.Visible = False
        '
        'Button27
        '
        Me.Button27.BackColor = System.Drawing.Color.DarkOrange
        Me.Button27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button27.ForeColor = System.Drawing.Color.Black
        Me.Button27.Location = New System.Drawing.Point(386, 244)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(153, 28)
        Me.Button27.TabIndex = 34
        Me.Button27.Text = "Establecer Precios"
        Me.Button27.UseVisualStyleBackColor = False
        Me.Button27.Visible = False
        '
        'DescuentoLabel1
        '
        Me.DescuentoLabel1.BackColor = System.Drawing.SystemColors.Control
        Me.DescuentoLabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescuentoLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRelCteDescuentoBindingSource, "Descuento", True))
        Me.DescuentoLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescuentoLabel1.Location = New System.Drawing.Point(270, 135)
        Me.DescuentoLabel1.Name = "DescuentoLabel1"
        Me.DescuentoLabel1.Size = New System.Drawing.Size(45, 20)
        Me.DescuentoLabel1.TabIndex = 34
        Me.DescuentoLabel1.Visible = False
        '
        'Button24
        '
        Me.Button24.BackColor = System.Drawing.Color.DarkOrange
        Me.Button24.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button24.ForeColor = System.Drawing.Color.White
        Me.Button24.Location = New System.Drawing.Point(184, 133)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(85, 27)
        Me.Button24.TabIndex = 85
        Me.Button24.Text = "Descuento"
        Me.Button24.UseVisualStyleBackColor = False
        '
        'BindingNavigator3
        '
        Me.BindingNavigator3.AddNewItem = Nothing
        Me.BindingNavigator3.BindingSource = Me.CONSULTACONTDIGBindingSource
        Me.BindingNavigator3.CountItem = Nothing
        Me.BindingNavigator3.DeleteItem = Nothing
        Me.BindingNavigator3.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSeparator7, Me.ToolStripButton9, Me.ToolStripSeparator8, Me.ToolStripButton10})
        Me.BindingNavigator3.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator3.MoveFirstItem = Nothing
        Me.BindingNavigator3.MoveLastItem = Nothing
        Me.BindingNavigator3.MoveNextItem = Nothing
        Me.BindingNavigator3.MovePreviousItem = Nothing
        Me.BindingNavigator3.Name = "BindingNavigator3"
        Me.BindingNavigator3.PositionItem = Nothing
        Me.BindingNavigator3.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.BindingNavigator3.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator3.Size = New System.Drawing.Size(549, 25)
        Me.BindingNavigator3.TabIndex = 28
        Me.BindingNavigator3.TabStop = True
        Me.BindingNavigator3.Text = "BindingNavigator3"
        '
        'CONSULTACONTDIGBindingSource
        '
        Me.CONSULTACONTDIGBindingSource.DataMember = "CONSULTACONTDIG"
        Me.CONSULTACONTDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton9
        '
        Me.ToolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton9.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton9.Name = "ToolStripButton9"
        Me.ToolStripButton9.RightToLeftAutoMirrorImage = True
        Me.ToolStripButton9.Size = New System.Drawing.Size(76, 22)
        Me.ToolStripButton9.Text = "CA&NCELAR"
        Me.ToolStripButton9.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.ToolStripButton9.ToolTipText = "CA&NCELAR"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton10
        '
        Me.ToolStripButton10.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton10.Image = CType(resources.GetObject("ToolStripButton10.Image"), System.Drawing.Image)
        Me.ToolStripButton10.Name = "ToolStripButton10"
        Me.ToolStripButton10.Size = New System.Drawing.Size(88, 22)
        Me.ToolStripButton10.Text = "GUA&RDAR"
        '
        'TextBox3
        '
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(210, 105)
        Me.TextBox3.MaxLength = 3
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(44, 21)
        Me.TextBox3.TabIndex = 83
        Me.TextBox3.TabStop = False
        Me.TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ComboBox8
        '
        Me.ComboBox8.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONSULTACONTDIGBindingSource, "Clv_Promocion", True))
        Me.ComboBox8.DisplayMember = "clave"
        Me.ComboBox8.Enabled = False
        Me.ComboBox8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox8.FormattingEnabled = True
        Me.ComboBox8.Location = New System.Drawing.Point(98, 195)
        Me.ComboBox8.Name = "ComboBox8"
        Me.ComboBox8.Size = New System.Drawing.Size(221, 23)
        Me.ComboBox8.TabIndex = 51
        Me.ComboBox8.TabStop = False
        Me.ComboBox8.ValueMember = "clave"
        '
        'ComboBox9
        '
        Me.ComboBox9.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONSULTACONTDIGBindingSource, "Clv_Vendedor", True))
        Me.ComboBox9.DataSource = Me.MuestraPromotoresNetBindingSource
        Me.ComboBox9.DisplayMember = "Nombre"
        Me.ComboBox9.Enabled = False
        Me.ComboBox9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox9.FormattingEnabled = True
        Me.ComboBox9.Location = New System.Drawing.Point(97, 167)
        Me.ComboBox9.Name = "ComboBox9"
        Me.ComboBox9.Size = New System.Drawing.Size(221, 23)
        Me.ComboBox9.TabIndex = 50
        Me.ComboBox9.TabStop = False
        Me.ComboBox9.ValueMember = "Clave"
        '
        'ComboBox10
        '
        Me.ComboBox10.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONSULTACONTDIGBindingSource, "status", True))
        Me.ComboBox10.DataSource = Me.StatusNetBindingSource
        Me.ComboBox10.DisplayMember = "Concepto"
        Me.ComboBox10.Enabled = False
        Me.ComboBox10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox10.FormattingEnabled = True
        Me.ComboBox10.Location = New System.Drawing.Point(85, 58)
        Me.ComboBox10.Name = "ComboBox10"
        Me.ComboBox10.Size = New System.Drawing.Size(107, 23)
        Me.ComboBox10.TabIndex = 48
        Me.ComboBox10.TabStop = False
        Me.ComboBox10.ValueMember = "Clv_StatusNet"
        '
        'Button17
        '
        Me.Button17.BackColor = System.Drawing.Color.DarkOrange
        Me.Button17.ForeColor = System.Drawing.Color.White
        Me.Button17.Location = New System.Drawing.Point(120, 131)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(24, 25)
        Me.Button17.TabIndex = 26
        Me.Button17.Text = "..."
        Me.Button17.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button17.UseVisualStyleBackColor = False
        '
        'Label38
        '
        Me.Label38.BackColor = System.Drawing.Color.DarkOrange
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.Color.White
        Me.Label38.Location = New System.Drawing.Point(244, 78)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(75, 25)
        Me.Label38.TabIndex = 60
        Me.Label38.Text = " Año"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label46
        '
        Me.Label46.BackColor = System.Drawing.Color.DarkOrange
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.ForeColor = System.Drawing.Color.White
        Me.Label46.Location = New System.Drawing.Point(206, 78)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(47, 25)
        Me.Label46.TabIndex = 58
        Me.Label46.Text = " Mes"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CortesiaCheckBox1
        '
        Me.CortesiaCheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSULTACONTDIGBindingSource, "Cortesia", True))
        Me.CortesiaCheckBox1.Enabled = False
        Me.CortesiaCheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CortesiaCheckBox1.Location = New System.Drawing.Point(102, 136)
        Me.CortesiaCheckBox1.Name = "CortesiaCheckBox1"
        Me.CortesiaCheckBox1.Size = New System.Drawing.Size(21, 24)
        Me.CortesiaCheckBox1.TabIndex = 57
        Me.CortesiaCheckBox1.TabStop = False
        '
        'Label47
        '
        Me.Label47.BackColor = System.Drawing.Color.DarkOrange
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.Color.White
        Me.Label47.Location = New System.Drawing.Point(206, 56)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(113, 25)
        Me.Label47.TabIndex = 59
        Me.Label47.Text = "Ultimo Pagado "
        Me.Label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CMBTextBox5
        '
        Me.CMBTextBox5.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox5.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox5.Location = New System.Drawing.Point(328, 57)
        Me.CMBTextBox5.Name = "CMBTextBox5"
        Me.CMBTextBox5.ReadOnly = True
        Me.CMBTextBox5.Size = New System.Drawing.Size(216, 19)
        Me.CMBTextBox5.TabIndex = 47
        Me.CMBTextBox5.TabStop = False
        Me.CMBTextBox5.Text = "Fechas de "
        Me.CMBTextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "DESCRIPCION", True))
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Navy
        Me.Label12.Location = New System.Drawing.Point(171, 28)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(373, 23)
        Me.Label12.TabIndex = 38
        '
        'TextBox6
        '
        Me.TextBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox6.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "Obs", True))
        Me.TextBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox6.Location = New System.Drawing.Point(21, 267)
        Me.TextBox6.Multiline = True
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox6.Size = New System.Drawing.Size(300, 40)
        Me.TextBox6.TabIndex = 27
        '
        'TextBox8
        '
        Me.TextBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox8.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "factura", True))
        Me.TextBox8.Location = New System.Drawing.Point(85, 89)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ReadOnly = True
        Me.TextBox8.Size = New System.Drawing.Size(75, 20)
        Me.TextBox8.TabIndex = 27
        Me.TextBox8.TabStop = False
        '
        'TextBox9
        '
        Me.TextBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox9.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "ultimo_anio", True))
        Me.TextBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox9.Location = New System.Drawing.Point(258, 105)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.ReadOnly = True
        Me.TextBox9.Size = New System.Drawing.Size(58, 21)
        Me.TextBox9.TabIndex = 25
        Me.TextBox9.TabStop = False
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox10
        '
        Me.TextBox10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox10.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "ultimo_mes", True))
        Me.TextBox10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox10.Location = New System.Drawing.Point(244, 105)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.ReadOnly = True
        Me.TextBox10.Size = New System.Drawing.Size(10, 21)
        Me.TextBox10.TabIndex = 23
        Me.TextBox10.TabStop = False
        '
        'CheckBox1
        '
        Me.CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONSULTACONTDIGBindingSource, "PrimerMensualidad", True))
        Me.CheckBox1.Enabled = False
        Me.CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox1.Location = New System.Drawing.Point(172, 112)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(20, 24)
        Me.CheckBox1.TabIndex = 21
        Me.CheckBox1.TabStop = False
        '
        'TextBox11
        '
        Me.TextBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox11.CausesValidation = False
        Me.TextBox11.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "FECHA_ULT_PAGO", True))
        Me.TextBox11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox11.Location = New System.Drawing.Point(440, 217)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.ReadOnly = True
        Me.TextBox11.Size = New System.Drawing.Size(100, 21)
        Me.TextBox11.TabIndex = 19
        Me.TextBox11.TabStop = False
        '
        'TextBox12
        '
        Me.TextBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox12.CausesValidation = False
        Me.TextBox12.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "fecha_Fuera_Area", True))
        Me.TextBox12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox12.Location = New System.Drawing.Point(440, 191)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.ReadOnly = True
        Me.TextBox12.Size = New System.Drawing.Size(100, 21)
        Me.TextBox12.TabIndex = 17
        Me.TextBox12.TabStop = False
        '
        'TextBox13
        '
        Me.TextBox13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox13.CausesValidation = False
        Me.TextBox13.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "fecha_baja", True))
        Me.TextBox13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox13.Location = New System.Drawing.Point(440, 164)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.ReadOnly = True
        Me.TextBox13.Size = New System.Drawing.Size(100, 21)
        Me.TextBox13.TabIndex = 15
        Me.TextBox13.TabStop = False
        '
        'TextBox14
        '
        Me.TextBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox14.CausesValidation = False
        Me.TextBox14.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "fecha_suspension", True))
        Me.TextBox14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox14.Location = New System.Drawing.Point(440, 139)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.ReadOnly = True
        Me.TextBox14.Size = New System.Drawing.Size(100, 21)
        Me.TextBox14.TabIndex = 13
        Me.TextBox14.TabStop = False
        '
        'TextBox15
        '
        Me.TextBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox15.CausesValidation = False
        Me.TextBox15.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "fecha_instalacio", True))
        Me.TextBox15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox15.Location = New System.Drawing.Point(440, 113)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.ReadOnly = True
        Me.TextBox15.Size = New System.Drawing.Size(100, 21)
        Me.TextBox15.TabIndex = 11
        Me.TextBox15.TabStop = False
        '
        'TextBox16
        '
        Me.TextBox16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox16.CausesValidation = False
        Me.TextBox16.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "fecha_solicitud", True))
        Me.TextBox16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox16.Location = New System.Drawing.Point(440, 87)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.ReadOnly = True
        Me.TextBox16.Size = New System.Drawing.Size(100, 21)
        Me.TextBox16.TabIndex = 9
        Me.TextBox16.TabStop = False
        '
        'TextBox17
        '
        Me.TextBox17.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "Clv_Servicio", True))
        Me.TextBox17.Location = New System.Drawing.Point(270, 2)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(26, 20)
        Me.TextBox17.TabIndex = 5
        Me.TextBox17.TabStop = False
        '
        'TextBox18
        '
        Me.TextBox18.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "ContratoNet", True))
        Me.TextBox18.Location = New System.Drawing.Point(305, 2)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.Size = New System.Drawing.Size(20, 20)
        Me.TextBox18.TabIndex = 3
        Me.TextBox18.TabStop = False
        Me.TextBox18.Visible = False
        '
        'TextBox19
        '
        Me.TextBox19.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTNETBindingSource, "Clv_UnicaNet", True))
        Me.TextBox19.Location = New System.Drawing.Point(223, 2)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.Size = New System.Drawing.Size(26, 20)
        Me.TextBox19.TabIndex = 1
        Me.TextBox19.TabStop = False
        Me.TextBox19.Visible = False
        '
        'TextBox20
        '
        Me.TextBox20.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "status", True))
        Me.TextBox20.Location = New System.Drawing.Point(147, 60)
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.Size = New System.Drawing.Size(39, 20)
        Me.TextBox20.TabIndex = 39
        Me.TextBox20.TabStop = False
        '
        'TextBox21
        '
        Me.TextBox21.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "Clv_Promocion", True))
        Me.TextBox21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox21.Location = New System.Drawing.Point(236, 196)
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Size = New System.Drawing.Size(22, 21)
        Me.TextBox21.TabIndex = 31
        Me.TextBox21.TabStop = False
        '
        'TextBox22
        '
        Me.TextBox22.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "Clv_Vendedor", True))
        Me.TextBox22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox22.Location = New System.Drawing.Point(235, 168)
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.Size = New System.Drawing.Size(22, 21)
        Me.TextBox22.TabIndex = 29
        Me.TextBox22.TabStop = False
        '
        'CONRel_ContDig_UsuariosBindingSource
        '
        Me.CONRel_ContDig_UsuariosBindingSource.DataMember = "CONRel_ContDig_Usuarios"
        Me.CONRel_ContDig_UsuariosBindingSource.DataSource = Me.DataSetEDGAR
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.Color.DarkOrange
        Me.Button11.Enabled = False
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.ForeColor = System.Drawing.Color.White
        Me.Button11.Location = New System.Drawing.Point(270, 289)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(126, 36)
        Me.Button11.TabIndex = 22
        Me.Button11.Text = "T&V Digital"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'ValidaTextBox
        '
        Me.ValidaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Valida_SiahiOrdSerBindingSource, "Valida", True))
        Me.ValidaTextBox.Location = New System.Drawing.Point(853, 9)
        Me.ValidaTextBox.Name = "ValidaTextBox"
        Me.ValidaTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ValidaTextBox.TabIndex = 26
        Me.ValidaTextBox.TabStop = False
        '
        'Valida_SiahiOrdSerBindingSource
        '
        Me.Valida_SiahiOrdSerBindingSource.DataMember = "Valida_SiahiOrdSer"
        Me.Valida_SiahiOrdSerBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'ValidaTextBox1
        '
        Me.ValidaTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Valida_SiahiQuejasBindingSource, "Valida", True))
        Me.ValidaTextBox1.Location = New System.Drawing.Point(853, 38)
        Me.ValidaTextBox1.Name = "ValidaTextBox1"
        Me.ValidaTextBox1.Size = New System.Drawing.Size(100, 20)
        Me.ValidaTextBox1.TabIndex = 28
        Me.ValidaTextBox1.TabStop = False
        '
        'Valida_SiahiQuejasBindingSource
        '
        Me.Valida_SiahiQuejasBindingSource.DataMember = "Valida_SiahiQuejas"
        Me.Valida_SiahiQuejasBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.DarkOrange
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.ForeColor = System.Drawing.Color.White
        Me.Button10.Location = New System.Drawing.Point(827, 148)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(181, 26)
        Me.Button10.TabIndex = 18
        Me.Button10.Text = "C&onsultar Cobro"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'MUESTRA_TIPOCLIENTESTableAdapter
        '
        Me.MUESTRA_TIPOCLIENTESTableAdapter.ClearBeforeFill = True
        '
        'CONRel_Clientes_TiposClientesTableAdapter
        '
        Me.CONRel_Clientes_TiposClientesTableAdapter.ClearBeforeFill = True
        '
        'GUARDARRel_Clientes_TiposClientesBindingSource
        '
        Me.GUARDARRel_Clientes_TiposClientesBindingSource.DataMember = "GUARDARRel_Clientes_TiposClientes"
        Me.GUARDARRel_Clientes_TiposClientesBindingSource.DataSource = Me.DataSetEDGAR
        '
        'GUARDARRel_Clientes_TiposClientesTableAdapter
        '
        Me.GUARDARRel_Clientes_TiposClientesTableAdapter.ClearBeforeFill = True
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.DarkOrange
        Me.Button12.Enabled = False
        Me.Button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button12.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.ForeColor = System.Drawing.Color.White
        Me.Button12.Location = New System.Drawing.Point(827, 177)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(181, 26)
        Me.Button12.TabIndex = 19
        Me.Button12.Text = "&Login y Password"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'MUESTRATABSTableAdapter
        '
        Me.MUESTRATABSTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACatalogoPeriodosCorteTableAdapter
        '
        Me.MUESTRACatalogoPeriodosCorteTableAdapter.ClearBeforeFill = True
        '
        'Valida_facturasBindingSource
        '
        Me.Valida_facturasBindingSource.DataMember = "valida_facturas"
        Me.Valida_facturasBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Valida_facturasTableAdapter
        '
        Me.Valida_facturasTableAdapter.ClearBeforeFill = True
        '
        'CONRel_ContDig_UsuariosTableAdapter
        '
        Me.CONRel_ContDig_UsuariosTableAdapter.ClearBeforeFill = True
        '
        'GUARDARRel_ContDig_UsuariosBindingSource
        '
        Me.GUARDARRel_ContDig_UsuariosBindingSource.DataMember = "GUARDARRel_ContDig_Usuarios"
        Me.GUARDARRel_ContDig_UsuariosBindingSource.DataSource = Me.DataSetEDGAR
        '
        'GUARDARRel_ContDig_UsuariosTableAdapter
        '
        Me.GUARDARRel_ContDig_UsuariosTableAdapter.ClearBeforeFill = True
        '
        'CONRel_ContNet_UsuariosTableAdapter
        '
        Me.CONRel_ContNet_UsuariosTableAdapter.ClearBeforeFill = True
        '
        'CONRel_ClientesTv_UsuariosTableAdapter
        '
        Me.CONRel_ClientesTv_UsuariosTableAdapter.ClearBeforeFill = True
        '
        'GUARDARRel_ClientesTv_UsuariosBindingSource
        '
        Me.GUARDARRel_ClientesTv_UsuariosBindingSource.DataMember = "GUARDARRel_ClientesTv_Usuarios"
        Me.GUARDARRel_ClientesTv_UsuariosBindingSource.DataSource = Me.DataSetEDGAR
        '
        'GUARDARRel_ClientesTv_UsuariosTableAdapter
        '
        Me.GUARDARRel_ClientesTv_UsuariosTableAdapter.ClearBeforeFill = True
        '
        'GUARDARRel_ContNet_UsuariosBindingSource
        '
        Me.GUARDARRel_ContNet_UsuariosBindingSource.DataMember = "GUARDARRel_ContNet_Usuarios"
        Me.GUARDARRel_ContNet_UsuariosBindingSource.DataSource = Me.DataSetEDGAR
        '
        'GUARDARRel_ContNet_UsuariosTableAdapter
        '
        Me.GUARDARRel_ContNet_UsuariosTableAdapter.ClearBeforeFill = True
        '
        'MuestraPromotoresNetTableAdapter
        '
        Me.MuestraPromotoresNetTableAdapter.ClearBeforeFill = True
        '
        'MuestraPromotoresTvTableAdapter
        '
        Me.MuestraPromotoresTvTableAdapter.ClearBeforeFill = True
        '
        'ConRel_ClientesTv_VendedorTableAdapter
        '
        Me.ConRel_ClientesTv_VendedorTableAdapter.ClearBeforeFill = True
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DAMEFECHADELSERVIDOR_2BindingSource
        '
        Me.DAMEFECHADELSERVIDOR_2BindingSource.DataMember = "DAMEFECHADELSERVIDOR_2"
        Me.DAMEFECHADELSERVIDOR_2BindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'DAMEFECHADELSERVIDOR_2TableAdapter
        '
        Me.DAMEFECHADELSERVIDOR_2TableAdapter.ClearBeforeFill = True
        '
        'TextBox1
        '
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DAMEFECHADELSERVIDOR_2BindingSource, "FECHA", True))
        Me.TextBox1.Location = New System.Drawing.Point(872, 696)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(79, 20)
        Me.TextBox1.TabIndex = 29
        '
        'Button20
        '
        Me.Button20.BackColor = System.Drawing.Color.DarkOrange
        Me.Button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button20.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button20.ForeColor = System.Drawing.Color.White
        Me.Button20.Location = New System.Drawing.Point(827, 206)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(181, 26)
        Me.Button20.TabIndex = 31
        Me.Button20.Text = "Bloquear Cliente"
        Me.Button20.UseVisualStyleBackColor = False
        Me.Button20.Visible = False
        '
        'ConRelCtePlacaTableAdapter
        '
        Me.ConRelCtePlacaTableAdapter.ClearBeforeFill = True
        '
        'Button21
        '
        Me.Button21.BackColor = System.Drawing.Color.DarkOrange
        Me.Button21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button21.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button21.ForeColor = System.Drawing.Color.White
        Me.Button21.Location = New System.Drawing.Point(721, 289)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(100, 30)
        Me.Button21.TabIndex = 32
        Me.Button21.Text = "Robo De Señal"
        Me.Button21.UseVisualStyleBackColor = False
        '
        'ChecaRoboDeSeñalBindingSource
        '
        Me.ChecaRoboDeSeñalBindingSource.DataMember = "ChecaRoboDeSeñal"
        Me.ChecaRoboDeSeñalBindingSource.DataSource = Me.DataSetEric
        '
        'ChecaRoboDeSeñalTableAdapter
        '
        Me.ChecaRoboDeSeñalTableAdapter.ClearBeforeFill = True
        '
        'Dime_Si_ESMiniBasicoBindingSource
        '
        Me.Dime_Si_ESMiniBasicoBindingSource.DataMember = "Dime_Si_ESMiniBasico"
        Me.Dime_Si_ESMiniBasicoBindingSource.DataSource = Me.DataSetEDGAR
        '
        'Dime_Si_ESMiniBasicoTableAdapter
        '
        Me.Dime_Si_ESMiniBasicoTableAdapter.ClearBeforeFill = True
        '
        'ChecaRelCteDescuentoBindingSource
        '
        Me.ChecaRelCteDescuentoBindingSource.DataMember = "ChecaRelCteDescuento"
        Me.ChecaRelCteDescuentoBindingSource.DataSource = Me.DataSetEric
        '
        'ChecaRelCteDescuentoTableAdapter
        '
        Me.ChecaRelCteDescuentoTableAdapter.ClearBeforeFill = True
        '
        'ConRelCteDescuentoTableAdapter
        '
        Me.ConRelCteDescuentoTableAdapter.ClearBeforeFill = True
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Valida_Direccion1BindingSource
        '
        Me.Valida_Direccion1BindingSource.DataMember = "Valida_Direccion1"
        Me.Valida_Direccion1BindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Valida_Direccion1TableAdapter
        '
        Me.Valida_Direccion1TableAdapter.ClearBeforeFill = True
        '
        'Dame_clv_session_clientesBindingSource
        '
        Me.Dame_clv_session_clientesBindingSource.DataMember = "Dame_clv_session_clientes"
        Me.Dame_clv_session_clientesBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Dame_clv_session_clientesTableAdapter
        '
        Me.Dame_clv_session_clientesTableAdapter.ClearBeforeFill = True
        '
        'Valida_servicioTvBindingSource
        '
        Me.Valida_servicioTvBindingSource.DataMember = "Valida_servicioTv"
        Me.Valida_servicioTvBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Valida_servicioTvTableAdapter
        '
        Me.Valida_servicioTvTableAdapter.ClearBeforeFill = True
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Inserta_Rel_cortesia_FechaBindingSource
        '
        Me.Inserta_Rel_cortesia_FechaBindingSource.DataMember = "Inserta_Rel_cortesia_Fecha"
        Me.Inserta_Rel_cortesia_FechaBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Inserta_Rel_cortesia_FechaTableAdapter
        '
        Me.Inserta_Rel_cortesia_FechaTableAdapter.ClearBeforeFill = True
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTACONTDIGBindingSource, "Clv_Servicio", True))
        Me.Label45.Location = New System.Drawing.Point(890, 708)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(45, 13)
        Me.Label45.TabIndex = 58
        Me.Label45.Text = "Label45"
        '
        'Button28
        '
        Me.Button28.BackColor = System.Drawing.Color.DarkOrange
        Me.Button28.Enabled = False
        Me.Button28.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button28.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button28.ForeColor = System.Drawing.Color.White
        Me.Button28.Location = New System.Drawing.Point(402, 289)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(125, 36)
        Me.Button28.TabIndex = 59
        Me.Button28.Text = "Telefonia"
        Me.Button28.UseVisualStyleBackColor = False
        '
        'Button29
        '
        Me.Button29.BackColor = System.Drawing.Color.DarkOrange
        Me.Button29.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button29.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button29.ForeColor = System.Drawing.Color.White
        Me.Button29.Location = New System.Drawing.Point(827, 235)
        Me.Button29.Name = "Button29"
        Me.Button29.Size = New System.Drawing.Size(181, 26)
        Me.Button29.TabIndex = 60
        Me.Button29.Text = "&Referencias"
        Me.Button29.UseVisualStyleBackColor = False
        '
        'StatusBasicoBindingSource1
        '
        Me.StatusBasicoBindingSource1.DataMember = "StatusBasico"
        Me.StatusBasicoBindingSource1.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRAADIGITALBindingSource
        '
        Me.MUESTRAADIGITALBindingSource.DataMember = "MUESTRA_A_DIGITAL"
        Me.MUESTRAADIGITALBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MuestraPromotoresBindingSource
        '
        Me.MuestraPromotoresBindingSource.DataMember = "MuestraPromotores"
        Me.MuestraPromotoresBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'DameFechaHabilitarBindingSource
        '
        Me.DameFechaHabilitarBindingSource.DataMember = "DameFechaHabilitar"
        Me.DameFechaHabilitarBindingSource.DataSource = Me.DataSetLidia
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MuestraServiciosBindingSource
        '
        Me.MuestraServiciosBindingSource.DataMember = "MuestraServicios"
        Me.MuestraServiciosBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONSULTARCLIENTETableAdapter
        '
        Me.CONSULTARCLIENTETableAdapter.ClearBeforeFill = True
        '
        'CALLESBindingSource
        '
        Me.CALLESBindingSource.DataMember = "CALLES"
        Me.CALLESBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CIUDADESBindingSource
        '
        Me.CIUDADESBindingSource.DataMember = "CIUDADES"
        Me.CIUDADESBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CALLESTableAdapter
        '
        Me.CALLESTableAdapter.ClearBeforeFill = True
        '
        'DAMECOLONIA_CALLETableAdapter
        '
        Me.DAMECOLONIA_CALLETableAdapter.ClearBeforeFill = True
        '
        'CIUDADESTableAdapter
        '
        Me.CIUDADESTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACALLESTableAdapter
        '
        Me.MUESTRACALLESTableAdapter.ClearBeforeFill = True
        '
        'CONCLIENTETVTableAdapter
        '
        Me.CONCLIENTETVTableAdapter.ClearBeforeFill = True
        '
        'MuestraTiposServicioTvTableAdapter
        '
        Me.MuestraTiposServicioTvTableAdapter.ClearBeforeFill = True
        '
        'MuestraMotivoCancelacionTableAdapter
        '
        Me.MuestraMotivoCancelacionTableAdapter.ClearBeforeFill = True
        '
        'StatusBasicoTableAdapter
        '
        Me.StatusBasicoTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACABLEMODEMSDELCLIBindingSource
        '
        Me.MUESTRACABLEMODEMSDELCLIBindingSource.DataMember = "MUESTRACABLEMODEMSDELCLI"
        Me.MUESTRACABLEMODEMSDELCLIBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MuestraCVECOLCIUTableAdapter
        '
        Me.MuestraCVECOLCIUTableAdapter.ClearBeforeFill = True
        '
        'CONSULTACLIENTESNETTableAdapter
        '
        Me.CONSULTACLIENTESNETTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACABLEMODEMSDELCLITableAdapter
        '
        Me.MUESTRACABLEMODEMSDELCLITableAdapter.ClearBeforeFill = True
        '
        'MUESTRACONTNETBindingSource
        '
        Me.MUESTRACONTNETBindingSource.DataMember = "MUESTRACONTNET"
        Me.MUESTRACONTNETBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACONTNETTableAdapter
        '
        Me.MUESTRACONTNETTableAdapter.ClearBeforeFill = True
        '
        'VerAparatodelClienteTableAdapter
        '
        Me.VerAparatodelClienteTableAdapter.ClearBeforeFill = True
        '
        'StatusNetTableAdapter
        '
        Me.StatusNetTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipSerInternetTableAdapter
        '
        Me.MuestraTipSerInternetTableAdapter.ClearBeforeFill = True
        '
        'TipoCablemodemTableAdapter
        '
        Me.TipoCablemodemTableAdapter.ClearBeforeFill = True
        '
        'StatusCableModemTableAdapter
        '
        Me.StatusCableModemTableAdapter.ClearBeforeFill = True
        '
        'CONSULTACONTNETTableAdapter
        '
        Me.CONSULTACONTNETTableAdapter.ClearBeforeFill = True
        '
        'MuestraPromotoresTableAdapter
        '
        Me.MuestraPromotoresTableAdapter.ClearBeforeFill = True
        '
        'HABILITACABLEMODEMBindingSource
        '
        Me.HABILITACABLEMODEMBindingSource.DataMember = "HABILITACABLEMODEM"
        Me.HABILITACABLEMODEMBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'HABILITACABLEMODEMTableAdapter
        '
        Me.HABILITACABLEMODEMTableAdapter.ClearBeforeFill = True
        '
        'MUESTRADIGITALDELCLITableAdapter
        '
        Me.MUESTRADIGITALDELCLITableAdapter.ClearBeforeFill = True
        '
        'MUESTRACONTDIGBindingSource
        '
        Me.MUESTRACONTDIGBindingSource.DataMember = "MUESTRACONTDIG"
        Me.MUESTRACONTDIGBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'MUESTRACONTDIGTableAdapter
        '
        Me.MUESTRACONTDIGTableAdapter.ClearBeforeFill = True
        '
        'CONSULTACLIENTESDIGTableAdapter
        '
        Me.CONSULTACLIENTESDIGTableAdapter.ClearBeforeFill = True
        '
        'CONSULTACONTDIGTableAdapter
        '
        Me.CONSULTACONTDIGTableAdapter.ClearBeforeFill = True
        '
        'VerAparatodelClientedigTableAdapter
        '
        Me.VerAparatodelClientedigTableAdapter.ClearBeforeFill = True
        '
        'MUESTRA_A_DIGITALTableAdapter
        '
        Me.MUESTRA_A_DIGITALTableAdapter.ClearBeforeFill = True
        '
        'MuestraServiciosTableAdapter
        '
        Me.MuestraServiciosTableAdapter.ClearBeforeFill = True
        '
        'MuestraServicios_digitalTableAdapter
        '
        Me.MuestraServicios_digitalTableAdapter.ClearBeforeFill = True
        '
        'ValidaDigitalTableAdapter
        '
        Me.ValidaDigitalTableAdapter.ClearBeforeFill = True
        '
        'Valida_SiahiOrdSerTableAdapter
        '
        Me.Valida_SiahiOrdSerTableAdapter.ClearBeforeFill = True
        '
        'Valida_SiahiQuejasTableAdapter
        '
        Me.Valida_SiahiQuejasTableAdapter.ClearBeforeFill = True
        '
        'HaberServicios_CliTableAdapter
        '
        Me.HaberServicios_CliTableAdapter.ClearBeforeFill = True
        '
        'MUESTRADIGITALDELCLI_porAparatoTableAdapter
        '
        Me.MUESTRADIGITALDELCLI_porAparatoTableAdapter.ClearBeforeFill = True
        '
        'BorraNetPor_NoGRaboBindingSource
        '
        Me.BorraNetPor_NoGRaboBindingSource.DataMember = "BorraNetPor_NoGRabo"
        Me.BorraNetPor_NoGRaboBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BorraNetPor_NoGRaboTableAdapter
        '
        Me.BorraNetPor_NoGRaboTableAdapter.ClearBeforeFill = True
        '
        'BorraDigPor_NoGRaboBindingSource
        '
        Me.BorraDigPor_NoGRaboBindingSource.DataMember = "BorraDigPor_NoGRabo"
        Me.BorraDigPor_NoGRaboBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BorraDigPor_NoGRaboTableAdapter
        '
        Me.BorraDigPor_NoGRaboTableAdapter.ClearBeforeFill = True
        '
        'DAMESTATUSHABBindingSource
        '
        Me.DAMESTATUSHABBindingSource.DataMember = "DAMESTATUSHAB"
        Me.DAMESTATUSHABBindingSource.DataSource = Me.DataSetLidia
        '
        'DAMESTATUSHABTableAdapter
        '
        Me.DAMESTATUSHABTableAdapter.ClearBeforeFill = True
        '
        'AsignaPeriodoBindingSource
        '
        Me.AsignaPeriodoBindingSource.DataMember = "AsignaPeriodo"
        Me.AsignaPeriodoBindingSource.DataSource = Me.DataSetLidia
        '
        'AsignaPeriodoTableAdapter
        '
        Me.AsignaPeriodoTableAdapter.ClearBeforeFill = True
        '
        'CONTARCLIENTESBindingSource
        '
        Me.CONTARCLIENTESBindingSource.DataMember = "CONTARCLIENTES"
        Me.CONTARCLIENTESBindingSource.DataSource = Me.DataSetLidia
        '
        'CONTARCLIENTESTableAdapter
        '
        Me.CONTARCLIENTESTableAdapter.ClearBeforeFill = True
        '
        'PrimerMesCLIENTESBindingSource
        '
        Me.PrimerMesCLIENTESBindingSource.DataMember = "PrimerMesCLIENTES"
        Me.PrimerMesCLIENTESBindingSource.DataSource = Me.DataSetLidia
        '
        'PrimerMesCLIENTESTableAdapter
        '
        Me.PrimerMesCLIENTESTableAdapter.ClearBeforeFill = True
        '
        'DameFechaHabilitarTableAdapter
        '
        Me.DameFechaHabilitarTableAdapter.ClearBeforeFill = True
        '
        'DIMEQUEPERIODODECORTEBindingSource
        '
        Me.DIMEQUEPERIODODECORTEBindingSource.DataMember = "DIMEQUEPERIODODECORTE"
        Me.DIMEQUEPERIODODECORTEBindingSource.DataSource = Me.DataSetLidia
        '
        'DIMEQUEPERIODODECORTETableAdapter
        '
        Me.DIMEQUEPERIODODECORTETableAdapter.ClearBeforeFill = True
        '
        'BuscaBloqueadoTableAdapter
        '
        Me.BuscaBloqueadoTableAdapter.ClearBeforeFill = True
        '
        'BuscaBloqueadoBindingSource
        '
        Me.BuscaBloqueadoBindingSource.DataMember = "BuscaBloqueado"
        Me.BuscaBloqueadoBindingSource.DataSource = Me.DataSetLidia
        '
        'Button30
        '
        Me.Button30.BackColor = System.Drawing.Color.DarkOrange
        Me.Button30.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button30.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button30.ForeColor = System.Drawing.Color.White
        Me.Button30.Location = New System.Drawing.Point(827, 264)
        Me.Button30.Name = "Button30"
        Me.Button30.Size = New System.Drawing.Size(181, 26)
        Me.Button30.TabIndex = 62
        Me.Button30.Text = "&Referencias Bancarias"
        Me.Button30.UseVisualStyleBackColor = False
        '
        'BtnEstadoDeCuenta
        '
        Me.BtnEstadoDeCuenta.BackColor = System.Drawing.Color.DarkOrange
        Me.BtnEstadoDeCuenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnEstadoDeCuenta.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnEstadoDeCuenta.ForeColor = System.Drawing.Color.White
        Me.BtnEstadoDeCuenta.Location = New System.Drawing.Point(591, 290)
        Me.BtnEstadoDeCuenta.Name = "BtnEstadoDeCuenta"
        Me.BtnEstadoDeCuenta.Size = New System.Drawing.Size(124, 29)
        Me.BtnEstadoDeCuenta.TabIndex = 64
        Me.BtnEstadoDeCuenta.Text = "Estado De Cuenta"
        Me.BtnEstadoDeCuenta.UseVisualStyleBackColor = False
        Me.BtnEstadoDeCuenta.Visible = False
        '
        'Button31
        '
        Me.Button31.BackColor = System.Drawing.Color.DarkOrange
        Me.Button31.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button31.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button31.ForeColor = System.Drawing.Color.White
        Me.Button31.Location = New System.Drawing.Point(826, 293)
        Me.Button31.Name = "Button31"
        Me.Button31.Size = New System.Drawing.Size(181, 26)
        Me.Button31.TabIndex = 66
        Me.Button31.Text = "Observación General"
        Me.Button31.UseVisualStyleBackColor = False
        '
        'btnHisDes
        '
        Me.btnHisDes.BackColor = System.Drawing.Color.DarkOrange
        Me.btnHisDes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnHisDes.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHisDes.ForeColor = System.Drawing.Color.White
        Me.btnHisDes.Location = New System.Drawing.Point(11, 692)
        Me.btnHisDes.Name = "btnHisDes"
        Me.btnHisDes.Size = New System.Drawing.Size(181, 26)
        Me.btnHisDes.TabIndex = 68
        Me.btnHisDes.Text = "Historial Desconexiones"
        Me.btnHisDes.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'FrmClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 730)
        Me.Controls.Add(Me.btnHisDes)
        Me.Controls.Add(Me.Button31)
        Me.Controls.Add(Me.BtnEstadoDeCuenta)
        Me.Controls.Add(Me.Button30)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button29)
        Me.Controls.Add(Me.Button28)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button21)
        Me.Controls.Add(Me.Button20)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(ValidaLabel1)
        Me.Controls.Add(Me.ValidaTextBox1)
        Me.Controls.Add(ValidaLabel)
        Me.Controls.Add(Me.ValidaTextBox)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label45)
        Me.Controls.Add(Me.Panel7)
        Me.IsMdiContainer = True
        Me.MaximizeBox = False
        Me.Name = "FrmClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Clientes"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        CType(Me.CONSULTARCLIENTEBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONSULTARCLIENTEBindingNavigator.ResumeLayout(False)
        Me.CONSULTARCLIENTEBindingNavigator.PerformLayout()
        CType(Me.CONSULTARCLIENTEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMECOLONIACALLEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraCVECOLCIUBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACatalogoPeriodosCorteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATABSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONRelClientesTiposClientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATIPOCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HaberServicios_CliBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONCLIENTETVBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRelClientesTvVendedorBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraPromotoresTvBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRelCtePlacaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTiposServicioTvBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBasicoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraMotivoCancelacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONRel_ClientesTv_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VerAparatodelClienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSULTACLIENTESNETBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusCableModemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TipoCablemodemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipSerInternetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONRel_ContNet_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSULTACONTNETBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusNetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraPromotoresNetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        CType(Me.VerAparatodelClientedigBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator4.ResumeLayout(False)
        Me.BindingNavigator4.PerformLayout()
        CType(Me.CONSULTACLIENTESDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.MuestraServiciosdigitalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRADIGITALDELCLIBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRADIGITALDELCLI_porAparatoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaDigitalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.PerformLayout()
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.ResumeLayout(False)
        CType(Me.BindingNavigator5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator5.ResumeLayout(False)
        Me.BindingNavigator5.PerformLayout()
        CType(Me.BindingNavigator6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator6.ResumeLayout(False)
        Me.BindingNavigator6.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        CType(Me.BindingNavigator3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator3.ResumeLayout(False)
        Me.BindingNavigator3.PerformLayout()
        CType(Me.CONSULTACONTDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONRel_ContDig_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_SiahiOrdSerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_SiahiQuejasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GUARDARRel_Clientes_TiposClientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_facturasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GUARDARRel_ContDig_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GUARDARRel_ClientesTv_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GUARDARRel_ContNet_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMEFECHADELSERVIDOR_2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChecaRoboDeSeñalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dime_Si_ESMiniBasicoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChecaRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_Direccion1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clv_session_clientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_servicioTvBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Rel_cortesia_FechaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBasicoBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAADIGITALBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraPromotoresBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameFechaHabilitarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraServiciosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CALLESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CIUDADESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACABLEMODEMSDELCLIBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTNETBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HABILITACABLEMODEMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACONTDIGBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorraNetPor_NoGRaboBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorraDigPor_NoGRaboBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMESTATUSHABBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AsignaPeriodoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONTARCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrimerMesCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DIMEQUEPERIODODECORTEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BuscaBloqueadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONSULTARCLIENTEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTARCLIENTETableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONSULTARCLIENTETableAdapter
    Friend WithEvents CALLESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CALLESTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CALLESTableAdapter
    Friend WithEvents DAMECOLONIACALLEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMECOLONIA_CALLETableAdapter As sofTV.NewSofTvDataSetTableAdapters.DAMECOLONIA_CALLETableAdapter
    Friend WithEvents CIUDADESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CIUDADESTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CIUDADESTableAdapter
    Friend WithEvents CONTRATOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_CalleTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CALLEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents NUMEROTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ENTRECALLESTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents COLONIAComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CodigoPostalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TELEFONOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CELULARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DESGLOSA_IvaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SoloInternetCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents EshotelCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CIUDADComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Clv_CiudadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONSULTARCLIENTEBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CONSULTARCLIENTEBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents MUESTRACALLESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACALLESTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACALLESTableAdapter
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents CONCLIENTETVBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCLIENTETVTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONCLIENTETVTableAdapter
    Friend WithEvents MuestraTiposServicioTvBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTiposServicioTvTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraTiposServicioTvTableAdapter
    Friend WithEvents MuestraMotivoCancelacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraMotivoCancelacionTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraMotivoCancelacionTableAdapter
    Friend WithEvents StatusBasicoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents StatusBasicoTableAdapter As sofTV.NewSofTvDataSetTableAdapters.StatusBasicoTableAdapter
    Friend WithEvents StatusBasicoBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents EmailTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MuestraCVECOLCIUBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraCVECOLCIUTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraCVECOLCIUTableAdapter
    Friend WithEvents CONSULTACLIENTESNETBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTACLIENTESNETTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONSULTACLIENTESNETTableAdapter
    Friend WithEvents MUESTRACABLEMODEMSDELCLIBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACABLEMODEMSDELCLITableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACABLEMODEMSDELCLITableAdapter
    Friend WithEvents MUESTRACONTNETBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTNETTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTNETTableAdapter
    Friend WithEvents VerAparatodelClienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VerAparatodelClienteTableAdapter As sofTV.NewSofTvDataSetTableAdapters.VerAparatodelClienteTableAdapter
    Friend WithEvents StatusNetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents StatusNetTableAdapter As sofTV.NewSofTvDataSetTableAdapters.StatusNetTableAdapter
    Friend WithEvents MuestraTipSerInternetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerInternetTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerInternetTableAdapter
    Friend WithEvents TipoCablemodemBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TipoCablemodemTableAdapter As sofTV.NewSofTvDataSetTableAdapters.TipoCablemodemTableAdapter
    Friend WithEvents StatusCableModemBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents StatusCableModemTableAdapter As sofTV.NewSofTvDataSetTableAdapters.StatusCableModemTableAdapter
    Friend WithEvents CONSULTACONTNETBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTACONTNETTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONSULTACONTNETTableAdapter
    Friend WithEvents MuestraPromotoresBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraPromotoresTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraPromotoresTableAdapter
    Friend WithEvents HABILITACABLEMODEMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents HABILITACABLEMODEMTableAdapter As sofTV.NewSofTvDataSetTableAdapters.HABILITACABLEMODEMTableAdapter
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents MUESTRADIGITALDELCLIBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRADIGITALDELCLITableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRADIGITALDELCLITableAdapter
    Friend WithEvents MUESTRACONTDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACONTDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRACONTDIGTableAdapter
    Friend WithEvents CONSULTACLIENTESDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTACLIENTESDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONSULTACLIENTESDIGTableAdapter
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents TextBox23 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox24 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox25 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox13 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox26 As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CMBTextBox28 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox29 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox30 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox32 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox33 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox34 As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents BindingNavigator4 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton11 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton12 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONSULTACONTDIGBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTACONTDIGTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONSULTACONTDIGTableAdapter
    Friend WithEvents VerAparatodelClientedigBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VerAparatodelClientedigTableAdapter As sofTV.NewSofTvDataSetTableAdapters.VerAparatodelClientedigTableAdapter
    Friend WithEvents MUESTRAADIGITALBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRA_A_DIGITALTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRA_A_DIGITALTableAdapter
    Friend WithEvents CMBLabel29 As System.Windows.Forms.Label
    Friend WithEvents ComboBox12 As System.Windows.Forms.ComboBox
    Friend WithEvents MuestraServiciosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServiciosTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraServiciosTableAdapter
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel32 As System.Windows.Forms.Label
    Friend WithEvents ComboBox14 As System.Windows.Forms.ComboBox
    Friend WithEvents BindingNavigator5 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton13 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton14 As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigator6 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripSeparator13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton15 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator14 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton16 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CMBLabel10 As System.Windows.Forms.Label
    Friend WithEvents TreeView3 As System.Windows.Forms.TreeView
    Friend WithEvents MuestraServiciosdigitalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServicios_digitalTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraServicios_digitalTableAdapter
    Friend WithEvents ValidaDigitalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaDigitalTableAdapter As sofTV.NewSofTvDataSetTableAdapters.ValidaDigitalTableAdapter
    Friend WithEvents RespuestaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Valida_SiahiOrdSerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_SiahiOrdSerTableAdapter As sofTV.NewSofTvDataSetTableAdapters.Valida_SiahiOrdSerTableAdapter
    Friend WithEvents ValidaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Valida_SiahiQuejasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_SiahiQuejasTableAdapter As sofTV.NewSofTvDataSetTableAdapters.Valida_SiahiQuejasTableAdapter
    Friend WithEvents ValidaTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents HaberServicios_CliBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents HaberServicios_CliTableAdapter As sofTV.NewSofTvDataSetTableAdapters.HaberServicios_CliTableAdapter
    Friend WithEvents DigitalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents InternetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BasicoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MUESTRADIGITALDELCLI_porAparatoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRADIGITALDELCLI_porAparatoTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRADIGITALDELCLI_porAparatoTableAdapter
    Friend WithEvents MACCABLEMODEMTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONTRATONETTextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents BorraNetPor_NoGRaboBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorraNetPor_NoGRaboTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BorraNetPor_NoGRaboTableAdapter
    Friend WithEvents BorraDigPor_NoGRaboBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorraDigPor_NoGRaboTableAdapter As sofTV.NewSofTvDataSetTableAdapters.BorraDigPor_NoGRaboTableAdapter
    Friend WithEvents ComboBox7 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRATIPOCLIENTESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents MUESTRA_TIPOCLIENTESTableAdapter As sofTV.DataSetEDGARTableAdapters.MUESTRA_TIPOCLIENTESTableAdapter
    Friend WithEvents CONRelClientesTiposClientesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONRel_Clientes_TiposClientesTableAdapter As sofTV.DataSetEDGARTableAdapters.CONRel_Clientes_TiposClientesTableAdapter
    Friend WithEvents GUARDARRel_Clientes_TiposClientesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GUARDARRel_Clientes_TiposClientesTableAdapter As sofTV.DataSetEDGARTableAdapters.GUARDARRel_Clientes_TiposClientesTableAdapter
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents ComboBox11 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox15 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRATABSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents MUESTRATABSTableAdapter As sofTV.DataSetarnoldoTableAdapters.MUESTRATABSTableAdapter
    Friend WithEvents MUESTRACatalogoPeriodosCorteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACatalogoPeriodosCorteTableAdapter As sofTV.DataSetEDGARTableAdapters.MUESTRACatalogoPeriodosCorteTableAdapter
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents DAMESTATUSHABBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMESTATUSHABTableAdapter As sofTV.DataSetLidiaTableAdapters.DAMESTATUSHABTableAdapter
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AsignaPeriodoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AsignaPeriodoTableAdapter As sofTV.DataSetLidiaTableAdapters.AsignaPeriodoTableAdapter
    Friend WithEvents CONTARCLIENTESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONTARCLIENTESTableAdapter As sofTV.DataSetLidiaTableAdapters.CONTARCLIENTESTableAdapter
    Friend WithEvents PrimerMesCLIENTESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PrimerMesCLIENTESTableAdapter As sofTV.DataSetLidiaTableAdapters.PrimerMesCLIENTESTableAdapter
    Friend WithEvents Valida_facturasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_facturasTableAdapter As sofTV.DataSetarnoldoTableAdapters.valida_facturasTableAdapter
    Friend WithEvents CMB2Label5 As System.Windows.Forms.Label
    Friend WithEvents DameFechaHabilitarBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameFechaHabilitarTableAdapter As sofTV.DataSetLidiaTableAdapters.DameFechaHabilitarTableAdapter
    Friend WithEvents DIMEQUEPERIODODECORTEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DIMEQUEPERIODODECORTETableAdapter As sofTV.DataSetLidiaTableAdapters.DIMEQUEPERIODODECORTETableAdapter
    Friend WithEvents CONRel_ContDig_UsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONRel_ContDig_UsuariosTableAdapter As sofTV.DataSetEDGARTableAdapters.CONRel_ContDig_UsuariosTableAdapter
    Friend WithEvents GUARDARRel_ContDig_UsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GUARDARRel_ContDig_UsuariosTableAdapter As sofTV.DataSetEDGARTableAdapters.GUARDARRel_ContDig_UsuariosTableAdapter
    Friend WithEvents CONRel_ContNet_UsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONRel_ContNet_UsuariosTableAdapter As sofTV.DataSetEDGARTableAdapters.CONRel_ContNet_UsuariosTableAdapter
    Friend WithEvents CONRel_ClientesTv_UsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONRel_ClientesTv_UsuariosTableAdapter As sofTV.DataSetEDGARTableAdapters.CONRel_ClientesTv_UsuariosTableAdapter
    Friend WithEvents GUARDARRel_ClientesTv_UsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GUARDARRel_ClientesTv_UsuariosTableAdapter As sofTV.DataSetEDGARTableAdapters.GUARDARRel_ClientesTv_UsuariosTableAdapter
    Friend WithEvents NOMBRELabel3 As System.Windows.Forms.Label
    Friend WithEvents GUARDARRel_ContNet_UsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GUARDARRel_ContNet_UsuariosTableAdapter As sofTV.DataSetEDGARTableAdapters.GUARDARRel_ContNet_UsuariosTableAdapter
    Friend WithEvents MuestraPromotoresNetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraPromotoresNetTableAdapter As sofTV.DataSetEDGARTableAdapters.MuestraPromotoresNetTableAdapter
    Friend WithEvents MuestraPromotoresTvBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraPromotoresTvTableAdapter As sofTV.DataSetEDGARTableAdapters.MuestraPromotoresTvTableAdapter
    Friend WithEvents ConRelClientesTvVendedorBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRel_ClientesTv_VendedorTableAdapter As sofTV.DataSetEDGARTableAdapters.ConRel_ClientesTv_VendedorTableAdapter
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents DAMEFECHADELSERVIDOR_2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMEFECHADELSERVIDOR_2TableAdapter As sofTV.DataSetEdgarRev2TableAdapters.DAMEFECHADELSERVIDOR_2TableAdapter
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button20 As System.Windows.Forms.Button
    Friend WithEvents BuscaBloqueadoTableAdapter As sofTV.DataSetLidiaTableAdapters.BuscaBloqueadoTableAdapter
    Friend WithEvents BuscaBloqueadoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConRelCtePlacaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRelCtePlacaTableAdapter As sofTV.DataSetEricTableAdapters.ConRelCtePlacaTableAdapter
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents ChecaRoboDeSeñalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ChecaRoboDeSeñalTableAdapter As sofTV.DataSetEricTableAdapters.ChecaRoboDeSeñalTableAdapter
    Friend WithEvents Dime_Si_ESMiniBasicoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dime_Si_ESMiniBasicoTableAdapter As sofTV.DataSetEDGARTableAdapters.Dime_Si_ESMiniBasicoTableAdapter
    Friend WithEvents ChecaRelCteDescuentoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ChecaRelCteDescuentoTableAdapter As sofTV.DataSetEricTableAdapters.ChecaRelCteDescuentoTableAdapter
    Friend WithEvents ConRelCteDescuentoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRelCteDescuentoTableAdapter As sofTV.DataSetEricTableAdapters.ConRelCteDescuentoTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Valida_Direccion1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_Direccion1TableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Valida_Direccion1TableAdapter
    Friend WithEvents Dame_clv_session_clientesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clv_session_clientesTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Dame_clv_session_clientesTableAdapter
    Friend WithEvents Valida_servicioTvBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_servicioTvTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Valida_servicioTvTableAdapter
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents Inserta_Rel_cortesia_FechaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Rel_cortesia_FechaTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Inserta_Rel_cortesia_FechaTableAdapter
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Button27 As System.Windows.Forms.Button
    Friend WithEvents DescuentoLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button24 As System.Windows.Forms.Button
    Friend WithEvents BindingNavigator3 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton9 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton10 As System.Windows.Forms.ToolStripButton
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox8 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox9 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox10 As System.Windows.Forms.ComboBox
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents CortesiaCheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents CMBTextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox20 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox21 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox22 As System.Windows.Forms.TextBox
    Friend WithEvents Button28 As System.Windows.Forms.Button
    Friend WithEvents Label41 As System.Windows.Forms.TextBox
    Friend WithEvents Button29 As System.Windows.Forms.Button
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents TextBox27 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel53 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel54 As System.Windows.Forms.Label
    Friend WithEvents Button30 As System.Windows.Forms.Button
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents TextBox28 As System.Windows.Forms.TextBox
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents LabelComboYRenta As System.Windows.Forms.Label
    Friend WithEvents TxtNumeroInt As System.Windows.Forms.TextBox
    Friend WithEvents BtnEstadoDeCuenta As System.Windows.Forms.Button
    Friend WithEvents Button31 As System.Windows.Forms.Button
    Friend WithEvents btnHisDes As System.Windows.Forms.Button
    Friend WithEvents ComboBox19 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox20 As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
