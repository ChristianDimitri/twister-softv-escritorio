﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SoftvConfiguration
{
    public class SoftvSection : ConfigurationSection
    {
        /// <summary>
        /// Gets default connection String. If it doesn't exist then
        /// returns global connection string
        /// </summary>
        [ConfigurationProperty("DefaultConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["DefaultConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ?
                    Globals.DataAccess.GlobalConectionString :
                    (string)base["DefaultConnectionString"];
                return connectionString;
            }
        }

        /// <summary>
        /// Gets RelMTATelefono configuration data
        /// </summary>
        [ConfigurationProperty("RelMTATelefono")]
        public RelMTATelefonoElement RelMTATelefono
        {
            get { return (RelMTATelefonoElement)base["RelMTATelefono"]; }
        }

        /// <summary>
        /// Gets default assembly name for TestSimulator data clases
        /// </summary>
        [ConfigurationProperty("DefaultAssembly", DefaultValue = "SoftvSQL")]
        public String Assembly
        {
            get { return (string)base["DefaultAssembly"]; }
        }

        /// <summary>
        /// Gets RangoLimiteDeCredito configuration data
        /// </summary>
        [ConfigurationProperty("RangoLimiteDeCredito")]
        public RangoLimiteDeCreditoElement RangoLimiteDeCredito
        {
            get { return (RangoLimiteDeCreditoElement)base["RangoLimiteDeCredito"]; }
        }

        /// <summary>
        /// Gets LimiteDeCredito configuration data
        /// </summary>
        [ConfigurationProperty("LimiteDeCredito")]
        public LimiteDeCreditoElement LimiteDeCredito
        {
            get { return (LimiteDeCreditoElement)base["LimiteDeCredito"]; }
        }

        /// <summary>
        /// Gets DetLimiteDeCredito configuration data
        /// </summary>
        [ConfigurationProperty("DetLimiteDeCredito")]
        public DetLimiteDeCreditoElement DetLimiteDeCredito
        {
            get { return (DetLimiteDeCreditoElement)base["DetLimiteDeCredito"]; }
        }

        /// <summary>
        /// Gets DetLimiteDeCredito configuration data
        /// </summary>
        [ConfigurationProperty("Colonia")]
        public ColoniaElement Colonia
        {
            get { return (ColoniaElement)base["Colonia"]; }
        }

        /// <summary>
        /// Gets Ciudad configuration data
        /// </summary>
        [ConfigurationProperty("Ciudad")]
        public CiudadElement Ciudad
        {
            get { return (CiudadElement)base["Ciudad"]; }
        }

        /// <summary>
        /// Gets Servicio configuration data
        /// </summary>
        [ConfigurationProperty("Servicio")]
        public ServicioElement Servicio
        {
            get { return (ServicioElement)base["Servicio"]; }
        }

        /// <summary>
        /// Gets Servicio configuration data
        /// </summary>
        [ConfigurationProperty("MesaControlUsuario")]
        public MesaControlUsuarioElement MesaControlUsuario
        {
            get { return (MesaControlUsuarioElement)base["MesaControlUsuario"]; }
        }

        /// <summary>
        /// Gets Cvecolciu configuration data
        /// </summary>
        [ConfigurationProperty("Cvecolciu")]
        public CvecolciuElement Cvecolciu
        {
            get { return (CvecolciuElement)base["Cvecolciu"]; }
        }

        /// <summary>
        /// Gets RelColoniasSer configuration data
        /// </summary>
        [ConfigurationProperty("RelColoniasSer")]
        public RelColoniasSerElement RelColoniasSer
        {
            get { return (RelColoniasSerElement)base["RelColoniasSer"]; }
        }

        /// <summary>
        /// Gets MotAtenTel configuration data
        /// </summary>
        [ConfigurationProperty("MotAtenTel")]
        public MotAtenTelElement MotAtenTel
        {
            get { return (MotAtenTelElement)base["MotAtenTel"]; }
        }

        /// <summary>
        /// Gets AtenTel configuration data
        /// </summary>
        [ConfigurationProperty("AtenTel")]
        public AtenTelElement AtenTel
        {
            get { return (AtenTelElement)base["AtenTel"]; }
        }
        /// <summary>
        /// Gets EstadosDeCuenta configuration data
        /// </summary>
        [ConfigurationProperty("EstadosDeCuenta")]
        public EstadosDeCuentaElement EstadosDeCuenta
        {
            get { return (EstadosDeCuentaElement)base["EstadosDeCuenta"]; }
        }

        /// <summary>
        /// Gets PeriodosDeCobro configuration data
        /// </summary>
        [ConfigurationProperty("PeriodosDeCobro")]
        public PeriodosDeCobroElement PeriodosDeCobro
        {
            get { return (PeriodosDeCobroElement)base["PeriodosDeCobro"]; }
        }

        /// <summary>
        /// Gets Ciclo configuration data
        /// </summary>
        [ConfigurationProperty("Ciclo")]
        public CicloElement Ciclo
        {
            get { return (CicloElement)base["Ciclo"]; }
        }


        /// <summary>
        /// Gets RelCicloSector configuration data
        /// </summary>
        [ConfigurationProperty("RelCicloSector")]
        public RelCicloSectorElement RelCicloSector
        {
            get { return (RelCicloSectorElement)base["RelCicloSector"]; }
        }

        /// <summary>
        /// Gets Sector configuration data
        /// </summary>
        [ConfigurationProperty("Sector")]
        public SectorElement Sector
        {
            get { return (SectorElement)base["Sector"]; }
        }
        
        /// <summary>
        /// Gets Promociones configuration data
        /// </summary>
        [ConfigurationProperty("Promociones")]
        public PromocionesElement Promociones
        {
            get { return (PromocionesElement)base["Promociones"]; }
        }

        /// <summary>
        /// Gets EsquemaContratacion configuration data
        /// </summary>
        [ConfigurationProperty("EsquemaContratacion")]
        public EsquemaContratacionElement EsquemaContratacion
        {
            get { return (EsquemaContratacionElement)base["EsquemaContratacion"]; }
        }

        /// Gets EsquemaMensualidad configuration data
        /// </summary>
        [ConfigurationProperty("EsquemaMensualidad")]
        public EsquemaMensualidadElement EsquemaMensualidad
        {
            get { return (EsquemaMensualidadElement)base["EsquemaMensualidad"]; }
        }
        /// <summary>
        /// Gets PeriodoDeInstalacion configuration data
        /// </summary>
        [ConfigurationProperty("PeriodoDeInstalacion")]
        public PeriodoDeInstalacionElement PeriodoDeInstalacion
        {
            get { return (PeriodoDeInstalacionElement)base["PeriodoDeInstalacion"]; }
        }
        /// <summary>
        /// Gets DescargaMaterial configuration data
        /// </summary>
        [ConfigurationProperty("DescargaMaterial")]
        public DescargaMaterialElement DescargaMaterial
        {
            get { return (DescargaMaterialElement)base["DescargaMaterial"]; }
        }

        /// <summary>
        /// Gets Trabajo configuration data
        /// </summary>
        [ConfigurationProperty("Trabajo")]
        public TrabajoElement Trabajo
        {
            get { return (TrabajoElement)base["Trabajo"]; }
        }

        /// <summary>
        /// Gets RelMaterialTrabajo configuration data
        /// </summary>
        [ConfigurationProperty("RelMaterialTrabajo")]
        public RelMaterialTrabajoElement RelMaterialTrabajo
        {
            get { return (RelMaterialTrabajoElement)base["RelMaterialTrabajo"]; }
        }

    }
}
