﻿using System.Configuration;

namespace SoftvConfiguration
{
    public class SoftvSettings : ConfigurationSection
    {
        public readonly static SoftvSection Settings =
            (SoftvSection)ConfigurationManager.GetSection("SoftvSection");
    }
}


