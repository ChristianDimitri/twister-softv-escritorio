﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SoftvConfiguration
{
    public class MesaControlUsuarioElement : ConfigurationElement
    {
        /// <summary>
        /// Gets assembly name for MesaControlUsuario class
        /// </summary>
        [ConfigurationProperty("Assembly")]
        public String Assembly
        {
            get
            {
                string assembly = (string)base["Assembly"];
                assembly = String.IsNullOrEmpty(assembly) ?
                    SoftvSettings.Settings.Assembly :
                    (string)base["Assembly"];
                return assembly;
            }
        }

        /// <summary>
        /// Gets class name for MesaControlUsuario 
        /// </summary>
        [ConfigurationProperty("DataClass", DefaultValue = "Softv.DAO.MesaControlUsuarioData")]
        public String DataClass
        {
            get { return (string)base["DataClass"]; }
        }

        /// <summary>
        /// Gets connection string for database MesaControlUsuario access
        /// </summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["ConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ?
                    SoftvSettings.Settings.ConnectionString :
                    (string)base["ConnectionString"];
                return connectionString;
            }
        }
    }
}
