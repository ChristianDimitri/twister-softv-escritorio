﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SoftvConfiguration
{
    public class ColoniaElement : ConfigurationElement
    {
        /// <summary>
        /// Gets assembly name for ColoniaElement class
        /// </summary>
        [ConfigurationProperty("Assembly")]
        public String Assembly
        {
            get
            {
                string assembly = (string)base["Assembly"];
                assembly = String.IsNullOrEmpty(assembly) ?
                    SoftvSettings.Settings.Assembly :
                    (string)base["Assembly"];
                return assembly;
            }
        }

        /// <summary>
        /// Gets class name for ColoniaElement 
        /// </summary>
        [ConfigurationProperty("DataClass", DefaultValue = "Softv.DAO.ColoniaData")]
        public String DataClass
        {
            get { return (string)base["DataClass"]; }
        }

        /// <summary>
        /// Gets connection string for database ColoniaElement access
        /// </summary>
        [ConfigurationProperty("ConnectionString")]
        public String ConnectionString
        {
            get
            {
                string connectionString = (string)base["ConnectionString"];
                connectionString = String.IsNullOrEmpty(connectionString) ?
                    SoftvSettings.Settings.ConnectionString :
                    (string)base["ConnectionString"];
                return connectionString;
            }
        }
    }
}
